<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="msapplication-TileColor" content="#0E0E0E">
    <meta name="template-color" content="#0E0E0E">
    <!-- <link rel="manifest" href="manifest.json" crossorigin> -->
    <meta name="msapplication-config" content="browserconfig.xml">
    <meta name="description" content="Index page">
    <meta name="keywords" content="index, page">
    <meta name="author" content="">
    <link rel="shortcut icon" type="image/x-icon" href="~/imgs/template/fav.png">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <link href="css/style.css?version=4.1" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
    <title>Job Board</title>
  </head>
  <body>
    <div id="preloader-active">
      <div class="preloader d-flex align-items-center justify-content-center">
        <div class="preloader-inner position-relative">
          <div class="text-center"><img src="~/imgs/template/loader.png" alt=""></div>
        </div>
      </div>
    </div>
    <div class="modal fade" id="ModalApplyJobForm" tabindex="-1" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content apply-job-form">
          <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
          <div class="modal-body pl-30 pr-30 pt-50">
            <div class="text-center">
              <p class="font-sm text-brand-2">Job Application </p>
              <h2 class="mt-10 mb-5 text-brand-1 text-capitalize">Start your career today</h2>
              <p class="font-sm text-muted mb-30">Please fill in your information and send it to the employer.</p>
            </div>
            <form class="login-register text-start mt-20 pb-30" action="#">
              <div class="form-group">
                <label class="form-label" for="input-1">Full Name *</label>
                <input class="form-control" id="input-1" type="text" required="" name="fullname" placeholder="Steven Job">
              </div>
              <div class="form-group">
                <label class="form-label" for="input-2">Email *</label>
                <input class="form-control" id="input-2" type="email" required="" name="emailaddress" placeholder="stevenjob@gmail.com">
              </div>
              <div class="form-group">
                <label class="form-label" for="number">Contact Number *</label>
                <input class="form-control" id="number" type="text" required="" name="phone" placeholder="(+01) 234 567 89">
              </div>
              <div class="form-group">
                <label class="form-label" for="des">Description</label>
                <input class="form-control" id="des" type="text" required="" name="Description" placeholder="Your description...">
              </div>
              <div class="form-group">
                <label class="form-label" for="file">Upload Resume</label>
                <input class="form-control" id="file" name="resume" type="file">
              </div>
              <div class="login_footer form-group d-flex justify-content-between">
                <label class="cb-container">
                  <input type="checkbox"><span class="text-small">Agree our terms and policy</span><span class="checkmark"></span>
                </label><a class="text-muted" href="#">Lean more</a>
              </div>
              <div class="form-group">
                <button class="btn btn-default hover-up w-100" type="submit" name="login">Apply Job</button>
              </div>
              <div class="text-muted text-center">Do you need support? <a href="#">Contact Us</a></div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <header class="header sticky-bar">
      <div class="container">
        <div class="main-header">
          <div class="header-left">
            <div class="header-logo"><a class="d-flex" href="index.php"><img alt="jobBox" src="~/imgs/template/sp-logo.png" width="225"></a></div>
          </div>
          <div class="header-nav">
            <nav class="nav-main-menu">
              <ul class="main-menu">
                <li><a class="active" href="index.php">Home</a></li>
                <li><a href="about-us.php">About Us</a></li>
                <li><a href="job-list.php">Find a Job</a></li>
                <li><a href="companies-list.php">Recruiters</a></li>
                <li><a href="candidates-list.php">Candidates</a></li>
                <li><a href="#">Blog</a></li>
                <li class="dashboard"><a href="dashboard.php">Dashboard</a></li>
              </ul>
            </nav>
            <div class="burger-icon burger-icon-white"><span class="burger-icon-top"></span><span class="burger-icon-mid"></span><span class="burger-icon-bottom"></span></div>
          </div>
          <div class="header-right">
            <div class="block-signin">
              <a class="text-link-bd-btom hover-up" href="register.php">Register</a>
              <a class="btn btn-default btn-shadow ml-40 hover-up" href="signin.php">Sign in</a>
            </div>
          </div>
        </div>
      </div>
    </header>
    <div class="mobile-header-active mobile-header-wrapper-style perfect-scrollbar">
      <div class="mobile-header-wrapper-inner">
        <div class="mobile-header-content-area">
          <div class="perfect-scroll">
            <div class="mobile-search mobile-header-border mb-30">
              <form action="#">
                <input type="text" placeholder="Search…"><i class="fi-rr-search"></i>
              </form>
            </div>
            <div class="mobile-menu-wrap mobile-header-border">
              <!-- mobile menu start-->
              <nav>
                <ul class="mobile-menu font-heading">
                  <li class="has-children"><a class="active" href="index.php">Home</a></li>
                  <li class="has-children"><a href="#">Find a Job</a>
                    <ul class="sub-menu">
                      <li><a href="#">Jobs Grid</a></li>
                      <li><a href="#">Jobs List</a></li>
                      <li><a href="#">Jobs Details  </a></li>
                      <li><a href="#">Jobs Details 2</a></li>
                    </ul>
                  </li>
                  <li class="has-children"><a href="#">Recruiters</a>
                    <ul class="sub-menu">
                      <li><a href="#">Recruiters</a></li>
                      <li><a href="#">Company Details</a></li>
                    </ul>
                  </li>
                  <li class="has-children"><a href="#">Candidates</a>
                    <ul class="sub-menu">
                      <li><a href="#">Candidates Grid</a></li>
                      <li><a href="#">Candidate Details</a></li>
                    </ul>
                  </li>
                  <li class="has-children"><a href="#">Pages</a>
                    <ul class="sub-menu">
                      <li><a href="#">About Us</a></li>
                      <li><a href="#">Pricing Plan</a></li>
                      <li><a href="#">Contact Us</a></li>
                      <li><a href="#">Register</a></li>
                      <li><a href="#">Signin</a></li>
                      <li><a href="#">Reset Password</a></li>
                      <li><a href="#">Content Protected</a></li>
                    </ul>
                  </li>
                  <li class="has-children"><a href="#">Blog</a>
                    <ul class="sub-menu">
                      <li><a href="#">Blog Grid</a></li>
                      <li><a href="#">Blog Grid 2</a></li>
                      <li><a href="#">Blog Single</a></li>
                    </ul>
                  </li>
                  <li><a href="#" target="_blank">Dashboard</a></li>
                </ul>
              </nav>
            </div>
            <div class="mobile-account">
              <h6 class="mb-10">Your Account</h6>
              <ul class="mobile-menu font-heading">
                <li><a href="#">Profile</a></li>
                <li><a href="#">Work Preferences</a></li>
                <li><a href="#">Account Settings</a></li>
                <li><a href="#">Go Pro</a></li>
                <li><a href="#">Sign Out</a></li>
              </ul>
            </div>
            <div class="site-copyright">Copyright 2022 &copy; JobBox. <br>Designed by AliThemes.</div>
          </div>
        </div>
      </div>
    </div>
    <div class="mobile-header-active mobile-header-wrapper-style perfect-scrollbar">
      <div class="mobile-header-wrapper-inner">
        <div class="mobile-header-content-area">
          <div class="perfect-scroll">
            <div class="mobile-search mobile-header-border mb-30">
              <form action="#">
                <input type="text" placeholder="Search…"><i class="fi-rr-search"></i>
              </form>
            </div>
            <div class="mobile-menu-wrap mobile-header-border">
              <!-- mobile menu start-->
              <nav>
                <ul class="mobile-menu font-heading">
                  <li><a class="active" href="index.php">Home</a></li>
                  <li class="has-children"><a href="#">Find a Job</a>
                    <ul class="sub-menu">
                      <li><a href="#">Jobs Grid</a></li>
                      <li><a href="#">Jobs List</a></li>
                      <li><a href="#">Jobs Details</a></li>
                      <li><a href="#">Jobs Details 2</a></li>
                    </ul>
                  </li>
                  <li class="has-children"><a href="#">Recruiters</a>
                    <ul class="sub-menu">
                      <li><a href="#">Recruiters</a></li>
                      <li><a href="#">Company Details</a></li>
                    </ul>
                  </li>
                  <li class="has-children"><a href="#">Candidates</a>
                    <ul class="sub-menu">
                      <li><a href="#">Candidates Grid</a></li>
                      <li><a href="#">Candidate Details</a></li>
                    </ul>
                  </li>
                  <li class="has-children"><a href="#">Pages</a>
                    <ul class="sub-menu">
                      <li><a href="#">About Us</a></li>
                      <li><a href="#">Pricing Plan</a></li>
                      <li><a href="#">Contact Us</a></li>
                      <li><a href="#">Register</a></li>
                      <li><a href="#">Signin</a></li>
                      <li><a href="#">Reset Password</a></li>
                      <li><a href="#">Content Protected</a></li>
                    </ul>
                  </li>
                  <li class="has-children"><a href="#">Blog</a>
                    <ul class="sub-menu">
                      <li><a href="#">Blog Grid</a></li>
                      <li><a href="#">Blog Grid 2</a></li>
                      <li><a href="#">Blog Single</a></li>
                    </ul>
                  </li>
                  <li><a href="#" target="_blank">Dashboard</a></li>
                </ul>
              </nav>
            </div>
            <div class="mobile-account">
              <h6 class="mb-10">Your Account</h6>
              <ul class="mobile-menu font-heading">
                <li><a href="#">Profile</a></li>
                <li><a href="#">Work Preferences</a></li>
                <li><a href="#">Account Settings</a></li>
                <li><a href="#">Go Pro</a></li>
                <li><a href="#">Sign Out</a></li>
              </ul>
            </div>
            <div class="site-copyright">Copyright 2022 &copy; JobBox. <br>Designed by AliThemes.</div>
          </div>
        </div>
      </div>
    </div>
    <main class="main">