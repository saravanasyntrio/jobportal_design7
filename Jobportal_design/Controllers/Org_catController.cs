﻿using AspNetCoreHero.ToastNotification.Abstractions;
using Jobportal_design.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Jobportal_design.Controllers
{
   
    public class Org_catController : Controller
    {
        private readonly INotyfService _notyf;
        private IConfiguration _Configure;
        string apiBaseUrl;
        public Org_catController(INotyfService notyf, IConfiguration configuration)
        {
            _notyf = notyf;
            _Configure = configuration;
            apiBaseUrl = _Configure.GetValue<string>("WebAPIBaseUrl");
        }
        public IActionResult Org_cat_list()
        {
            List<Org_cat> customers = new List<Org_cat>();
            string apiUrl = apiBaseUrl + "/Org_cat/getAll";

            HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync(apiUrl).Result;
            if (response.IsSuccessStatusCode)
            {
                customers = JsonConvert.DeserializeObject<List<Org_cat>>(response.Content.ReadAsStringAsync().Result);
            }

            return View(customers);
        }

        public IActionResult Add_org_cat()
        {

            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Create(Org_cat org_cat_ab)
        {


            if (org_cat_ab.isactive == "Active")
            {
                org_cat_ab.is_active = true;
                // country_ab.identifier = "true";
            }
            else
            {
                org_cat_ab.is_active = false;
                //  country_ab.identifier = false;
            }

            HttpClient hc = new HttpClient();

            hc.BaseAddress = new Uri(apiBaseUrl + "/Org_cat/");
            var locationconsume = hc.PostAsJsonAsync<Org_cat>("Create", org_cat_ab);
            locationconsume.Wait();
            var readdata = locationconsume.Result;

            if (readdata.IsSuccessStatusCode)
            {
                _notyf.Success("Success",5);
                return RedirectToAction("Org_cat_list", "Org_cat");

            }
            else
            {
                _notyf.Error("Error..! Try Again..", 5);
                return RedirectToAction("Org_cat_list", "Org_cat");
            }
            //return View(org_cat_ab);


        }

        public async Task<ActionResult> Edit_org_cat(int id)
        {
            Org_cat org_cat = null;
            string apiUrl = apiBaseUrl + "/Org_cat/get_by_id/";
            HttpClient client = new HttpClient();

            HttpResponseMessage readdata = await client.GetAsync(apiUrl + id);

            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<Org_cat>();
                displayresults.Wait();
                org_cat = displayresults.Result;

            }
            if (org_cat.is_active == true)
            {
                org_cat.isactive = "Active";
            }
            else
            {
                org_cat.isactive = "InActive";
            }
            return View(org_cat);

        }

        public async Task<ActionResult> Update(Org_cat org_cat_ab)
        {
            if (org_cat_ab.isactive == "Active")
            {
                org_cat_ab.is_active = true;
            }
            else
            {
                org_cat_ab.is_active = false;
            }
            HttpClient hc = new HttpClient();

            hc.BaseAddress = new Uri(apiBaseUrl + "/Org_cat/");
            var locationconsume = hc.PutAsJsonAsync<Org_cat>("Update", org_cat_ab);
            locationconsume.Wait();
            var readdata = locationconsume.Result;

            if (readdata.IsSuccessStatusCode)
            {
                _notyf.Information("Updated Successfully",5);
                return RedirectToAction("Org_cat_list", "Org_cat");

            }
            else
            {
                _notyf.Success("Error..! Try Again.", 5);
                return RedirectToAction("Org_cat_list", "Org_cat");
            }
            return View(org_cat_ab);


        }

        public async Task<ActionResult> Delete_org_cat(int id)
        {
          
            string apiUrl = apiBaseUrl + "/Org_cat/Delete/";

            HttpClient client = new HttpClient();

            HttpResponseMessage readdata = await client.GetAsync(apiUrl + id);
            if (readdata.IsSuccessStatusCode)
            {

                //var displayresults = readdata.Content.ReadAsAsync<Currency_tbl>();
                //displayresults.Wait();
                //currency = displayresults.Result;
                _notyf.Error("Deleted Successfully",5);

            }
            else
            {
                _notyf.Error("Try Again..!",5);
            }

            return RedirectToAction("Org_cat_list", "Org_cat");

        }

    }
}
