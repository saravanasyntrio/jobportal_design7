﻿using AspNetCoreHero.ToastNotification.Abstractions;
using Jobportal_design.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Jobportal_design.Controllers
{
    public class LocationController : Controller
    {
        private IWebHostEnvironment Environment;
        private readonly INotyfService _notyf;
        private IConfiguration _Configure;
        string apiBaseUrl;
        public LocationController(IWebHostEnvironment _environment, INotyfService notyf, IConfiguration configuration)
        {
            Environment = _environment;
            _notyf = notyf;
            _Configure = configuration;
            apiBaseUrl = _Configure.GetValue<string>("WebAPIBaseUrl");
        }
        public IActionResult Locationlist()
        {
            List<Location_tbl> customers = new List<Location_tbl>();
            string apiUrl = apiBaseUrl + "/Location/getAll";

            List<District_tbl> state = new List<District_tbl>();
            string apiUrl1 = apiBaseUrl + "/District/getAll";

            HttpClient client = new HttpClient();
            HttpResponseMessage response1 = client.GetAsync(apiUrl1).Result;
            if (response1.IsSuccessStatusCode)
            {
                state = JsonConvert.DeserializeObject<List<District_tbl>>(response1.Content.ReadAsStringAsync().Result);
            }
            ViewBag.message = state;

            HttpResponseMessage response = client.GetAsync(apiUrl).Result;
            if (response.IsSuccessStatusCode)
            {
                customers = JsonConvert.DeserializeObject<List<Location_tbl>>(response.Content.ReadAsStringAsync().Result);
            }

            return View(customers);
        }

        public IActionResult AddLocation()
        {


            List<District_tbl> state = new List<District_tbl>();
            string apiUrl = apiBaseUrl + "/District/getAll";

            HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync(apiUrl).Result;
            if (response.IsSuccessStatusCode)
            {
                state = JsonConvert.DeserializeObject<List<District_tbl>>(response.Content.ReadAsStringAsync().Result);
            }
            ViewBag.message = state;
            return View();

        }

        [HttpPost]
        public async Task<ActionResult> Create(Location_tbl location_ab , List<IFormFile> loc_pic)
        {
            if (location_ab.isactive == "Active")
            {
                location_ab.is_active = true;
                // country_ab.identifier = "true";
            }
            else
            {
                location_ab.is_active = false;
                //  country_ab.identifier = false;
            }
            string wwwPath = this.Environment.WebRootPath;
            string contentPath = this.Environment.ContentRootPath;
            string path = Path.Combine(this.Environment.WebRootPath, "General_uploads");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            List<string> uploadedFiles = new List<string>();
            foreach (IFormFile postedFile in loc_pic)
            {
                string fileName = Path.GetFileName(postedFile.FileName);
                using (FileStream stream = new FileStream(Path.Combine(path, fileName), FileMode.Create))
                {
                    postedFile.CopyTo(stream);
                    uploadedFiles.Add(fileName);
                    // ViewBag.Message += string.Format("<b>{0}</b> uploaded.<br />", fileName);
                }
                location_ab.loc_path = path;
                location_ab.loc_pic = fileName;
            }
            HttpClient hc = new HttpClient();

            hc.BaseAddress = new Uri(apiBaseUrl + "/Location/");
            var locationconsume = hc.PostAsJsonAsync<Location_tbl>("Create", location_ab);
            locationconsume.Wait();
            var readdata = locationconsume.Result;

            if (readdata.IsSuccessStatusCode)
            {
                _notyf.Success("Success", 5);
                return RedirectToAction("Locationlist", "Location");


            }
            else
            {
                _notyf.Error("Error..! Try Again..", 5);
                return RedirectToAction("Locationlist", "Location");
            }
            //return View(location_ab);


        }

        public async Task<ActionResult> EditLocation(int id)
        {
            HttpClient client = new HttpClient();
            List<District_tbl> state = new List<District_tbl>();
            Location_tbl location = null;
            string apiUrl = apiBaseUrl + "/District/getAll";
            string apiUrl1 = apiBaseUrl + "/Location/get_by_id/";


            HttpResponseMessage response = client.GetAsync(apiUrl).Result;
            if (response.IsSuccessStatusCode)
            {
                state = JsonConvert.DeserializeObject<List<District_tbl>>(response.Content.ReadAsStringAsync().Result);
            }
            ViewBag.message = state;
            
           
            HttpResponseMessage readdata = await client.GetAsync(apiUrl1 + id);
            //  var result = responseTask.Result;
            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<Location_tbl>();
                displayresults.Wait();
                location = displayresults.Result;

            }

            return View(location);

        }
        public async Task<ActionResult> DeleteLocation(int id)
        {
            //Currency_tbl currency = null;
            //IEnumerable<Currency_tbl> locobj = null;
            string apiUrl = apiBaseUrl + "/Location/Delete/";

            HttpClient client = new HttpClient();

            HttpResponseMessage readdata = await client.GetAsync(apiUrl + id);
            //  var result = responseTask.Result;
            if (readdata.IsSuccessStatusCode)
            {
                //var displayresults = readdata.Content.ReadAsAsync<Currency_tbl>();
                //displayresults.Wait();
                //currency = displayresults.Result;
                _notyf.Error("Deleted Successfully", 3);

            }
            else
            {
                _notyf.Error("Error..! Try Again..", 5);
               
            }

            return RedirectToAction("Locationlist", "Location");

        }

        public async Task<ActionResult> Update(Location_tbl location_ab, List<IFormFile> s_resume1)
        {
            string wwwPath = this.Environment.WebRootPath;
            string contentPath = this.Environment.ContentRootPath;
            string path = Path.Combine(this.Environment.WebRootPath, "General_uploads");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            List<string> uploadedFiles = new List<string>();
            foreach (IFormFile postedFile in s_resume1)
            {
                string fileName = Path.GetFileName(postedFile.FileName);
                using (FileStream stream = new FileStream(Path.Combine(path, fileName), FileMode.Create))
                {
                    postedFile.CopyTo(stream);
                    uploadedFiles.Add(fileName);
                    // ViewBag.Message += string.Format("<b>{0}</b> uploaded.<br />", fileName);
                }
                location_ab.loc_path = path;
                location_ab.loc_pic = fileName;
            }
            if (location_ab.isactive == "Active")
            {
                location_ab.is_active = true;
            }
            else
            {
                location_ab.is_active = false;
            }
            HttpClient hc = new HttpClient();

            hc.BaseAddress = new Uri(apiBaseUrl + "/Location/");
            var locationconsume = hc.PutAsJsonAsync<Location_tbl>("Update", location_ab);
            locationconsume.Wait();
            var readdata = locationconsume.Result;

            if (readdata.IsSuccessStatusCode)
            {
                _notyf.Information("Updated Successfully", 3);
                return RedirectToAction("Locationlist", "Location");

            }
            else
            {
                _notyf.Error("Error..! Try Again..", 5);
                return RedirectToAction("Locationlist", "Location");
            }
            //return View(location_ab);


        }
    }
}
