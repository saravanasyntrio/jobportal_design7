﻿using AspNetCoreHero.ToastNotification.Abstractions;
using Jobportal_design.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Jobportal_design.Controllers
{
    public class StateController : Controller
    {
        private readonly INotyfService _notyf;
        private IConfiguration _Configure;
        string apiBaseUrl;
        public StateController(INotyfService notyf, IConfiguration configuration)
        {
            _notyf = notyf;
            _Configure = configuration;
            apiBaseUrl = _Configure.GetValue<string>("WebAPIBaseUrl");
        }
        public IActionResult Statelist()
        {
            List<State_tbl> customers = new List<State_tbl>();
            string apiUrl = apiBaseUrl + "/State/getAll";

            HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync(apiUrl).Result;
            if (response.IsSuccessStatusCode)
            {
                customers = JsonConvert.DeserializeObject<List<State_tbl>>(response.Content.ReadAsStringAsync().Result);
            }

            return View(customers);
        }

        public IActionResult AddState()
        {


            //List<Country_tbl> country = new List<Country_tbl>();
            //string apiUrl = "http://localhost:62332/Country/getAll";

            //HttpClient client = new HttpClient();
            //HttpResponseMessage response = client.GetAsync(apiUrl).Result;
            //if (response.IsSuccessStatusCode)
            //{
            //    country = JsonConvert.DeserializeObject<List<Country_tbl>>(response.Content.ReadAsStringAsync().Result);
            //}
            //ViewBag.message = country;
            return View();

        }

        [HttpPost]
        public async Task<ActionResult> Create(State_tbl state_ab)
        {
            if (state_ab.isactive == "Active")
            {
                state_ab.is_active = true;
                // country_ab.identifier = "true";
            }
            else
            {
                state_ab.is_active = false;
                //  country_ab.identifier = false;
            }

            HttpClient hc = new HttpClient();

            hc.BaseAddress = new Uri(apiBaseUrl + "/State/");
            var locationconsume = hc.PostAsJsonAsync<State_tbl>("Create", state_ab);
            locationconsume.Wait();
            var readdata = locationconsume.Result;

            if (readdata.IsSuccessStatusCode)
            {
                _notyf.Success("Success", 3);
                return RedirectToAction("Statelist", "State");
            }
            else
            {
                _notyf.Error("Error..!", 3);
                return RedirectToAction("Statelist", "State");
            }
            //return View(state_ab);


        }

        public async Task<ActionResult> EditState(int id)
        {
            HttpClient client = new HttpClient();
            //List<Country_tbl> country = new List<Country_tbl>();
            State_tbl state = null;
            //string apiUrl = "http://localhost:62332/Country/getAll";
            string apiUrl1 = apiBaseUrl + "/State/get_by_id/";
            //HttpResponseMessage response = client.GetAsync(apiUrl).Result;
            //if (response.IsSuccessStatusCode)
            //{
            //    country = JsonConvert.DeserializeObject<List<Country_tbl>>(response.Content.ReadAsStringAsync().Result);
            //}
            //ViewBag.message = country;
            HttpResponseMessage readdata = await client.GetAsync(apiUrl1 + id);
            //  var result = responseTask.Result;
            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<State_tbl>();
                displayresults.Wait();
                state = displayresults.Result;

            }

            return View(state);

        }
        public async Task<ActionResult> DeleteState(int id)
        {
            //Currency_tbl currency = null;
            //IEnumerable<Currency_tbl> locobj = null;
            string apiUrl = apiBaseUrl + "/State/Delete/";

            HttpClient client = new HttpClient();

            HttpResponseMessage readdata = await client.GetAsync(apiUrl + id);
            //  var result = responseTask.Result;
            if (readdata.IsSuccessStatusCode)
            {
                //var displayresults = readdata.Content.ReadAsAsync<Currency_tbl>();
                //displayresults.Wait();
                //currency = displayresults.Result;
                _notyf.Success("Deleted Successfully", 3);

            }
            else
            {
                _notyf.Error("Error..!", 3);
            }
            return RedirectToAction("Statelist", "State");

        }

        public async Task<ActionResult> Update(State_tbl state_ab)
        {
            if (state_ab.isactive == "Active")
            {
                state_ab.is_active = true;
            }
            else
            {
                state_ab.is_active = false;
            }
            HttpClient hc = new HttpClient();

            hc.BaseAddress = new Uri(apiBaseUrl + "/State/");
            var locationconsume = hc.PutAsJsonAsync<State_tbl>("Update", state_ab);
            locationconsume.Wait();
            var readdata = locationconsume.Result;

            if (readdata.IsSuccessStatusCode)
            {
                _notyf.Information("Updated Successfully", 3);
                return RedirectToAction("Statelist", "State");

            }
            else
            {
                _notyf.Error("Error..!", 3);
                return RedirectToAction("Statelist", "State");
            }
            return View(state_ab);


        }
    }
}
