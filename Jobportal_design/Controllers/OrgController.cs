﻿using Jobportal_design.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http.Headers;
using Microsoft.Extensions.Configuration;
using AspNetCoreHero.ToastNotification.Abstractions;
using System.Net.Mail;
using System.Data;
using ExcelDataReader;
namespace Jobportal_design.Controllers
{
    public class OrgController : Controller
    {
        private  IWebHostEnvironment Environment;
        private IConfiguration _Configure;
        string apiBaseUrl;
        private readonly INotyfService _notyf;
        public OrgController(IWebHostEnvironment _environment, IConfiguration configuration, INotyfService notyf)
        {
            _notyf = notyf;
            Environment = _environment;
            _Configure = configuration;
            apiBaseUrl = _Configure.GetValue<string>("WebAPIBaseUrl");
        }
        //public OrgController()
        //{
        //    ViewBag.state = "";
        //    ViewBag.country = "";
        //    ViewBag.currency = "";
        //    ViewBag.district = "";
        //    ViewBag.org_cat = "";


        //}
        public IActionResult AddOrg()
        {
            List<State_tbl> state = new List<State_tbl>();
            List<County_tbl> county = new List<County_tbl>();
            List<District_tbl> district = new List<District_tbl>();
            List<Currency_tbl> currency = new List<Currency_tbl>();
            List<Org_cat> org_cat = new List<Org_cat>();
            List<Location_tbl> location = new List<Location_tbl>();
            string apiUrl = apiBaseUrl + "/State/getAllActiveState";
            string apiUrl1 = apiBaseUrl + "/County/getAllActiveCounty";
            string apiUrl2 = apiBaseUrl + "/Currency/getAllActiveCurrency";
            string apiUrl3 = apiBaseUrl + "/District/getAllActiveDistrict";
            string apiUrl4 = apiBaseUrl + "/Org_cat/getAllActiveOrgCategory";
            string apiUrl5 = apiBaseUrl + "/Location/getAllActiveLocations";

            HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync(apiUrl).Result;
            if (response.IsSuccessStatusCode)
            {
                state = JsonConvert.DeserializeObject<List<State_tbl>>(response.Content.ReadAsStringAsync().Result);
            }
            ViewBag.state = state;

            HttpResponseMessage response1 = client.GetAsync(apiUrl1).Result;
            if (response.IsSuccessStatusCode)
            {
                county = JsonConvert.DeserializeObject<List<County_tbl>>(response1.Content.ReadAsStringAsync().Result);
            }
            ViewBag.county = county;

            HttpResponseMessage response2 = client.GetAsync(apiUrl2).Result;
            if (response.IsSuccessStatusCode)
            {
                currency = JsonConvert.DeserializeObject<List<Currency_tbl>>(response2.Content.ReadAsStringAsync().Result);
            }
            ViewBag.currency = currency;

            HttpResponseMessage response3 = client.GetAsync(apiUrl3).Result;
            if (response.IsSuccessStatusCode)
            {
                district = JsonConvert.DeserializeObject<List<District_tbl>>(response3.Content.ReadAsStringAsync().Result);
            }
            ViewBag.district = district;

            HttpResponseMessage response4 = client.GetAsync(apiUrl4).Result;
            if (response4.IsSuccessStatusCode)
            {
                org_cat = JsonConvert.DeserializeObject<List<Org_cat>>(response4.Content.ReadAsStringAsync().Result);
            }
            ViewBag.org_cat = org_cat;

            HttpResponseMessage response5 = client.GetAsync(apiUrl5).Result;
            if (response5.IsSuccessStatusCode)
            {
                location = JsonConvert.DeserializeObject<List<Location_tbl>>(response5.Content.ReadAsStringAsync().Result);
            }
            ViewBag.location = location;
            return View();
        }
        public IActionResult Orglist1()
        {
            List<Org_physical> customers = new List<Org_physical>();
            string apiUrl = apiBaseUrl + "/Org/getAll";

            HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync(apiUrl).Result;
            if (response.IsSuccessStatusCode)
            {
                customers = JsonConvert.DeserializeObject<List<Org_physical>>(response.Content.ReadAsStringAsync().Result);
            }

            return View(customers);
        }

        public async Task<ActionResult> Orglist(int county_id,int dist_id, int org_cat_id, string search)
        {
            List<District_tbl> district = new List<District_tbl>();
            List<Org_cat> org_cat = new List<Org_cat>();
            List<Org_physical> customers = new List<Org_physical>();

            string apiUrl = apiBaseUrl + "/Org/getAllOrgwithJobcount/";
            string apiUrl3 = apiBaseUrl + "/District/getAllActiveDistrict";
            string apiUrl4 = apiBaseUrl + "/Org_cat/getAllActiveOrgCategory";

            string categorytext = org_cat_id.ToString();
            string city = dist_id.ToString();
            string county = county_id.ToString();
            string searchval = search;

            if (string.IsNullOrEmpty(categorytext))
            {
                categorytext = "0";
            }
            if (string.IsNullOrEmpty(city))
            {
                city = "0";
            }
            if (string.IsNullOrEmpty(county))
            {
                county = "0";
            }
            if (string.IsNullOrEmpty(searchval))
            {
                searchval = "all";
            }

            HttpClient client = new HttpClient();


            HttpResponseMessage response3 = client.GetAsync(apiUrl3).Result;
            if (response3.IsSuccessStatusCode)
            {
                district = JsonConvert.DeserializeObject<List<District_tbl>>(response3.Content.ReadAsStringAsync().Result);
            }
            ViewBag.district = district;

            HttpResponseMessage response4 = client.GetAsync(apiUrl4).Result;
            if (response4.IsSuccessStatusCode)
            {
                org_cat = JsonConvert.DeserializeObject<List<Org_cat>>(response4.Content.ReadAsStringAsync().Result);
            }
            ViewBag.org_cat = org_cat;

            HttpResponseMessage response = client.GetAsync(apiUrl + categorytext +"/"+ county+ "/" + city + "/" + searchval).Result;
            
            if (response.IsSuccessStatusCode)
            {
                customers = JsonConvert.DeserializeObject<List<Org_physical>>(response.Content.ReadAsStringAsync().Result);
            }

            ViewBag.orgCount = customers.Count();
            
            ViewBag.searchText = search;
            return View(customers);
        }
        public async Task<ActionResult> OrglistPartialView(int county_id, int dist_id, int org_cat_id, string search)
        {
            List<District_tbl> district = new List<District_tbl>();
            List<Org_cat> org_cat = new List<Org_cat>();
            List<Org_physical> customers = new List<Org_physical>();

            string apiUrl = apiBaseUrl + "/Org/getAllOrgwithJobcount/";
            string apiUrl3 = apiBaseUrl + "/District/getAllActiveDistrict";
            string apiUrl4 = apiBaseUrl + "/Org_cat/getAllActiveOrgCategory";

            string categorytext = org_cat_id.ToString();
            string city = dist_id.ToString();
            string county = county_id.ToString();
            string searchval = search;

            if (string.IsNullOrEmpty(categorytext))
            {
                categorytext = "0";
            }
            if (string.IsNullOrEmpty(city))
            {
                city = "0";
            }
            if (string.IsNullOrEmpty(county))
            {
                county = "0";
            }
            if (string.IsNullOrEmpty(searchval))
            {
                searchval = "all";
            }

            HttpClient client = new HttpClient();


           

            HttpResponseMessage response = client.GetAsync(apiUrl + categorytext + "/" + county + "/" + city + "/" + searchval).Result;

            if (response.IsSuccessStatusCode)
            {
                customers = JsonConvert.DeserializeObject<List<Org_physical>>(response.Content.ReadAsStringAsync().Result);

            }

            ViewBag.orgCount = customers.Count();

            ViewBag.searchText = search;
            
            return PartialView("OrglistPartialView", customers);
        }
        public async Task<ActionResult> OrganizationList(int county_id,int dist_id, int org_cat_id, string search)
        {
            List<District_tbl> district = new List<District_tbl>();
            List<Org_cat> org_cat = new List<Org_cat>();
            List<Org_physical> customers = new List<Org_physical>();

            string apiUrl = apiBaseUrl + "/Org/getAllOrgwithJobcount/";
            string apiUrl3 = apiBaseUrl + "/District/getAllActiveDistrict";
            string apiUrl4 = apiBaseUrl + "/Org_cat/getAllActiveOrgCategory";

            string categorytext = org_cat_id.ToString();
            string city = dist_id.ToString();
            string searchval = search;
            string county = county_id.ToString();
            if (string.IsNullOrEmpty(categorytext))
            {
                categorytext = "0";
            }
            if (string.IsNullOrEmpty(city))
            {
                city = "0";
            }
            if (string.IsNullOrEmpty(county))
            {
                county = "0";
            }
            if (string.IsNullOrEmpty(searchval))
            {
                searchval = "all";
            }

            HttpClient client = new HttpClient();


            HttpResponseMessage response3 = client.GetAsync(apiUrl3).Result;
            if (response3.IsSuccessStatusCode)
            {
                district = JsonConvert.DeserializeObject<List<District_tbl>>(response3.Content.ReadAsStringAsync().Result);
            }
            ViewBag.district = district;

            HttpResponseMessage response4 = client.GetAsync(apiUrl4).Result;
            if (response4.IsSuccessStatusCode)
            {
                org_cat = JsonConvert.DeserializeObject<List<Org_cat>>(response4.Content.ReadAsStringAsync().Result);
            }
            ViewBag.org_cat = org_cat;

            HttpResponseMessage response = client.GetAsync(apiUrl + categorytext + "/" + county + "/"+"/" + city + "/" + searchval).Result;

            if (response.IsSuccessStatusCode)
            {
                customers = JsonConvert.DeserializeObject<List<Org_physical>>(response.Content.ReadAsStringAsync().Result);
            }

            ViewBag.orgCount = customers.Count();
            ViewBag.searchText = search;
            return View(customers);
        }
        public async Task<ActionResult> OrganizationListPartialView(int county_id, int dist_id, int org_cat_id, string search)
        {
            List<District_tbl> district = new List<District_tbl>();
            List<Org_cat> org_cat = new List<Org_cat>();
            List<Org_physical> customers = new List<Org_physical>();

            string apiUrl = apiBaseUrl + "/Org/getAllOrgwithJobcount/";
            string apiUrl3 = apiBaseUrl + "/District/getAllActiveDistrict";
            string apiUrl4 = apiBaseUrl + "/Org_cat/getAllActiveOrgCategory";

            string categorytext = org_cat_id.ToString();
            string city = dist_id.ToString();
            string searchval = search;
            string county = county_id.ToString();
            if (string.IsNullOrEmpty(categorytext))
            {
                categorytext = "0";
            }
            if (string.IsNullOrEmpty(city))
            {
                city = "0";
            }
            if (string.IsNullOrEmpty(county))
            {
                county = "0";
            }
            if (string.IsNullOrEmpty(searchval))
            {
                searchval = "all";
            }

            HttpClient client = new HttpClient();


            HttpResponseMessage response3 = client.GetAsync(apiUrl3).Result;
            if (response3.IsSuccessStatusCode)
            {
                district = JsonConvert.DeserializeObject<List<District_tbl>>(response3.Content.ReadAsStringAsync().Result);
            }
            ViewBag.district = district;

            HttpResponseMessage response4 = client.GetAsync(apiUrl4).Result;
            if (response4.IsSuccessStatusCode)
            {
                org_cat = JsonConvert.DeserializeObject<List<Org_cat>>(response4.Content.ReadAsStringAsync().Result);
            }
            ViewBag.org_cat = org_cat;

            HttpResponseMessage response = client.GetAsync(apiUrl + categorytext + "/" + county + "/" + "/" + city + "/" + searchval).Result;

            if (response.IsSuccessStatusCode)
            {
                customers = JsonConvert.DeserializeObject<List<Org_physical>>(response.Content.ReadAsStringAsync().Result);
            }

            ViewBag.orgCount = customers.Count();
            ViewBag.searchText = search;
            return PartialView("OrganizationListPartialView", customers);
        }
        public IActionResult Create()
        {
            List<State_tbl> state = new List<State_tbl>();
            List<County_tbl> county = new List<County_tbl>();
            List<District_tbl> district = new List<District_tbl>();
            List<Currency_tbl> currency = new List<Currency_tbl>();
            List<Org_cat> org_cat = new List<Org_cat>();
            List<Location_tbl> location = new List<Location_tbl>();
            string apiUrl = apiBaseUrl + "/State/getAllActiveState";
            string apiUrl1 = apiBaseUrl + "/County/getAllActiveCounty";
            string apiUrl2 = apiBaseUrl + "/Currency/getAllActiveCurrency";
            string apiUrl3 = apiBaseUrl + "/District/getAllActiveDistrict";
            string apiUrl4 = apiBaseUrl + "/Org_cat/getAllActiveOrgCategory";
            string apiUrl5 = apiBaseUrl + "/Location/getAllActiveLocations";

            HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync(apiUrl).Result;
            if (response.IsSuccessStatusCode)
            {
                state = JsonConvert.DeserializeObject<List<State_tbl>>(response.Content.ReadAsStringAsync().Result);
            }
            ViewBag.state = state;

            HttpResponseMessage response1 = client.GetAsync(apiUrl1).Result;
            if (response.IsSuccessStatusCode)
            {
                county = JsonConvert.DeserializeObject<List<County_tbl>>(response1.Content.ReadAsStringAsync().Result);
            }
            ViewBag.county = county;

            HttpResponseMessage response2 = client.GetAsync(apiUrl2).Result;
            if (response.IsSuccessStatusCode)
            {
                currency = JsonConvert.DeserializeObject<List<Currency_tbl>>(response2.Content.ReadAsStringAsync().Result);
            }
            ViewBag.currency = currency;

            HttpResponseMessage response3 = client.GetAsync(apiUrl3).Result;
            if (response.IsSuccessStatusCode)
            {
                district = JsonConvert.DeserializeObject<List<District_tbl>>(response3.Content.ReadAsStringAsync().Result);
            }
            ViewBag.district = district;

            HttpResponseMessage response4 = client.GetAsync(apiUrl4).Result;
            if (response4.IsSuccessStatusCode)
            {
                org_cat = JsonConvert.DeserializeObject<List<Org_cat>>(response4.Content.ReadAsStringAsync().Result);
            }
            ViewBag.org_cat = org_cat;

            HttpResponseMessage response5 = client.GetAsync(apiUrl5).Result;
            if (response5.IsSuccessStatusCode)
            {
                location = JsonConvert.DeserializeObject<List<Location_tbl>>(response5.Content.ReadAsStringAsync().Result);
            }
            ViewBag.location = location;
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> Create(Org_physical org_ab, List<IFormFile> cmp_image, List<IFormFile> cmplogo)
        {
            string contentPath = this.Environment.ContentRootPath;
            string path = Path.Combine(this.Environment.WebRootPath, "Org_img_uploads");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            List<string> uploadedFiles = new List<string>();
            foreach (IFormFile postedFile in cmp_image)
            {
                string fileName = Path.GetFileName(postedFile.FileName);
                using (FileStream stream = new FileStream(Path.Combine(path, fileName), FileMode.Create))
                {
                    postedFile.CopyTo(stream);
                    uploadedFiles.Add(fileName);

                }
                org_ab.cmp_image = fileName;
                org_ab.cmpimage_path = path;
            }
            string contentPath1 = this.Environment.ContentRootPath;
            string path1 = Path.Combine(this.Environment.WebRootPath, "Org_img_uploads");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            List<string> uploadedFiles1 = new List<string>();
            foreach (IFormFile postedFile in cmplogo)
            {
                string fileName = Path.GetFileName(postedFile.FileName);
                using (FileStream stream = new FileStream(Path.Combine(path1, fileName), FileMode.Create))
                {
                    postedFile.CopyTo(stream);
                    uploadedFiles1.Add(fileName);

                }
                org_ab.cmplogo = fileName;
                org_ab.cmplogo_path = path1;
            }

            if (org_ab.isactive == "Active")
            {
                org_ab.status = true;
                // country_ab.identifier = "true";
            }
            else
            {
                org_ab.status = false;
                //  country_ab.identifier = false;
            }

            //HttpClient hc = new HttpClient();

            //hc.BaseAddress = new Uri(apiBaseUrl + "/Org/Create");
            //var locationconsume = hc.PostAsJsonAsync<Org_physical>("Create", org_ab);
            //locationconsume.Wait();
            //var readdata = locationconsume.Result;
            var httpClient = new HttpClient();
            var response = await httpClient.PostAsJsonAsync(apiBaseUrl + "/Org/Create", org_ab);
            jb_responce returnValue = await response.Content.ReadAsAsync<jb_responce>();

            int jobseeker_id = org_ab.org_id;
            string org_name = org_ab.org_name;
            

            HttpContext.Session.SetString("org_id_sess", org_ab.org_id.ToString());
            HttpContext.Session.SetString("org_name_sess", org_ab.org_name.ToString());

            

            //if (readdata.IsSuccessStatusCode)
                if (response.IsSuccessStatusCode)
                {
                    if (returnValue.status == true)
                    {
                        //Send Email==============

                        using (MailMessage mm = new MailMessage("childplus11@gmail.com", "childplus11@gmail.com"))
                        {
                            mm.Subject = "Organization Registration";
                            mm.Body = "Welcome to Jobportal. Hi " + org_ab.org_name + " ,Your organization profile has been created. Login Information Username: " + org_ab.user_name + " Password:org@123";
                            //if (model.Attachment.Length > 0)
                            //{
                            //    string fileName = Path.GetFileName(model.Attachment.FileName);
                            //    mm.Attachments.Add(new Attachment(model.Attachment.OpenReadStream(), fileName));
                            //}
                            mm.IsBodyHtml = false;
                            using (SmtpClient smtp = new SmtpClient())
                            {
                                smtp.Host = "smtp.gmail.com";
                                smtp.EnableSsl = true;
                                NetworkCredential NetworkCred = new NetworkCredential("childplus11@gmail.com", "mgtxdfwmpbqzrtil");
                                smtp.UseDefaultCredentials = true;
                                smtp.Credentials = NetworkCred;
                                smtp.Port = 587;
                                smtp.Send(mm);
                                ViewBag.Message = "Email sent.";
                            }
                        }


                        _notyf.Success(returnValue.message, 5);
                        return RedirectToAction("Orglist_admin", "Org");
                    }
                    else
                    {
                        _notyf.Error(returnValue.message, 9);
                    return RedirectToAction("Create", "Org");
                    //return View(org_ab);
                    }
                    
                }
                else
                {
                    _notyf.Success("Error..! Try Again..", 5);
                     return View(org_ab);
            }
            //return View(org_ab);


        }
        public async Task<ActionResult> EditOrg()
        {
            Org_physical org = null;

            IEnumerable<Org_physical> locobj = null;

            List<State_tbl> state = new List<State_tbl>();
            List<County_tbl> county = new List<County_tbl>();
            List<District_tbl> district = new List<District_tbl>();
            List<Location_tbl> location = new List<Location_tbl>();
            List<Org_cat> org_cat = new List<Org_cat>();

            
            string apiUrl5 = apiBaseUrl + "/State/getAll";
            string apiUrl1 = apiBaseUrl + "/County/getAll";
            string apiUrl2 = apiBaseUrl + "/Location/getAll";
            string apiUrl3 = apiBaseUrl + "/District/getAll";
            string apiUrl4 = apiBaseUrl + "/Org_cat/getAll";
            string apiUrl = apiBaseUrl + "/Org/get_by_id/";

            HttpClient client = new HttpClient();

            HttpResponseMessage response = client.GetAsync(apiUrl5).Result;
            if (response.IsSuccessStatusCode)
            {
                state = JsonConvert.DeserializeObject<List<State_tbl>>(response.Content.ReadAsStringAsync().Result);
            }
            ViewBag.state = state;

            HttpResponseMessage response1 = client.GetAsync(apiUrl1).Result;
            if (response1.IsSuccessStatusCode)
            {
                county = JsonConvert.DeserializeObject<List<County_tbl>>(response1.Content.ReadAsStringAsync().Result);
            }
            ViewBag.county = county;

            HttpResponseMessage response2 = client.GetAsync(apiUrl2).Result;
            if (response2.IsSuccessStatusCode)
            {
                location = JsonConvert.DeserializeObject<List<Location_tbl>>(response2.Content.ReadAsStringAsync().Result);
            }
            ViewBag.location = location;

            HttpResponseMessage response3 = client.GetAsync(apiUrl3).Result;
            if (response3.IsSuccessStatusCode)
            {
                district = JsonConvert.DeserializeObject<List<District_tbl>>(response3.Content.ReadAsStringAsync().Result);
            }
            ViewBag.district = district;

            HttpResponseMessage response4 = client.GetAsync(apiUrl4).Result;
            if (response4.IsSuccessStatusCode)
            {
                org_cat = JsonConvert.DeserializeObject<List<Org_cat>>(response4.Content.ReadAsStringAsync().Result);
            }
            ViewBag.org_cat = org_cat;

            int id = Convert.ToInt32(HttpContext.Session.GetString("org_id_admin"));

            HttpResponseMessage readdata = await client.GetAsync(apiUrl + id);
            //  var result = responseTask.Result;
            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<Org_physical>();
                displayresults.Wait();
                org = displayresults.Result;

            }
            ViewBag.profilepic = org.cmp_image;
            return View(org);

        }
        public async Task<ActionResult> EditOrg_admin()
        {
            Org_physical org = null;

            IEnumerable<Org_physical> locobj = null;

            List<State_tbl> state = new List<State_tbl>();
            List<County_tbl> county = new List<County_tbl>();
            List<District_tbl> district = new List<District_tbl>();
            List<Location_tbl> location = new List<Location_tbl>();
            List<Org_cat> org_cat = new List<Org_cat>();


            string apiUrl5 = apiBaseUrl + "/State/getAll";
            string apiUrl1 = apiBaseUrl + "/County/getAll";
            string apiUrl2 = apiBaseUrl + "/Location/getAll";
            string apiUrl3 = apiBaseUrl + "/District/getAll";
            string apiUrl4 = apiBaseUrl + "/Org_cat/getAll";
            string apiUrl = apiBaseUrl + "/Org/get_by_id/";

            HttpClient client = new HttpClient();

            HttpResponseMessage response = client.GetAsync(apiUrl5).Result;
            if (response.IsSuccessStatusCode)
            {
                state = JsonConvert.DeserializeObject<List<State_tbl>>(response.Content.ReadAsStringAsync().Result);
            }
            ViewBag.state = state;

            HttpResponseMessage response1 = client.GetAsync(apiUrl1).Result;
            if (response1.IsSuccessStatusCode)
            {
                county = JsonConvert.DeserializeObject<List<County_tbl>>(response1.Content.ReadAsStringAsync().Result);
            }
            ViewBag.county = county;

            HttpResponseMessage response2 = client.GetAsync(apiUrl2).Result;
            if (response2.IsSuccessStatusCode)
            {
                location = JsonConvert.DeserializeObject<List<Location_tbl>>(response2.Content.ReadAsStringAsync().Result);
            }
            ViewBag.location = location;

            HttpResponseMessage response3 = client.GetAsync(apiUrl3).Result;
            if (response3.IsSuccessStatusCode)
            {
                district = JsonConvert.DeserializeObject<List<District_tbl>>(response3.Content.ReadAsStringAsync().Result);
            }
            ViewBag.district = district;

            HttpResponseMessage response4 = client.GetAsync(apiUrl4).Result;
            if (response4.IsSuccessStatusCode)
            {
                org_cat = JsonConvert.DeserializeObject<List<Org_cat>>(response4.Content.ReadAsStringAsync().Result);
            }
            ViewBag.org_cat = org_cat;

            int id = Convert.ToInt32(HttpContext.Session.GetString("org_id_admin"));

            HttpResponseMessage readdata = await client.GetAsync(apiUrl + id);
            //  var result = responseTask.Result;
            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<Org_physical>();
                displayresults.Wait();
                org = displayresults.Result;

            }
            ViewBag.profilepic = org.cmp_image;

            return View(org);

        }
        public async Task<ActionResult> UpdateOrg_admin(Org_physical org_ab, List<IFormFile> cmp_image, List<IFormFile> cmplogo)
        {
            string contentPath = this.Environment.ContentRootPath;
            string path = Path.Combine(this.Environment.WebRootPath, "Org_img_uploads");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            List<string> uploadedFiles = new List<string>();
            foreach (IFormFile postedFile in cmp_image)
            {
                string fileName = Path.GetFileName(postedFile.FileName);
                using (FileStream stream = new FileStream(Path.Combine(path, fileName), FileMode.Create))
                {
                    postedFile.CopyTo(stream);
                    uploadedFiles.Add(fileName);

                }
                org_ab.cmp_image = fileName;
                org_ab.cmpimage_path = path;
            }
            string contentPath1 = this.Environment.ContentRootPath;
            string path1 = Path.Combine(this.Environment.WebRootPath, "Org_img_uploads");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            List<string> uploadedFiles1 = new List<string>();
            foreach (IFormFile postedFile in cmplogo)
            {
                string fileName = Path.GetFileName(postedFile.FileName);
                using (FileStream stream = new FileStream(Path.Combine(path1, fileName), FileMode.Create))
                {
                    postedFile.CopyTo(stream);
                    uploadedFiles1.Add(fileName);

                }
                org_ab.cmplogo = fileName;
                org_ab.cmplogo_path = path1;
            }

            if (org_ab.isactive == "1")
            {
                org_ab.status = true;
            }
            else
            {
                org_ab.status = false;
            }
            HttpClient hc = new HttpClient();

            hc.BaseAddress = new Uri(apiBaseUrl + "/Org/");
            var locationconsume = hc.PutAsJsonAsync<Org_physical>("Update", org_ab);
            locationconsume.Wait();
            var readdata = locationconsume.Result;

            if (readdata.IsSuccessStatusCode)
            {
                _notyf.Information("Updated Successfully..!", 5);
                return RedirectToAction("OrgProfile_admin", "Org");

            }
            else
            {
                _notyf.Error("Error.! Try Again..", 5);
                return RedirectToAction("OrgProfile_admin", "Org");
            }
            //return View(org_ab);
        }
        public async Task<ActionResult> DeleteOrg(int id)
        {
            //Currency_tbl currency = null;
            //IEnumerable<Currency_tbl> locobj = null;
            string apiUrl = apiBaseUrl + "/Org/Delete/";

            HttpClient client = new HttpClient();

            HttpResponseMessage readdata = await client.GetAsync(apiUrl + id);
            //  var result = responseTask.Result;
            if (readdata.IsSuccessStatusCode)
            {

                _notyf.Error("Deleted Successfully..!", 5);

            }
            else
            {
                _notyf.Error("Error..!", 5);
            }

            return RedirectToAction("Orglist", "Org");

        }
        public async Task<ActionResult> Update(Org_physical org_ab, List<IFormFile> cmp_image, List<IFormFile> cmplogo)
        {
            string contentPath = this.Environment.ContentRootPath;
            string path = Path.Combine(this.Environment.WebRootPath, "Org_img_uploads");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            List<string> uploadedFiles = new List<string>();
            foreach (IFormFile postedFile in cmp_image)
            {
                string fileName = Path.GetFileName(postedFile.FileName);
                using (FileStream stream = new FileStream(Path.Combine(path, fileName), FileMode.Create))
                {
                    postedFile.CopyTo(stream);
                    uploadedFiles.Add(fileName);

                }
                org_ab.cmp_image = fileName;
                org_ab.cmpimage_path = path;
            }
            string contentPath1 = this.Environment.ContentRootPath;
            string path1 = Path.Combine(this.Environment.WebRootPath, "Org_img_uploads");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            List<string> uploadedFiles1 = new List<string>();
            foreach (IFormFile postedFile in cmplogo)
            {
                string fileName = Path.GetFileName(postedFile.FileName);
                using (FileStream stream = new FileStream(Path.Combine(path1, fileName), FileMode.Create))
                {
                    postedFile.CopyTo(stream);
                    uploadedFiles1.Add(fileName);

                }
                org_ab.cmplogo = fileName;
                org_ab.cmplogo_path = path1;
            }

            if (org_ab.isactive == "1")
            {
                org_ab.status = true;
            }
            else
            {
                org_ab.status = false;
            }
            HttpClient hc = new HttpClient();

            hc.BaseAddress = new Uri(apiBaseUrl + "/Org/");
            var locationconsume = hc.PutAsJsonAsync<Org_physical>("Update", org_ab);
            locationconsume.Wait();
            var readdata = locationconsume.Result;

            if (readdata.IsSuccessStatusCode)
            {
                _notyf.Information("Updated Successfully..!", 5);
                return RedirectToAction("OrgProfile", "Org");

            }
            else
            {
                _notyf.Error("Error.! Try Again..", 5);
                return RedirectToAction("OrgProfile", "Org");
            }
            //return View(org_ab);
        }
        public async Task<ActionResult> Org_status(string activeStat, int passedId, Org_physical Org_physical)
        {
            //Employer.id = Convert.ToInt32(HttpContext.Session.GetString("employer_loginid"));

            if (Org_physical.activeStat == "Active")
            {
                Org_physical.status = false;
                Org_physical.isactive = "InActive";

            }
            else
            {
                Org_physical.status = true;
                Org_physical.isactive = "Active";

                //  country_ab.identifier = false;
            }
            Org_physical.org_id = Org_physical.passedId;
            HttpClient hc = new HttpClient();

            hc.BaseAddress = new Uri(apiBaseUrl + "/Org/");
            var locationconsume = hc.PutAsJsonAsync<Org_physical>("Update_status", Org_physical);
            locationconsume.Wait();
            var readdata = locationconsume.Result;

            if (readdata.IsSuccessStatusCode)
            {
                _notyf.Success("Status Updated Successfully", 5);
                return RedirectToAction("Orglist_admin", "Org");
            }
            else
            {
                _notyf.Success("Error..! Try Again..", 5);
                return RedirectToAction("Orglist_admin", "Org");
            }
            //return View(Employer);


        }
        public IActionResult Orglist_admin()
        {
            List<Org_physical> customers = new List<Org_physical>();
            string apiUrl = apiBaseUrl + "/Org/getAll1";

            HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync(apiUrl).Result;
            if (response.IsSuccessStatusCode)
            {
                customers = JsonConvert.DeserializeObject<List<Org_physical>>(response.Content.ReadAsStringAsync().Result);
            }
            

            return View(customers);
        }
        public async Task<ActionResult> OrgProfile(int id)
        {
            if (id == 0)
            {
                id = Convert.ToInt32(HttpContext.Session.GetString("org_loginid"));
            }
            HttpClient client = new HttpClient();

            Org_physical Org = null;

            string apiUrl = apiBaseUrl + "/Org/get_by_id/";
          //  string apiUrl1 = "http://localhost:62332/Org_cont/get_by_id/";
            //string apiUrl2 = apiBaseUrl + "/Org_mem/getall_job_by_id/";

            HttpResponseMessage readdata = await client.GetAsync(apiUrl + id);

            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<Org_physical>();
                displayresults.Wait();
                Org = displayresults.Result;

            }
            ViewBag.Org = Org;
            ViewBag.org_name = Org.org_name;
            ViewBag.short_name = Org.short_name;
            ViewBag.DBA = Org.DBA.ToString("MM/dd/yyyy");
          
            ViewBag.org_address = Org.org_address;

            ViewBag.zip = Org.zip;


            ViewBag.ProviderID = Org.ProviderID;
            ViewBag.LicenseID = Org.LicenseID;
            ViewBag.Providertype = Org.Providertype;

            ViewBag.category_name = Org.category_name;
            ViewBag.dist_name = Org.dist_name;
            ViewBag.state_name = Org.state_name;
            ViewBag.county_name = Org.county_name;
            ViewBag.location_name = Org.location_name;
            ViewBag.Phone1 = Org.Phone1;
            ViewBag.Phone2 = Org.Phone2;
            ViewBag.Email1 = Org.Email1;
            ViewBag.Email2 = Org.Email2;
            ViewBag.website = Org.website;
            ViewBag.orgdescription = Org.orgdescription;
            ViewBag.cmp_image = Org.cmp_image;
            ViewBag.cmp_logo = Org.cmplogo;

            int org_id = Convert.ToInt32(HttpContext.Session.GetString("org_loginid"));

            job_reg_tbl job = new job_reg_tbl();

            List<job_reg_tbl> customers = new List<job_reg_tbl>();
            job.org_id = org_id;
            //string apiUrl1 = apiBaseUrl + "/Employer/getall_job_by_Orgid/";
            string apiUrl2 = apiBaseUrl + "/Jobregister/getall_job_by_orgid/";

            HttpResponseMessage response = client.GetAsync(apiUrl2 + org_id).Result;
            if (response.IsSuccessStatusCode)
            {
                customers = JsonConvert.DeserializeObject<List<job_reg_tbl>>(response.Content.ReadAsStringAsync().Result);
            }
            ViewBag.jobs_org = customers;
            ViewBag.jobs_count = customers.Count();

            int Org_id = Org.org_id;
            HttpContext.Session.SetString("org_id_admin", Org.org_id.ToString());

            return View(Org);
        }
        public async Task<ActionResult> Admin_Org_passwordchange(int id)
        {  
            Org_physical Org = null;
            HttpClient client = new HttpClient();
            string apiUrl = apiBaseUrl + "/Org/get_by_id/";
        
       

        HttpResponseMessage readdata = await client.GetAsync(apiUrl + id);

            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<Org_physical>();
                displayresults.Wait();
                Org = displayresults.Result;

            }
 
        
            return View(Org);
        }
        public async Task<ActionResult> OrganizationProfile(int id)
        {
            if (id == 0)
            {
                id = Convert.ToInt32(HttpContext.Session.GetString("org_loginid"));
            }

            HttpClient client = new HttpClient();

            Org_physical Org = null;

            string apiUrl = apiBaseUrl + "/Org/get_by_id/";
            //  string apiUrl1 = "http://localhost:62332/Org_cont/get_by_id/";
            string apiUrl2 = apiBaseUrl + "/Org_mem/getall_job_by_id/";

            HttpResponseMessage readdata = await client.GetAsync(apiUrl + id);

            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<Org_physical>();
                displayresults.Wait();
                Org = displayresults.Result;

            }
            ViewBag.Org = Org;
            ViewBag.org_name = Org.org_name;
            ViewBag.short_name = Org.short_name;
            ViewBag.DBA = Org.DBA.ToString("MM/dd/yyyy");

            ViewBag.org_address = Org.org_address;

            ViewBag.zip = Org.zip;


            ViewBag.ProviderID = Org.ProviderID;
            ViewBag.LicenseID = Org.LicenseID;
            ViewBag.Providertype = Org.Providertype;

            ViewBag.category_name = Org.category_name;
            ViewBag.dist_name = Org.dist_name;
            ViewBag.state_name = Org.state_name;
            ViewBag.county_name = Org.county_name;
            ViewBag.location_name = Org.location_name;
            ViewBag.Phone1 = Org.Phone1;
            ViewBag.Phone2 = Org.Phone2;
            ViewBag.Email1 = Org.Email1;
            ViewBag.Email2 = Org.Email2;
            ViewBag.website = Org.website;
            ViewBag.orgdescription = Org.orgdescription;
            ViewBag.cmp_image = Org.cmp_image;
            ViewBag.cmp_logo = Org.cmplogo;

            int seeker_id= Convert.ToInt32(HttpContext.Session.GetString("jobseeker_loginid").ToString());
            List<job_reg_tbl> customers = new List<job_reg_tbl>();
            string apiUrl1 = apiBaseUrl + "/Employer/getall_job_by_seeker_Orgid/";
            int org_id = id;
            HttpResponseMessage response = client.GetAsync(apiUrl1 + org_id+"/"+ seeker_id).Result;

            if (response.IsSuccessStatusCode)
            {
                customers = JsonConvert.DeserializeObject<List<job_reg_tbl>>(response.Content.ReadAsStringAsync().Result);
            }
            ViewBag.jobs_org = customers;
            ViewBag.jobs_count = customers.Count();


            List<Org_mem_ab> Org_mem = new List<Org_mem_ab>();
            HttpResponseMessage response2 = client.GetAsync(apiUrl2 + id).Result;
            if (response2.IsSuccessStatusCode)
            {
                Org_mem = JsonConvert.DeserializeObject<List<Org_mem_ab>>(response2.Content.ReadAsStringAsync().Result);
            }
            ViewBag.Org_mem = Org_mem;
            ViewBag.Org_mem_count = Org_mem.Count();

            int Org_id = Org.org_id;
            HttpContext.Session.SetString("org_id_admin", Org.org_id.ToString());

            return View(Org);
        }
        public async Task<ActionResult> OrganizationProfilenew(int id)
        {
            if (id == 0)
            {
                id = Convert.ToInt32(HttpContext.Session.GetString("org_loginid"));
            }
            HttpClient client = new HttpClient();

            Org_physical Org = null;

            string apiUrl = apiBaseUrl + "/Org/get_by_id/";
            //  string apiUrl1 = "http://localhost:62332/Org_cont/get_by_id/";
            string apiUrl2 = apiBaseUrl + "/Org_mem/getall_job_by_id/";

            HttpResponseMessage readdata = await client.GetAsync(apiUrl + id);

            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<Org_physical>();
                displayresults.Wait();
                Org = displayresults.Result;

            }
            ViewBag.Org = Org;
            ViewBag.org_name = Org.org_name;
            ViewBag.short_name = Org.short_name;
            ViewBag.DBA = Org.DBA.ToString("MM/dd/yyyy");
            ViewBag.orgdescription = Org.orgdescription;
            ViewBag.org_address = Org.org_address;

            ViewBag.zip = Org.zip;


            ViewBag.ProviderID = Org.ProviderID;
            ViewBag.LicenseID = Org.LicenseID;
            ViewBag.Providertype = Org.Providertype;

            ViewBag.category_name = Org.category_name;
            ViewBag.dist_name = Org.dist_name;
            ViewBag.state_name = Org.state_name;
            ViewBag.county_name = Org.county_name;
            ViewBag.location_name = Org.location_name;
            ViewBag.Phone1 = Org.Phone1;
            ViewBag.Phone2 = Org.Phone2;
            ViewBag.Email1 = Org.Email1;
            ViewBag.Email2 = Org.Email2;
            ViewBag.website = Org.website;
            ViewBag.orgdescription = Org.orgdescription;
            ViewBag.cmp_image = Org.cmp_image;
            ViewBag.cmp_logo = Org.cmplogo;


            List<job_reg_tbl> customers = new List<job_reg_tbl>();
            string apiUrl1 = apiBaseUrl + "/Employer/getall_active_job_by_Orgid/";
            int org_id = id;
            HttpResponseMessage response = client.GetAsync(apiUrl1 + org_id).Result;
            if (response.IsSuccessStatusCode)
            {
                customers = JsonConvert.DeserializeObject<List<job_reg_tbl>>(response.Content.ReadAsStringAsync().Result);
            }
            ViewBag.jobs_org = customers;
            ViewBag.jobs_count = customers.Count();


            List<Org_mem_ab> Org_mem = new List<Org_mem_ab>();
            HttpResponseMessage response2 = client.GetAsync(apiUrl2 + id).Result;
            if (response2.IsSuccessStatusCode)
            {
                Org_mem = JsonConvert.DeserializeObject<List<Org_mem_ab>>(response2.Content.ReadAsStringAsync().Result);
            }
            ViewBag.Org_mem = Org_mem;
            ViewBag.Org_mem_count = Org_mem.Count();

            int Org_id = Org.org_id;
            HttpContext.Session.SetString("org_id_admin", Org.org_id.ToString());

            return View(Org);
        }


        public async Task<ActionResult> OrgProfile_admin(int id)
        {
            if (id == 0 )
                id = Convert.ToInt32(HttpContext.Session.GetString("org_id_admin"));
            


            HttpClient client = new HttpClient();

            Org_physical Org = null;

            string apiUrl = apiBaseUrl + "/Org/get_by_id/";
           // string apiUrl1 = "http://localhost:62332/Org_cont/get_by_id/";
            string apiUrl2 = apiBaseUrl + "/Org_mem/getall_job_by_id/";

            HttpResponseMessage readdata = await client.GetAsync(apiUrl + id);

            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<Org_physical>();
                displayresults.Wait();
                Org = displayresults.Result;

            }
            ViewBag.Org = Org;
            ViewBag.org_name = Org.org_name;
            ViewBag.short_name = Org.short_name;
            ViewBag.DBA = Org.DBA.ToString("MM/dd/yyyy");

            ViewBag.org_address = Org.org_address;

            ViewBag.zip = Org.zip;


            ViewBag.ProviderID = Org.ProviderID;
            ViewBag.LicenseID = Org.LicenseID;
            ViewBag.Providertype = Org.Providertype;

            ViewBag.category_name = Org.category_name;
            ViewBag.dist_name = Org.dist_name;
            ViewBag.state_name = Org.state_name;
            ViewBag.county_name = Org.county_name;
            ViewBag.location_name = Org.location_name;
            ViewBag.Phone1 = Org.Phone1;
            ViewBag.Phone2 = Org.Phone2;
            ViewBag.Email1 = Org.Email1;
            ViewBag.Email2 = Org.Email2;
            ViewBag.website = Org.website;
            ViewBag.orgdescription = Org.orgdescription;
            ViewBag.cmp_image = Org.cmp_image;
            ViewBag.cmp_logo = Org.cmplogo;


            List<job_reg_tbl> customers = new List<job_reg_tbl>();
            string apiUrl1 = apiBaseUrl + "/Employer/getall_job_by_Orgid/";
            int org_id = id;
            HttpResponseMessage response = client.GetAsync(apiUrl1 + org_id).Result;
            if (response.IsSuccessStatusCode)
            {
                customers = JsonConvert.DeserializeObject<List<job_reg_tbl>>(response.Content.ReadAsStringAsync().Result);
            }
            ViewBag.jobs_org = customers;
            ViewBag.jobs_count = customers.Count();


            List<Org_mem_ab> Org_mem = new List<Org_mem_ab>();
            HttpResponseMessage response2 = client.GetAsync(apiUrl2 + id).Result;
            if (response2.IsSuccessStatusCode)
            {
                Org_mem = JsonConvert.DeserializeObject<List<Org_mem_ab>>(response2.Content.ReadAsStringAsync().Result);
            }
            ViewBag.Org_mem = Org_mem;
            ViewBag.Org_mem_count = Org_mem.Count();

            int Org_id = Org.org_id;
            HttpContext.Session.SetString("org_id_admin", Org.org_id.ToString());

            List<Employer> employees = new List<Employer>();
            string apiUrl3 = apiBaseUrl + "/Employer/getAll/";

            HttpResponseMessage response3 = client.GetAsync(apiUrl3 + org_id).Result;
            if (response3.IsSuccessStatusCode)
            {
                employees = JsonConvert.DeserializeObject<List<Employer>>(response3.Content.ReadAsStringAsync().Result);
            }
            ViewBag.employees_org = employees;
            ViewBag.employees_count = employees.Count();

            return View(Org);
        }
        public IActionResult Org_employee()
        {
            List<Employer> customers = new List<Employer>();
             int org_id= Convert.ToInt32(HttpContext.Session.GetString("org_id_admin"));

            string apiUrl = apiBaseUrl + "/Org/Org_employee/";

            HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync(apiUrl + org_id).Result;
            if (response.IsSuccessStatusCode)
            {
                customers = JsonConvert.DeserializeObject<List<Employer>>(response.Content.ReadAsStringAsync().Result);
            }

            return View(customers);
        }

        public async Task<ActionResult> getStateForCountry(string country)
        {
            List<State_tbl> state = new List<State_tbl>();

            string apiUrl = apiBaseUrl + "/State/getAllByCountry";

            HttpClient client = new HttpClient();

            HttpResponseMessage response = await client.GetAsync(apiUrl + country);

            if (response.IsSuccessStatusCode)
            {
                state = JsonConvert.DeserializeObject<List<State_tbl>>(response.Content.ReadAsStringAsync().Result);
            }

            ViewBag.state = state;
            return View();
        }

        public IActionResult Changepassword()
        {
            return View();
        }
         public async Task<ActionResult> UpdatePassword(Org_physical org)
        {
           
            HttpClient client = new HttpClient();
            List<Org_physical> org_name = new List<Org_physical>();
            Org_physical Org = null;
            string apiUrl2 = apiBaseUrl + "/Org/get_by_id/";
            int id = Convert.ToInt32(HttpContext.Session.GetString("org_loginid"));
            HttpResponseMessage readdata = await client.GetAsync(apiUrl2 + id);

            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<Org_physical>();
                displayresults.Wait();
                Org = displayresults.Result;

            }

            if (Org.isactive == "Active")
            {
                Org.status = true;

            }
            else
            {
                Org.status = false;

                //  country_ab.identifier = false;
            }
            string org_password = org.new_password; 
            string orgname = Org.org_name;
            string OldPasswrd = org.org_password;
            Org.org_password = org_password;
            Org.org_id= id;
            Org.old_password = OldPasswrd;
            string org_repassword = org.retype_password;
            if(org_password!= org_repassword)
            {
                TempData["Repassword"] = "New password Does not match with Re-Type password.. enter again.!";
                _notyf.Error("Error..! Try Again..", 5);
                return RedirectToAction("Changepassword", "Org");
            }
            else
            {
                HttpClient hc = new HttpClient();

                hc.BaseAddress = new Uri(apiBaseUrl + "/Org/");
                var locationconsume = hc.PutAsJsonAsync<Org_physical>("Updatepassword", Org);
                locationconsume.Wait();
                var readdata1 = locationconsume.Result;

                if (readdata1.IsSuccessStatusCode)
                {
                    using (MailMessage mm = new MailMessage("childplus11@gmail.com", "childplus11@gmail.com"))
                    {
                        mm.Subject = "Password Changed";
                        mm.Body = "Hi "+ orgname + " , Your Password has been successfully changed !....New Password '"+ Org.org_password + "'";
                        //if (model.Attachment.Length > 0)
                        //{
                        //    string fileName = Path.GetFileName(model.Attachment.FileName);
                        //    mm.Attachments.Add(new Attachment(model.Attachment.OpenReadStream(), fileName));
                        //}
                        mm.IsBodyHtml = false;
                        using (SmtpClient smtp = new SmtpClient())
                        {
                            smtp.Host = "smtp.gmail.com";
                            smtp.EnableSsl = true;
                            NetworkCredential NetworkCred = new NetworkCredential("childplus11@gmail.com", "mgtxdfwmpbqzrtil");
                            smtp.UseDefaultCredentials = true;
                            smtp.Credentials = NetworkCred;
                            smtp.Port = 587;
                            smtp.Send(mm);
                            ViewBag.Message = "Email sent.";
                        }
                    }
                    //_notyf.Information("Updated Successfully", 3);
                    //return RedirectToAction("jobregisterlist", "Jobregister");
                    _notyf.Success("Password changed Successfully", 5);
                    return RedirectToAction("Signin", "Login");
                }

                else
                {
                    _notyf.Error("Error..! Try Again..", 5);
                    TempData["Oldpassword"] = "Old Password is incorrect.. Re-enter the correct Password!";
                    return RedirectToAction("Changepassword", "Org");
                }




            }

        }
        public async Task<ActionResult> OrgUpdatePasswordAdmin(Org_physical org)
        {

            HttpClient client = new HttpClient();
            List<Org_physical> org_name = new List<Org_physical>();
            Org_physical Org = null;
             string apiUrl2 = apiBaseUrl + "/Org/get_by_id/";
            // int id = Convert.ToInt32(HttpContext.Session.GetString("org_loginid"));
            int id = org.org_id;
             HttpResponseMessage readdata = await client.GetAsync(apiUrl2 + id);

            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<Org_physical>();
                displayresults.Wait();
                Org = displayresults.Result;

            }

           
            string org_password = org.new_password;
            string orgname = Org.org_name;
            string OldPasswrd = org.org_password;
            Org.org_password = org_password;
            Org.org_id = id;
            Org.old_password = OldPasswrd;
            string emp_repassword = org.retype_password;
            if (org_password != emp_repassword)
            {
                //_notyf.Error("Password Does not Match", 5);
                TempData["Repassword"] = "New password does not match with Re-Type password.. enter again.!";
                _notyf.Information("Error..! Try Again..", 5);
               return RedirectToAction("Admin_Org_passwordchange", "Org", new { id = id });

            }
            else
            {
                HttpClient hc = new HttpClient();

                hc.BaseAddress = new Uri(apiBaseUrl + "/Org/");
                var locationconsume = hc.PutAsJsonAsync<Org_physical>("Updatepassword", Org);
                locationconsume.Wait();
                var readdata1 = locationconsume.Result;

                if (readdata1.IsSuccessStatusCode)
                {
                    using (MailMessage mm = new MailMessage("childplus11@gmail.com", "childplus11@gmail.com"))
                    {
                        mm.Subject = "Password Changed";
                        mm.Body = "Hi " + Org.org_name + " , Your Password has been successfully changed !....New Password '" + org_password + "'";
                        //if (model.Attachment.Length > 0)
                        //{
                        //    string fileName = Path.GetFileName(model.Attachment.FileName);
                        //    mm.Attachments.Add(new Attachment(model.Attachment.OpenReadStream(), fileName));
                        //}
                        mm.IsBodyHtml = false;
                        using (SmtpClient smtp = new SmtpClient())
                        {
                            smtp.Host = "smtp.gmail.com";
                            smtp.EnableSsl = true;
                            NetworkCredential NetworkCred = new NetworkCredential("childplus11@gmail.com", "mgtxdfwmpbqzrtil");
                            smtp.UseDefaultCredentials = true;
                            smtp.Credentials = NetworkCred;
                            smtp.Port = 587;
                            smtp.Send(mm);
                            ViewBag.Message = "Email sent.";
                        }
                    }

                    _notyf.Information("Password changed..!", 5);
                    //return RedirectToAction("jobregisterlist", "Jobregister");

                }
                else
                {
                    _notyf.Error("Error..! Try Again..", 5);

                }
                return RedirectToAction("Orglist_admin", "Org");




            }

        }
        public async Task<ActionResult> ImportOrg( IFormFile imp_file)
        {
            if (imp_file != null)
            {
                string wwwPath = this.Environment.WebRootPath;
                string contentPath = this.Environment.ContentRootPath;
                string path = Path.Combine(this.Environment.WebRootPath, "Org_img_uploads");
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                List<string> uploadedFiles = new List<string>();
                // foreach (IFormFile postedFile in imp_file)
                //{

                string fileName = Path.GetFileName(imp_file.FileName);
                using (FileStream stream = new FileStream(Path.Combine(path, fileName), FileMode.Create))
                {
                    imp_file.CopyTo(stream);
                    uploadedFiles.Add(fileName);
                    // ViewBag.Message += string.Format("<b>{0}</b> uploaded.<br />", fileName);
                }
                // }
                ////Import Excel File to Server
                ////
                string excelfileName = path + "/" + fileName;
                // excelfileName = path + "/" + fileName;

                DataSet dsexcelRecords = new DataSet();
                IExcelDataReader exreader = null;
                System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
                using (var stream = System.IO.File.Open(excelfileName, FileMode.Open, FileAccess.Read))
                {
                    // using (FileStream fs = File.OpenRead(fileName))
                    //  {

                    using (exreader = ExcelReaderFactory.CreateReader(stream))
                    {



                        dsexcelRecords = exreader.AsDataSet();
                        exreader.Close();
                        DataTable dtOrgRecords = dsexcelRecords.Tables[0];
                        int ct = dtOrgRecords.Rows.Count;
                        Console.WriteLine(ct);
                        List<Org_physical> obj_List = new List<Org_physical>();
                        for (int i = 1; i < dtOrgRecords.Rows.Count; i++)
                        {
                            if (dtOrgRecords.Rows[i][0].ToString() == null || dtOrgRecords.Rows[i][0].ToString() == "")
                            {
                                break;
                            }
                            else
                            {
                                // continue;
                            }
                           // if (dtOrgRecords.Rows[i][0].ToString() != "Record ID"|| dtOrgRecords.Rows[i][0].ToString() != "Organization*")
                            //{
                                Org_physical objOrgab = new Org_physical();

                                objOrgab.record_id = dtOrgRecords.Rows[i][0].ToString();
                                objOrgab.org_name = dtOrgRecords.Rows[i][1].ToString();
                                objOrgab.LicenseID = dtOrgRecords.Rows[i][2].ToString();
                                objOrgab.org_address = dtOrgRecords.Rows[i][3].ToString();
                                objOrgab.county_name = dtOrgRecords.Rows[i][4].ToString();
                                objOrgab.state_name = dtOrgRecords.Rows[i][5].ToString();
                                objOrgab.dist_name = dtOrgRecords.Rows[i][6].ToString();
                                objOrgab.category_name = dtOrgRecords.Rows[i][7].ToString();
                                objOrgab.Providertype = dtOrgRecords.Rows[i][8].ToString();
                                objOrgab.ProviderID = dtOrgRecords.Rows[i][9].ToString();
                                objOrgab.lat_loc = dtOrgRecords.Rows[i][10].ToString();
                                objOrgab.lng_loc = dtOrgRecords.Rows[i][11].ToString();
                                objOrgab.Phone1 = dtOrgRecords.Rows[i][12].ToString();
                                objOrgab.Email1 = dtOrgRecords.Rows[i][13].ToString();
                                objOrgab.website = dtOrgRecords.Rows[i][14].ToString();
                                //objOrgab.DBA = dtOrgRecords.Rows[i][15].ToString();
                                objOrgab.user_name = dtOrgRecords.Rows[i][16].ToString();
                                objOrgab.orgdescription = dtOrgRecords.Rows[i][17].ToString();

                                obj_List.Add(objOrgab);

                            //}
                        }
                        ImportOrgModel orgModel = new ImportOrgModel();
                        HttpClient client = new HttpClient();
                        List<Org_physical> customers = new List<Org_physical>();
                        if (obj_List.Count > 0)
                        {
                            orgModel.orgab = obj_List;

                            client.BaseAddress = new Uri(apiBaseUrl + "/Employee/");

                            var locationconsume = client.PostAsJsonAsync<ImportOrgModel>("ImportOrganizations", orgModel);
                            locationconsume.Wait();
                            var readdata = locationconsume.Result;

                            if (readdata.IsSuccessStatusCode)
                            {

                                _notyf.Success("Success", 5);
                                return RedirectToAction("Orglist_admin", "Org");



                            }


                        }
                        _notyf.Error("Data Imports Failed", 5);
                        return RedirectToAction("Orglist_admin", "Org");

                    }
                }
            }  
            else
            {
                _notyf.Error("Data Imports Failed", 5);
                return RedirectToAction("Orglist_admin", "Org");
            }
        }
        }
}
