﻿using AspNetCoreHero.ToastNotification.Abstractions;
using Jobportal_design.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Threading.Tasks;


namespace Jobportal_design.Controllers
{


    public class JobseekerController : Controller
    {
        private IWebHostEnvironment Environment;
        private readonly INotyfService _notyf;
        private IConfiguration _Configure;
        string apiBaseUrl;
        public JobseekerController(IWebHostEnvironment _environment, INotyfService notyf, IConfiguration configuration)
        {
            Environment = _environment;
            _notyf = notyf;
            _Configure = configuration;
            apiBaseUrl = _Configure.GetValue<string>("WebAPIBaseUrl");
        }
        
        int jobseeker_id = 0;



        job_reg_tbl vm = new job_reg_tbl()
        {
            Select = new List<SelectListItem>
            {
                //new SelectListItem {Text = "All Job Types", Value = "all"},
                new SelectListItem {Text = "Full Time", Value = "Full Time"},
                new SelectListItem {Text = "Part Time", Value = "Part Time"},
                new SelectListItem {Text = "Remote", Value = "Remote"},
                new SelectListItem {Text = "Hybrid", Value = "Hybrid"},
            }
        };

        job_reg_tbl vm1 = new job_reg_tbl()
        {
            Select = new List<SelectListItem>
            {
                //new SelectListItem {Text = "All Experiences", Value = "all"},
                new SelectListItem {Text = "Fresher", Value = "0"},
                new SelectListItem {Text = "1 Year", Value = "1"},
                new SelectListItem {Text = "2 Years", Value = "2"},
                new SelectListItem {Text = "3 Years", Value = "3"},

                new SelectListItem {Text = "4 Years", Value = "4"},
                new SelectListItem {Text = "5 Years", Value = "5"},
                new SelectListItem {Text = "6 Years", Value = "6"},
                new SelectListItem {Text = "7 Years", Value = "7"},
                new SelectListItem {Text = "8 Years", Value = "8"},
            }
        };
        job_reg_tbl vm2 = new job_reg_tbl()
        {
            Select = new List<SelectListItem>
            {

            }
        };

        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Changepassword()
        {
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> UpdatePassword(seek_tbl seek_ab)
        {
            int id = Convert.ToInt32(HttpContext.Session.GetString("jobseeker_loginid"));

            string oldPassord = seek_ab.e_passwordViewbag;
            string oldPassType = seek_ab.old_password;
            string emp_password = seek_ab.e_password;
            string empname = seek_ab.fname;
            seek_ab.e_password = emp_password;
            string emp_repassword = seek_ab.retype_password;
            seek_ab.id = id;
            //if (oldPassord != oldPassType)
            //{
            //    TempData["Oldpassword"] = "Old Password is incorrect.. Re-enter the correct Password!";
            //    _notyf.Success("Error..! Try Again..", 5);
            //    return RedirectToAction("Appliedjobdetail", "jobseeker", new { ActiveTab = "tab-my-profile", isRedirect = "true" });
            //}
           if (emp_password != emp_repassword)
            {
                TempData["Repassword"] = "New password Does not match with Re-Type password.. enter again.!";
                _notyf.Success("Error..! Try Again..", 5);
                return RedirectToAction("Appliedjobdetail", "jobseeker", new { ActiveTab = "tab-my-profile", isRedirect = "true" });
            
            }
            else
            {

                HttpClient hc = new HttpClient();

                hc.BaseAddress = new Uri(apiBaseUrl + "/Seek/Updatepassword");
                var locationconsume = hc.PutAsJsonAsync<seek_tbl>("Updatepassword", seek_ab);
                locationconsume.Wait();
                var readdata1 = locationconsume.Result;

                if (readdata1.IsSuccessStatusCode)
                {
                    using (MailMessage mm = new MailMessage("childplus11@gmail.com", "childplus11@gmail.com"))
                    {
                        mm.Subject = "Password Changed";
                        mm.Body = "Hi " + seek_ab.fname + " , Your Password has been successfully changed !....New Password '" + seek_ab.e_password + "'";
                        //if (model.Attachment.Length > 0)
                        //{
                        //    string fileName = Path.GetFileName(model.Attachment.FileName);
                        //    mm.Attachments.Add(new Attachment(model.Attachment.OpenReadStream(), fileName));
                        //}
                        mm.IsBodyHtml = false;
                        using (SmtpClient smtp = new SmtpClient())
                        {
                            smtp.Host = "smtp.gmail.com";
                            smtp.EnableSsl = true;
                            NetworkCredential NetworkCred = new NetworkCredential("childplus11@gmail.com", "mgtxdfwmpbqzrtil");
                            smtp.UseDefaultCredentials = true;
                            smtp.Credentials = NetworkCred;
                            smtp.Port = 587;
                            smtp.Send(mm);
                            ViewBag.Message = "Email sent.";
                        }
                    }
                    _notyf.Success("Password changed Successfully", 5);
                    return RedirectToAction("Signin", "Login");
                }
                else
                {
                    _notyf.Success("Error..! Try Again..", 5);
                    TempData["Oldpassword"] = "Old Password is incorrect.. Re-enter the correct Password!";
                    return RedirectToAction("Appliedjobdetail", "jobseeker", new { ActiveTab = "tab-my-profile", isRedirect = "true" });
                }

               
            }
        }
       
        
        public IActionResult Signup()
        {
            
            List<State_tbl> state = new List<State_tbl>();
            string apiUrl = apiBaseUrl + "/State/getAllActiveState";
            string apiUrl1 = apiBaseUrl + "/County/getAllActiveCounty";
            string apiUrl2 = apiBaseUrl + "/District/getAllActiveDistrict";

            HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync(apiUrl).Result;
            if (response.IsSuccessStatusCode)
            {
                state = JsonConvert.DeserializeObject<List<State_tbl>>(response.Content.ReadAsStringAsync().Result);
            }
            ViewBag.message = state;


            List<County_tbl> county = new List<County_tbl>();
          
         
            HttpResponseMessage response1 = client.GetAsync(apiUrl1).Result;
            if (response1.IsSuccessStatusCode)
            {
                county = JsonConvert.DeserializeObject<List<County_tbl>>(response1.Content.ReadAsStringAsync().Result);
            }
            ViewBag.message1 = county;

            List<District_tbl> customers = new List<District_tbl>();
            HttpResponseMessage response2 = client.GetAsync(apiUrl2).Result;
            if (response2.IsSuccessStatusCode)
            {
                customers = JsonConvert.DeserializeObject<List<District_tbl>>(response2.Content.ReadAsStringAsync().Result);
            }
            ViewBag.dist_name = customers;


            return View();
        }
        [HttpPost]
        public async Task<ActionResult> Signup(seek_tbl seek_ab, List<IFormFile> s_resume, List<IFormFile> s_pic)
        {
            string wwwPath = this.Environment.WebRootPath;
            string contentPath = this.Environment.ContentRootPath;
            string path = Path.Combine(this.Environment.WebRootPath, "Jobseeker_uploads");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            List<string> uploadedFiles = new List<string>();
            foreach (IFormFile postedFile in s_resume)
            {
                string fileName = Path.GetFileName(postedFile.FileName);
                using (FileStream stream = new FileStream(Path.Combine(path, fileName), FileMode.Create))
                {
                    postedFile.CopyTo(stream);
                    uploadedFiles.Add(fileName);
                    // ViewBag.Message += string.Format("<b>{0}</b> uploaded.<br />", fileName);
                }
                seek_ab.sresume_path = path;
                seek_ab.s_resume = fileName;
            }
            foreach (IFormFile postedFile in s_pic)
            {
                string fileName = Path.GetFileName(postedFile.FileName);
                using (FileStream stream = new FileStream(Path.Combine(path, fileName), FileMode.Create))
                {
                    postedFile.CopyTo(stream);
                    uploadedFiles.Add(fileName);
                    // ViewBag.Message += string.Format("<b>{0}</b> uploaded.<br />", fileName);
                }
                seek_ab.spic_path = path;
                seek_ab.s_pic = fileName;
            }
            string seek_fname = seek_ab.fname;
            string last_name = seek_ab.lname;
            string email = seek_ab.email_id;
            string password = seek_ab.e_password;
            string phone = seek_ab.phone;
            //Employer.e_password = emp_password;
            //Employer.id = id;
            HttpClient hc = new HttpClient();
            seek_ab.status = "Active";
            seek_ab.is_active = true;
            ViewBag.seek_fname = seek_fname;
            /* hc.BaseAddress = new Uri(apiBaseUrl + "/Seek/Create");
             var locationconsume = hc.PostAsJsonAsync<seek_tbl>("Create", seek_ab);
             locationconsume.Wait();
             var readdata = locationconsume.Result;*/

            var httpClient = new HttpClient();
            var response = await httpClient.PostAsJsonAsync(apiBaseUrl + "/Seek/Create", seek_ab);
            jb_responce returnValue = await response.Content.ReadAsAsync<jb_responce>();

            if (response.IsSuccessStatusCode)
           //     if (readdata.IsSuccessStatusCode)
            {
                if (returnValue.status == true)
                {
                    using (MailMessage mm = new MailMessage("childplus11@gmail.com", "childplus11@gmail.com"))
                    {
                        mm.Subject = "Account Created";
                        mm.Body = "Hi " + seek_fname + " , Your account is created successfully...! Your Username :' " + email + "' and Password :' " + password + "'";
                        //if (model.Attachment.Length > 0)
                        //{
                        //    string fileName = Path.GetFileName(model.Attachment.FileName);
                        //    mm.Attachments.Add(new Attachment(model.Attachment.OpenReadStream(), fileName));
                        //}
                        mm.IsBodyHtml = false;
                        using (SmtpClient smtp = new SmtpClient())
                        {
                            smtp.Host = "smtp.gmail.com";
                            smtp.EnableSsl = true;
                            NetworkCredential NetworkCred = new NetworkCredential("childplus11@gmail.com", "mgtxdfwmpbqzrtil");
                            smtp.UseDefaultCredentials = true;
                            smtp.Credentials = NetworkCred;
                            smtp.Port = 587;
                            smtp.Send(mm);
                            ViewBag.Message = "Email sent.";
                        }
                    }
                    _notyf.Success(returnValue.message, 9);
                    return RedirectToAction("Signin", "Login");
                }
                else
                {
                    _notyf.Error(returnValue.message, 9);
                    // return RedirectToAction("Signup", "Jobseeker");
                    return View(seek_ab);
                }
            }
           
            else
            {
                _notyf.Error("Error..! Try Again..", 5);
                return RedirectToAction("Signup", "Jobseeker");
            }
            //return View(seek_ab);


        }
        public async Task<ActionResult> Profile(int id, string ActiveTab, string isRedirect)
        {
            ViewBag.ActiveTab = ActiveTab;
            ViewBag.reDirect = isRedirect;
            if (id == 0)
            {
                 id = Convert.ToInt32(HttpContext.Session.GetString("jobseeker_loginid"));
            }

            HttpClient client = new HttpClient();

            seek_tbl seek = null;

            string apiUrl = apiBaseUrl + "/Seek/get_by_id_profile/";
            string apiUrl1 = apiBaseUrl + "/Seek/getall_by_id_education/";
            string apiUrl2 = apiBaseUrl + "/Seek/getall_by_id_experience/";
            HttpResponseMessage readdata = await client.GetAsync(apiUrl + id);

            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<seek_tbl>();
                displayresults.Wait();
                seek = displayresults.Result;

            }
            ViewBag.name = seek.fname + ' ' + seek.lname;
            ViewBag.regdate = seek.registration_date.ToString("MM/dd/yyyy");
            ViewBag.email = seek.email_id;
            ViewBag.city = seek.dist_name;
            ViewBag.county = seek.county_name;
            ViewBag.Address = seek.seekaddress;
            ViewBag.gender = seek.gender;
            ViewBag.primaryLang = seek.primary_lng;
            ViewBag.phone = seek.contact1 + ' ' + seek.primary_extension;
            ViewBag.phone1 = seek.contact2 + ' ' + seek.secondary_extension;
            ViewBag.aboutme = seek.about_me;
            ViewBag.skill = seek.list_skill;
            // ViewBag.profilepic = "~/Jobseeker_uploads/"+seek.s_pic;
            ViewBag.resume = seek.s_resume;
            ViewBag.profilepic = seek.s_pic;
            ViewBag.highest_degree=seek.highest_degree;
            ViewBag.highest_workHistory = seek.highest_workHistory;
        List<seekeducation_tbl> seekeducation = new List<seekeducation_tbl>();
            HttpResponseMessage response1 = client.GetAsync(apiUrl1 + id).Result;
            if (response1.IsSuccessStatusCode)
            {
                seekeducation = JsonConvert.DeserializeObject<List<seekeducation_tbl>>(response1.Content.ReadAsStringAsync().Result);
            }
            ViewBag.seekeducation = seekeducation;
            ViewBag.seekeducationCount = seekeducation.Count();

            List<seekexperience_tbl> seekexperience = new List<seekexperience_tbl>();
            HttpResponseMessage response2 = client.GetAsync(apiUrl2 + id).Result;
            if (response2.IsSuccessStatusCode)
            {
                seekexperience = JsonConvert.DeserializeObject<List<seekexperience_tbl>>(response2.Content.ReadAsStringAsync().Result);
            }
            ViewBag.seekexperience = seekexperience;
            ViewBag.seekexperienceCount = seekexperience.Count();
            return View(seek);
        }
        public async Task<ActionResult> Profile_Employer(int id)
        {
            if (id == 0)
            {
                id = Convert.ToInt32(HttpContext.Session.GetString("jobseeker_loginid"));
            }

            HttpClient client = new HttpClient();

            seek_tbl seek = null;

            string apiUrl = apiBaseUrl + "/Seek/get_by_id_profile/";
            string apiUrl1 = apiBaseUrl + "/Seek/getall_by_id_education/";
            string apiUrl2 = apiBaseUrl + "/Seek/getall_by_id_experience/";
            HttpResponseMessage readdata = await client.GetAsync(apiUrl + id);

            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<seek_tbl>();
                displayresults.Wait();
                seek = displayresults.Result;

            }
            ViewBag.name = seek.fname + ' ' + seek.lname;
            ViewBag.regdate = seek.registration_date.ToString("MM/dd/yyyy");
            ViewBag.email = seek.email_id;
            ViewBag.city = seek.dist_name;
            ViewBag.county = seek.county_name;
            ViewBag.Address = seek.seekaddress;
            ViewBag.gender = seek.gender;
            ViewBag.primaryLang = seek.primary_lng;
            ViewBag.secondaryLang = seek.secondary_lng;
            ViewBag.phone = seek.contact1 + ' ' + seek.primary_extension;
            ViewBag.phone1 = seek.contact2 + ' ' + seek.secondary_extension;
            ViewBag.aboutme = seek.about_me;
            ViewBag.skill = seek.list_skill;
            // ViewBag.profilepic = "~/Jobseeker_uploads/"+seek.s_pic;
            ViewBag.resume = seek.s_resume;
            ViewBag.profilepic = seek.s_pic;
            ViewBag.highest_degree = seek.highest_degree;
            ViewBag.highest_workHistory = seek.highest_workHistory;
            List<seekeducation_tbl> seekeducation = new List<seekeducation_tbl>();
            HttpResponseMessage response1 = client.GetAsync(apiUrl1 + id).Result;
            if (response1.IsSuccessStatusCode)
            {
                seekeducation = JsonConvert.DeserializeObject<List<seekeducation_tbl>>(response1.Content.ReadAsStringAsync().Result);
            }
            ViewBag.seekeducation = seekeducation;
            ViewBag.seekeducationCount = seekeducation.Count();

            List<seekexperience_tbl> seekexperience = new List<seekexperience_tbl>();
            HttpResponseMessage response2 = client.GetAsync(apiUrl2 + id).Result;
            if (response2.IsSuccessStatusCode)
            {
                seekexperience = JsonConvert.DeserializeObject<List<seekexperience_tbl>>(response2.Content.ReadAsStringAsync().Result);
            }
            ViewBag.seekexperience = seekexperience;
            ViewBag.seekexperienceCount = seekexperience.Count();

            return View(seek);
        }


        public IActionResult Educationdetails()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Educationdetails(seekeducation_tbl seekeducation_ab)
        {
            int jobseeker_id = Convert.ToInt32(HttpContext.Session.GetString("jobseeker_loginid"));
            HttpClient hc = new HttpClient();
            seekeducation_ab.seek_id = jobseeker_id;
            //seekeducation_ab.seek_id = 2;
            hc.BaseAddress = new Uri(apiBaseUrl + "/Seek/");
            var locationconsume = hc.PostAsJsonAsync<seekeducation_tbl>("Createeducation", seekeducation_ab);
            locationconsume.Wait();
            var readdata = locationconsume.Result;
            if (readdata.IsSuccessStatusCode)
            {
                _notyf.Success("Success", 5);
                return RedirectToAction("Profile", "Jobseeker", new { ActiveTab = "tab-skills", isRedirect = "true" });


            }
            else
            {
                _notyf.Error("Error..! Try Again..", 5);
                return RedirectToAction("Educationdetails", "Jobseeker");
            }


        }
        public IActionResult Experiencedetails()
        {
            return View();
        }



        [HttpPost]
        public async Task<ActionResult> Experiencedetails(seekexperience_tbl seekxperience_ab)
        {
            int jobseeker_id = Convert.ToInt32(HttpContext.Session.GetString("jobseeker_loginid"));
            HttpClient hc = new HttpClient();
            seekxperience_ab.seek_id = jobseeker_id;
            // seekxperience_ab.seek_id = 2;
            hc.BaseAddress = new Uri(apiBaseUrl + "/Seek/");
            var locationconsume = hc.PostAsJsonAsync<seekexperience_tbl>("Createexperience", seekxperience_ab);
            locationconsume.Wait();
            var readdata = locationconsume.Result;
            if (readdata.IsSuccessStatusCode)
            {
                _notyf.Success("Success", 5);
                return RedirectToAction("Profile", "Jobseeker", new { ActiveTab = "tab-work-experience", isRedirect = "true" });


            }
            else
            {
                _notyf.Error("Error..! Try Again..", 5);
                return RedirectToAction("Experiencedetails", "Jobseeker");
            }


        }
        public async Task<ActionResult> EditProfile()
        {
            List<SelectListItem> ObjListdisability = new List<SelectListItem>()
            {
                new SelectListItem { Text = "Select Disability", Value = "0" },
                new SelectListItem { Text = "No", Value = "No" },
                new SelectListItem { Text = "Yes", Value = "Yes" },

            };
            List<SelectListItem> ObjListgender = new List<SelectListItem>()
            {
                new SelectListItem { Text = "Select Gender", Value = "0" },
                new SelectListItem { Text = "Male", Value = "Male" },
                new SelectListItem { Text = "Female", Value = "Female" },
                new SelectListItem { Text = "Nonbinary", Value = "Nonbinary" },
                new SelectListItem { Text = "Transgender", Value = "Transgender" },
                new SelectListItem { Text = "Unknown* or decline to state", Value = "Unknown* or decline to state" },
            };
            List<SelectListItem> ObjListracial = new List<SelectListItem>()
            {
                new SelectListItem { Text = "Select Race", Value = "0" },
                new SelectListItem { Text = "Asian", Value = "Asian" },
                new SelectListItem { Text = "Black / African American", Value = "Black / African American" },
                new SelectListItem { Text = "White / Caucasian", Value = "White / Caucasian" },
                new SelectListItem { Text = "American Indian / Alaska Native", Value = "American Indian / Alaska Native" },
                new SelectListItem { Text = "Multiracial / Multi Ethnic", Value = "Multiracial / Multi Ethnic" },

                new SelectListItem { Text = "Native Hawaiian / Native Pasific Islander", Value = "Native Hawaiian / Native Pasific Islander" },
                new SelectListItem { Text = "Some Other Race", Value = "Some Other Race" },
                new SelectListItem { Text = "Unknown or Decline to State", Value = "Unknown or Decline to State" },

            };

            List<SelectListItem> ObjPhoneType = new List<SelectListItem>()
            {
                new SelectListItem { Text = "Select Phone Type", Value = "0" },
                new SelectListItem { Text = "Cell", Value = "Cell" },
                new SelectListItem { Text = "Home", Value = "Home" },
                new SelectListItem { Text = "Work", Value = "Work" },
                new SelectListItem { Text = "Other", Value = "Other" },
                
            };
            List<SelectListItem> ObjListplng = new List<SelectListItem>()
            {
                new SelectListItem { Text = "Select Primary Language", Value = "0" },
                new SelectListItem { Text = "English", Value = "English" },
                new SelectListItem { Text = "Dutch", Value = "Dutch" },
                new SelectListItem { Text = "Korean", Value = "Korean" },
                new SelectListItem { Text = "Spanish", Value = "Spanish" },
                new SelectListItem { Text = "Russian", Value = "Russian" },

                new SelectListItem { Text = "Turkish", Value = "Turkish" },
                new SelectListItem { Text = "Middle Eastern", Value = "Middle Eastern" },
                new SelectListItem { Text = "Chinese", Value = "Chinese" },
                new SelectListItem { Text = "Indian", Value = "Indian" },
                new SelectListItem { Text = "Japanese", Value = "Japanese" },
                new SelectListItem { Text = "Other Asian", Value = "Other Asian" },
                new SelectListItem { Text = "African", Value = "African" },
            };
            List<SelectListItem> ObjListslng = new List<SelectListItem>()
            {
                new SelectListItem { Text = "Select Secondary Language", Value = "0" },
                new SelectListItem { Text = "English", Value = "English" },
                new SelectListItem { Text = "Dutch", Value = "Dutch" },
                new SelectListItem { Text = "Korean", Value = "Korean" },
                new SelectListItem { Text = "Spanish", Value = "Spanish" },
                new SelectListItem { Text = "Russian", Value = "Russian" },

                new SelectListItem { Text = "Turkish", Value = "Turkish" },
                new SelectListItem { Text = "Middle Eastern", Value = "Middle Eastern" },
                new SelectListItem { Text = "Chinese", Value = "Chinese" },
                new SelectListItem { Text = "Indian", Value = "Indian" },
                new SelectListItem { Text = "Japanese", Value = "Japanese" },
                new SelectListItem { Text = "Other Asian", Value = "Other Asian" },
                new SelectListItem { Text = "African", Value = "African" },
            };

            List<SelectListItem> ObjHighestDegree = new List<SelectListItem>()
            {
                new SelectListItem { Text = "Select Highest degree", Value = "0" },
                new SelectListItem { Text = "High School", Value = "High School" },
                new SelectListItem { Text = "CDA Certification", Value = "CDA Certification" },
                new SelectListItem { Text = "Associate Degree", Value = "Associate Degree" },
                new SelectListItem { Text = "Bachelor's Degree", Value = "Bachelor's Degree" },
                new SelectListItem { Text = "Master's Degree", Value = "Master's Degree" },
                new SelectListItem { Text = "Doctorate Degree", Value = "Doctorate Degree" }

            };


            ViewBag.gender = ObjListgender;
            ViewBag.racial = ObjListracial;
            ViewBag.plng = ObjListplng;
            ViewBag.slng = ObjListslng;
            ViewBag.disability = ObjListdisability;
            ViewBag.PhoneType = ObjPhoneType;
            ViewBag.highest_degree = ObjHighestDegree;
            HttpClient client = new HttpClient();
            List<County_tbl> county = new List<County_tbl>();
            List<State_tbl> state = new List<State_tbl>();
           
            string apiUrl = apiBaseUrl + "/County/getAll";
            string apiUrl1 = apiBaseUrl + "/State/getAll/";
            string apiUrl2 = apiBaseUrl + "/Seek/get_by_id_edit/";
            string apiUrl3 = apiBaseUrl + "/District/getAll";

            HttpResponseMessage response = client.GetAsync(apiUrl).Result;
            if (response.IsSuccessStatusCode)
            {
                county = JsonConvert.DeserializeObject<List<County_tbl>>(response.Content.ReadAsStringAsync().Result);
            }
            ViewBag.county = county;

            HttpResponseMessage response1 = client.GetAsync(apiUrl1).Result;
            if (response1.IsSuccessStatusCode)
            {
                state = JsonConvert.DeserializeObject<List<State_tbl>>(response1.Content.ReadAsStringAsync().Result);
            }
            ViewBag.state = state;

            List<District_tbl> customers = new List<District_tbl>();
            HttpResponseMessage response2 = client.GetAsync(apiUrl3).Result;
            if (response2.IsSuccessStatusCode)
            {
                customers = JsonConvert.DeserializeObject<List<District_tbl>>(response2.Content.ReadAsStringAsync().Result);
            }
            ViewBag.dist = customers;
            seek_tbl seek = new seek_tbl();
            seek.status = "Active";
            int id = Convert.ToInt32(HttpContext.Session.GetString("jobseeker_loginid"));
            HttpResponseMessage readdata = await client.GetAsync(apiUrl2 + id);

            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<seek_tbl>();
                displayresults.Wait();
                seek = displayresults.Result;

            }
            ViewBag.yearFbirth = seek.year_of_birth;
            ViewBag.profilepic = seek.s_pic;
            ViewBag.resume = seek.s_resume;
            //ViewBag.Year_of_Birth = seek.year_of_birth;

            return View(seek);



        }

        public async Task<ActionResult> Editeducation(int id)
        {
            List<SelectListItem> ObjListlgrade = new List<SelectListItem>()
            {

                new SelectListItem { Text = "Pre-K", Value = "Pre-K" },
                new SelectListItem { Text = "K", Value = "K" },
                new SelectListItem { Text = "1", Value = "1" },
                new SelectListItem { Text = "2", Value = "2" },
                new SelectListItem { Text = "3", Value = "3" },

                new SelectListItem { Text = "4", Value = "4" },
                new SelectListItem { Text = "5", Value = "5" },
                new SelectListItem { Text = "6", Value = "6" },
                new SelectListItem { Text = "7", Value = "7" },
                new SelectListItem { Text = "8", Value = "8" },
                new SelectListItem { Text = "9", Value = "9" },
                new SelectListItem { Text = "10", Value = "10" },
                new SelectListItem { Text = "11", Value = "11" },
                new SelectListItem { Text = "12", Value = "12" },
                new SelectListItem { Text = "HS Diploma", Value = "HS Diploma" },
            };
            List<SelectListItem> ObjListdegree = new List<SelectListItem>()
            {

                new SelectListItem { Text = "High School", Value = "High School" },
                new SelectListItem { Text = "CDA Certification", Value = "CDA Certification" },
                new SelectListItem { Text = "Associate Degree", Value = "Associate Degree" },
                new SelectListItem { Text = "Bachelor's Degree", Value = "Bachelor's Degree" },
                new SelectListItem { Text = "Master's Degree", Value = "Master's Degree" },

                new SelectListItem { Text = "Doctorate Degree", Value = "Doctorate Degree" },

            };
            ViewBag.lastgrade = ObjListlgrade;
            ViewBag.degreereceived = ObjListdegree;
            HttpClient client = new HttpClient();

            seekeducation_tbl seek = null;


            string apiUrl2 = apiBaseUrl + "/Seek/get_by_id_education/";


            HttpResponseMessage readdata = await client.GetAsync(apiUrl2 + id);

            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<seekeducation_tbl>();
                displayresults.Wait();
                seek = displayresults.Result;

            }

            return View(seek);



        }        
        public async Task<ActionResult> Removeeducation([FromRoute] int id)
        {

            string apiUrl = apiBaseUrl + "/Seek/Deleteeducation/";

            HttpClient client = new HttpClient();

            HttpResponseMessage readdata = await client.GetAsync(apiUrl + id);
            //  var result = responseTask.Result;
            if (readdata.IsSuccessStatusCode)
            {
                _notyf.Success("Removed Successfully..!", 5);
            }
            else
            {
                _notyf.Error("Error..!", 5);
            }


            return RedirectToAction("Profile", "Jobseeker", new { ActiveTab = "tab-skills", isRedirect = "true" });

        }
        public async Task<ActionResult> RemoveSavedJob(int id)
        {
            //int seek_id = Convert.ToInt32(HttpContext.Session.GetString("jobseeker_loginid"));
            string apiUrl = apiBaseUrl + "/Jobregister/Removed_job/";
            //int job_reg_id = id;
            HttpClient client = new HttpClient();

            //HttpResponseMessage readdata = await client.GetAsync(apiUrl + job_reg_id + "/" + seek_id);
            HttpResponseMessage readdata = await client.GetAsync(apiUrl + id);
            //  var result = responseTask.Result;
            if (readdata.IsSuccessStatusCode)
            {
                _notyf.Success("Saved Job Removed Successfully..!", 5);
                return RedirectToAction("Appliedjobdetail", "Jobseeker", new { ActiveTab = "tab-saved-jobs", isRedirect = "true" });
            }
            else
            {
                _notyf.Error("Try again.Error..!", 5);
                return RedirectToAction("Appliedjobdetail", "Jobseeker", new { ActiveTab = "tab-saved-jobs", isRedirect = "true" });
            }

       }
        public async Task<ActionResult> Removeexperience(int id)
        {

            string apiUrl = apiBaseUrl + "/Seek/Deleteexperience/";

            HttpClient client = new HttpClient();

            HttpResponseMessage readdata = await client.GetAsync(apiUrl + id);
            //  var result = responseTask.Result;
            if (readdata.IsSuccessStatusCode)
            {
                _notyf.Success("Removed Successfully..!", 5);
            }
            else
            {
                _notyf.Error("Error..!", 5);
            }

            return RedirectToAction("Profile", "Jobseeker", new { ActiveTab = "tab-work-experience", isRedirect = "true" });

        }

        public async Task<ActionResult> Editexperience(int id)
        {
            List<SelectListItem> ObjListetype = new List<SelectListItem>()
            {

                new SelectListItem { Text = "Full Time", Value = "Full Time" },
                new SelectListItem { Text = "Part Time", Value = "Part Time" },
                new SelectListItem { Text = "InOffice", Value = "InOffice" },
                new SelectListItem { Text = "Remote", Value = "Remote" },
                new SelectListItem { Text = "Hybrid", Value = "Hybrid" },

                new SelectListItem { Text = "Others", Value = "Others" },

            };
            ViewBag.employmenttype = ObjListetype;
            HttpClient client = new HttpClient();

            seekexperience_tbl seek = null;

            string apiUrl2 = apiBaseUrl + "/Seek/get_by_id_experience/";


            HttpResponseMessage readdata = await client.GetAsync(apiUrl2 + id);

            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<seekexperience_tbl>();
                displayresults.Wait();
                seek = displayresults.Result;
           
              
            }

            return View(seek);



        }
        public async Task<ActionResult> Updateprofile(seek_tbl seek_ab, List<IFormFile> s_resume1, List<IFormFile> s_pic1)
        {
            string wwwPath = this.Environment.WebRootPath;
            string contentPath = this.Environment.ContentRootPath;
            string path = Path.Combine(this.Environment.WebRootPath, "Jobseeker_uploads");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            List<string> uploadedFiles = new List<string>();
            foreach (IFormFile postedFile in s_resume1)
            {
                string fileName = Path.GetFileName(postedFile.FileName);
                using (FileStream stream = new FileStream(Path.Combine(path, fileName), FileMode.Create))
                {
                    postedFile.CopyTo(stream);
                    uploadedFiles.Add(fileName);
                    // ViewBag.Message += string.Format("<b>{0}</b> uploaded.<br />", fileName);
                }
                seek_ab.sresume_path = path;
                seek_ab.s_resume = fileName;
            }
            foreach (IFormFile postedFile in s_pic1)
            {
                string fileName = Path.GetFileName(postedFile.FileName);
                using (FileStream stream = new FileStream(Path.Combine(path, fileName), FileMode.Create))
                {
                    postedFile.CopyTo(stream);
                    uploadedFiles.Add(fileName);
                    // ViewBag.Message += string.Format("<b>{0}</b> uploaded.<br />", fileName);
                }
                seek_ab.spic_path = path;
                seek_ab.s_pic = fileName;
            }
            HttpClient hc = new HttpClient();
            seek_ab.status = "Active";
            hc.BaseAddress = new Uri(apiBaseUrl + "/Seek/");
            var locationconsume = hc.PutAsJsonAsync<seek_tbl>("Updateprofile", seek_ab);
            locationconsume.Wait();
            var readdata = locationconsume.Result;

            if (readdata.IsSuccessStatusCode)
            {
                _notyf.Information("Updated Successfully", 5);
                return RedirectToAction("Profile", "Jobseeker");

            }
            else
            {
                _notyf.Error("Error.! Try Again..", 5);
                return RedirectToAction("EditProfile", "Jobseeker");
            }
            //return View(seek_ab);


        }
        public async Task<ActionResult> UpdateResume(seek_tbl seek_ab, List<IFormFile> s_resume1)
        {
             seek_ab.id = Convert.ToInt32(HttpContext.Session.GetString("jobseeker_loginid"));
            string wwwPath = this.Environment.WebRootPath;
            string contentPath = this.Environment.ContentRootPath;
            string path = Path.Combine(this.Environment.WebRootPath, "Jobseeker_uploads");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            List<string> uploadedFiles = new List<string>();
            foreach (IFormFile postedFile in s_resume1)
            {
                string fileName = Path.GetFileName(postedFile.FileName);
                using (FileStream stream = new FileStream(Path.Combine(path, fileName), FileMode.Create))
                {
                    postedFile.CopyTo(stream);
                    uploadedFiles.Add(fileName);
                    // ViewBag.Message += string.Format("<b>{0}</b> uploaded.<br />", fileName);
                }
                seek_ab.sresume_path = path;
                seek_ab.s_resume = fileName;
            }
            
            HttpClient hc = new HttpClient();
            seek_ab.status = "Active";
            hc.BaseAddress = new Uri(apiBaseUrl + "/Seek/");
            var locationconsume = hc.PutAsJsonAsync<seek_tbl>("UpdateResume", seek_ab);
            locationconsume.Wait();
            var readdata = locationconsume.Result;

            if (readdata.IsSuccessStatusCode)
            {
                _notyf.Information("Resume Added Successfully..!", 9);
                return RedirectToAction("Profile", "Jobseeker");

            }
            else
            {
                _notyf.Error("Error.! Try Again..", 5);
                return RedirectToAction("EditProfile", "Jobseeker");
            }
            //return View(seek_ab);


        }
        public async Task<ActionResult> seeker_status(string activeStat, int passedId, seek_tbl seek_ab)
        {
            //Employer.id = Convert.ToInt32(HttpContext.Session.GetString("employer_loginid"));

            if (seek_ab.activeStat == "Active")
            {
                seek_ab.status = "InActive";

            }
            else
            {
                seek_ab.status = "Active";

            }
            seek_ab.id = seek_ab.passedId;
            HttpClient hc = new HttpClient();

            hc.BaseAddress = new Uri(apiBaseUrl + "/Seek/");
            var locationconsume = hc.PutAsJsonAsync<seek_tbl>("Update_status", seek_ab);
            locationconsume.Wait();
            var readdata = locationconsume.Result;

            if (readdata.IsSuccessStatusCode)
            {

                _notyf.Information("Updated Successfully", 5);
                return RedirectToAction("Admin_seek_List", "Jobseeker");

            }
            else
            {
                _notyf.Error("Error..! Try Again..", 5);
                return RedirectToAction("Admin_seek_List", "Jobseeker");
            }
            //return View(Employer);


        }

        public async Task<ActionResult> Updateeducation(seekeducation_tbl seekeducation_ab)
        {

            HttpClient hc = new HttpClient();

            hc.BaseAddress = new Uri(apiBaseUrl + "/Seek/");
            var locationconsume = hc.PutAsJsonAsync<seekeducation_tbl>("Updateeducation", seekeducation_ab);
            locationconsume.Wait();
            var readdata = locationconsume.Result;

            if (readdata.IsSuccessStatusCode)
            {
                _notyf.Information("Updated Successfully", 5);
                return RedirectToAction("Profile", "jobseeker", new { ActiveTab = "tab-skills", isRedirect = "true" });
            }
            else
            {
                _notyf.Success("Error.! Try Again..", 5);
                return RedirectToAction("Profile", "jobseeker", new { ActiveTab = "tab-skills", isRedirect = "true" });
            }
            //return View(seekeducation_ab);


        }

        public async Task<ActionResult> Updateexperience(seekexperience_tbl seekexperience_ab)
        {

            HttpClient hc = new HttpClient();

            hc.BaseAddress = new Uri(apiBaseUrl + "/Seek/");
            var locationconsume = hc.PutAsJsonAsync<seekexperience_tbl>("Updateexperience", seekexperience_ab);
            locationconsume.Wait();
            var readdata = locationconsume.Result;

            if (readdata.IsSuccessStatusCode)
            {
                _notyf.Information("Updated Successfully", 5);
                return RedirectToAction("Profile", "jobseeker", new { ActiveTab = "tab-work-experience", isRedirect = "true" });
            }
            else
            {
                _notyf.Success("Error.! Try Again..", 5);
                return RedirectToAction("Profile", "jobseeker", new { ActiveTab = "tab-work-experience", isRedirect = "true" });
            }
            //return View(seekexperience_ab);


        }
        public async Task<ActionResult> Home(string jobtypetext, string categorytext, string exptext, string searchval)
        {
            List<job_reg_tbl> jobs = new List<job_reg_tbl>();
            List<Org_cat> orgcat = new List<Org_cat>();
            string apiUrl1 = apiBaseUrl + "/Org_cat/getAll";
            // string apiUrl = "http://localhost:62332/Jobregister/getAll_details";
            string apiUrl = apiBaseUrl + "/Jobregister/get_by_iddemo/";
            HttpClient client = new HttpClient();

            HttpResponseMessage response1 = client.GetAsync(apiUrl1).Result;
            if (response1.IsSuccessStatusCode)
            {
                orgcat = JsonConvert.DeserializeObject<List<Org_cat>>(response1.Content.ReadAsStringAsync().Result);
            }
            // ViewData["vm2.Select"] = new SelectList(vm2.Select, "Value", "Value", orgcat);
            ViewBag.message = orgcat;
            List<SelectListItem> list = new List<SelectListItem>();
            foreach (var item in orgcat)
            {
                SelectListItem temp = new SelectListItem();
                temp.Text = item.category_name.ToString();
                temp.Value = item.category_name.ToString();
                list.Add(temp);

            }
            ViewBag.message = list;
            if (string.IsNullOrEmpty(jobtypetext))
            {
                jobtypetext = "all";
            }
            if (string.IsNullOrEmpty(categorytext))
            {
                categorytext = "all";
            }
            if (string.IsNullOrEmpty(exptext))
            {
                exptext = "all";
            }
            if (string.IsNullOrEmpty(searchval))
            {
                searchval = "all";
            }

            HttpResponseMessage response = client.GetAsync(apiUrl + jobtypetext + "/" + categorytext + "/" + exptext + "/" + searchval).Result;
            if (response.IsSuccessStatusCode)
            {
                jobs = JsonConvert.DeserializeObject<List<job_reg_tbl>>(response.Content.ReadAsStringAsync().Result);
            }
          
            ViewBag.viewjobs = jobs;
            ViewBag.countjobs = jobs.Count;
            if (jobtypetext == "all")
            {
                ViewData["vm.Select"] = new SelectList(vm.Select, "Value", "Value");
            }
            else
            {
                ViewData["vm.Select"] = new SelectList(vm.Select, "Value", "Value", jobtypetext);

            }

            if (exptext == "all")
            {
                ViewData["vm1.Select"] = new SelectList(vm1.Select, "Value", "Value");
            }
            else
            {
                ViewData["vm1.Select"] = new SelectList(vm1.Select, "Value", "Value", exptext);

            }
            if (categorytext == "all")
            {
                ViewData["vm2.Select"] = new SelectList(vm2.Select, list);
            }
            else
            {


            }


            return View(jobs);

        }
        public async Task<ActionResult> Jobdetails(int id)
        {
            HttpClient client = new HttpClient();
            //string apiUrl2 = "http://localhost:62332/Jobregister/get_by_id/";
            string apiUrl2 = apiBaseUrl + "/Jobregister/get_by_id_edit/";
            // emp_tbl emp = null;
            job_reg_tbl jobs = null;
            HttpResponseMessage readdata = await client.GetAsync(apiUrl2 + id);

            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<job_reg_tbl>();
                displayresults.Wait();
                jobs = displayresults.Result;

            }
            if (jobs != null)
            {
                ViewBag.id = jobs.id;
            }
            jobs.posting_date.ToString("yyyy-MM-dd");
            ViewBag.posting_date = jobs.posting_date.ToString("MM/dd/yyyy");
            ViewBag.end_date = jobs.end_date.ToString("MM/dd/yyyy");
            int job_reg_id= id;
            int seek_id = 0;
            string job_title = jobs.job_title;
            string apiUrl3 = apiBaseUrl + "/Jobregister/get_all_similar_jobs_by_seeker/";
            List<Job_register_ab> similarJob = new List<Job_register_ab>();
            HttpResponseMessage response3 = client.GetAsync(apiUrl3 + seek_id+"/"+ job_reg_id+"/"+ job_title).Result;
            if (response3.IsSuccessStatusCode)
            {
                similarJob = JsonConvert.DeserializeObject<List<Job_register_ab>>(response3.Content.ReadAsStringAsync().Result);
            }
            ViewBag.similarJob = similarJob;
            ViewBag.savedJobCount = similarJob.Count();
            //HttpContext.Session.SetString("JobApplyId", jobs.id.ToString());
            return View(jobs);
            //return View();
        }
        public async Task<ActionResult> JobdetailsView(int id)
        {
            
            if (id == 0)
            {
                if(Convert.ToInt32(HttpContext.Session.GetString("JobApplyId")) == 0){
                    id = Convert.ToInt32(HttpContext.Session.GetString("JobApplyId1").ToString());
                }
                else
                {
                    id = Convert.ToInt32(HttpContext.Session.GetString("JobApplyId").ToString());
                }
                 
                
            }
            
            HttpClient client = new HttpClient();
            string apiUrl = apiBaseUrl + "/Jobregister/get_job_is_saved/";
            string apiUrl2 = apiBaseUrl + "/Jobregister/get_by_id_edit/";

            int seeker_id = Convert.ToInt32(HttpContext.Session.GetString("jobseeker_loginid"));
            int job_reg_id = id;


           Boolean issaved =false;
            HttpResponseMessage readdata1 = await client.GetAsync(apiUrl + seeker_id + "/"+ job_reg_id); 

            if (readdata1.IsSuccessStatusCode)
            {
                var displayresults1 = readdata1.Content.ReadAsAsync<Boolean>();
                displayresults1.Wait();
                issaved = displayresults1.Result;

            }
            ViewBag.issaved = issaved;

            job_reg_tbl jobs = null;
            HttpResponseMessage readdata = await client.GetAsync(apiUrl2 + id);

            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<job_reg_tbl>();
                displayresults.Wait();
                jobs = displayresults.Result;

            }
            if (jobs != null)
            {
                ViewBag.id = jobs.id;
            }
            jobs.posting_date.ToString("yyyy-MM-dd");
            ViewBag.posting_date = jobs.posting_date.ToString("MM/dd/yyyy");
            ViewBag.end_date = jobs.end_date.ToString("MM/dd/yyyy");

           
            int seek_id = seeker_id;
            string job_title = jobs.job_title;
            string apiUrl3 = apiBaseUrl + "/Jobregister/get_all_similar_jobs_by_seeker/";
            List<Job_register_ab> similarJob = new List<Job_register_ab>();
            HttpResponseMessage response3 = client.GetAsync(apiUrl3 + seek_id + "/" + job_reg_id + "/" + job_title).Result;
            if (response3.IsSuccessStatusCode)
            {
                similarJob = JsonConvert.DeserializeObject<List<Job_register_ab>>(response3.Content.ReadAsStringAsync().Result);
            }
            ViewBag.similarJob = similarJob;
            ViewBag.similarJobCount = similarJob.Count();



            return View(jobs);
            //return View();
        }

        public async Task<ActionResult> Applyjob(job_reg_tbl job_reg_ab)
        {
            int id = Convert.ToInt32(HttpContext.Session.GetString("jobseeker_loginid"));
            HttpClient hc = new HttpClient();
            app_job_tbl appjob_ab = new app_job_tbl();
            appjob_ab.seek_id = id;
            appjob_ab.employeer_id = job_reg_ab.employeer_id;
            appjob_ab.job_reg_id = job_reg_ab.id;
            appjob_ab.job_title = job_reg_ab.job_title;
            appjob_ab.job_skills = job_reg_ab.job_skills;
            appjob_ab.job_desc = job_reg_ab.job_desc;
            appjob_ab.posting_date = job_reg_ab.posting_date;
            appjob_ab.org_id = job_reg_ab.org_id;
            appjob_ab.org_name = job_reg_ab.org_name;
            appjob_ab.job_type = job_reg_ab.job_type;
            appjob_ab.salary_package = job_reg_ab.salary_package;
            appjob_ab.experience = job_reg_ab.experience;
            appjob_ab.org_cat_id = job_reg_ab.org_cat_id;
            appjob_ab.category_name = job_reg_ab.category_name;
            appjob_ab.date_applied = DateTime.Now;
            appjob_ab.end_date = job_reg_ab.end_date;
          /*  hc.BaseAddress = new Uri(apiBaseUrl + "/ApplyJob/");
            var locationconsume = hc.PostAsJsonAsync<app_job_tbl>("Create", appjob_ab);
            locationconsume.Wait();
            var readdata = locationconsume.Result;*/
            var response = await hc.PostAsJsonAsync(apiBaseUrl + "/ApplyJob/Create", appjob_ab);
            jb_responce returnValue = await response.Content.ReadAsAsync<jb_responce>();
            if (response.IsSuccessStatusCode)
            {
                //var content = readdata.Content;
                //string jsonContent = content.ReadAsStringAsync().Result;

                if (returnValue.status == true)
                {
                    //var content1 = await res.Content.ReadAsStringAsync();
                    _notyf.Success("Job Applied Successfully..!", 15);

                    return RedirectToAction("Appliedjobdetail", "Jobseeker");

                }
                else
                {
                    HttpContext.Session.SetString("JobApplyId1", job_reg_ab.id.ToString());
                    _notyf.Error(returnValue.message, 15);

                    return RedirectToAction("JobdetailsView", "Jobseeker");
                }
            }
            else
            {
                
                HttpContext.Session.SetString("JobApplyId1", job_reg_ab.id.ToString());
                _notyf.Error("Job Applied Already.. try another Job..!", 15);
                return RedirectToAction("JobdetailsView", "Jobseeker");
            }

        }
        public async Task<ActionResult> Appliedjobdetail(string ActiveTab, string isRedirect)
        {
            ViewBag.ActiveTab = ActiveTab;
            ViewBag.reDirect = isRedirect;
            int id = Convert.ToInt32(HttpContext.Session.GetString("jobseeker_loginid"));
            HttpClient client = new HttpClient();

            seek_tbl seek = null;
            string apiUrl = apiBaseUrl + "/Seek/get_by_id_profile/";
            HttpResponseMessage readdata = await client.GetAsync(apiUrl + id);

            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<seek_tbl>();
                displayresults.Wait();
                seek = displayresults.Result;

            }
            ViewBag.resume = seek.s_resume;
            ViewBag.name = seek.fname + ' ' + seek.lname;
            ViewBag.city = seek.dist_name;
            ViewBag.password = seek.e_password;
            ViewBag.profilepic = seek.s_pic;

            string apiUrl2 = apiBaseUrl + "/ApplyJob/getall_by_id_appliedjobdetail/";
            List<app_job_tbl> appjob = new List<app_job_tbl>();
            HttpResponseMessage response2 = client.GetAsync(apiUrl2 + id).Result;
            if (response2.IsSuccessStatusCode)
            {
                appjob = JsonConvert.DeserializeObject<List<app_job_tbl>>(response2.Content.ReadAsStringAsync().Result);
            }
            ViewBag.appjob = appjob;
            ViewBag.appJobCount = appjob.Count();

            string apiUrl3 = apiBaseUrl + "/Jobregister/get_all_saved_jobs_by_seeker/";
            List<Job_register_ab> savedJob = new List<Job_register_ab>();
            HttpResponseMessage response3 = client.GetAsync(apiUrl3 + id).Result;
            if (response3.IsSuccessStatusCode)
            {
                savedJob = JsonConvert.DeserializeObject<List<Job_register_ab>>(response3.Content.ReadAsStringAsync().Result);
            }
            ViewBag.savedJob = savedJob;
            ViewBag.savedJobCount = savedJob.Count();

            return View(appjob);
        }

        public async Task<ActionResult> Viewdetail(int id)
        {
            HttpClient client = new HttpClient();

            job_reg_tbl jobs = null;

            string apiUrl2 = apiBaseUrl + "/Jobregister/get_by_id/";


            HttpResponseMessage readdata = await client.GetAsync(apiUrl2 + id);

            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<job_reg_tbl>();
                displayresults.Wait();
                jobs = displayresults.Result;
                ViewBag.id = jobs.id;
                ViewBag.job_title = jobs.job_title;
                ViewBag.job_skills = jobs.job_skills;
                ViewBag.job_desc = jobs.job_desc;
                ViewBag.posting_date = jobs.posting_date.ToString("MM-dd-yyyy");
                ViewBag.end_date = jobs.end_date.ToString("MM-dd-yyyy");
                ViewBag.job_type = jobs.job_type;
                ViewBag.org_name = jobs.org_name;
                ViewBag.salary_pay = jobs.salary_pay;
                ViewBag.salary_package = jobs.salary_package;
                ViewBag.salary_to = jobs.salary_to;
                ViewBag.experience = jobs.experience;
                ViewBag.shedule = jobs.shedule;
                ViewBag.org_category = jobs.category_name;
                ViewBag.job_category = jobs.job_category_name;
                ViewBag.dist_name = jobs.district_name;
                ViewBag.county_name = jobs.org_county_name;
                ViewBag.state_name = jobs.org_state;
                ViewBag.org_desc = jobs.org_desc;
                ViewBag.org_address = jobs.org_address;
                ViewBag.org_phone = jobs.org_phone;
                ViewBag.org_email = jobs.org_mail;

            }

            return View(jobs);



        }
        public IActionResult Admin_seek_list()
        {
            List<seek_tbl> customers = new List<seek_tbl>();
            string apiUrl = apiBaseUrl + "/Seek/getAll";

            HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync(apiUrl).Result;
            if (response.IsSuccessStatusCode)
            {
                customers = JsonConvert.DeserializeObject<List<seek_tbl>>(response.Content.ReadAsStringAsync().Result);
            }

            return View(customers);
            //   return View();
        }
        public IActionResult Seek_List()
        {
            List<seek_tbl> customers = new List<seek_tbl>();
            string apiUrl = apiBaseUrl + "/Seek/getAll";

            HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync(apiUrl).Result;
            if (response.IsSuccessStatusCode)
            {
                customers = JsonConvert.DeserializeObject<List<seek_tbl>>(response.Content.ReadAsStringAsync().Result);
            }

            return View(customers);
            //   return View();
        }
        public IActionResult getSeekerListByOrg()
        {
            int org_id = Convert.ToInt32(HttpContext.Session.GetString("org_loginid"));

            List<seek_tbl> customers = new List<seek_tbl>();
            string apiUrl1 = apiBaseUrl + "/Org/getSeekerListByOrg/";

            HttpClient client = new HttpClient();
            
            HttpResponseMessage response = client.GetAsync(apiUrl1 + org_id).Result;
            if (response.IsSuccessStatusCode)
            {
                customers = JsonConvert.DeserializeObject<List<seek_tbl>>(response.Content.ReadAsStringAsync().Result);
            }
            ViewBag.jobs_org = customers;
            ViewBag.countjobs = customers.Count();

            return View(customers);
            //   return View();
        }
        public IActionResult getSeekerListByEmployee()
        {
            int emp_id = Convert.ToInt32(HttpContext.Session.GetString("employer_loginid"));

            List<seek_tbl> customers = new List<seek_tbl>();
            string apiUrl1 = apiBaseUrl + "/Employer/getSeekerListByEmployee/";

            HttpClient client = new HttpClient();

            HttpResponseMessage response = client.GetAsync(apiUrl1 + emp_id).Result;
            if (response.IsSuccessStatusCode)
            {
                customers = JsonConvert.DeserializeObject<List<seek_tbl>>(response.Content.ReadAsStringAsync().Result);
            }
            ViewBag.jobs_org = customers;
            ViewBag.countjobs = customers.Count();

            return View(customers);
            //   return View();
        }

        public async Task<ActionResult> Admin_seek_Profile(int id)
        {

            //int seekid=id;


            HttpClient client = new HttpClient();

            seek_tbl seek = null;

            string apiUrl = apiBaseUrl + "/Seek/get_by_id_profile/";
            string apiUrl1 = apiBaseUrl + "/Seek/getall_by_id_education/";
            string apiUrl2 = apiBaseUrl + "/Seek/getall_by_id_experience/";
            HttpResponseMessage readdata = await client.GetAsync(apiUrl + id);

            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<seek_tbl>();
                displayresults.Wait();
                seek = displayresults.Result;

            }
            //ViewBag.id = seek.id;
            ViewBag.name = seek.fname + ' ' + seek.lname;
            ViewBag.regdate = seek.registration_date.ToString("MM/dd/yyyy");
            ViewBag.email = seek.email_id;
            ViewBag.city = seek.dist_name;
            ViewBag.county = seek.county_name;
            ViewBag.Address = seek.seekaddress;
            ViewBag.gender = seek.gender;
            ViewBag.primaryLang = seek.primary_lng;
            ViewBag.phone = seek.contact1 + ' ' + seek.primary_extension;
            ViewBag.phone1 = seek.contact2 + ' ' + seek.secondary_extension;
            ViewBag.aboutme = seek.about_me;
            ViewBag.skill = seek.list_skill;
            // ViewBag.profilepic = "~/Jobseeker_uploads/"+seek.s_pic;
            ViewBag.resume = seek.s_resume;
            ViewBag.profilepic = seek.s_pic;
            ViewBag.highest_degree = seek.highest_degree;
            ViewBag.highest_workHistory = seek.highest_workHistory;
            int jobseeker_id = seek.id;
            HttpContext.Session.SetString("admin_jobseeker_loginid", seek.id.ToString());

            List<seekeducation_tbl> seekeducation = new List<seekeducation_tbl>();
            HttpResponseMessage response1 = client.GetAsync(apiUrl1 + id).Result;
            if (response1.IsSuccessStatusCode)
            {
                seekeducation = JsonConvert.DeserializeObject<List<seekeducation_tbl>>(response1.Content.ReadAsStringAsync().Result);

            }

            ViewBag.seekeducation = seekeducation;

            List<seekexperience_tbl> seekexperience = new List<seekexperience_tbl>();
            HttpResponseMessage response2 = client.GetAsync(apiUrl2 + id).Result;
            if (response2.IsSuccessStatusCode)
            {
                seekexperience = JsonConvert.DeserializeObject<List<seekexperience_tbl>>(response2.Content.ReadAsStringAsync().Result);
            }
            ViewBag.seekexperience = seekexperience;
            
            return View(seek);
        }

        public async Task<ActionResult> Admin_seek_EditProfile()
        {
            List<SelectListItem> ObjListdisability = new List<SelectListItem>()
            {
                //new SelectListItem { Text = "Select Gender", Value = "0" },
                new SelectListItem { Text = "No", Value = "No" },
                new SelectListItem { Text = "Yes", Value = "Yes" },

            };
            List<SelectListItem> ObjListgender = new List<SelectListItem>()
            {
                //new SelectListItem { Text = "Select Gender", Value = "0" },
                new SelectListItem { Text = "Male", Value = "Male" },
                new SelectListItem { Text = "Female", Value = "Female" },
                new SelectListItem { Text = "Nonbinary", Value = "Nonbinary" },
                new SelectListItem { Text = "Transgender", Value = "Transgender" },
                new SelectListItem { Text = "Unknown* or decline to state", Value = "Unknown* or decline to state" },
            };
            List<SelectListItem> ObjListracial = new List<SelectListItem>()
            {
                //new SelectListItem { Text = "Select Racial", Value = "Select Racial" },
                new SelectListItem { Text = "Asian", Value = "Asian" },
                new SelectListItem { Text = "Black / African American", Value = "Black / African American" },
                new SelectListItem { Text = "White / Caucasian", Value = "White / Caucasian" },
                new SelectListItem { Text = "American Indian / Alaska Native", Value = "American Indian / Alaska Native" },
                new SelectListItem { Text = "Multiracial / Multi Ethnic", Value = "Multiracial / Multi Ethnic" },

                new SelectListItem { Text = "Native Hawaiian / Native Pasific Islander", Value = "Native Hawaiian / Native Pasific Islander" },
                new SelectListItem { Text = "Some Other Race", Value = "Some Other Race" },
                new SelectListItem { Text = "Unknown or Decline to State", Value = "Unknown or Decline to State" },

            };
            List<SelectListItem> ObjListplng = new List<SelectListItem>()
            {
                 //new SelectListItem { Text = "Select Primary Language", Value = "Select Primary Language" },
                new SelectListItem { Text = "English", Value = "English" },
                new SelectListItem { Text = "Dutch", Value = "Dutch" },
                new SelectListItem { Text = "Korean", Value = "Korean" },
                new SelectListItem { Text = "Spanish", Value = "Spanish" },
                new SelectListItem { Text = "Russian", Value = "Russian" },

                new SelectListItem { Text = "Turkish", Value = "Turkish" },
                new SelectListItem { Text = "Middle Eastern", Value = "Middle Eastern" },
                new SelectListItem { Text = "Chinese", Value = "Chinese" },
                new SelectListItem { Text = "Indian", Value = "Indian" },
                new SelectListItem { Text = "Japanese", Value = "Japanese" },
                new SelectListItem { Text = "Other Asian", Value = "Other Asian" },
                new SelectListItem { Text = "African", Value = "African" },
            };
            List<SelectListItem> ObjListslng = new List<SelectListItem>()
            {
                //new SelectListItem { Text = "Select Secondary Language", Value = "Select Secondary Language" },
                new SelectListItem { Text = "English", Value = "English" },
                new SelectListItem { Text = "Dutch", Value = "Dutch" },
                new SelectListItem { Text = "Korean", Value = "Korean" },
                new SelectListItem { Text = "Spanish", Value = "Spanish" },
                new SelectListItem { Text = "Russian", Value = "Russian" },

                new SelectListItem { Text = "Turkish", Value = "Turkish" },
                new SelectListItem { Text = "Middle Eastern", Value = "Middle Eastern" },
                new SelectListItem { Text = "Chinese", Value = "Chinese" },
                new SelectListItem { Text = "Indian", Value = "Indian" },
                new SelectListItem { Text = "Japanese", Value = "Japanese" },
                new SelectListItem { Text = "Other Asian", Value = "Other Asian" },
                new SelectListItem { Text = "African", Value = "African" },
            };

            ViewBag.gender = ObjListgender;
            ViewBag.racial = ObjListracial;
            ViewBag.plng = ObjListplng;
            ViewBag.slng = ObjListslng;
            ViewBag.disability = ObjListdisability;

            HttpClient client = new HttpClient();
            List<County_tbl> county = new List<County_tbl>();
            List<State_tbl> state = new List<State_tbl>();
            seek_tbl seek = null;
            string apiUrl = apiBaseUrl + "/County/getAll";
            string apiUrl1 = apiBaseUrl + "/State/getAll/";
            string apiUrl2 = apiBaseUrl + "/Seek/get_by_id_profile/";
            string apiUrl3 = apiBaseUrl + "/District/getAll";
            HttpResponseMessage response = client.GetAsync(apiUrl).Result;
            if (response.IsSuccessStatusCode)
            {
                county = JsonConvert.DeserializeObject<List<County_tbl>>(response.Content.ReadAsStringAsync().Result);
            }
            ViewBag.country = county;
            HttpResponseMessage response1 = client.GetAsync(apiUrl1).Result;
            if (response1.IsSuccessStatusCode)
            {
                state = JsonConvert.DeserializeObject<List<State_tbl>>(response1.Content.ReadAsStringAsync().Result);
            }
            ViewBag.state = state;
          
            List<District_tbl> customers = new List<District_tbl>();
            HttpResponseMessage response2 = client.GetAsync(apiUrl3).Result;
            if (response2.IsSuccessStatusCode)
            {
                customers = JsonConvert.DeserializeObject<List<District_tbl>>(response2.Content.ReadAsStringAsync().Result);
            }
            ViewBag.dist_name = customers;
            int id = Convert.ToInt32(HttpContext.Session.GetString("admin_jobseeker_loginid"));
            HttpResponseMessage readdata = await client.GetAsync(apiUrl2 + id);

            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<seek_tbl>();
                displayresults.Wait();
                seek = displayresults.Result;

            }

            return View(seek);



        }

        public async Task<ActionResult> Admin_seek_Updateprofile(seek_tbl seek_ab)
        {

            HttpClient hc = new HttpClient();

            hc.BaseAddress = new Uri(apiBaseUrl + "/Seek/");
            var locationconsume = hc.PutAsJsonAsync<seek_tbl>("Updateprofile", seek_ab);
            locationconsume.Wait();
            var readdata = locationconsume.Result;

            if (readdata.IsSuccessStatusCode)
            {

                _notyf.Information("Updated Successfully", 5);
                return RedirectToAction("Admin_seek_list", "Jobseeker");

            }
            else
            {
                _notyf.Success("Error.! Try Again..", 5);
                return RedirectToAction("Admin_seek_list", "Jobseeker");
            }
            //return View(seek_ab);


        }

        public async Task<ActionResult> Admin_seek_Editeducation(int id)
        {
            List<SelectListItem> ObjListlgrade = new List<SelectListItem>()
              {

                new SelectListItem { Text = "Pre-K", Value = "Pre-K" },
                new SelectListItem { Text = "K", Value = "K" },
                new SelectListItem { Text = "1", Value = "1" },
                new SelectListItem { Text = "2", Value = "2" },
                new SelectListItem { Text = "3", Value = "3" },

                new SelectListItem { Text = "4", Value = "4" },
                new SelectListItem { Text = "5", Value = "5" },
                new SelectListItem { Text = "6", Value = "6" },
                new SelectListItem { Text = "7", Value = "7" },
                new SelectListItem { Text = "8", Value = "8" },
                new SelectListItem { Text = "9", Value = "9" },
                new SelectListItem { Text = "10", Value = "10" },
                new SelectListItem { Text = "11", Value = "11" },
                new SelectListItem { Text = "12", Value = "12" },
                new SelectListItem { Text = "HS Diploma", Value = "HS Diploma" },
             };
            List<SelectListItem> ObjListdegree = new List<SelectListItem>()
            {

                new SelectListItem { Text = "High School", Value = "High School" },
                new SelectListItem { Text = "CDA Certification", Value = "CDA Certification" },
                new SelectListItem { Text = "Associate Degree", Value = "Associate Degree" },
                new SelectListItem { Text = "Bachelor's Degree", Value = "Bachelor's Degree" },
                new SelectListItem { Text = "Master's Degree", Value = "Master's Degree" },

                new SelectListItem { Text = "Doctorate Degree", Value = "Doctorate Degree" },

            };
            ViewBag.lastgrade = ObjListlgrade;
            ViewBag.degreereceived = ObjListdegree;

            HttpClient client = new HttpClient();

            seekeducation_tbl seek = null;


            string apiUrl2 = apiBaseUrl + "/Seek/get_by_id_education/";


            HttpResponseMessage readdata = await client.GetAsync(apiUrl2 + id);

            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<seekeducation_tbl>();
                displayresults.Wait();
                seek = displayresults.Result;

            }

            return View(seek);



        }
        public async Task<ActionResult> Admin_seek_Updateeducation(seekeducation_tbl seekeducation_ab)
        {

            HttpClient hc = new HttpClient();

            hc.BaseAddress = new Uri(apiBaseUrl + "/Seek/");
            var locationconsume = hc.PutAsJsonAsync<seekeducation_tbl>("Updateeducation", seekeducation_ab);
            locationconsume.Wait();
            var readdata = locationconsume.Result;

            if (readdata.IsSuccessStatusCode)
            {
                _notyf.Information("Updated Successfully", 5);
                return RedirectToAction("Admin_seek_list", "Jobseeker");

            }
            else
            {
                _notyf.Success("Error.! Try Again..", 5);
                return RedirectToAction("Admin_seek_list", "Jobseeker");
            }
            //return View(seekeducation_ab);


        }
        public async Task<ActionResult> Admin_seek_Editexperience(int id)
        {
            List<SelectListItem> ObjListetype = new List<SelectListItem>()
            {

                new SelectListItem { Text = "Full Time", Value = "Full Time" },
                new SelectListItem { Text = "Part Time", Value = "Part Time" },
                new SelectListItem { Text = "InOffice", Value = "InOffice" },
                new SelectListItem { Text = "Remote", Value = "Remote" },
                new SelectListItem { Text = "Hybrid", Value = "Hybrid" },

                new SelectListItem { Text = "Others", Value = "Others" },

            };
            ViewBag.employmenttype = ObjListetype;
            HttpClient client = new HttpClient();

            seekexperience_tbl seek = null;

            string apiUrl2 = apiBaseUrl + "/Seek/get_by_id_experience/";


            HttpResponseMessage readdata = await client.GetAsync(apiUrl2 + id);

            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<seekexperience_tbl>();
                displayresults.Wait();
                seek = displayresults.Result;

            }

            return View(seek);



        }
        public async Task<ActionResult> Admin_seek_Updateexperience(seekexperience_tbl seekexperience_ab)
        {

            HttpClient hc = new HttpClient();

            hc.BaseAddress = new Uri(apiBaseUrl + "/Seek/");
            var locationconsume = hc.PutAsJsonAsync<seekexperience_tbl>("Updateexperience", seekexperience_ab);
            locationconsume.Wait();
            var readdata = locationconsume.Result;

            if (readdata.IsSuccessStatusCode)
            {
                _notyf.Information("Updated Successfully", 5);
                return RedirectToAction("Admin_seek_list", "Jobseeker");

            }
            else
            {
                _notyf.Success("Error.! Try Again..", 5);
                return RedirectToAction("Admin_seek_list", "Jobseeker");
            }
            //return View(seekexperience_ab);


        }
        public IActionResult Admin_seek_Appliedjobdetail(int id)
        {
            HttpClient client = new HttpClient();
            string apiUrl2 = apiBaseUrl + "/ApplyJob/getall_by_id_appliedjobdetail/";
            List<app_job_tbl> appjob = new List<app_job_tbl>();
            HttpResponseMessage response2 = client.GetAsync(apiUrl2 + id).Result;
            if (response2.IsSuccessStatusCode)
            {
                appjob = JsonConvert.DeserializeObject<List<app_job_tbl>>(response2.Content.ReadAsStringAsync().Result);
            }
            ViewBag.appjob = appjob;

            return View(appjob);
        }
        public async Task<ActionResult> Remove_seek_profile(int id)
        {

            string apiUrl = apiBaseUrl + "/Seek/Delete/";

            HttpClient client = new HttpClient();

            HttpResponseMessage readdata = await client.GetAsync(apiUrl + id);
            if (readdata.IsSuccessStatusCode)
            {
                _notyf.Error("Deleted Successfully..!", 5);
            }
            else
            {
                _notyf.Error("Error..!", 5);
            }

            return RedirectToAction("Admin_seek_list", "Jobseeker");

        }

        public async Task<ActionResult> Remove_admin_seek(int id)
        {

            string apiUrl = apiBaseUrl + "/Seek/Delete/";

            HttpClient client = new HttpClient();

            HttpResponseMessage readdata = await client.GetAsync(apiUrl + id);
            if (readdata.IsSuccessStatusCode)
            {
                _notyf.Error("Deleted Successfully..!", 5);
            }
            else
            {
                _notyf.Error("Error..!", 5);
            }

            return RedirectToAction("Admin_seek_list", "Jobseeker");

        }

    }
}
