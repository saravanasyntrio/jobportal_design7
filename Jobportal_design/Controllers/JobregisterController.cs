﻿using Jobportal_design.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Net;
using Microsoft.AspNetCore.Mvc.Rendering;
using AspNetCoreHero.ToastNotification.Abstractions;
using Microsoft.Extensions.Configuration;

namespace Jobportal_design.Controllers
{
    public class JobregisterController : Controller
    {
        private readonly INotyfService _notyf;
        private IConfiguration _Configure;
        string apiBaseUrl;
        public JobregisterController(INotyfService notyf, IConfiguration configuration)
        {
            _notyf = notyf;
            _Configure = configuration;
            apiBaseUrl = _Configure.GetValue<string>("WebAPIBaseUrl");
        }
        public async Task<ActionResult> AddJobregister()
        {
            List<State_tbl> state = new List<State_tbl>();
 
            string apiUrl = apiBaseUrl + "/state/getAllActiveState";

            
            HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync(apiUrl).Result;
            if (response.IsSuccessStatusCode)
            {
                state = JsonConvert.DeserializeObject<List<State_tbl>>(response.Content.ReadAsStringAsync().Result);
            }
            ViewBag.state = state;

            List<District_tbl> customers = new List<District_tbl>();
            string apiUrl1 = apiBaseUrl + "/District/getAllActiveDistrict";
            

            HttpResponseMessage response1 = client.GetAsync(apiUrl1).Result;
            if (response1.IsSuccessStatusCode)
            {
                customers = JsonConvert.DeserializeObject<List<District_tbl>>(response1.Content.ReadAsStringAsync().Result);
            }
            ViewBag.dist = customers;

            List<Job_cat> job_cat = new List<Job_cat>();

            string apiUrl2 = apiBaseUrl + "/Job_cat/getAllActiveJobcategory";
            HttpResponseMessage response2 = client.GetAsync(apiUrl2).Result;
            if (response2.IsSuccessStatusCode)
            {
                job_cat = JsonConvert.DeserializeObject<List<Job_cat>>(response2.Content.ReadAsStringAsync().Result);
            }
            ViewBag.job_cat = job_cat;

            List<County_tbl> county = new List<County_tbl>();
            string apiUrl3 = apiBaseUrl + "/County/getAllActiveCounty";


            HttpResponseMessage response3 = client.GetAsync(apiUrl3).Result;
            if (response3.IsSuccessStatusCode)
            {
                county = JsonConvert.DeserializeObject<List<County_tbl>>(response3.Content.ReadAsStringAsync().Result);
            }
            ViewBag.county = county;

          
            int org_loginid = Convert.ToInt32(HttpContext.Session.GetString("org_loginid"));
            int id = Convert.ToInt32(HttpContext.Session.GetString("employer_loginid"));

             
            string apiUrl4 = apiBaseUrl + "/Org/getAddress_id/";


            Org_emp Org = null;
            //if (org_loginid != 0)
            //{
                HttpResponseMessage readdata1 = await client.GetAsync(apiUrl4 + id + "/" + org_loginid);
                if (readdata1.IsSuccessStatusCode)
                {
                    var displayresults = readdata1.Content.ReadAsAsync<Org_emp>();
                    displayresults.Wait();
                    Org = displayresults.Result;

                }
            //}

            //else
            //{
            //    HttpResponseMessage readdata2 = await client.GetAsync(apiUrl4 + id);
            //    if (readdata2.IsSuccessStatusCode)
            //    {
            //        var displayresults = readdata2.Content.ReadAsAsync<Org_emp>();
            //        displayresults.Wait();
            //        Org = displayresults.Result;

            //    }
            //}
            ViewBag.address = Org.org_address;


                return View();

           

        }
        public async Task<ActionResult> Create(Job_register_ab Job_register_ab)
        {
 
            int org_loginid = Convert.ToInt32(HttpContext.Session.GetString("org_loginid"));
           
            int emp_loginid = Convert.ToInt32(HttpContext.Session.GetString("employer_loginid"));
            Job_register_ab.org_id = org_loginid;
            Job_register_ab.employeer_id = emp_loginid;
             HttpClient hc = new HttpClient();
            hc.BaseAddress = new Uri(apiBaseUrl + "/Jobregister/Create");
            var locationconsume = hc.PostAsJsonAsync<Job_register_ab>("Create", Job_register_ab);
            locationconsume.Wait();
            var readdata = locationconsume.Result;

            if (readdata.IsSuccessStatusCode)
            {
                if (org_loginid == 0)
                {
                    _notyf.Success("Job submitted for Admin approval..!", 5);
                    return RedirectToAction("jobregisterlist", "Jobregister");
                }
                else{
                    _notyf.Success("Job submitted for Admin approval..!", 5);
                    return RedirectToAction("Org_JobList", "Employer");
                }
              
            }
            else
            {
                _notyf.Error("Error..! Try Again..", 9);
                return RedirectToAction("AddJobregister", "Jobregister"); 
            }
            //return View(Job_register_ab);


        }
        public async Task<IActionResult> jobregisterlist(string searchval)
         {
            int id = Convert.ToInt32(HttpContext.Session.GetString("employer_loginid"));
            int org_id = 0;
            //List<Job_register_ab> customers = new List<Job_register_ab>();
            HttpClient client = new HttpClient();

            Employer emp = null;
            Org_physical Org = null;

            string apiUrl2 = apiBaseUrl + "/Org/get_by_id/";
            string apiUrl = apiBaseUrl + "/Employer/getAllemployer_by_id/";

            string apiUrl1 = apiBaseUrl + "/Jobregister/getall_job_by_employid/";

            HttpResponseMessage readdata = await client.GetAsync(apiUrl + id);


            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<Employer>();
                displayresults.Wait();
                emp = displayresults.Result;
                org_id = emp.org_id;
            }
            //ViewBag.profilepic = emp.emp_image;
            ViewBag.profilepic = emp.empimage_path;
            HttpResponseMessage readdata1 = await client.GetAsync(apiUrl2 + org_id);
            if (readdata1.IsSuccessStatusCode)
            {
                var displayresults = readdata1.Content.ReadAsAsync<Org_physical>();
                displayresults.Wait();
                Org = displayresults.Result;

            }
            ViewBag.employerlist = emp;
            ViewBag.org_name = emp.org_name;
            ViewBag.e_name = emp.e_name;
            ViewBag.email = emp.email_id;
            ViewBag.phone = emp.contact1;
            //ViewBag.roletype = emp.roletype;
            //ViewBag.city = emp.emp_city;
            //ViewBag.state = emp.state_name;
            ViewBag.city = Org.dist_name;
            ViewBag.state = Org.state_name;
            ViewBag.county_name = Org.county_name;
            ViewBag.website = Org.website;
            ViewBag.description = Org.orgdescription;

            List<Job_register_ab> job = new List<Job_register_ab>();
            if (searchval == null)
                searchval = "All";
            HttpResponseMessage response1 = client.GetAsync(apiUrl1 + id ).Result;
            if (response1.IsSuccessStatusCode)
            {
                job = JsonConvert.DeserializeObject<List<Job_register_ab>>(response1.Content.ReadAsStringAsync().Result);

            }
            ViewBag.job = job;
            ViewBag.job_count = job.Count();


            return View(emp);
        }
        public async Task<ActionResult> EditJob(int id)
        {
            List<SelectListItem> ObjListetype = new List<SelectListItem>()
            {

                new SelectListItem { Text = "Administrator/Director", Value = "Administrator/Director" },
                new SelectListItem { Text = "Assistant Teacher", Value = "Assistant Teacher" },
                new SelectListItem { Text = "Floater", Value = "Floater" },
                new SelectListItem { Text = "Lead Teacher", Value = "Lead Teacher" },
                new SelectListItem { Text = "Substitute", Value = "Substitute" },
                //new SelectListItem { Text = "Administrator on JFS license", Value = "Administrator on JFS license" },
                new SelectListItem { Text = "Day Camp Administrator", Value = "Day Camp Administrator" },
                new SelectListItem { Text = "Day Camp Junior Counselor", Value = "Day Camp Junior Counselor" },
                new SelectListItem { Text = "Day Camp Lead Counselor", Value = "Day Camp Lead Counselor" },
                new SelectListItem { Text = "Day Camp Owner", Value = "Day Camp Owner" }

            };
            ViewBag.job_title = ObjListetype;

            List<SelectListItem> ObjListetype1 = new List<SelectListItem>()
            {

                new SelectListItem { Text = "Full Time", Value = "Full Time" },
                new SelectListItem { Text = "Part Time", Value = "Part Time" },
                new SelectListItem { Text = "In Office", Value = "In Office" },
                new SelectListItem { Text = "Remote", Value = "Remote" },
                new SelectListItem { Text = "Hybrid", Value = "Hybrid" },

                new SelectListItem { Text = "Others", Value = "Others" },

            };
            ViewBag.employmenttype = ObjListetype1;

            List<SelectListItem> ObjListetype2 = new List<SelectListItem>()
            {

                new SelectListItem { Text = "Hourly", Value = "Hourly" },
                new SelectListItem { Text = "Weekly", Value = "Weekly" },
                new SelectListItem { Text = "Monthly", Value = "Monthly" }

            };
            ViewBag.salary_pay = ObjListetype2;

            List<SelectListItem> ObjListetype3 = new List<SelectListItem>()
            {

                new SelectListItem { Text = "High School", Value = "High School" },
                new SelectListItem { Text = "CDA Certification", Value = "CDA Certification" },
                new SelectListItem { Text = "Associate Degree", Value = "Associate Degree" },
                new SelectListItem { Text = "Bachelor's Degree", Value = "Bachelor's Degree" },
                new SelectListItem { Text = "Master's Degree", Value = "Master's Degree" },
                new SelectListItem { Text = "Doctorate Degree", Value = "Doctorate Degree" }

            };
            ViewBag.certification = ObjListetype3;


            HttpClient client = new HttpClient();
            List<Org_cat> category_name = new List<Org_cat>();
            List<State_tbl> state_name = new List<State_tbl>();
            Job_register_ab seek = null;

            //string apiUrl4 = apiBaseUrl + "/Org_cat/getAll";
            string apiUrl1 = apiBaseUrl + "/State/getAll/";
            string apiUrl2 = apiBaseUrl + "/Jobregister/get_by_id_edit/";


            //HttpResponseMessage response4 = client.GetAsync(apiUrl4).Result;
            //if (response4.IsSuccessStatusCode)
            //{
            //    category_name = JsonConvert.DeserializeObject<List<Org_cat>>(response4.Content.ReadAsStringAsync().Result);
            //}
            //ViewBag.category_name = category_name;

            HttpResponseMessage response1 = client.GetAsync(apiUrl1).Result;
            if (response1.IsSuccessStatusCode)
            {
                state_name = JsonConvert.DeserializeObject<List<State_tbl>>(response1.Content.ReadAsStringAsync().Result);
            }
            ViewBag.state_name = state_name;

            List<County_tbl> county_name = new List<County_tbl>();
            string apiUrl3 = apiBaseUrl + "/County/getAll";


            HttpResponseMessage response2 = client.GetAsync(apiUrl3).Result;
            if (response2.IsSuccessStatusCode)
            {
                county_name = JsonConvert.DeserializeObject<List<County_tbl>>(response2.Content.ReadAsStringAsync().Result);
            }
            ViewBag.county_name = county_name;

          

            List<Job_cat> job_cat = new List<Job_cat>();
            string apiUrl5 = apiBaseUrl + "/Job_cat/getAll";
            HttpResponseMessage response5 = client.GetAsync(apiUrl5).Result;
            if (response5.IsSuccessStatusCode)
            {
                job_cat = JsonConvert.DeserializeObject<List<Job_cat>>(response5.Content.ReadAsStringAsync().Result);
            }
            ViewBag.job_cat = job_cat;

            List<District_tbl> dist_name = new List<District_tbl>();
            string apiUrl6 = apiBaseUrl + "/District/getAll";


            HttpResponseMessage response6 = client.GetAsync(apiUrl6).Result;
            if (response6.IsSuccessStatusCode)
            {
                dist_name = JsonConvert.DeserializeObject<List<District_tbl>>(response6.Content.ReadAsStringAsync().Result);
            }
            ViewBag.dist_name = dist_name;

            //int id = Convert.ToInt32(HttpContext.Session.GetString("employer_loginid"));

            HttpResponseMessage readdata = await client.GetAsync(apiUrl2 + id);

            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<Job_register_ab>();
                displayresults.Wait();
                seek = displayresults.Result;

            }

            return View(seek);

        }
       
        public async Task<ActionResult> Update(Job_register_ab Job_register_ab)
        {
 
            HttpClient hc = new HttpClient();

            hc.BaseAddress = new Uri(apiBaseUrl + "/Jobregister/Update");
            var locationconsume = hc.PutAsJsonAsync<Job_register_ab>("Update", Job_register_ab);
            locationconsume.Wait();
            var readdata = locationconsume.Result;

            if (readdata.IsSuccessStatusCode)
            {

                _notyf.Information("Updated Successfully", 5);
                return RedirectToAction("jobregisterlist", "Jobregister");

            }
            else
            {
                _notyf.Error("Error..! Try Again..", 5);
                return RedirectToAction("EditProfile", "Jobregister");
            }
            //return View(Job_register_ab);


        }
        public async Task<ActionResult> Saved_job(int job_reg_id)
        {
            int id = Convert.ToInt32(HttpContext.Session.GetString("jobseeker_loginid"));
            HttpClient hc = new HttpClient();
            Saved_job_ab Saved_job_ab = new Saved_job_ab();
            Saved_job_ab.seek_id = id;
            Saved_job_ab.job_reg_id = job_reg_id;
            
            hc.BaseAddress = new Uri(apiBaseUrl + "/Jobregister/");
            var locationconsume = hc.PostAsJsonAsync<Saved_job_ab>("Saved_job", Saved_job_ab);
            locationconsume.Wait();
            var readdata = locationconsume.Result;
            if (readdata.IsSuccessStatusCode)
            {
                //var content = readdata.Content;
                //string jsonContent = content.ReadAsStringAsync().Result;


                //var content1 = await res.Content.ReadAsStringAsync();
                _notyf.Success("Job Saved Successfully, You can apply later..", 15);

                return RedirectToAction("Appliedjobdetail", "Jobseeker", new { ActiveTab = "tab-saved-jobs", isRedirect = "true" });


            }
            else
            {
                _notyf.Error("Job Saved already.!", 15);
                return RedirectToAction("JobdetailsView", "Jobseeker");
            }

        }
        public async Task<ActionResult> ViewJobdetail(int id)
        {
            HttpClient client = new HttpClient();

            Job_register_ab jobs = null;

            string apiUrl2 = apiBaseUrl + "/Jobregister/get_by_id_edit/";


            HttpResponseMessage readdata = await client.GetAsync(apiUrl2 + id);

            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<Job_register_ab>();
                displayresults.Wait();
                jobs = displayresults.Result;
                ViewBag.id = jobs.id;
                ViewBag.job_title = jobs.job_title;
                ViewBag.job_category_name = jobs.job_category_name;
                ViewBag.dist_name = jobs.dist_name;
                ViewBag.job_type = jobs.job_type;
                ViewBag.job_skills = jobs.job_skills;
                ViewBag.job_desc = jobs.job_desc;
                ViewBag.responsibilities = jobs.responsibilities;
                ViewBag.benefits = jobs.benefits;
                ViewBag.schedule = jobs.shedule;
                ViewBag.licence_certification = jobs.licence_certification;

                ViewBag.start_date = jobs.start_date.ToString("MM/dd/yyyy");
                ViewBag.end_date = jobs.end_date.ToString("MM/dd/yyyy");

                ViewBag.org_name = jobs.org_name;
                ViewBag.org_desc = jobs.org_desc;
                ViewBag.salary_pay = jobs.salary_pay;
                ViewBag.salary_package = jobs.salary_package;
                ViewBag.salary_to = jobs.salary_to;
                ViewBag.experience = jobs.experience;
                ViewBag.address = jobs.address;
            }

            return View(jobs);



        }

        public async Task<IActionResult> getall_Jobseeker_id(int id)
        {
            //int employeer_id = Convert.ToInt32(HttpContext.Session.GetString("employer_loginid"));
            HttpClient client = new HttpClient();

            //app_job_tbl seek = null;
            List<job_seek> seek = new List<job_seek>();

            string apiUrl = apiBaseUrl + "/Jobregister/getall_Jobseeker_id/";

            HttpResponseMessage readdata = await client.GetAsync(apiUrl + id);

            if (readdata.IsSuccessStatusCode)
            {
                //job = JsonConvert.DeserializeObject<List<Job_register_ab>>(response1.Content.ReadAsStringAsync().Result);
                seek = JsonConvert.DeserializeObject<List<job_seek>>(readdata.Content.ReadAsStringAsync().Result);
               // displayresults.Wait();
               // seek = displayresults.Result;

            }
            ViewBag.Jobseeker = seek;
            ViewBag.countjobs = seek.Count();



            return View(seek);
        }
        public async Task<ActionResult> Jobdetails_admin(int id)
        {
            HttpClient client = new HttpClient();
            //string apiUrl2 = "http://localhost:62332/Jobregister/get_by_id/";
            string apiUrl2 = apiBaseUrl + "/Jobregister/get_by_id_edit/";
            // emp_tbl emp = null;
            job_reg_tbl jobs = null;
            HttpResponseMessage readdata = await client.GetAsync(apiUrl2 + id);

            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<job_reg_tbl>();
                displayresults.Wait();
                jobs = displayresults.Result;

            }
            if (jobs != null)
            {
                ViewBag.id = jobs.id;
            }
            jobs.posting_date.ToString("MM/dd/yyyy");
            ViewBag.posting_date = jobs.posting_date.ToString("MM/dd/yyyy");
            ViewBag.end_date = jobs.end_date.ToString("MM/dd/yyyy");

            return View(jobs);
            //return View();
        }

        public async Task<ActionResult> Delete(int id)
        {
           
            string apiUrl = apiBaseUrl + "/Jobregister/Delete/";

            HttpClient client = new HttpClient();

            HttpResponseMessage readdata = await client.GetAsync(apiUrl + id);
            //  var result = responseTask.Result;
            if (readdata.IsSuccessStatusCode)
            {
                //var displayresults = readdata.Content.ReadAsAsync<Currency_tbl>();
                //displayresults.Wait();
                //currency = displayresults.Result;
                _notyf.Error("Deleted Successfully", 5);

            }

            return RedirectToAction("jobregisterlist", "Jobregister");

        }

        public IActionResult Joblist_Admin()
        {
            List<Job_register_ab> customers = new List<Job_register_ab>();
            string apiUrl = apiBaseUrl + "/Jobregister/getAll";

            HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync(apiUrl).Result;
            if (response.IsSuccessStatusCode)
            {
                customers = JsonConvert.DeserializeObject<List<Job_register_ab>>(response.Content.ReadAsStringAsync().Result);
            }
            ViewBag.jobCount = customers.Count();
            List<Job_cat> job_cat = new List<Job_cat>();
            string apiUrl2 = apiBaseUrl + "/Job_cat/getAll";
            HttpResponseMessage response2 = client.GetAsync(apiUrl2).Result;
            if (response2.IsSuccessStatusCode)
            {
                job_cat = JsonConvert.DeserializeObject<List<Job_cat>>(response2.Content.ReadAsStringAsync().Result);
            }

            ViewBag.job_cat = job_cat;
            string apiUrl1 = apiBaseUrl + "/County/getAllActiveCounty";
            List<County_tbl> county = new List<County_tbl>();
            HttpResponseMessage response1 = client.GetAsync(apiUrl1).Result;
            if (response1.IsSuccessStatusCode)
            {
                county = JsonConvert.DeserializeObject<List<County_tbl>>(response1.Content.ReadAsStringAsync().Result);
            }
            ViewBag.county = county;
            int seek_id = 0;
            string apiUrl4 = apiBaseUrl + "/Jobregister/getJobTitleWithJobCount/";
            List<job_title> jobTitle = new List<job_title>();
            HttpResponseMessage response4 = client.GetAsync(apiUrl4 + seek_id).Result;

            if (response4.IsSuccessStatusCode)
            {
                jobTitle = JsonConvert.DeserializeObject<List<job_title>>(response4.Content.ReadAsStringAsync().Result);
            }
            ViewBag.jobTitle = jobTitle;

            return View(customers);
        }
        public async Task<ActionResult> Joblist_AdminPartialView(int job_cat_id, string Value, int county_id)
        {

            HttpClient client = new HttpClient();

            List<Job_register_ab> customers = new List<Job_register_ab>();
            
            string apiUrl = apiBaseUrl + "/Jobregister/getall_job_filter_admin/";
            if(Value==null)
            {
                Value = "all";
            }
            if (job_cat_id == 0)
            {
                job_cat_id = -1;
            }
            if (county_id == 0)
            {
                county_id = -1;
            }
            HttpResponseMessage response = client.GetAsync(apiUrl + job_cat_id + "/" + Value + "/" + county_id).Result;

            if (response.IsSuccessStatusCode)
            {
                customers = JsonConvert.DeserializeObject<List<Job_register_ab>>(response.Content.ReadAsStringAsync().Result);
            }
            ViewBag.jobCount = customers.Count();


            return PartialView("Joblist_AdminPartialView", customers);
            //return View(customers);
        }
      
        public async Task<ActionResult> EditJob_admin(int id)
        {
            HttpClient client = new HttpClient();
            List<Org_cat> category_name = new List<Org_cat>();
            List<State_tbl> state_name = new List<State_tbl>();
            Job_register_ab seek = null;

            string apiUrl4 = apiBaseUrl + "/Org_cat/getAll";
            string apiUrl1 = apiBaseUrl + "/State/getAll/";
            string apiUrl2 = apiBaseUrl + "/Jobregister/get_by_id_edit/";

            HttpResponseMessage response4 = client.GetAsync(apiUrl4).Result;
            if (response4.IsSuccessStatusCode)
            {
                category_name = JsonConvert.DeserializeObject<List<Org_cat>>(response4.Content.ReadAsStringAsync().Result);
            }
            ViewBag.category_name = category_name;
            HttpResponseMessage response1 = client.GetAsync(apiUrl1).Result;
            if (response1.IsSuccessStatusCode)
            {
                state_name = JsonConvert.DeserializeObject<List<State_tbl>>(response1.Content.ReadAsStringAsync().Result);
            }
            ViewBag.state_name = state_name;

            List<Job_cat> job_cat = new List<Job_cat>();
            string apiUrl5 = apiBaseUrl + "/Job_cat/getAll";
            HttpResponseMessage response5 = client.GetAsync(apiUrl5).Result;
            if (response5.IsSuccessStatusCode)
            {
                job_cat = JsonConvert.DeserializeObject<List<Job_cat>>(response5.Content.ReadAsStringAsync().Result);
            }
            ViewBag.job_cat = job_cat;

            //int id = Convert.ToInt32(HttpContext.Session.GetString("employer_loginid"));

            HttpResponseMessage readdata = await client.GetAsync(apiUrl2 + id);

            if (readdata.IsSuccessStatusCode)
            {
                //var displayresults = readdata.Content.ReadAsAsync<Job_register_ab>();
                //.Wait();
                //seek = displayresults.Result;
                JsonConvert.DeserializeObject<List<seek_tbl>>(response4.Content.ReadAsStringAsync().Result);
            }

            return View(seek);



        }

        public async Task<ActionResult> UpdateJob_Admin(Job_register_ab Job_register_ab)
        {
            //Job_register_ab.employeer_id = Convert.ToInt32(HttpContext.Session.GetString("employer_loginid"));

            HttpClient hc = new HttpClient();

            hc.BaseAddress = new Uri(apiBaseUrl + "/Jobregister/updateJob_Admin");
            var locationconsume = hc.PutAsJsonAsync<Job_register_ab>("UpdateJob_Admin", Job_register_ab);
            locationconsume.Wait();
            var readdata = locationconsume.Result;

            if (readdata.IsSuccessStatusCode)
            {
                _notyf.Information("Updated Successfully", 3);

                return RedirectToAction("Joblist_Admin", "Jobregister");

            }
            return View(Job_register_ab);


        }
        public async Task<ActionResult> Jobapproved_admin(string activeStat, int passedId, Org_physical Org_physical)
        {
            //Employer.id = Convert.ToInt32(HttpContext.Session.GetString("employer_loginid"));

            job_reg_tbl jb = new job_reg_tbl();
            jb.id = passedId;
            jb.isactive = activeStat;
            //  country_ab.identifier = false;

            HttpClient hc = new HttpClient();
            //string apiUrl = apiBaseUrl + "/Jobregister/ApproveJob/";
           
           // HttpResponseMessage readdata = await hc.GetAsync(apiUrl + passedId  + "/" + activeStat);

            hc.BaseAddress = new Uri(apiBaseUrl + "/Jobregister/");
            var locationconsume = hc.PutAsJsonAsync<job_reg_tbl>("ApproveJob", jb);
            locationconsume.Wait();
            var readdata = locationconsume.Result;

            if (readdata.IsSuccessStatusCode)
            {
                _notyf.Success("Job Appoval Successfully", 5);
                return RedirectToAction("Joblist_Admin", "Jobregister");
            }
            else
            {
                _notyf.Success("Error..! Try Again..", 5);
                return RedirectToAction("Joblist_Admin", "Jobregister");
            }
            //return View(Employer);


        }
        public async Task<IActionResult> getall_Jobseeker_admin(int id)
        {
            //int employeer_id = Convert.ToInt32(HttpContext.Session.GetString("employer_loginid"));
            HttpClient client = new HttpClient();

            //app_job_tbl seek = null;
            List<job_seek> seek = new List<job_seek>();

            string apiUrl = apiBaseUrl + "/Jobregister/getall_Jobseeker_admin/";

            HttpResponseMessage readdata = await client.GetAsync(apiUrl + id);

            if (readdata.IsSuccessStatusCode)
            {
                //job = JsonConvert.DeserializeObject<List<Job_register_ab>>(response1.Content.ReadAsStringAsync().Result);
                seek = JsonConvert.DeserializeObject<List<job_seek>>(readdata.Content.ReadAsStringAsync().Result);
                // displayresults.Wait();
                // seek = displayresults.Result;

            }
            ViewBag.Jobseeker = seek;
            ViewBag.countjobs = seek.Count();




            return View(seek);
        }
        public async Task<ActionResult> EditJobs_by_Org(int id)
        {
            List<SelectListItem> ObjListetype = new List<SelectListItem>()
            {

                new SelectListItem { Text = "Administrator/Director", Value = "Administrator/Director" },
                new SelectListItem { Text = "Assistant Teacher", Value = "Assistant Teacher" },
                new SelectListItem { Text = "Floater", Value = "Floater" },
                new SelectListItem { Text = "Lead Teacher", Value = "Lead Teacher" },
                new SelectListItem { Text = "Substitute", Value = "Substitute" },
                //new SelectListItem { Text = "Administrator on JFS license", Value = "Administrator on JFS license" },
                new SelectListItem { Text = "Day Camp Administrator", Value = "Day Camp Administrator" },
                new SelectListItem { Text = "Day Camp Junior Counselor", Value = "Day Camp Junior Counselor" },
                new SelectListItem { Text = "Day Camp Lead Counselor", Value = "Day Camp Lead Counselor" },
                new SelectListItem { Text = "Day Camp Owner", Value = "Day Camp Owner" }

            };
            ViewBag.job_title = ObjListetype;

            List<SelectListItem> ObjListetype1 = new List<SelectListItem>()
            {

                new SelectListItem { Text = "Full Time", Value = "Full Time" },
                new SelectListItem { Text = "Part Time", Value = "Part Time" },
                new SelectListItem { Text = "In Office", Value = "In Office" },
                new SelectListItem { Text = "Remote", Value = "Remote" },
                new SelectListItem { Text = "Hybrid", Value = "Hybrid" },

                new SelectListItem { Text = "Others", Value = "Others" },

            };
            ViewBag.employmenttype = ObjListetype1;

            List<SelectListItem> ObjListetype2 = new List<SelectListItem>()
            {

                new SelectListItem { Text = "Hourly", Value = "Hourly" },
                new SelectListItem { Text = "Weekly", Value = "Weekly" },
                new SelectListItem { Text = "Monthly", Value = "Monthly" }

            };
            ViewBag.salary_pay = ObjListetype2;

            List<SelectListItem> ObjListetype3 = new List<SelectListItem>()
            {

                new SelectListItem { Text = "High School", Value = "High School" },
                new SelectListItem { Text = "CDA Certification", Value = "CDA Certification" },
                new SelectListItem { Text = "Associate Degree", Value = "Associate Degree" },
                new SelectListItem { Text = "Bachelor's Degree", Value = "Bachelor's Degree" },
                new SelectListItem { Text = "Master's Degree", Value = "Master's Degree" },
                new SelectListItem { Text = "Doctorate Degree", Value = "Doctorate Degree" }

            };
            ViewBag.certification = ObjListetype3;


            HttpClient client = new HttpClient();
            List<Org_cat> category_name = new List<Org_cat>();
            List<State_tbl> state_name = new List<State_tbl>();
            Job_register_ab seek = null;

            //string apiUrl4 = apiBaseUrl + "/Org_cat/getAll";
            string apiUrl1 = apiBaseUrl + "/State/getAll/";
            string apiUrl2 = apiBaseUrl + "/Jobregister/get_by_id_edit/";


            //HttpResponseMessage response4 = client.GetAsync(apiUrl4).Result;
            //if (response4.IsSuccessStatusCode)
            //{
            //    category_name = JsonConvert.DeserializeObject<List<Org_cat>>(response4.Content.ReadAsStringAsync().Result);
            //}
            //ViewBag.category_name = category_name;

            HttpResponseMessage response1 = client.GetAsync(apiUrl1).Result;
            if (response1.IsSuccessStatusCode)
            {
                state_name = JsonConvert.DeserializeObject<List<State_tbl>>(response1.Content.ReadAsStringAsync().Result);
            }
            ViewBag.state_name = state_name;

            List<County_tbl> county_name = new List<County_tbl>();
            string apiUrl3 = apiBaseUrl + "/County/getAll";


            HttpResponseMessage response2 = client.GetAsync(apiUrl3).Result;
            if (response2.IsSuccessStatusCode)
            {
                county_name = JsonConvert.DeserializeObject<List<County_tbl>>(response2.Content.ReadAsStringAsync().Result);
            }
            ViewBag.county_name = county_name;



            List<Job_cat> job_cat = new List<Job_cat>();
            string apiUrl5 = apiBaseUrl + "/Job_cat/getAll";
            HttpResponseMessage response5 = client.GetAsync(apiUrl5).Result;
            if (response5.IsSuccessStatusCode)
            {
                job_cat = JsonConvert.DeserializeObject<List<Job_cat>>(response5.Content.ReadAsStringAsync().Result);
            }
            ViewBag.job_cat = job_cat;

            List<District_tbl> dist_name = new List<District_tbl>();
            string apiUrl6 = apiBaseUrl + "/District/getAll";


            HttpResponseMessage response6 = client.GetAsync(apiUrl6).Result;
            if (response6.IsSuccessStatusCode)
            {
                dist_name = JsonConvert.DeserializeObject<List<District_tbl>>(response6.Content.ReadAsStringAsync().Result);
            }
            ViewBag.dist_name = dist_name;

            //int id = Convert.ToInt32(HttpContext.Session.GetString("employer_loginid"));

            HttpResponseMessage readdata = await client.GetAsync(apiUrl2 + id);

            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<Job_register_ab>();
                displayresults.Wait();
                seek = displayresults.Result;

            }

            return View(seek);

        }

        public async Task<ActionResult> UpdateJobs_by_Org(Job_register_ab Job_register_ab)
        {

            HttpClient hc = new HttpClient();

            hc.BaseAddress = new Uri(apiBaseUrl + "/Jobregister/Update");
            var locationconsume = hc.PutAsJsonAsync<Job_register_ab>("Update", Job_register_ab);
            locationconsume.Wait();
            var readdata = locationconsume.Result;

            if (readdata.IsSuccessStatusCode)
            {

                _notyf.Information("Updated Successfully", 5);
                return RedirectToAction("Org_JobList", "Employer");

            }
            else
            {
                _notyf.Error("Error..! Try Again..", 5);
                return RedirectToAction("EditJobs_by_Org", "Jobregister");
            }
            //return View(Job_register_ab);


        }

        public async Task<ActionResult> Edit_Org_Jobs(int id)
        {
            List<SelectListItem> ObjListetype = new List<SelectListItem>()
            {

                new SelectListItem { Text = "Administrator/Director", Value = "Administrator/Director" },
                new SelectListItem { Text = "Assistant Teacher", Value = "Assistant Teacher" },
                new SelectListItem { Text = "Floater", Value = "Floater" },
                new SelectListItem { Text = "Lead Teacher", Value = "Lead Teacher" },
                new SelectListItem { Text = "Substitute", Value = "Substitute" },
                //new SelectListItem { Text = "Administrator on JFS license", Value = "Administrator on JFS license" },
                new SelectListItem { Text = "Day Camp Administrator", Value = "Day Camp Administrator" },
                new SelectListItem { Text = "Day Camp Junior Counselor", Value = "Day Camp Junior Counselor" },
                new SelectListItem { Text = "Day Camp Lead Counselor", Value = "Day Camp Lead Counselor" },
                new SelectListItem { Text = "Day Camp Owner", Value = "Day Camp Owner" }

            };
            ViewBag.job_title = ObjListetype;

            List<SelectListItem> ObjListetype1 = new List<SelectListItem>()
            {

                new SelectListItem { Text = "Full Time", Value = "Full Time" },
                new SelectListItem { Text = "Part Time", Value = "Part Time" },
                new SelectListItem { Text = "In Office", Value = "In Office" },
                new SelectListItem { Text = "Remote", Value = "Remote" },
                new SelectListItem { Text = "Hybrid", Value = "Hybrid" },

                new SelectListItem { Text = "Others", Value = "Others" },

            };
            ViewBag.employmenttype = ObjListetype1;

            List<SelectListItem> ObjListetype2 = new List<SelectListItem>()
            {

                new SelectListItem { Text = "Hourly", Value = "Hourly" },
                new SelectListItem { Text = "Weekly", Value = "Weekly" },
                new SelectListItem { Text = "Monthly", Value = "Monthly" }

            };
            ViewBag.salary_pay = ObjListetype2;

            List<SelectListItem> ObjListetype3 = new List<SelectListItem>()
            {

                new SelectListItem { Text = "High School", Value = "High School" },
                new SelectListItem { Text = "CDA Certification", Value = "CDA Certification" },
                new SelectListItem { Text = "Associate Degree", Value = "Associate Degree" },
                new SelectListItem { Text = "Bachelor's Degree", Value = "Bachelor's Degree" },
                new SelectListItem { Text = "Master's Degree", Value = "Master's Degree" },
                new SelectListItem { Text = "Doctorate Degree", Value = "Doctorate Degree" }

            };
            ViewBag.certification = ObjListetype3;


            HttpClient client = new HttpClient();
            List<Org_cat> category_name = new List<Org_cat>();
            List<State_tbl> state_name = new List<State_tbl>();
            Job_register_ab seek = null;

            //string apiUrl4 = apiBaseUrl + "/Org_cat/getAll";
            string apiUrl1 = apiBaseUrl + "/State/getAll/";
            string apiUrl2 = apiBaseUrl + "/Jobregister/get_by_id_edit/";


            //HttpResponseMessage response4 = client.GetAsync(apiUrl4).Result;
            //if (response4.IsSuccessStatusCode)
            //{
            //    category_name = JsonConvert.DeserializeObject<List<Org_cat>>(response4.Content.ReadAsStringAsync().Result);
            //}
            //ViewBag.category_name = category_name;

            HttpResponseMessage response1 = client.GetAsync(apiUrl1).Result;
            if (response1.IsSuccessStatusCode)
            {
                state_name = JsonConvert.DeserializeObject<List<State_tbl>>(response1.Content.ReadAsStringAsync().Result);
            }
            ViewBag.state_name = state_name;

            List<County_tbl> county_name = new List<County_tbl>();
            string apiUrl3 = apiBaseUrl + "/County/getAll";


            HttpResponseMessage response2 = client.GetAsync(apiUrl3).Result;
            if (response2.IsSuccessStatusCode)
            {
                county_name = JsonConvert.DeserializeObject<List<County_tbl>>(response2.Content.ReadAsStringAsync().Result);
            }
            ViewBag.county_name = county_name;



            List<Job_cat> job_cat = new List<Job_cat>();
            string apiUrl5 = apiBaseUrl + "/Job_cat/getAll";
            HttpResponseMessage response5 = client.GetAsync(apiUrl5).Result;
            if (response5.IsSuccessStatusCode)
            {
                job_cat = JsonConvert.DeserializeObject<List<Job_cat>>(response5.Content.ReadAsStringAsync().Result);
            }
            ViewBag.job_cat = job_cat;

            List<District_tbl> dist_name = new List<District_tbl>();
            string apiUrl6 = apiBaseUrl + "/District/getAll";


            HttpResponseMessage response6 = client.GetAsync(apiUrl6).Result;
            if (response6.IsSuccessStatusCode)
            {
                dist_name = JsonConvert.DeserializeObject<List<District_tbl>>(response6.Content.ReadAsStringAsync().Result);
            }
            ViewBag.dist_name = dist_name;

            //int id = Convert.ToInt32(HttpContext.Session.GetString("employer_loginid"));

            HttpResponseMessage readdata = await client.GetAsync(apiUrl2 + id);

            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<Job_register_ab>();
                displayresults.Wait();
                seek = displayresults.Result;

            }

            return View(seek);

        }

        public async Task<ActionResult> Update_Org_Jobs(Job_register_ab Job_register_ab)
        {

            HttpClient hc = new HttpClient();

            hc.BaseAddress = new Uri(apiBaseUrl + "/Jobregister/Update");
            var locationconsume = hc.PutAsJsonAsync<Job_register_ab>("Update", Job_register_ab);
            locationconsume.Wait();
            var readdata = locationconsume.Result;

            if (readdata.IsSuccessStatusCode)
            {

                _notyf.Information("Updated Successfully", 15);
                return RedirectToAction("OrgProfile", "Org");

            }
            else
            {
                _notyf.Error("Error..! Try Again..", 15);
                return RedirectToAction("Edit_Org_Jobs", "Org");
            }
            //return View(Job_register_ab);


        }

    }
}
