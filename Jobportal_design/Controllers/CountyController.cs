﻿using AspNetCoreHero.ToastNotification.Abstractions;
using Jobportal_design.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Jobportal_design.Controllers
{
    public class CountyController : Controller
    {
        private IWebHostEnvironment Environment;
        private readonly INotyfService _notyf;
        private IConfiguration _Configure;
        string apiBaseUrl;
        public CountyController(IWebHostEnvironment _environment, INotyfService notyf, IConfiguration configuration)
        {
            Environment = _environment;
            _notyf = notyf;
            _Configure = configuration;
            apiBaseUrl = _Configure.GetValue<string>("WebAPIBaseUrl");
        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Countylist()
        {
            List<County_tbl> customers = new List<County_tbl>();
            string apiUrl = apiBaseUrl + "/County/getAll";
           

            HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync(apiUrl).Result;
            if (response.IsSuccessStatusCode)
            {
                customers = JsonConvert.DeserializeObject<List<County_tbl>>(response.Content.ReadAsStringAsync().Result);
            }

            return View(customers);
            //   return View();
        }
        public IActionResult AddCounty()
        {
            //List<Country_tbl> country = new List<Country_tbl>();
            //string apiUrl = "http://localhost:62332/Country/getAll";

            //HttpClient client = new HttpClient();
            //HttpResponseMessage response = client.GetAsync(apiUrl).Result;
            //if (response.IsSuccessStatusCode)
            //{
            //    country = JsonConvert.DeserializeObject<List<Country_tbl>>(response.Content.ReadAsStringAsync().Result);
            //}
            //ViewBag.message = country;
            return View();

        }

        [HttpPost]
        public async Task<ActionResult> Create(County_tbl county_ab, List<IFormFile> cnty_pic)
        {
            if (county_ab.isactive == "Active")
            {
                county_ab.is_active = true;
                // country_ab.identifier = "true";
            }
            else
            {
                county_ab.is_active = false;
                //  country_ab.identifier = false;
            }

            string wwwPath = this.Environment.WebRootPath;
            string contentPath = this.Environment.ContentRootPath;
            string path = Path.Combine(this.Environment.WebRootPath, "General_uploads");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            List<string> uploadedFiles = new List<string>();
            foreach (IFormFile postedFile in cnty_pic)
            {
                string fileName = Path.GetFileName(postedFile.FileName);
                using (FileStream stream = new FileStream(Path.Combine(path, fileName), FileMode.Create))
                {
                    postedFile.CopyTo(stream);
                    uploadedFiles.Add(fileName);
                    // ViewBag.Message += string.Format("<b>{0}</b> uploaded.<br />", fileName);
                }
                county_ab.cnty_path = path;
                county_ab.cnty_pic = fileName;
            }

            HttpClient hc = new HttpClient();

            hc.BaseAddress = new Uri(apiBaseUrl + "/County/");
           
            var locationconsume = hc.PostAsJsonAsync<County_tbl>("Create", county_ab);
            locationconsume.Wait();
            var readdata = locationconsume.Result;

            if (readdata.IsSuccessStatusCode)
            {
                _notyf.Success("Success", 5);
                return RedirectToAction("Countylist", "County");


            }
            else
            {
                _notyf.Success("Error..! Try Again..", 5);
                return RedirectToAction("Countylist", "County");
            }
            //return View(county_ab);


        }
        public async Task<ActionResult> EditCounty(int id)
        {
            HttpClient client = new HttpClient();
          
            County_tbl county = null;

            string apiUrl1 = apiBaseUrl + "/County/get_by_id/";
        
            HttpResponseMessage readdata = await client.GetAsync(apiUrl1 + id);

            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<County_tbl>();
                displayresults.Wait();
                county = displayresults.Result;

            }

            return View(county);

        }
        public async Task<ActionResult> Update(County_tbl county_ab, List<IFormFile> cnty_pic)
        {

            string wwwPath = this.Environment.WebRootPath;
            string contentPath = this.Environment.ContentRootPath;
            string path = Path.Combine(this.Environment.WebRootPath, "General_uploads");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            List<string> uploadedFiles = new List<string>();
            foreach (IFormFile postedFile in cnty_pic)
            {
                string fileName = Path.GetFileName(postedFile.FileName);
                using (FileStream stream = new FileStream(Path.Combine(path, fileName), FileMode.Create))
                {
                    postedFile.CopyTo(stream);
                    uploadedFiles.Add(fileName);
                    // ViewBag.Message += string.Format("<b>{0}</b> uploaded.<br />", fileName);
                }
                county_ab.cnty_path = path;
                county_ab.cnty_pic = fileName;
            }
            if (county_ab.isactive == "Active")
            {
                county_ab.is_active = true;
            }
            else
            {
                county_ab.is_active = false;
            }
            HttpClient hc = new HttpClient();

            hc.BaseAddress = new Uri(apiBaseUrl + "/County/");
            
            var locationconsume = hc.PutAsJsonAsync<County_tbl>("Update", county_ab);
            locationconsume.Wait();
            var readdata = locationconsume.Result;

            if (readdata.IsSuccessStatusCode)
            {
                _notyf.Information("Updated Successfully", 5);
                return RedirectToAction("Countylist", "County");

            }
            else
            {
                _notyf.Success("Error.! Try Again..", 5);
                return RedirectToAction("Countylist", "County");
            }
            //return View(county_ab);


        }
        public async Task<ActionResult> DeleteCounty(int id)
        {
           
            string apiUrl = apiBaseUrl + "/County/Delete/";
           
            HttpClient client = new HttpClient();

            HttpResponseMessage readdata = await client.GetAsync(apiUrl + id);
           
            if (readdata.IsSuccessStatusCode)
            {
              
                _notyf.Error("Deleted Successfully..!",5);

            }
            else
            {
                _notyf.Error("Error..!", 5);
            }

            return RedirectToAction("Countylist", "County");

        }

    }
}
