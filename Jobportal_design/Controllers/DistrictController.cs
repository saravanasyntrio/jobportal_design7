﻿using AspNetCoreHero.ToastNotification.Abstractions;
using Jobportal_design.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Jobportal_design.Controllers
{
    public class DistrictController : Controller
    {
        private readonly INotyfService _notyf;
        private IConfiguration _Configure;
        string apiBaseUrl;
        public DistrictController(INotyfService notyf, IConfiguration configuration)
        {
            _notyf = notyf;
            _Configure = configuration;
            apiBaseUrl = _Configure.GetValue<string>("WebAPIBaseUrl");
        }
        public IActionResult Districtlist()
        {
            List<District_tbl> customers = new List<District_tbl>();
            List<State_tbl> state = new List<State_tbl>();
            string apiUrl1 = apiBaseUrl + "/State/getAll";
            string apiUrl = apiBaseUrl + "/District/getAll";

            HttpClient client = new HttpClient();
            HttpResponseMessage response1 = client.GetAsync(apiUrl1).Result;
            if (response1.IsSuccessStatusCode)
            {
                state = JsonConvert.DeserializeObject<List<State_tbl>>(response1.Content.ReadAsStringAsync().Result);
            }
            ViewBag.message = state;

            HttpResponseMessage response = client.GetAsync(apiUrl).Result;
            if (response.IsSuccessStatusCode)
            {
                customers = JsonConvert.DeserializeObject<List<District_tbl>>(response.Content.ReadAsStringAsync().Result);
            }

            return View(customers);
        }

        public IActionResult AddDistrict()
        {


            List<State_tbl> state = new List<State_tbl>();
            string apiUrl = apiBaseUrl + "/State/getAll";

            HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync(apiUrl).Result;
            if (response.IsSuccessStatusCode)
            {
                state = JsonConvert.DeserializeObject<List<State_tbl>>(response.Content.ReadAsStringAsync().Result);
            }
            ViewBag.message = state;
            return View();

        }

        [HttpPost]
        public async Task<ActionResult> Create(District_tbl district_ab)
        {
            if (district_ab.isactive == "Active")
            {
                district_ab.is_active = true;
                // country_ab.identifier = "true";
            }
            else
            {
                district_ab.is_active = false;
                //  country_ab.identifier = false;
            }

            HttpClient hc = new HttpClient();

            hc.BaseAddress = new Uri(apiBaseUrl + "/District/");
            var locationconsume = hc.PostAsJsonAsync<District_tbl>("Create", district_ab);
            locationconsume.Wait();
            var readdata = locationconsume.Result;

            if (readdata.IsSuccessStatusCode)
            {
                _notyf.Success("Success",3);
                return RedirectToAction("Districtlist", "District");


            }
            else
            {
                _notyf.Error("Error..! Try Again..", 5);
                return RedirectToAction("Districtlist", "District");
            }
            //return View(district_ab);


        }

        public async Task<ActionResult> EditDistrict(int id)
        {
            HttpClient client = new HttpClient();
            List<State_tbl> state = new List<State_tbl>();
            District_tbl district = null;
            string apiUrl = apiBaseUrl + "/State/getAll";
            string apiUrl1 = apiBaseUrl + "/District/get_by_id/";


            HttpResponseMessage response = client.GetAsync(apiUrl).Result;
            if (response.IsSuccessStatusCode)
            {
                state = JsonConvert.DeserializeObject<List<State_tbl>>(response.Content.ReadAsStringAsync().Result);
            }
            ViewBag.message = state;
            HttpResponseMessage readdata = await client.GetAsync(apiUrl1 + id);
            //  var result = responseTask.Result;
            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<District_tbl>();
                displayresults.Wait();
                district = displayresults.Result;

            }

            return View(district);

        }
        public async Task<ActionResult> DeleteDistrict(int id)
        {
            //Currency_tbl currency = null;
            //IEnumerable<Currency_tbl> locobj = null;
            string apiUrl = apiBaseUrl + "/District/Delete/";

            HttpClient client = new HttpClient();

            HttpResponseMessage readdata = await client.GetAsync(apiUrl + id);
            //  var result = responseTask.Result;
            if (readdata.IsSuccessStatusCode)
            {
                //var displayresults = readdata.Content.ReadAsAsync<Currency_tbl>();
                //displayresults.Wait();
                //currency = displayresults.Result;
                _notyf.Error("Deleted Successfully", 3);

            }
            else
            {
                _notyf.Error("Error..! Try Again..", 5);
               
            }

            return RedirectToAction("Districtlist", "District");

        }

        public async Task<ActionResult> Update(District_tbl district_ab)
        {
            if (district_ab.isactive == "Active")
            {
                district_ab.is_active = true;
            }
            else
            {
                district_ab.is_active = false;
            }
            HttpClient hc = new HttpClient();

            hc.BaseAddress = new Uri(apiBaseUrl + "/District/");
            var locationconsume = hc.PutAsJsonAsync<District_tbl>("Update", district_ab);
            locationconsume.Wait();
            var readdata = locationconsume.Result;

            if (readdata.IsSuccessStatusCode)
            {
                _notyf.Information("Updated Successfully", 3);
                return RedirectToAction("Districtlist", "District");

            }
            else
            {
                _notyf.Error("Error..! Try Again..", 5);
                return RedirectToAction("Districtlist", "District");
            }
            //return View(district_ab);


        }
    }
}
