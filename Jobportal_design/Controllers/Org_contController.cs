﻿using Jobportal_design.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Jobportal_design.Controllers
{
    public class Org_contController : Controller
    {
        private IConfiguration _Configure;
        string apiBaseUrl;
        public Org_contController(IConfiguration configuration)
        {
            
            _Configure = configuration;
            apiBaseUrl = _Configure.GetValue<string>("WebAPIBaseUrl");
        }
        public IActionResult AddOrg_cont()
        {

            List<Org_physical> org_name = new List<Org_physical>();
            string apiUrl = apiBaseUrl + "/Org/getAll";

            HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync(apiUrl).Result;
            if (response.IsSuccessStatusCode)
            {
                org_name = JsonConvert.DeserializeObject<List<Org_physical>>(response.Content.ReadAsStringAsync().Result);
            }
            ViewBag.org_name = org_name;
           

            return View();

        }
        [HttpPost]
        public async Task<ActionResult> Create(Org_cont org_cont)
        {
            if (org_cont.isactive == "1" & org_cont.isactive1=="1")
            {
                org_cont.org_closed = true;
                org_cont.org_inactive = true;
                // country_ab.identifier = "true";
            }
            else
            {
                org_cont.org_closed = false;
                org_cont.org_inactive = false;
                //  country_ab.identifier = false;
            }
            //org_cont.org_name = (HttpContext.Session.GetString("org_name_sess"));

            org_cont.org_id = Convert.ToInt32(HttpContext.Session.GetString("org_id_admin"));




            HttpClient hc = new HttpClient();

            hc.BaseAddress = new Uri(apiBaseUrl + "/Org_cont/Create");
            var locationconsume = hc.PostAsJsonAsync<Org_cont>("Create", org_cont);
            locationconsume.Wait();
            var readdata = locationconsume.Result;

            if (readdata.IsSuccessStatusCode)
            {
                return RedirectToAction("OrgProfile_admin", "Org");


            }
            return View(org_cont);


        }
        public IActionResult Org_contlist()
        {
            List<Org_cont> customers = new List<Org_cont>();
            string apiUrl = apiBaseUrl + "/Org_cont/getAll";

            HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync(apiUrl).Result;
            if (response.IsSuccessStatusCode)
            {
                customers = JsonConvert.DeserializeObject<List<Org_cont>>(response.Content.ReadAsStringAsync().Result);
            }

            return View(customers);
        }
        public async Task<ActionResult> EditOrg_cont()
        {
            Org_cont org = null;

            IEnumerable<Org_cont> locobj = null;

           
            List<Org_physical> org_name = new List<Org_physical>();


            
            string apiUrl1 = apiBaseUrl + "/Org/getAll1";
           
           
            string apiUrl = apiBaseUrl + "/Org_cont/Editget_by_id/";

            HttpClient client = new HttpClient();

          

            HttpResponseMessage response1 = client.GetAsync(apiUrl1).Result;
            if (response1.IsSuccessStatusCode)
            {
                org_name = JsonConvert.DeserializeObject<List<Org_physical>>(response1.Content.ReadAsStringAsync().Result);
            }
            ViewBag.org_name = org_name;

            int id = Convert.ToInt32(HttpContext.Session.GetString("org_id_admin"));


            HttpResponseMessage readdata = await client.GetAsync(apiUrl + id);
            //  var result = responseTask.Result;
            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<Org_cont>();
                displayresults.Wait();
                org = displayresults.Result;

            }

            return View(org);

        }
        public async Task<ActionResult> Update(Org_cont org_cont)
        {
            if (org_cont.isactive == "1" & org_cont.isactive1 == "1")
            {
                org_cont.org_closed = true;
                org_cont.org_inactive = true;
                // country_ab.identifier = "true";
            }
            else
            {
                org_cont.org_closed = false;
                org_cont.org_inactive = true;
                //  country_ab.identifier = false;
            }
            //org_cont.org_name = (HttpContext.Session.GetString("org_name_sess"));
            org_cont.org_id = Convert.ToInt32(HttpContext.Session.GetString("org_id_admin"));


            HttpClient hc = new HttpClient();

            hc.BaseAddress = new Uri(apiBaseUrl + "/Org_cont/");
            var locationconsume = hc.PutAsJsonAsync<Org_cont>("Update", org_cont);
            locationconsume.Wait();
            var readdata = locationconsume.Result;

            if (readdata.IsSuccessStatusCode)
            {

                return RedirectToAction("OrgProfile_admin", "Org");

            }
            return View(org_cont);


        }
        public async Task<ActionResult> DeleteOrg_cont(int id)
        {
            //Currency_tbl currency = null;
            //IEnumerable<Currency_tbl> locobj = null;
            string apiUrl = apiBaseUrl + "/Org_cont/Delete/";

            HttpClient client = new HttpClient();

            HttpResponseMessage readdata = await client.GetAsync(apiUrl + id);
            //  var result = responseTask.Result;
            if (readdata.IsSuccessStatusCode)
            {
                //var displayresults = readdata.Content.ReadAsAsync<Currency_tbl>();
                //displayresults.Wait();
                //currency = displayresults.Result;

            }

            return RedirectToAction("Org_contlist", "Org_cont");

        }

    }
}
