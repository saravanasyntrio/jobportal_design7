﻿using AspNetCoreHero.ToastNotification.Abstractions;
using Jobportal_design.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Jobportal_design.Controllers
{
    public class HomeController : Controller
    {
        private IWebHostEnvironment Environment;
        private readonly INotyfService _notyf;
        private IConfiguration _Configure;
        string apiBaseUrl;

        public HomeController(IWebHostEnvironment _environment, INotyfService notyf, IConfiguration configuration)
        {
            Environment = _environment;
            _notyf = notyf;
            _Configure = configuration;
            apiBaseUrl = _Configure.GetValue<string>("WebAPIBaseUrl");
        }

        public async Task<ActionResult> Index(string jobtypetext, string categorytext, string exptext, string searchval)
        {
            List<job_reg_tbl> jobs = new List<job_reg_tbl>();
            List<Org_cat> orgcat = new List<Org_cat>();

           // string apiUrl = apiBaseUrl + "/Jobregister/get_by_iddemo/";
            string apiUrl = apiBaseUrl + "/Jobregister/getLatestjobs/";
            string apiUrl1 = apiBaseUrl + "/Org/getTopOrganizations/";
            string apiUrl2 = apiBaseUrl + "/Job_cat/getJobcategoryWithJobCount/";
            string apiUrl3 = apiBaseUrl + "/Jobregister/get_County_with_jobcount";

            HttpClient client = new HttpClient();


            if (string.IsNullOrEmpty(jobtypetext))
            {
                jobtypetext = "all";
            }
            if (string.IsNullOrEmpty(categorytext))
            {
                categorytext = "all";
            }
            if (string.IsNullOrEmpty(exptext))
            {
                exptext = "all";
            }
            if (string.IsNullOrEmpty(searchval))
            {
                searchval = "all";
            }

            // HttpResponseMessage response = client.GetAsync(apiUrl + jobtypetext + "/" + categorytext + "/" + exptext + "/" + searchval).Result;
            HttpResponseMessage response = client.GetAsync(apiUrl).Result;
            if (response.IsSuccessStatusCode)
            {
                jobs = JsonConvert.DeserializeObject<List<job_reg_tbl>>(response.Content.ReadAsStringAsync().Result);
            }
            ViewBag.viewjobs = jobs;
            ViewBag.countjobs = jobs.Count;


            List<Org_physical> customers = new List<Org_physical>();
            //if (searchval == null)
            //    searchval = "All";
            HttpResponseMessage response1 = client.GetAsync(apiUrl1).Result;

            if (response1.IsSuccessStatusCode)
            {
                customers = JsonConvert.DeserializeObject<List<Org_physical>>(response1.Content.ReadAsStringAsync().Result);
            }
            ViewBag.customers = customers;
            ViewBag.OrgCount = customers.Count;

            List<seek_tbl> seek = new List<seek_tbl>();
            string apiUrl5 = apiBaseUrl + "/Seek/getAll";
            HttpResponseMessage response5 = client.GetAsync(apiUrl5).Result;
            if (response5.IsSuccessStatusCode)
            {
                seek = JsonConvert.DeserializeObject<List<seek_tbl>>(response5.Content.ReadAsStringAsync().Result);
            }
            ViewBag.seek = seek.Count();

            List<Job_cat_count> category = new List<Job_cat_count>();
            HttpResponseMessage response2 = client.GetAsync(apiUrl2).Result;

            if (response2.IsSuccessStatusCode)
            {
                category = JsonConvert.DeserializeObject<List<Job_cat_count>>(response2.Content.ReadAsStringAsync().Result);
            }
            ViewBag.category = category;
            ViewBag.Job_catCount = category.Count();
            List<County_tbl> county = new List<County_tbl>();

            List<Org_physical> org_name = new List<Org_physical>();
            string apiUrl6 = apiBaseUrl + "/Org/getAll1/";
            HttpResponseMessage response6 = client.GetAsync(apiUrl6).Result;
            if (response6.IsSuccessStatusCode)
            {
                org_name = JsonConvert.DeserializeObject<List<Org_physical>>(response6.Content.ReadAsStringAsync().Result);
            }
            ViewBag.org_name = org_name;
            ViewBag.org_count = org_name.Count();

            HttpResponseMessage response3 = client.GetAsync(apiUrl3).Result;
            if (response3.IsSuccessStatusCode)
            {
                county = JsonConvert.DeserializeObject<List<County_tbl>>(response3.Content.ReadAsStringAsync().Result);
            }
            ViewBag.county = county;
            
            return View(jobs);
            // return View();
        }
        //[HttpGet("{jobtypetext}/{search}/{county_id}/{job_cat_id}")]
        public async Task<ActionResult> FindJob(string jobtypetext,  string search, int county_id,int job_cat_id,jobFilterValues category)
        {
            List<job_reg_tbl> jobs = new List<job_reg_tbl>();
            List<Job_cat> job_cat = new List<Job_cat>();

            string apiUrl1 = apiBaseUrl + "/County/getAllActiveCounty";
            string apiUrl = apiBaseUrl + "/Jobregister/get_by_iddemo/";
            HttpClient client = new HttpClient();
            if (category.filterOn != "true")
            {
                string categorytext = job_cat_id.ToString();
                string exptext = county_id.ToString();
                string searchval = search;

                if (string.IsNullOrEmpty(jobtypetext))
                {
                    jobtypetext = "all";
                }
                if (string.IsNullOrEmpty(categorytext))
                {
                    categorytext = "all";
                }
                if (string.IsNullOrEmpty(exptext))
                {
                    exptext = "all";
                }
                if (string.IsNullOrEmpty(searchval))
                {
                    searchval = "all";
                }

                HttpResponseMessage response = client.GetAsync(apiUrl + jobtypetext + "/" + categorytext + "/" + exptext + "/" + searchval).Result;
                if (response.IsSuccessStatusCode)
                {
                    jobs = JsonConvert.DeserializeObject<List<job_reg_tbl>>(response.Content.ReadAsStringAsync().Result);
                }
                //HttpContext.Session.SetString("admin_jobseeker_loginid", jobs.ToString());
                ViewBag.viewjobs = jobs;
                ViewBag.countjobs = jobs.Count;
                ViewBag.searchText = search;

            }
            else
            {
              //  List<job_reg_tbl> jobs = new List<job_reg_tbl>();

                List<job_title> jobTitleList = new List<job_title>();
                List<String> jobFilterValues = new List<String>();

                List<String> jobTitleValues = new List<String>();
                List<String> jobTypeValues = new List<String>();
                List<String> jobCategoryValues = new List<String>();
                List<String> filterValues = new List<String>();
                JobFilter jbFilter = new JobFilter();
                if (category.Administrator == "on")
                {
                    //  jobTitleList.Add(new job_title { Text = "Administrator/Director", Value = "Administrator/Director" });
                    jobTitleValues.Add("Administrator/Director");
                    filterValues.Add("Administrator");
                }
                if (category.AssistantTeacher == "on")
                {
                    //jobTitleList.Add(new job_title { Text = "Assistant Teacher", Value = "Assistant Teacher" });
                    jobTitleValues.Add("Assistant Teacher");
                    filterValues.Add("AssistantTeacher");
                }
                if (category.Floater == "on")
                {
                    //jobTitleList.Add(new job_title { Text = "Floater", Value = "Floater" });
                    jobTitleValues.Add("Floater");
                    filterValues.Add("Floater");
                }
                if (category.LeadTeacher == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Lead Teacher", Value = "Lead Teacher" });
                    jobTitleValues.Add("Lead Teacher");
                    filterValues.Add("LeadTeacher");
                }
                if (category.Substitute == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Substitute", Value = "Substitute" });
                    jobTitleValues.Add("Substitute");
                    filterValues.Add("Substitute");
                }
                if (category.DayCampAdministrator == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Day Camp Administrator", Value = "Day Camp Administrator" });
                    jobTitleValues.Add("Day Camp Administrator");
                    filterValues.Add("DayCampAdministrator");
                }
                if (category.DayCampJuniorCounselor == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Day Camp Junior Counselor", Value = "Day Camp Junior Counselor" });
                    jobTitleValues.Add("Day Camp Junior Counselor");
                    filterValues.Add("DayCampJuniorCounselor");
                }
                if (category.DayCampLeadCounselor == "on")
                {
                    //  jobTitleList.Add(new job_title { Text = "Day Camp Lead Counselor", Value = "Day Camp Lead Counselor" });
                    jobTitleValues.Add("Day Camp Lead Counselor");
                    filterValues.Add("DayCampLeadCounselor");
                }
                if (category.DayCampOwner == "on")
                {
                    //  jobTitleList.Add(new job_title { Text = "Day Camp Owner", Value = "Day Camp Owner" });
                    jobTitleValues.Add("Day Camp Owner");
                    filterValues.Add("DayCampOwner");
                }
                if (category.FullTime == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Full Time", Value = "Full Time" });
                    jobTypeValues.Add("Full Time");
                    filterValues.Add("FullTime");
                }
                if (category.PartTime == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Part Time", Value = "Part Time" });
                    jobTypeValues.Add("Part Time");
                    filterValues.Add("PartTime");
                }
                if (category.Hybrid == "on")
                {
                    //  jobTitleList.Add(new job_title { Text = "Hybrid", Value = "Hybrid" });
                    jobTypeValues.Add("Hybrid");
                    filterValues.Add("Hybrid");
                }
                if (category.Remote == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Remote", Value = "Remote" });
                    jobTypeValues.Add("Remote");
                    filterValues.Add("Remote");
                }
                if (category.Others == "on")
                {
                    //jobTitleList.Add(new job_title { Text = "Others", Value = "Others" });
                    jobTypeValues.Add("Others");
                    filterValues.Add("Others");
                }

                if (category.InOffice == "on")
                {
                    //  jobTitleList.Add(new job_title { Text = "In Office", Value = "In Office" });
                    jobTypeValues.Add("In Office");
                    filterValues.Add("In Office");
                }
                if (category.Administration == "on")
                {
                    //  jobTitleList.Add(new job_title { Text = "Administration", Value = "Administration" });
                    jobCategoryValues.Add("Administration");
                    filterValues.Add("Administration");
                }
                if (category.Management == "on")
                {
                    //  jobTitleList.Add(new job_title { Text = "Management", Value = "Management" });
                    jobCategoryValues.Add("Management");
                    filterValues.Add("Management");
                }
                if (category.EarlyEducation == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Education", Value = "Education" });
                    jobCategoryValues.Add("Early Education");
                    filterValues.Add("EarlyEducation");
                }



                int seek_id1 = 0;
                //List<Job_cat> job_cat = new List<Job_cat>();


                //string apiUrl1 = apiBaseUrl + "/County/getAllActiveCounty";
                string apiUrlFilter = apiBaseUrl + "/Jobregister/get_job_search_by_filter/";
                jbFilter.jobCategoryValues = jobCategoryValues;
                jbFilter.seek_id = seek_id1;
                jbFilter.jobTitleValues = jobTitleValues;
                jbFilter.jobTypeValues = jobTypeValues;
                //HttpClient client = new HttpClient();

                //  HttpResponseMessage response = client.GetAsync(apiUrl + seek_id+"/" + jobTitleValues + "/" + jobTypeValues+"/"+ jobCategoryValues).Result;
                client.BaseAddress = new Uri(apiBaseUrl + "/Jobregister/");

                var locationconsume = client.PostAsJsonAsync<JobFilter>("get_job_search_by_filter", jbFilter);
                locationconsume.Wait();
                var readdata = locationconsume.Result;

                if (readdata.IsSuccessStatusCode)
                {
                    jobs = JsonConvert.DeserializeObject<List<job_reg_tbl>>(readdata.Content.ReadAsStringAsync().Result);
                }

                ViewBag.viewjobs = jobs;
                ViewBag.countjobs = jobs.Count;
                ViewBag.Adm = category.Administrator;
                ViewBag.cate = filterValues;
                
            }
            string apiUrl2 = apiBaseUrl + "/Job_cat/getAll";
            HttpResponseMessage response2 = client.GetAsync(apiUrl2).Result;
            if (response2.IsSuccessStatusCode)
            {
                job_cat = JsonConvert.DeserializeObject<List<Job_cat>>(response2.Content.ReadAsStringAsync().Result);
            }
            ViewBag.job_cat = job_cat;

            int seek_id = 0;
            string apiUrl3 = apiBaseUrl + "/Jobregister/getJobcategoryWithJobCount/";
            List<Job_cat_count> category1 = new List<Job_cat_count>();
            HttpResponseMessage response3 = client.GetAsync(apiUrl3 + seek_id).Result;

            if (response3.IsSuccessStatusCode)
            {
                category1 = JsonConvert.DeserializeObject<List<Job_cat_count>>(response3.Content.ReadAsStringAsync().Result);
            }
            ViewBag.category = category1;
            ViewBag.Job_catCount = category1.Count();

            string apiUrl4 = apiBaseUrl + "/Jobregister/getJobTitleWithJobCount/";
            List<job_title> jobTitle = new List<job_title>();
            HttpResponseMessage response4 = client.GetAsync(apiUrl4 + seek_id).Result;

            if (response4.IsSuccessStatusCode)
            {
                jobTitle = JsonConvert.DeserializeObject<List<job_title>>(response4.Content.ReadAsStringAsync().Result);
            }
            ViewBag.jobTitle = jobTitle;

            string apiUrl5 = apiBaseUrl + "/Jobregister/getJobTypeWithJobCount/";
            List<job_title> jobType = new List<job_title>();
            HttpResponseMessage response5 = client.GetAsync(apiUrl5 + seek_id).Result;

            if (response5.IsSuccessStatusCode)
            {
                jobType = JsonConvert.DeserializeObject<List<job_title>>(response5.Content.ReadAsStringAsync().Result);
            }
            ViewBag.jobType = jobType;

            List<County_tbl> county = new List<County_tbl>();
            HttpResponseMessage response1 = client.GetAsync(apiUrl1).Result;
            if (response1.IsSuccessStatusCode)
            {
                county = JsonConvert.DeserializeObject<List<County_tbl>>(response1.Content.ReadAsStringAsync().Result);
            }
            ViewBag.county = county;

            return View(jobs);
            
        }
        
       
        public async Task<ActionResult> JobList(string search, int county_id, int job_cat_id, jobFilterValues category)
        {
            List<job_reg_tbl> jobs = new List<job_reg_tbl>();
            List<Org_cat> orgcat = new List<Org_cat>();
            List<Job_cat> job_cat = new List<Job_cat>();

            string apiUrl1 = apiBaseUrl + "/County/getAllActiveCounty";

            string apiUrl = apiBaseUrl + "/Jobregister/get_job_search_by_seeker/";
            HttpClient client = new HttpClient();

            int seek_id = 0;
            seek_id=Convert.ToInt32(HttpContext.Session.GetString("jobseeker_loginid"));

            if (category.filterOn != "true")
            {
                string categorytext = job_cat_id.ToString();
                string county = county_id.ToString();
                string searchval = search;

                if (string.IsNullOrEmpty(categorytext))
                {
                    categorytext = "0";
                }
                if (string.IsNullOrEmpty(county))
                {
                    county = "0";
                }
                if (string.IsNullOrEmpty(searchval))
                {
                    searchval = "all";
                }


                HttpResponseMessage response = client.GetAsync(apiUrl + seek_id + "/" + categorytext + "/" + county + "/" + searchval).Result;
                if (response.IsSuccessStatusCode)
                {
                    jobs = JsonConvert.DeserializeObject<List<job_reg_tbl>>(response.Content.ReadAsStringAsync().Result);
                }
                ViewBag.viewjobs = jobs;
                ViewBag.countjobs = jobs.Count;
                ViewBag.searchText = search;
            }
            else
            {
                List<job_title> jobTitleList = new List<job_title>();
                List<String> jobFilterValues = new List<String>();

                List<String> jobTitleValues = new List<String>();
                List<String> jobTypeValues = new List<String>();
                List<String> jobCategoryValues = new List<String>();
                List<String> filterValues = new List<String>();
                JobFilter jbFilter = new JobFilter();

                if (category.Administrator == "on")
                {
                    //  jobTitleList.Add(new job_title { Text = "Administrator/Director", Value = "Administrator/Director" });
                    jobTitleValues.Add("Administrator/Director");
                    filterValues.Add("Administrator");
                }
                if (category.AssistantTeacher == "on")
                {
                    //jobTitleList.Add(new job_title { Text = "Assistant Teacher", Value = "Assistant Teacher" });
                    jobTitleValues.Add("Assistant Teacher");
                    filterValues.Add("AssistantTeacher");
                }
                if (category.Floater == "on")
                {
                    //jobTitleList.Add(new job_title { Text = "Floater", Value = "Floater" });
                    jobTitleValues.Add("Floater");
                    filterValues.Add("Floater");
                }
                if (category.LeadTeacher == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Lead Teacher", Value = "Lead Teacher" });
                    jobTitleValues.Add("Lead Teacher");
                    filterValues.Add("LeadTeacher");
                }
                if (category.Substitute == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Substitute", Value = "Substitute" });
                    jobTitleValues.Add("Substitute");
                    filterValues.Add("Substitute");
                }
                if (category.DayCampAdministrator == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Day Camp Administrator", Value = "Day Camp Administrator" });
                    jobTitleValues.Add("Day Camp Administrator");
                    filterValues.Add("DayCampAdministrator");
                }
                if (category.DayCampJuniorCounselor == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Day Camp Junior Counselor", Value = "Day Camp Junior Counselor" });
                    jobTitleValues.Add("Day Camp Junior Counselor");
                    filterValues.Add("DayCampJuniorCounselor");
                }
                if (category.DayCampLeadCounselor == "on")
                {
                    //  jobTitleList.Add(new job_title { Text = "Day Camp Lead Counselor", Value = "Day Camp Lead Counselor" });
                    jobTitleValues.Add("Day Camp Lead Counselor");
                    filterValues.Add("DayCampLeadCounselor");
                }
                if (category.DayCampOwner == "on")
                {
                    //  jobTitleList.Add(new job_title { Text = "Day Camp Owner", Value = "Day Camp Owner" });
                    jobTitleValues.Add("Day Camp Owner");
                    filterValues.Add("DayCampOwner");
                }
                if (category.FullTime == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Full Time", Value = "Full Time" });
                    jobTypeValues.Add("Full Time");
                    filterValues.Add("FullTime");
                }
                if (category.PartTime == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Part Time", Value = "Part Time" });
                    jobTypeValues.Add("Part Time");
                    filterValues.Add("PartTime");
                }
                if (category.Hybrid == "on")
                {
                    //  jobTitleList.Add(new job_title { Text = "Hybrid", Value = "Hybrid" });
                    jobTypeValues.Add("Hybrid");
                    filterValues.Add("Hybrid");
                }
                if (category.Remote == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Remote", Value = "Remote" });
                    jobTypeValues.Add("Remote");
                    filterValues.Add("Remote");
                }
                if (category.Others == "on")
                {
                    //jobTitleList.Add(new job_title { Text = "Others", Value = "Others" });
                    jobTypeValues.Add("Others");
                    filterValues.Add("Others");
                }

                if (category.InOffice == "on")
                {
                    //  jobTitleList.Add(new job_title { Text = "In Office", Value = "In Office" });
                    jobTypeValues.Add("In Office");
                    filterValues.Add("In Office");
                }
                if (category.Administration == "on")
                {
                    //  jobTitleList.Add(new job_title { Text = "Administration", Value = "Administration" });
                    jobCategoryValues.Add("Administration");
                    filterValues.Add("Administration");
                }
                if (category.Management == "on")
                {
                    //  jobTitleList.Add(new job_title { Text = "Management", Value = "Management" });
                    jobCategoryValues.Add("Management");
                    filterValues.Add("Management");
                }
                if (category.EarlyEducation == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Education", Value = "Education" });
                    jobCategoryValues.Add("Early Education");
                    filterValues.Add("EarlyEducation");
                }



                //string apiUrl1 = apiBaseUrl + "/County/getAllActiveCounty";
                string apiUrlFilter = apiBaseUrl + "/Jobregister/get_job_search_by_filter/";
                jbFilter.jobCategoryValues = jobCategoryValues;
                jbFilter.seek_id = seek_id;
                jbFilter.jobTitleValues = jobTitleValues;
                jbFilter.jobTypeValues = jobTypeValues;
                //HttpClient client = new HttpClient();

                //  HttpResponseMessage response = client.GetAsync(apiUrl + seek_id+"/" + jobTitleValues + "/" + jobTypeValues+"/"+ jobCategoryValues).Result;
                client.BaseAddress = new Uri(apiBaseUrl + "/Jobregister/");

                var locationconsume = client.PostAsJsonAsync<JobFilter>("get_job_search_by_filter", jbFilter);
                locationconsume.Wait();
                var readdata = locationconsume.Result;

                if (readdata.IsSuccessStatusCode)
                {
                    jobs = JsonConvert.DeserializeObject<List<job_reg_tbl>>(readdata.Content.ReadAsStringAsync().Result);
                }

                ViewBag.viewjobs = jobs;
                ViewBag.countjobs = jobs.Count;
                ViewBag.Adm = category.Administrator;
                ViewBag.cate = filterValues;

            }


            string apiUrl2 = apiBaseUrl + "/Job_cat/getAll";
            HttpResponseMessage response2 = client.GetAsync(apiUrl2).Result;
            if (response2.IsSuccessStatusCode)
            {
                job_cat = JsonConvert.DeserializeObject<List<Job_cat>>(response2.Content.ReadAsStringAsync().Result);
            }
            ViewBag.job_cat = job_cat;
            string apiUrl3 = apiBaseUrl + "/Jobregister/getJobcategoryWithJobCount/";
            List<Job_cat_count> category1 = new List<Job_cat_count>();
            HttpResponseMessage response3 = client.GetAsync(apiUrl3 + seek_id).Result;

            if (response3.IsSuccessStatusCode)
            {
                category1 = JsonConvert.DeserializeObject<List<Job_cat_count>>(response3.Content.ReadAsStringAsync().Result);
            }
            ViewBag.category = category1;
            ViewBag.Job_catCount = category1.Count();

            string apiUrl4 = apiBaseUrl + "/Jobregister/getJobTitleWithJobCount/";
            List<job_title> jobTitle = new List<job_title>();
            HttpResponseMessage response4 = client.GetAsync(apiUrl4 + seek_id).Result;

            if (response4.IsSuccessStatusCode)
            {
                jobTitle = JsonConvert.DeserializeObject<List<job_title>>(response4.Content.ReadAsStringAsync().Result);
            }
            ViewBag.jobTitle = jobTitle;

            string apiUrl5 = apiBaseUrl + "/Jobregister/getJobTypeWithJobCount/";
            List<job_title> jobType = new List<job_title>();
            HttpResponseMessage response5 = client.GetAsync(apiUrl5 + seek_id).Result;

            if (response5.IsSuccessStatusCode)
            {
                jobType = JsonConvert.DeserializeObject<List<job_title>>(response5.Content.ReadAsStringAsync().Result);
            }
            ViewBag.jobType = jobType;

            List<County_tbl> countyList = new List<County_tbl>();


            HttpResponseMessage response1 = client.GetAsync(apiUrl1).Result;
            if (response1.IsSuccessStatusCode)
            {
                countyList = JsonConvert.DeserializeObject<List<County_tbl>>(response1.Content.ReadAsStringAsync().Result);
            }
            ViewBag.county = countyList;

            return View(jobs);
            // return View();
        }
        public async Task<ActionResult> JobList_Category(jobFilterValues category,int job_cat_id, string job_category_name,string job_title, string search, int county_id)
        {

            List<job_reg_tbl> jobs = new List<job_reg_tbl>();
            List<Job_cat> job_cat = new List<Job_cat>();

            string apiUrl1 = apiBaseUrl + "/County/getAllActiveCounty";
            string apiUrl = apiBaseUrl + "/Jobregister/get_jobs_by_category/";
            HttpClient client = new HttpClient();
            if (category.filterOn != "true")
            {
                string county = county_id.ToString();
                string searchval = search;

                if (string.IsNullOrEmpty(job_title))
                {
                    job_title = "all";
                }
                if (string.IsNullOrEmpty(county))
                {
                    county = "0";
                }
                if (string.IsNullOrEmpty(searchval))
                {
                    searchval = "all";
                }



                HttpResponseMessage response = client.GetAsync(apiUrl + job_cat_id).Result;
                if (response.IsSuccessStatusCode)
                {
                    jobs = JsonConvert.DeserializeObject<List<job_reg_tbl>>(response.Content.ReadAsStringAsync().Result);
                }
                //HttpContext.Session.SetString("admin_jobseeker_loginid", jobs.ToString());
                ViewBag.viewjobs = jobs;
                ViewBag.countjobs = jobs.Count;
                ViewBag.job_cat_id = job_cat_id;
                ViewBag.job_category_name = job_category_name;

            }
            else
            {
                //  List<job_reg_tbl> jobs = new List<job_reg_tbl>();

                List<job_title> jobTitleList = new List<job_title>();
                List<String> jobFilterValues = new List<String>();

                List<String> jobTitleValues = new List<String>();
                List<String> jobTypeValues = new List<String>();
                List<String> jobCategoryValues = new List<String>();
                List<String> filterValues = new List<String>();
                JobFilter jbFilter = new JobFilter();
                if (category.Administrator == "on")
                {
                    //  jobTitleList.Add(new job_title { Text = "Administrator/Director", Value = "Administrator/Director" });
                    jobTitleValues.Add("Administrator/Director");
                    filterValues.Add("Administrator");
                }
                if (category.AssistantTeacher == "on")
                {
                    //jobTitleList.Add(new job_title { Text = "Assistant Teacher", Value = "Assistant Teacher" });
                    jobTitleValues.Add("Assistant Teacher");
                    filterValues.Add("AssistantTeacher");
                }
                if (category.Floater == "on")
                {
                    //jobTitleList.Add(new job_title { Text = "Floater", Value = "Floater" });
                    jobTitleValues.Add("Floater");
                    filterValues.Add("Floater");
                }
                if (category.LeadTeacher == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Lead Teacher", Value = "Lead Teacher" });
                    jobTitleValues.Add("Lead Teacher");
                    filterValues.Add("LeadTeacher");
                }
                if (category.Substitute == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Substitute", Value = "Substitute" });
                    jobTitleValues.Add("Substitute");
                    filterValues.Add("Substitute");
                }
                if (category.DayCampAdministrator == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Day Camp Administrator", Value = "Day Camp Administrator" });
                    jobTitleValues.Add("Day Camp Administrator");
                    filterValues.Add("DayCampAdministrator");
                }
                if (category.DayCampJuniorCounselor == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Day Camp Junior Counselor", Value = "Day Camp Junior Counselor" });
                    jobTitleValues.Add("Day Camp Junior Counselor");
                    filterValues.Add("DayCampJuniorCounselor");
                }
                if (category.DayCampLeadCounselor == "on")
                {
                    //  jobTitleList.Add(new job_title { Text = "Day Camp Lead Counselor", Value = "Day Camp Lead Counselor" });
                    jobTitleValues.Add("Day Camp Lead Counselor");
                    filterValues.Add("DayCampLeadCounselor");
                }
                if (category.DayCampOwner == "on")
                {
                    //  jobTitleList.Add(new job_title { Text = "Day Camp Owner", Value = "Day Camp Owner" });
                    jobTitleValues.Add("Day Camp Owner");
                    filterValues.Add("DayCampOwner");
                }
                if (category.FullTime == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Full Time", Value = "Full Time" });
                    jobTypeValues.Add("Full Time");
                    filterValues.Add("FullTime");
                }
                if (category.PartTime == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Part Time", Value = "Part Time" });
                    jobTypeValues.Add("Part Time");
                    filterValues.Add("PartTime");
                }
                if (category.Hybrid == "on")
                {
                    //  jobTitleList.Add(new job_title { Text = "Hybrid", Value = "Hybrid" });
                    jobTypeValues.Add("Hybrid");
                    filterValues.Add("Hybrid");
                }
                if (category.Remote == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Remote", Value = "Remote" });
                    jobTypeValues.Add("Remote");
                    filterValues.Add("Remote");
                }
                if (category.Others == "on")
                {
                    //jobTitleList.Add(new job_title { Text = "Others", Value = "Others" });
                    jobTypeValues.Add("Others");
                    filterValues.Add("Others");
                }

                if (category.InOffice == "on")
                {
                    //  jobTitleList.Add(new job_title { Text = "In Office", Value = "In Office" });
                    jobTypeValues.Add("In Office");
                    filterValues.Add("In Office");
                }
                if (category.Administration == "on")
                {
                    //  jobTitleList.Add(new job_title { Text = "Administration", Value = "Administration" });
                    jobCategoryValues.Add("Administration");
                    filterValues.Add("Administration");
                }
                if (category.Management == "on")
                {
                    //  jobTitleList.Add(new job_title { Text = "Management", Value = "Management" });
                    jobCategoryValues.Add("Management");
                    filterValues.Add("Management");
                }
                if (category.EarlyEducation == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Education", Value = "Education" });
                    jobCategoryValues.Add("Early Education");
                    filterValues.Add("EarlyEducation");
                }



                int seek_id1 = 0;
                //List<Job_cat> job_cat = new List<Job_cat>();


                //string apiUrl1 = apiBaseUrl + "/County/getAllActiveCounty";
                string apiUrlFilter = apiBaseUrl + "/Jobregister/get_job_search_by_filter/";
                jbFilter.jobCategoryValues = jobCategoryValues;
                jbFilter.seek_id = seek_id1;
                jbFilter.jobTitleValues = jobTitleValues;
                jbFilter.jobTypeValues = jobTypeValues;
                //HttpClient client = new HttpClient();

                //  HttpResponseMessage response = client.GetAsync(apiUrl + seek_id+"/" + jobTitleValues + "/" + jobTypeValues+"/"+ jobCategoryValues).Result;
                client.BaseAddress = new Uri(apiBaseUrl + "/Jobregister/");

                var locationconsume = client.PostAsJsonAsync<JobFilter>("get_job_search_by_filter", jbFilter);
                locationconsume.Wait();
                var readdata = locationconsume.Result;

                if (readdata.IsSuccessStatusCode)
                {
                    jobs = JsonConvert.DeserializeObject<List<job_reg_tbl>>(readdata.Content.ReadAsStringAsync().Result);
                }

                ViewBag.viewjobs = jobs;
                ViewBag.countjobs = jobs.Count;
                ViewBag.Adm = category.Administrator;
                ViewBag.cate = filterValues;

            }
            //string categorytext = job_cat_id.ToString();

            string apiUrl2 = apiBaseUrl + "/Job_cat/getAll";
            HttpResponseMessage response2 = client.GetAsync(apiUrl2).Result;
            if (response2.IsSuccessStatusCode)
            {
                job_cat = JsonConvert.DeserializeObject<List<Job_cat>>(response2.Content.ReadAsStringAsync().Result);
            }
            ViewBag.job_cat = job_cat;
            int seek_id = 0;
            string apiUrl3 = apiBaseUrl + "/Jobregister/getJobcategoryWithJobCount/";
            List<Job_cat_count> category1 = new List<Job_cat_count>();
            HttpResponseMessage response3 = client.GetAsync(apiUrl3 + seek_id).Result;

            if (response3.IsSuccessStatusCode)
            {
                category1 = JsonConvert.DeserializeObject<List<Job_cat_count>>(response3.Content.ReadAsStringAsync().Result);
            }
            ViewBag.category = category1;
            ViewBag.Job_catCount = category1.Count();

            string apiUrl4 = apiBaseUrl + "/Jobregister/getJobTitleWithJobCount/";
            List<job_title> jobTitle = new List<job_title>();
            HttpResponseMessage response4 = client.GetAsync(apiUrl4 + seek_id).Result;

            if (response4.IsSuccessStatusCode)
            {
                jobTitle = JsonConvert.DeserializeObject<List<job_title>>(response4.Content.ReadAsStringAsync().Result);
            }
            ViewBag.jobTitle = jobTitle;

            string apiUrl5 = apiBaseUrl + "/Jobregister/getJobTypeWithJobCount/";
            List<job_title> jobType = new List<job_title>();
            HttpResponseMessage response5 = client.GetAsync(apiUrl5 + seek_id).Result;

            if (response5.IsSuccessStatusCode)
            {
                jobType = JsonConvert.DeserializeObject<List<job_title>>(response5.Content.ReadAsStringAsync().Result);
            }
            ViewBag.jobType = jobType;


            List<County_tbl> county1 = new List<County_tbl>();

            HttpResponseMessage response1 = client.GetAsync(apiUrl1).Result;
            if (response1.IsSuccessStatusCode)
            {
                county1 = JsonConvert.DeserializeObject<List<County_tbl>>(response1.Content.ReadAsStringAsync().Result);
            }
            ViewBag.county = county1;
            return View(jobs);
            // return View();
        }

        public IActionResult Contact()
        {
            return View();
        }
        public IActionResult FaQ()
        {
            return View();
        }
        public IActionResult TermsAndConditions()
        {
            return View();
        }
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
