﻿using AspNetCoreHero.ToastNotification.Abstractions;
using Jobportal_design.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Jobportal_design.Controllers
{
    public class SuperadminController : Controller
    {
       
        private IConfiguration _Configure;
        string apiBaseUrl;
        private readonly INotyfService _notyf;
        public SuperadminController( IConfiguration configuration, INotyfService notyf)
        {
            _notyf = notyf;
            _Configure = configuration;
            apiBaseUrl = _Configure.GetValue<string>("WebAPIBaseUrl");
        }
        public IActionResult Index()
        {
            return View();

        }
        public IActionResult Dashboard()
        {
            List<Org_physical> org_name = new List<Org_physical>();
            List<Employer> employer = new List<Employer>();
            List<job_reg_tbl> category = new List<job_reg_tbl>();
            List<seek_tbl> seek = new List<seek_tbl>();
            List<Job_register_ab> jobs = new List<Job_register_ab>();

            string apiUrl = apiBaseUrl + "/Org/getAll1/";
            string apiUrl1 = apiBaseUrl + "/Employer/getAll";
            string apiUrl2 = apiBaseUrl + "/Job_cat/getJobcategoryWithJobCount/";
            string apiUrl3 = apiBaseUrl + "/Seek/getAll";
            string apiUrl4 = apiBaseUrl + "/Jobregister/getAll";

            
            HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync(apiUrl).Result;
            if (response.IsSuccessStatusCode)
            {
                org_name = JsonConvert.DeserializeObject<List<Org_physical>>(response.Content.ReadAsStringAsync().Result);
            }
            ViewBag.org_name = org_name;
            ViewBag.org_count = org_name.Count();

            HttpResponseMessage response1 = client.GetAsync(apiUrl1).Result;
            if (response1.IsSuccessStatusCode)
            {
                employer = JsonConvert.DeserializeObject<List<Employer>>(response1.Content.ReadAsStringAsync().Result);
            }
            ViewBag.employer = employer.Count();

            HttpResponseMessage response2 = client.GetAsync(apiUrl2).Result;
            if (response2.IsSuccessStatusCode)
            {
                category = JsonConvert.DeserializeObject<List<job_reg_tbl>>(response2.Content.ReadAsStringAsync().Result);
            }
            ViewBag.category = category.Count();

            HttpResponseMessage response3 = client.GetAsync(apiUrl3).Result;
            if (response3.IsSuccessStatusCode)
            {
                seek = JsonConvert.DeserializeObject<List<seek_tbl>>(response3.Content.ReadAsStringAsync().Result);
            }
            ViewBag.seek = seek.Count();

            HttpResponseMessage response4 = client.GetAsync(apiUrl4).Result;
            if (response4.IsSuccessStatusCode)
            {
                jobs = JsonConvert.DeserializeObject<List<Job_register_ab>>(response4.Content.ReadAsStringAsync().Result);
            }
            ViewBag.jobs = jobs.Count();
            return View();

        }
        public async Task<ActionResult> Seeker_passwordchange(int id)
        {
            HttpClient client = new HttpClient();


            seek_tbl seek = null;

            string apiUrl4 = apiBaseUrl + "/Seek/get_by_id_profile/";


            HttpResponseMessage readdata = await client.GetAsync(apiUrl4 + id);

            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<seek_tbl>();
                displayresults.Wait();
                seek = displayresults.Result;

            }

            return View(seek);

        }
        public async Task<ActionResult> UpdatePassword_Seeker(seek_tbl seek)
        {

            HttpClient client = new HttpClient();

            seek_tbl Seek = null;
            int id = seek.id;
            string apiUrl2 = apiBaseUrl + "/Seek/get_by_id_profile/";

            HttpResponseMessage readdata = await client.GetAsync(apiUrl2 + id);

            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<seek_tbl>();
                displayresults.Wait();
                Seek = displayresults.Result;

            }

            Seek.e_password = seek.e_password; //old password
            
            string new_password = seek.old_password; //new password
            string seekname = seek.fname;
            string emp_repassword = seek.retype_password; //retype

            seek.id = id;
           
            if (new_password != emp_repassword)
            {
                //_notyf.Error("Password Does not Match", 5);
                TempData["Repassword"] = "New password Does not match with Re-Type password.. enter again.!";
                
                _notyf.Success("Error..! Try Again..", 5);
                return RedirectToAction("Seeker_passwordchange", "Superadmin", new { id = seek.id });

            }
            else
            {
                HttpClient hc = new HttpClient();

                hc.BaseAddress = new Uri(apiBaseUrl + "/Seek/");
                var locationconsume = hc.PutAsJsonAsync<seek_tbl>("Seeker_Updatepassword", seek);
                locationconsume.Wait();
                var readdata1 = locationconsume.Result;

                if (readdata1.IsSuccessStatusCode)
                {
                    using (MailMessage mm = new MailMessage("childplus11@gmail.com", "childplus11@gmail.com"))
                    {
                        
                        mm.Subject = "Password Change Request";
                        mm.Body = "Hi " + seekname + " , Your Password has been successfully changed !....New Password is'" + seek.old_password + "'.";

                        //mm.Body = "sincerely, Starting Point";
                        //if (model.Attachment.Length > 0)
                        //{
                        //    string fileName = Path.GetFileName(model.Attachment.FileName);
                        //    mm.Attachments.Add(new Attachment(model.Attachment.OpenReadStream(), fileName));
                        //}
                        mm.IsBodyHtml = false;
                        using (SmtpClient smtp = new SmtpClient())
                        {
                            smtp.Host = "smtp.gmail.com";
                            smtp.EnableSsl = true;
                            NetworkCredential NetworkCred = new NetworkCredential("childplus11@gmail.com", "mgtxdfwmpbqzrtil");
                            smtp.UseDefaultCredentials = true;
                            smtp.Credentials = NetworkCred;
                            smtp.Port = 587;
                            smtp.Send(mm);
                            ViewBag.Message = "Email sent.";
                        }
                    }

                    _notyf.Information("Password changed..!", 5);
                    //return RedirectToAction("jobregisterlist", "Jobregister");

                }
                else
                {
                    _notyf.Success("Error..! Try Again..", 5);
                    return RedirectToAction("Seeker_passwordchange", "Superadmin", new { id = seek.id });
                }
                return RedirectToAction("Admin_seek_list", "Jobseeker");




            }

        }
        public IActionResult list()
        {
            return View();

        }
       
        public IActionResult Role()
        {
            List<adminrole_tbl> roles = new List<adminrole_tbl>();
            string apiUrl = apiBaseUrl + "/AddRole/getAll";

            HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync(apiUrl).Result;
            if (response.IsSuccessStatusCode)
            {
                roles = JsonConvert.DeserializeObject<List<adminrole_tbl>>(response.Content.ReadAsStringAsync().Result);

            }
            roles.Insert(0, new adminrole_tbl { id = 0, roletype = "Select Role" });
            ViewBag.roles = roles;

            List<sadmin_tbl> admin = new List<sadmin_tbl>();
            string apiUrl1 = apiBaseUrl + "/Superadmin/getAll";
          

            HttpResponseMessage response1 = client.GetAsync(apiUrl1).Result;
            if (response1.IsSuccessStatusCode)
            {
                admin = JsonConvert.DeserializeObject<List<sadmin_tbl>>(response1.Content.ReadAsStringAsync().Result);
            }
            admin.Insert(0, new sadmin_tbl { id = 0, name = "Select User" });
            ViewBag.admin = admin;
            return View();

        }
       
        public IActionResult Register()
        {

            List<adminrole_tbl> role = new List<adminrole_tbl>();
            string apiUrl = apiBaseUrl + "/AddRole/getAll";
           
            HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync(apiUrl).Result;
            if (response.IsSuccessStatusCode)
            {
                role = JsonConvert.DeserializeObject<List<adminrole_tbl>>(response.Content.ReadAsStringAsync().Result);
            }
            ViewBag.message = role;
            return View();

        }
        public async Task<IActionResult> HomepageAdmin()
        {
            List<Org_physical> org_name = new List<Org_physical>();
            List<Employer> employer = new List<Employer>();

            string apiUrl = apiBaseUrl + "/Org/getAll1";
            string apiUrl1 = apiBaseUrl + "/Employer/getAll";

            HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync(apiUrl).Result;
            if (response.IsSuccessStatusCode)
            {
                org_name = JsonConvert.DeserializeObject<List<Org_physical>>(response.Content.ReadAsStringAsync().Result);
            }
            ViewBag.org_name = org_name;
            ViewBag.org_count = org_name.Count();

            HttpResponseMessage response1 = client.GetAsync(apiUrl1).Result;
            if (response.IsSuccessStatusCode)
            {
                employer = JsonConvert.DeserializeObject<List<Employer>>(response1.Content.ReadAsStringAsync().Result);
            }
            ViewBag.employer = employer.Count();

            return View();
        }

        public IActionResult Create()
        {
            return View();
        }

        public IActionResult Adminlist()
        {
            List<sadmin_tbl> adminlist = new List<sadmin_tbl>();
            string apiUrl = apiBaseUrl + "/Superadmin/getAllAdmin";
           
            HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync(apiUrl).Result;
            if (response.IsSuccessStatusCode)
            {
                adminlist = JsonConvert.DeserializeObject<List<sadmin_tbl>>(response.Content.ReadAsStringAsync().Result);
            }

            return View(adminlist);
        }
        public async Task<ActionResult> admin_status(string activeStat, int passedId, sadmin_tbl sadmin)
        {
            //Employer.id = Convert.ToInt32(HttpContext.Session.GetString("employer_loginid"));

            if (sadmin.activeStat == "Active")
            {
                sadmin.isactive = "InActive";
                sadmin.status = false;
            }
            else
            {
                sadmin.isactive = "Active";
                sadmin.status = true;
            }
            sadmin.id = sadmin.passedId;
            HttpClient hc = new HttpClient();

            hc.BaseAddress = new Uri(apiBaseUrl + "/Superadmin/");
            var locationconsume = hc.PutAsJsonAsync<sadmin_tbl>("Update_status", sadmin);
            locationconsume.Wait();
            var readdata = locationconsume.Result;

            if (readdata.IsSuccessStatusCode)
            {

                _notyf.Information("Updated Successfully", 5);
                return RedirectToAction("Adminlist", "Superadmin");

            }
            else
            {
                _notyf.Error("Error..! Try Again..", 5);
                return RedirectToAction("Adminlist", "Superadmin");
            }
            //return View(Employer);


        }
        [HttpPost]
        public async Task<ActionResult> Create(sadmin_tbl sdamin_ab)
        {

            if (sdamin_ab.isactive == "True")
            {
                sdamin_ab.status = true;
            }
            else
            {
                sdamin_ab.status = false;
            }
            sdamin_ab.isactive = "Active";
            sdamin_ab.employee_type = sdamin_ab.roletype;
            HttpClient hc = new HttpClient();

            hc.BaseAddress = new Uri(apiBaseUrl + "/Superadmin/");

            var locationconsume = hc.PostAsJsonAsync<sadmin_tbl>("Create", sdamin_ab);
            locationconsume.Wait();
            var readdata = locationconsume.Result;

            if (readdata.IsSuccessStatusCode)
            {

                return RedirectToAction("Signin", "Login");

            }
            return View(sdamin_ab);


        }
        [HttpPost]
        public async Task<ActionResult> Rolecreate(adminrole_tbl adminrole_ab)
        {


            HttpClient hc = new HttpClient();

            hc.BaseAddress = new Uri(apiBaseUrl + "/AddRole/");
          
            var locationconsume = hc.PostAsJsonAsync<adminrole_tbl>("Create", adminrole_ab);
            locationconsume.Wait();
            var readdata = locationconsume.Result;

            if (readdata.IsSuccessStatusCode)
            {

                return RedirectToAction("Role", "Superadmin");

            }
            return View(adminrole_ab);


        }


        public async Task<ActionResult> Rolestatus(string drpvalue, string drptext, string rbtn)
        {

            adminrole_tbl adminrole_ab = new adminrole_tbl();
            adminrole_ab.id = Convert.ToInt32(drpvalue);
            adminrole_ab.roletype = drptext;
            adminrole_ab.isactive = rbtn;

            HttpClient hc = new HttpClient();

            hc.BaseAddress = new Uri(apiBaseUrl + "/AddRole/");
            
            var locationconsume = hc.PutAsJsonAsync<adminrole_tbl>("Updatestatus", adminrole_ab);
            locationconsume.Wait();
            var readdata = locationconsume.Result;

            if (readdata.IsSuccessStatusCode)
            {

                return RedirectToAction("Role", "Superadmin");

            }
            return View(adminrole_ab);


        }
        public async Task<ActionResult> Edit(int id)
        {
            sadmin_tbl sadmin = null;
            string apiUrl = apiBaseUrl + "/Superadmin/get_by_id/";
           
            HttpClient client = new HttpClient();

            HttpResponseMessage readdata = await client.GetAsync(apiUrl + id);

            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<sadmin_tbl>();
                displayresults.Wait();
                sadmin = displayresults.Result;

            }

            return View(sadmin);

        }

        public async Task<ActionResult> Delete(int id)
        {

            string apiUrl = apiBaseUrl + "/Superadmin/Delete/";
           
            HttpClient client = new HttpClient();

            HttpResponseMessage readdata = await client.GetAsync(apiUrl + id);
            //  var result = responseTask.Result;
            if (readdata.IsSuccessStatusCode)
            {

            }

            return RedirectToAction("Adminlist", "Superadmin");

        }
        public IActionResult ChangePassword()
        {
            return View();
        }
        public async Task<ActionResult> UpdatePassword(sadmin_tbl org)
        {

            HttpClient client = new HttpClient();
            List<Org_physical> org_name = new List<Org_physical>();
            sadmin_tbl superadmin = null;
            string apiUrl2 = apiBaseUrl + "/Superadmin/get_by_id/";
            int id = Convert.ToInt32(HttpContext.Session.GetString("admin_loginid"));
            HttpResponseMessage readdata = await client.GetAsync(apiUrl2 + id);

            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<sadmin_tbl>();
                displayresults.Wait();
                superadmin = displayresults.Result;

            }

            string org_password = org.new_password;
            string orgname = superadmin.name;
            string OldPasswrd = org.a_password;
            superadmin.a_password = org_password;
            superadmin.id = id;
            superadmin.old_password = OldPasswrd;
            string org_repassword = org.retype_password;
            if (org_password != org_repassword)
            {
                TempData["Repassword"] = "New password Does not match with Re-Type password.. enter again.!";
                _notyf.Error("Error..! Try Again..", 5);
                return RedirectToAction("ChangePassword", "Superadmin");
            }
            else
            {
                HttpClient hc = new HttpClient();

                hc.BaseAddress = new Uri(apiBaseUrl + "/Superadmin/");
                var locationconsume = hc.PutAsJsonAsync<sadmin_tbl>("Updatepassword", superadmin);
                locationconsume.Wait();
                var readdata1 = locationconsume.Result;

                if (readdata1.IsSuccessStatusCode)
                {
                    using (MailMessage mm = new MailMessage("childplus11@gmail.com", "childplus11@gmail.com"))
                    {
                        mm.Subject = "Password Changed";
                        mm.Body = "Hi " + orgname + " , Your Password has been successfully changed !....New Password '" + superadmin.a_password + "'";
                        //if (model.Attachment.Length > 0)
                        //{
                        //    string fileName = Path.GetFileName(model.Attachment.FileName);
                        //    mm.Attachments.Add(new Attachment(model.Attachment.OpenReadStream(), fileName));
                        //}
                        mm.IsBodyHtml = false;
                        using (SmtpClient smtp = new SmtpClient())
                        {
                            smtp.Host = "smtp.gmail.com";
                            smtp.EnableSsl = true;
                            NetworkCredential NetworkCred = new NetworkCredential("childplus11@gmail.com", "mgtxdfwmpbqzrtil");
                            smtp.UseDefaultCredentials = true;
                            smtp.Credentials = NetworkCred;
                            smtp.Port = 587;
                            smtp.Send(mm);
                            ViewBag.Message = "Email sent.";
                        }
                    }
                    //_notyf.Information("Updated Successfully", 3);
                    //return RedirectToAction("jobregisterlist", "Jobregister");
                    _notyf.Success("Password changed Successfully", 5);
                    return RedirectToAction("Signin", "Login");
                }

                else
                {
                    _notyf.Error("Error..! Try Again..", 5);
                    TempData["Oldpassword"] = "Old Password is incorrect.. Re-enter the correct Password!";
                    return RedirectToAction("Changepassword", "Org");
                }




            }

        }
        public async Task<ActionResult> Rolechange(string drpuservalue, string drpusertext, string drpvalue, string drptext, string rbtn)
        {
            sadmin_tbl sadmin_ab = new sadmin_tbl();
            sadmin_ab.id = Convert.ToInt32(drpuservalue);
            sadmin_ab.name = drpusertext;
            sadmin_ab.employee_type = drptext;
            sadmin_ab.roleid = Convert.ToInt32(drpvalue);
            sadmin_ab.isactive = rbtn;
            int id = sadmin_ab.id;
            HttpClient client = new HttpClient();

            sadmin_tbl sadmin = null;

            string apiUrl2 = apiBaseUrl + "/Superadmin/get_by_data/";
           

            HttpResponseMessage readdata = await client.GetAsync(apiUrl2 + id);

            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<sadmin_tbl>();
                displayresults.Wait();
                sadmin = displayresults.Result;

            }
            sadmin_ab.email_id = sadmin.email_id;
            sadmin_ab.a_password = sadmin.a_password;
            sadmin_ab.email_id = sadmin.email_id;
            sadmin_ab.contact1 = sadmin.contact1;
            sadmin_ab.contact2 = sadmin.contact2;
            sadmin_ab.start_date = sadmin.start_date;
            sadmin_ab.end_date = sadmin.end_date;
            sadmin_ab.status = sadmin.status;
            sadmin_ab.contact1 = sadmin.contact1;
            sadmin_ab.contact2 = sadmin.contact2;
            sadmin_ab.a_token = sadmin.a_token;
            sadmin_ab.last_login = sadmin.last_login;
            HttpClient hc = new HttpClient();


            hc.BaseAddress = new Uri(apiBaseUrl + "/Superadmin/");
           
            var locationconsume = hc.PutAsJsonAsync<sadmin_tbl>("Update", sadmin_ab);
            locationconsume.Wait();
            var readdata1 = locationconsume.Result;

            if (readdata1.IsSuccessStatusCode)
            {

                return RedirectToAction("Role", "Superadmin");

            }
            return View(sadmin_ab);


        }
    }
}
