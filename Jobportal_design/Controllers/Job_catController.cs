﻿using AspNetCoreHero.ToastNotification.Abstractions;
using Jobportal_design.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Jobportal_design.Controllers
{
    public class Job_catController : Controller
    {
        private readonly INotyfService _notyf;
        private IConfiguration _Configure;
        string apiBaseUrl;
        public Job_catController(INotyfService notyf, IConfiguration configuration)
        {
            _notyf = notyf;
            _Configure = configuration;
            apiBaseUrl = _Configure.GetValue<string>("WebAPIBaseUrl");
        }
        public IActionResult Job_cat_list()
        {
            List<Job_cat> category = new List<Job_cat>();
            string apiUrl = apiBaseUrl + "/Job_cat/getAll";

            HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync(apiUrl).Result;
            if (response.IsSuccessStatusCode)
            {
                category = JsonConvert.DeserializeObject<List<Job_cat>>(response.Content.ReadAsStringAsync().Result);
            }
            
            return View(category);
        }
        public IActionResult Add_job_cat()
        {

            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Create(Job_cat job_cat_ab)
        {


            if (job_cat_ab.isactive == "Active")
            {
                job_cat_ab.is_active = true;
                // country_ab.identifier = "true";
            }
            else
            {
               job_cat_ab.is_active = false;
                //  country_ab.identifier = false;
            }

            HttpClient hc = new HttpClient();

            hc.BaseAddress = new Uri(apiBaseUrl + "/Job_cat/");
            var locationconsume = hc.PostAsJsonAsync<Job_cat>("Create", job_cat_ab);
            locationconsume.Wait();
            var readdata = locationconsume.Result;

            if (readdata.IsSuccessStatusCode)
            {
                _notyf.Success("Success", 5);
                return RedirectToAction("Job_cat_list", "Job_cat");

            }
            else
            {
                _notyf.Error("Error..! Try Again..", 5);
                return RedirectToAction("Job_cat_list", "Job_cat");
            }
            //return View(org_cat_ab);


        }

        public async Task<ActionResult> Edit_job_cat(int id)
        {
            Job_cat job_cat = null;
            string apiUrl = apiBaseUrl + "/Job_cat/get_by_id1/";
            HttpClient client = new HttpClient();

            HttpResponseMessage readdata = await client.GetAsync(apiUrl + id);

            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<Job_cat>();
                displayresults.Wait();
                job_cat = displayresults.Result;

            }
            if (job_cat.is_active == true)
            {
                job_cat.isactive = "Active";
            }
            else
            {
                job_cat.isactive = "InActive";
            }

            return View(job_cat);

        }

        public async Task<ActionResult> Update(Job_cat job_cat_ab)
        {
            if (job_cat_ab.isactive == "Active")
            {
                job_cat_ab.is_active = true;
            }
            else
            {
                job_cat_ab.is_active = false;
            }
            HttpClient hc = new HttpClient();

            hc.BaseAddress = new Uri(apiBaseUrl + "/Job_cat/");
            var locationconsume = hc.PutAsJsonAsync<Job_cat>("Update", job_cat_ab);
            locationconsume.Wait();
            var readdata = locationconsume.Result;

            if (readdata.IsSuccessStatusCode)
            {
                _notyf.Information("Updated Successfully", 5);
                return RedirectToAction("Job_cat_list", "Job_cat");

            }
            else
            {
                _notyf.Success("Error..! Try Again.", 5);
                return RedirectToAction("Job_cat_list", "Job_cat");
            }
            return View(job_cat_ab);


        }
      

    }
}
