﻿using AspNetCoreHero.ToastNotification.Abstractions;
using Jobportal_design.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Jobportal_design.Controllers
{
    public class CountryController : Controller
    {
        private readonly INotyfService _notyf;
        private IConfiguration _Configure;
        string apiBaseUrl;
        public CountryController(INotyfService notyf, IConfiguration configuration)
        {
            _notyf = notyf;
            _Configure = configuration;
            apiBaseUrl = _Configure.GetValue<string>("WebAPIBaseUrl");
        }
        public IActionResult Countrylist()
        {
            List<Country_tbl> customers = new List<Country_tbl>();
            string apiUrl = apiBaseUrl + "/Country/getAll";

            HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync(apiUrl).Result;
            if (response.IsSuccessStatusCode)
            {
                customers = JsonConvert.DeserializeObject<List<Country_tbl>>(response.Content.ReadAsStringAsync().Result);
            }

            return View(customers);
            //   return View();
        }
        public IActionResult AddCountry()
        {


            List<Currency_tbl> currency = new List<Currency_tbl>();
            string apiUrl = apiBaseUrl + "/Currency/getAll";

            HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync(apiUrl).Result;
            if (response.IsSuccessStatusCode)
            {
                currency = JsonConvert.DeserializeObject<List<Currency_tbl>>(response.Content.ReadAsStringAsync().Result);
            }
            ViewBag.message = currency;
            return View();

        }

        [HttpPost]
        public async Task<ActionResult> Create(Country_tbl country_ab)
        {
            if (country_ab.isactive == "Active")
            {
                country_ab.is_active = true;
               // country_ab.identifier = "true";
            }
            else
            {
                country_ab.is_active = false;
              //  country_ab.identifier = false;
            }

            HttpClient hc = new HttpClient();
          
            hc.BaseAddress = new Uri(apiBaseUrl + "/Country/");
            var locationconsume = hc.PostAsJsonAsync<Country_tbl>("Create", country_ab);
            locationconsume.Wait();
            var readdata = locationconsume.Result;

            if (readdata.IsSuccessStatusCode)
            {
                _notyf.Success("Success",3);
                return RedirectToAction("Countrylist", "Country");


            }
            return View(country_ab);


        }

        public async Task<ActionResult> EditCountry(int id)
        {
            HttpClient client = new HttpClient();
            List<Currency_tbl> currency = new List<Currency_tbl>();
            Country_tbl country = null;
            string apiUrl = apiBaseUrl + "/Currency/getAll";
            string apiUrl1 = apiBaseUrl + "/Country/get_by_id/";
            HttpResponseMessage response = client.GetAsync(apiUrl).Result;
            if (response.IsSuccessStatusCode)
            {
                currency = JsonConvert.DeserializeObject<List<Currency_tbl>>(response.Content.ReadAsStringAsync().Result);
            }
            ViewBag.message = currency;
            HttpResponseMessage readdata = await client.GetAsync(apiUrl1 + id);
         
            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<Country_tbl>();
                displayresults.Wait();
                country = displayresults.Result;

            }

            return View(country);

        }
        public async Task<ActionResult> DeleteCountry(int id)
        {
            //Currency_tbl currency = null;
            //IEnumerable<Currency_tbl> locobj = null;
            string apiUrl = apiBaseUrl + "/Country/Delete/";

            HttpClient client = new HttpClient();

            HttpResponseMessage readdata = await client.GetAsync(apiUrl + id);
            //  var result = responseTask.Result;
            if (readdata.IsSuccessStatusCode)
            {
                //var displayresults = readdata.Content.ReadAsAsync<Currency_tbl>();
                //displayresults.Wait();
                //currency = displayresults.Result;
                _notyf.Error("Deleted Successfully", 3);

            }

            return RedirectToAction("Countrylist", "Country");

        }

        public async Task<ActionResult> Update(Country_tbl country_ab)
        {
            if (country_ab.isactive == "Active")
            {
                country_ab.is_active = true;
            }
            else
            {
                country_ab.is_active = false;
            }
            HttpClient hc = new HttpClient();

            hc.BaseAddress = new Uri(apiBaseUrl + "/Country/");
            var locationconsume = hc.PutAsJsonAsync<Country_tbl>("Update", country_ab);
            locationconsume.Wait();
            var readdata = locationconsume.Result;

            if (readdata.IsSuccessStatusCode)
            {
                _notyf.Information("Updated Successfully",3);
                return RedirectToAction("Countrylist", "Country");

            }
            return View(country_ab);


        }

    }
}

