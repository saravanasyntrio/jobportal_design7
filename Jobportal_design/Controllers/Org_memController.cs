﻿using Jobportal_design.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Jobportal_design.Controllers
{
    public class Org_memController : Controller
    {
        private IConfiguration _Configure;
        string apiBaseUrl;

        public Org_memController(IConfiguration configuration)
        {

            _Configure = configuration;
            apiBaseUrl = _Configure.GetValue<string>("WebAPIBaseUrl");
        }
        public IActionResult AddOrg_mem()
        {
            List<Org_physical> org_name = new List<Org_physical>();
            List<Org_cont> org_cont = new List<Org_cont>();

            string apiUrl = apiBaseUrl + "/Org/getAll";
            string apiUrl1 = apiBaseUrl + "/Org_cont/getAll";

            HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync(apiUrl).Result;
            if (response.IsSuccessStatusCode)
            {
                org_name = JsonConvert.DeserializeObject<List<Org_physical>>(response.Content.ReadAsStringAsync().Result);
            }
            ViewBag.org_name = org_name;
            HttpResponseMessage response1 = client.GetAsync(apiUrl1).Result;
            if (response1.IsSuccessStatusCode)
            {
                org_cont = JsonConvert.DeserializeObject<List<Org_cont>>(response1.Content.ReadAsStringAsync().Result);
            }
            ViewBag.org_cont = org_cont;
            return View();
           
        }
        [HttpPost]
        public async Task<ActionResult> Create(Org_mem_ab Org_mem_ab)
        {
            if (Org_mem_ab.isactive == "1")
            {
                Org_mem_ab.status = true;
            
                // country_ab.identifier = "true";
            }
            else
            {
                Org_mem_ab.status = false;
                
                //  country_ab.identifier = false;
            }
            Org_mem_ab.org_id = Convert.ToInt32(HttpContext.Session.GetString("org_id_admin"));


            HttpClient hc = new HttpClient();

            hc.BaseAddress = new Uri(apiBaseUrl + "/Org_mem/Create");
            var locationconsume = hc.PostAsJsonAsync<Org_mem_ab>("Create", Org_mem_ab);
            locationconsume.Wait();
            var readdata = locationconsume.Result;

            if (readdata.IsSuccessStatusCode)
            {
                return RedirectToAction("OrgProfile_admin", "Org");


            }
            return View(Org_mem_ab);


        }
        public IActionResult Org_memlist()
        {
            List<Org_mem_ab> customers = new List<Org_mem_ab>();
            string apiUrl = apiBaseUrl + "/Org_mem/getAll";

            HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync(apiUrl).Result;
            if (response.IsSuccessStatusCode)
            {
                customers = JsonConvert.DeserializeObject<List<Org_mem_ab>>(response.Content.ReadAsStringAsync().Result);
            }

            return View(customers);
        }
        public async Task<ActionResult> EditOrg_mem(int id)
        {
            Org_mem_ab org = null;

            IEnumerable<Org_mem_ab> locobj = null;


            List<Org_physical> org_name = new List<Org_physical>();



            string apiUrl1 = apiBaseUrl + "/Org/getAll";


            string apiUrl = apiBaseUrl + "/Org_mem/Edit_get_by_id/";

            HttpClient client = new HttpClient();



            HttpResponseMessage response1 = client.GetAsync(apiUrl1).Result;
            if (response1.IsSuccessStatusCode)
            {
                org_name = JsonConvert.DeserializeObject<List<Org_physical>>(response1.Content.ReadAsStringAsync().Result);
            }
            ViewBag.org_name = org_name;



            HttpResponseMessage readdata = await client.GetAsync(apiUrl + id);
            //  var result = responseTask.Result;
            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<Org_mem_ab>();
                displayresults.Wait();
                org = displayresults.Result;

            }
            ViewBag.status = org;

            return View(org);

        }
        public async Task<ActionResult> Update(Org_mem_ab Org_mem_ab)
        {
            if (Org_mem_ab.isactive == "1")
            {
                Org_mem_ab.status = true;
               
            }
            else
            {
                Org_mem_ab.status = false;
               
            }
            Org_mem_ab.org_id = Convert.ToInt32(HttpContext.Session.GetString("org_id_admin"));

            HttpClient hc = new HttpClient();

            hc.BaseAddress = new Uri(apiBaseUrl + "/Org_mem/");
            var locationconsume = hc.PutAsJsonAsync<Org_mem_ab>("Update", Org_mem_ab);
            locationconsume.Wait();
            var readdata = locationconsume.Result;

            if (readdata.IsSuccessStatusCode)
            {

                return RedirectToAction("OrgProfile_admin", "Org");

            }
            return View(Org_mem_ab);


        }
        public async Task<ActionResult> DeleteOrg_mem(int id)
        {
            //Currency_tbl currency = null;
            //IEnumerable<Currency_tbl> locobj = null;
            string apiUrl = apiBaseUrl + "/Org_mem/Delete/";

            HttpClient client = new HttpClient();

            HttpResponseMessage readdata = await client.GetAsync(apiUrl + id);
            //  var result = responseTask.Result;
            if (readdata.IsSuccessStatusCode)
            {
                //var displayresults = readdata.Content.ReadAsAsync<Currency_tbl>();
                //displayresults.Wait();
                //currency = displayresults.Result;

            }

            return RedirectToAction("Org_memlist", "Org_mem");

        }
    }
}
