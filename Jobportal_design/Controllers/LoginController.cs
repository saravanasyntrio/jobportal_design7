﻿using AspNetCoreHero.ToastNotification.Abstractions;
using Jobportal_design.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Jobportal_design.Controllers
{
    public class LoginController : Controller
    {
        
        private IConfiguration _Configure;
        string apiBaseUrl;
        private readonly INotyfService _notyf;
        public LoginController(IConfiguration configuration, INotyfService notyf)
        {
            _notyf = notyf;
            _Configure = configuration;
            apiBaseUrl = _Configure.GetValue<string>("WebAPIBaseUrl");
        }
        public IActionResult Validatelogin()
        {
            return View();
        }
        public IActionResult Signin(int id)
        {
            HttpContext.Session.SetString("JobApplyId", id.ToString());

            return View();
        }
        [HttpPost]
        public async Task<ActionResult> Signin(Login login)
        {
            string email = login.email_id;
            string e_password = login.e_password;
            HttpClient client = new HttpClient();
            string apiUrl = apiBaseUrl + "/Login/get_by_id/";
            HttpResponseMessage readdata = await client.GetAsync(apiUrl + email + "/" + e_password);
            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<Login>();
                displayresults.Wait();
                login = displayresults.Result;

            }
            int login_id = login.id;
            string role = login.role;
            int role_id = login.role_id;
            int org_id = login.org_id;
            string org_name = login.e_name;
            string image_name = login.image_name;
            string image_path = login.image_path;
            string AdminRole = login.adminRole;
            
            if (image_name == null)
            {
                HttpContext.Session.SetString("image_name", "");
            }
            else
            {
                HttpContext.Session.SetString("image_name", image_name);
            }
               
             HttpContext.Session.SetString("org_loginid", "0");
            if ( role != null)
            {
                HttpContext.Session.SetString("role", role);
                HttpContext.Session.SetString("login_name", org_name.ToString());
                HttpContext.Session.SetString("role_id", role_id.ToString());

                if (role == "Employeer")
                {
                    if (role_id == 3)
                    {
                        HttpContext.Session.SetString("employer_loginid", login_id.ToString());
                        if (e_password == "employer@001")
                        {
                            return RedirectToAction("Changepassword", "Employer");
                        }
                        else
                        {
                            return RedirectToAction("jobregisterlist", "Jobregister");
                        }
                    }
                    else
                    {
                        HttpContext.Session.SetString("employer_loginid", login_id.ToString());
                        HttpContext.Session.SetString("org_loginid", org_id.ToString());
                        return RedirectToAction("Org_EmployerList", "Employer");
                    }
                    

                }
                if (role == "Seek")
                {
                    HttpContext.Session.SetString("jobseeker_loginid", login_id.ToString());
                    int jobId = 0;
                    if  (HttpContext.Session.GetString("JobApplyId") != null)
                    {
                         jobId = Convert.ToInt32(HttpContext.Session.GetString("JobApplyId").ToString());
                    }
                    else
                    {
                         jobId = 0;
                    }
                    if (jobId == 0)
                    {
                        return RedirectToAction("JobList", "Home");
                    }
                    else
                    {
                        return RedirectToAction("JobdetailsView", "Jobseeker");
                    }

                }
                if (role == "Admin")
                {
                    HttpContext.Session.SetString("AdminRole", AdminRole.ToString());
                    HttpContext.Session.SetString("admin_loginid", login_id.ToString());
                    return RedirectToAction("Dashboard", "Superadmin");
                }
                if (role == "Org")
                {
                    HttpContext.Session.SetString("org_loginid", login_id.ToString());
                    HttpContext.Session.SetString("org_name", org_name.ToString());
                    return RedirectToAction("Org_EmployerList", "Employer");
                }
                
            }
            else
            {
                ViewBag.error = "Username or Password is incorrect...!";
                TempData["error"] = "Username or Password is incorrect...!";
                return RedirectToAction("Signin", "Login");
            }
            //TempData["role"] = role;
            
            return RedirectToAction("Signin", "Login");

        }
        public IActionResult About()
        {
            return View();
        }
        public IActionResult ResetPassword()
        {
            return View();
        }
        public async Task<ActionResult> ForgotPassword(user_tbl_ab ub)
        {

            HttpClient client = new HttpClient();
            


            {
                HttpClient hc = new HttpClient();
                user_tbl_ab user_ab = new user_tbl_ab();
                string email = ub.user_name;
             
                string apiUrl = apiBaseUrl + "/Login/ForgotPassword/";
                HttpResponseMessage readdata = await hc.GetAsync(apiUrl + email);
                if (readdata.IsSuccessStatusCode)
                {
                    var displayresults = readdata.Content.ReadAsAsync<user_tbl_ab>();
                    displayresults.Wait();
                    user_ab = displayresults.Result;

                    if (user_ab.id > 0)
                    {
                        using (MailMessage mm = new MailMessage("childplus11@gmail.com", "childplus11@gmail.com"))
                        {
                            mm.Subject = "Password";
                            mm.Body = "Hi user , Your login password for jobboard is '" + user_ab.password + "'.Please reset your password after Login.";
                            //if (model.Attachment.Length > 0)
                            //{
                            //    string fileName = Path.GetFileName(model.Attachment.FileName);
                            //    mm.Attachments.Add(new Attachment(model.Attachment.OpenReadStream(), fileName));
                            //}
                            mm.IsBodyHtml = false;
                            using (SmtpClient smtp = new SmtpClient())
                            {
                                smtp.Host = "smtp.gmail.com";
                                smtp.EnableSsl = true;
                                NetworkCredential NetworkCred = new NetworkCredential("childplus11@gmail.com", "mgtxdfwmpbqzrtil");
                                smtp.UseDefaultCredentials = true;
                                smtp.Credentials = NetworkCred;
                                smtp.Port = 587;
                                smtp.Send(mm);
                                ViewBag.Message = "Email sent.";
                            }
                        }

                        _notyf.Information("Password mailed..!", 5);
                        //return RedirectToAction("jobregisterlist", "Jobregister");
                        return RedirectToAction("Signin", "Login");
                    }
                    else
                    {
                        _notyf.Information("Email not found..!", 5);
                        //return RedirectToAction("jobregisterlist", "Jobregister");
                        return RedirectToAction("Signin", "Login");
                    }
                }
                else
                {
                    _notyf.Error("Error..! Try Again..", 5);
                    return RedirectToAction("ForgotPassword", "Login");
                }
                




            }

        }
        public async Task<ActionResult> Index(string jobtypetext, string categorytext, string exptext, string searchval)
        {
            List<job_reg_tbl> jobs = new List<job_reg_tbl>();
            List<Org_cat> orgcat = new List<Org_cat>();

            string apiUrl = apiBaseUrl + "/Jobregister/get_by_iddemo/";
            string apiUrl1 = apiBaseUrl + "/Org/getAll/";
            string apiUrl2 = apiBaseUrl + "/Job_cat/getAll";
            HttpClient client = new HttpClient();


            if (string.IsNullOrEmpty(jobtypetext))
            {
                jobtypetext = "all";
            }
            if (string.IsNullOrEmpty(categorytext))
            {
                categorytext = "all";
            }
            if (string.IsNullOrEmpty(exptext))
            {
                exptext = "all";
            }
            if (string.IsNullOrEmpty(searchval))
            {
                searchval = "all";
            }

            HttpResponseMessage response = client.GetAsync(apiUrl + jobtypetext + "/" + categorytext + "/" + exptext + "/" + searchval).Result;
            if (response.IsSuccessStatusCode)
            {
                jobs = JsonConvert.DeserializeObject<List<job_reg_tbl>>(response.Content.ReadAsStringAsync().Result);
            }
            ViewBag.viewjobs = jobs;
            ViewBag.countjobs = jobs.Count;


            List<Org_physical> customers = new List<Org_physical>();
            if (searchval == null)
                searchval = "All";
            HttpResponseMessage response1 = client.GetAsync(apiUrl1 + searchval).Result;

            if (response1.IsSuccessStatusCode)
            {
                customers = JsonConvert.DeserializeObject<List<Org_physical>>(response1.Content.ReadAsStringAsync().Result);
            }
            ViewBag.customers = customers;
            //ViewBag.OrgCount = customers.Count;

            List<Job_cat> category = new List<Job_cat>();
            HttpResponseMessage response2 = client.GetAsync(apiUrl2).Result;

            if (response2.IsSuccessStatusCode)
            {
                category = JsonConvert.DeserializeObject<List<Job_cat>>(response2.Content.ReadAsStringAsync().Result);
            }
            ViewBag.category = category;

            return View(jobs);
            // return View();
        }
        public IActionResult Logout()
        {
            if (HttpContext.Session.GetString("role") != null)
            {
                HttpContext.Session.Remove("role");
               
            }
            if (HttpContext.Session.GetString("JobApplyId") != null)
            {
                HttpContext.Session.Remove("JobApplyId");

            }
            if (HttpContext.Session.GetString("login_name") != null)
            {
                HttpContext.Session.Remove("login_name");

            }
            return RedirectToAction("Index", "Home");
        }

    }
}
