﻿using AspNetCoreHero.ToastNotification.Abstractions;
using Jobportal_design.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Jobportal_design.Controllers
{
    public class EmployerController : Controller
    {
        private IWebHostEnvironment Environment;
        private readonly INotyfService _notyf;
        private IConfiguration _Configure;
        string apiBaseUrl;
        public EmployerController(IWebHostEnvironment _environment, INotyfService notyf, IConfiguration configuration)
        {
            Environment = _environment;
            _notyf = notyf;
            _Configure = configuration;
            apiBaseUrl = _Configure.GetValue<string>("WebAPIBaseUrl");
        }
        public IActionResult EmployerList()
        {
            List<Employer> customers = new List<Employer>();
            string apiUrl = apiBaseUrl + "/Employer/getAll";

            HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync(apiUrl).Result;
            if (response.IsSuccessStatusCode)
            {
                customers = JsonConvert.DeserializeObject<List<Employer>>(response.Content.ReadAsStringAsync().Result);
            }

            return View(customers);
        }


        [HttpPost]
        public JsonResult AutoComplete(string prefix)
        {
            List<Org_physical> org_name = new List<Org_physical>();
            string apiUrl = apiBaseUrl + "/Org/getAll1";

            HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync(apiUrl).Result;
            if (response.IsSuccessStatusCode)
            {
                org_name = JsonConvert.DeserializeObject<List<Org_physical>>(response.Content.ReadAsStringAsync().Result);
            }
            var customers = (from customer in org_name
                             where customer.org_name.StartsWith(prefix)
                             select new
                             {
                                 label = customer.org_name,
                                 val = customer.org_id
                             }).ToList();
            return Json(customers);
        }
       
        public IActionResult AddEmployer()
        {
          
           
            return View();

        }
        public IActionResult Create()
        {


            return View();

        }
        [HttpPost]
        public async Task<ActionResult> Create(Employer employer)
        {
            employer.org_id = Convert.ToInt32(HttpContext.Session.GetString("org_loginid"));
          
            if (employer.isactive == "Active")
            {
                employer.status = true;

            }
            else
            {
                employer.status = false;

                //  country_ab.identifier = false;
            }

           
            HttpClient hc = new HttpClient();
            var httpClient = new HttpClient();
            var response = await httpClient.PostAsJsonAsync(apiBaseUrl + "/Employer/Create", employer);
            jb_responce returnValue = await response.Content.ReadAsAsync<jb_responce>();
            //hc.BaseAddress = new Uri(apiBaseUrl + "/Employer/Create");
            //var locationconsume = hc.PostAsJsonAsync<Employer>("Create", employer);

            
          
            string empname = employer.e_name;
            string email = employer.user_name;
            int emp_id = employer.id;
            employer.id = emp_id;
            employer.email_id = email;

            //if (readdata.IsSuccessStatusCode)
                if (response.IsSuccessStatusCode)
                {
                    if (returnValue.status == true)
                    {
                        using (MailMessage mm = new MailMessage("childplus11@gmail.com", "childplus11@gmail.com"))
                        {
                            mm.Subject = "New Account";
                            mm.Body = "Hi " + empname + " , your account has been created successfully! Username: '" + email + "' and Password for new account is 'emp@123'";
                            //if (model.Attachment.Length > 0)
                            //{
                            //    string fileName = Path.GetFileName(model.Attachment.FileName);
                            //    mm.Attachments.Add(new Attachment(model.Attachment.OpenReadStream(), fileName));
                            //}
                            mm.IsBodyHtml = false;
                            using (SmtpClient smtp = new SmtpClient())
                            {
                                smtp.Host = "smtp.gmail.com";
                                smtp.EnableSsl = true;
                                NetworkCredential NetworkCred = new NetworkCredential("childplus11@gmail.com", "mgtxdfwmpbqzrtil");
                                smtp.UseDefaultCredentials = true;
                                smtp.Credentials = NetworkCred;
                                smtp.Port = 587;
                                smtp.Send(mm);
                                ViewBag.Message = "Email sent.";
                            }
                        }
                        _notyf.Success(returnValue.message, 10);
                        return RedirectToAction("Org_EmployerList", "Employer");
                    }
                    else
                    {
                    _notyf.Error(returnValue.message, 9);
                    return View(employer);
                }
                   


                }
                else
                {
                    _notyf.Error("Error..! Try Again..", 15);
                    return RedirectToAction("AddEmployer", "Employer");
                }
            //return View(employer);


        }
      
       

        public IActionResult Changepassword()
        {
            return View();
        }
        public async Task<IActionResult> Org_admin_employee_ChangepasswordAsync(int id)
        {
            HttpClient client = new HttpClient();


            Employer emp = null;

            string apiUrl4 = apiBaseUrl + "/Employer/getAllemployer_by_id/";


            HttpResponseMessage readdata = await client.GetAsync(apiUrl4 + id);

            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<Employer>();
                displayresults.Wait();
                emp = displayresults.Result;

            }

            return View(emp);
        }
        public async Task<ActionResult> UpdatePassword(Employer emp)
        {
           
            HttpClient client = new HttpClient();
            List<Org_physical> org_name = new List<Org_physical>();
            Employer Employer = null;
            string apiUrl2 = apiBaseUrl + "/Employer/getAllemployer_by_id/";
            int id = Convert.ToInt32(HttpContext.Session.GetString("employer_loginid"));
            HttpResponseMessage readdata = await client.GetAsync(apiUrl2 + id);

            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<Employer>();
                displayresults.Wait();
                Employer = displayresults.Result;

            }

            if (Employer.isactive == "Active")
            {
                Employer.status = true;

            }
            else
            {
                Employer.status = false;

                //  country_ab.identifier = false;
            }
            string emp_password = emp.new_password; 
            string empname = Employer.e_name;
            string OldPasswrd = emp.e_password;
            Employer.e_password = emp_password;
            Employer.old_password = OldPasswrd;
            Employer.id= id;
            string emp_repassword = emp.retype_password;
            if(emp_password!= emp_repassword)
                //|| emp_password== "emp@123")
            {
                TempData["Repassword"] = "New password Does not match with Re-Type password.. enter again.!";
                _notyf.Success("Error..! Try Again..", 5);
                return RedirectToAction("Changepassword", "Employer");
            }
            else
            {
                HttpClient hc = new HttpClient();

                hc.BaseAddress = new Uri(apiBaseUrl + "/Employer/");
                var locationconsume = hc.PutAsJsonAsync<Employer>("Updatepassword", Employer);
                locationconsume.Wait();
                var readdata1 = locationconsume.Result;

                if (readdata1.IsSuccessStatusCode)
                {
                    using (MailMessage mm = new MailMessage("childplus11@gmail.com", "childplus11@gmail.com"))
                    {
                        mm.Subject = "Password Changed";
                        mm.Body = "Hi "+ empname + " , Your Password has been successfully changed !....New Password '"+ Employer.e_password + "'";
                        //if (model.Attachment.Length > 0)
                        //{
                        //    string fileName = Path.GetFileName(model.Attachment.FileName);
                        //    mm.Attachments.Add(new Attachment(model.Attachment.OpenReadStream(), fileName));
                        //}
                        mm.IsBodyHtml = false;
                        using (SmtpClient smtp = new SmtpClient())
                        {
                            smtp.Host = "smtp.gmail.com";
                            smtp.EnableSsl = true;
                            NetworkCredential NetworkCred = new NetworkCredential("childplus11@gmail.com", "mgtxdfwmpbqzrtil");
                            smtp.UseDefaultCredentials = true;
                            smtp.Credentials = NetworkCred;
                            smtp.Port = 587;
                            smtp.Send(mm);
                            ViewBag.Message = "Email sent.";
                        }
                    }
                    //_notyf.Information("Updated Successfully", 3);
                    //return RedirectToAction("jobregisterlist", "Jobregister");
                    _notyf.Success("Password changed Successfully", 5);
                    return RedirectToAction("Signin", "Login");
                }

                else
                {
                    _notyf.Success("Error..! Try Again..", 5);
                    TempData["Oldpassword"] = "Old Password is incorrect.. Re-enter the correct Password!";
                    return RedirectToAction("Changepassword", "Employer");
                }




            }

        }
       
        public async Task<ActionResult> ViewEmployer(int id)
        {
            HttpClient client = new HttpClient();

            Employer jobs = null;

            string apiUrl2 = apiBaseUrl + "/Employer/getAllemployer_by_id/";
            string apiUrl1 = apiBaseUrl + "/Jobregister/getall_job_by_employid/";

            List<Job_register_ab> job = new List<Job_register_ab>();
            
            HttpResponseMessage response1 = client.GetAsync(apiUrl1 + id).Result;
            if (response1.IsSuccessStatusCode)
            {
                job = JsonConvert.DeserializeObject<List<Job_register_ab>>(response1.Content.ReadAsStringAsync().Result);

            }
            ViewBag.job = job;
            ViewBag.job_count = job.Count();

            HttpResponseMessage readdata = await client.GetAsync(apiUrl2 + id);

            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<Employer>();
                displayresults.Wait();
                jobs = displayresults.Result;
                ViewBag.id = jobs.id;
                ViewBag.e_name = jobs.e_name;
                ViewBag.last_name = jobs.last_name;
                ViewBag.org_name = jobs.org_name;
                //ViewBag.job_skills = jobs.job_skills;
                //ViewBag.responsibilities = jobs.responsibilities;
                //ViewBag.benefits = jobs.benefits;
                //ViewBag.schedule = jobs.shedule;
                //ViewBag.licence_certification = jobs.licence_certification;
                ViewBag.profilepic = jobs.empimage_path;
                ViewBag.start_date = jobs.start_date.ToString("MM/dd/yyyy");
                ViewBag.end_date = jobs.end_date.ToString("MM/dd/yyyy");
                ViewBag.orgdescription = jobs.orgdescription;
                ViewBag.org_address = jobs.org_address;
                ViewBag.contact1 = jobs.contact1;
                ViewBag.email_id = jobs.email_id;
                ViewBag.org_county = jobs.org_county;
               



            }

            return View(jobs);



        }

       
            public async Task<ActionResult> Admin_passwordchange(int id)
        {
            HttpClient client = new HttpClient();


            Employer seek = null;

            string apiUrl4 = apiBaseUrl + "/Employer/getAllemployer_by_id/";


            HttpResponseMessage readdata = await client.GetAsync(apiUrl4 + id);

            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<Employer>();
                displayresults.Wait();
                seek = displayresults.Result;

            }

            return View(seek);

        }
        public async Task<ActionResult> UpdatePassword_Admin(Employer emp)
        {

            HttpClient client = new HttpClient();

            Employer Employer = null;
            int id = emp.id;
            string apiUrl2 = apiBaseUrl + "/Employer/getAllemployer_by_id/";
           
            HttpResponseMessage readdata = await client.GetAsync(apiUrl2 + id);

            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<Employer>();
                displayresults.Wait();
                Employer = displayresults.Result;

            }

           /* if (Employer.isactive == "Active")
            {
                Employer.status = true;

            }
            else
            {
                Employer.status = false;

                //  country_ab.identifier = false;
            }*/
           
            Employer.old_password = emp.e_password;
            string emp_password = emp.new_password;
            string empname = Employer.e_name;
            Employer.e_password = emp_password;
           
            Employer.id = id;
            string emp_repassword = emp.retype_password;
            if (emp_password != emp_repassword)
            {
                //_notyf.Error("Password Does not Match", 5);
                TempData["Repassword"] = "New password does not match with Re-Type password.. enter again.!";
                _notyf.Error("Error..! Try Again..", 5);
                return RedirectToAction("Admin_passwordchange", "Employer",new { id = id });

            }
            else
            {
                HttpClient hc = new HttpClient();

                hc.BaseAddress = new Uri(apiBaseUrl + "/Employer/");
                var locationconsume = hc.PutAsJsonAsync<Employer>("Updatepassword", Employer);
                locationconsume.Wait();
                var readdata1 = locationconsume.Result;

                if (readdata1.IsSuccessStatusCode)
                {
                    using (MailMessage mm = new MailMessage("childplus11@gmail.com", "childplus11@gmail.com"))
                    {
                        mm.Subject = "Password Changed";
                        mm.Body = "Hi " + empname + " , Your Password has been successfully changed !....New Password '" + Employer.e_password + "'";
                        //if (model.Attachment.Length > 0)
                        //{
                        //    string fileName = Path.GetFileName(model.Attachment.FileName);
                        //    mm.Attachments.Add(new Attachment(model.Attachment.OpenReadStream(), fileName));
                        //}
                        mm.IsBodyHtml = false;
                        using (SmtpClient smtp = new SmtpClient())
                        {
                            smtp.Host = "smtp.gmail.com";
                            smtp.EnableSsl = true;
                            NetworkCredential NetworkCred = new NetworkCredential("childplus11@gmail.com", "mgtxdfwmpbqzrtil");
                            smtp.UseDefaultCredentials = true;
                            smtp.Credentials = NetworkCred;
                            smtp.Port = 587;
                            smtp.Send(mm);
                            ViewBag.Message = "Email sent.";
                        }
                    }

                    _notyf.Information("Password changed..!", 5);
                    //return RedirectToAction("jobregisterlist", "Jobregister");

                }
                else
                {
                    _notyf.Error("Error..! Try Again..", 5);
                    
                }
                return RedirectToAction("Admin_EmployerList", "Employer");




            }

        }
        public async Task<ActionResult> UpdatePassword_Org_Admin(Employer emp)
        {

            HttpClient client = new HttpClient();

            Employer Employer = null;
            int id = emp.id;
            string apiUrl2 = apiBaseUrl + "/Employer/getAllemployer_by_id/";

            HttpResponseMessage readdata = await client.GetAsync(apiUrl2 + id);

            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<Employer>();
                displayresults.Wait();
                Employer = displayresults.Result;

            }

            /* if (Employer.isactive == "Active")
             {
                 Employer.status = true;

             }
             else
             {
                 Employer.status = false;

                 //  country_ab.identifier = false;
             }*/

            Employer.old_password = emp.e_password;
            string emp_password = emp.new_password;
            string empname = Employer.e_name;
            Employer.e_password = emp_password;

            Employer.id = id;
            string emp_repassword = emp.retype_password;
            if (emp_password != emp_repassword)
            {
                //_notyf.Error("Password Does not Match", 5);
                TempData["Repassword"] = "New password does not match with Re-Type password.. enter again.!";
                _notyf.Error("Error..! Try Again..", 5);
                return RedirectToAction("Admin_passwordchange", "Employer", new { id = id });

            }
            else
            {
                HttpClient hc = new HttpClient();

                hc.BaseAddress = new Uri(apiBaseUrl + "/Employer/");
                var locationconsume = hc.PutAsJsonAsync<Employer>("Updatepassword", Employer);
                locationconsume.Wait();
                var readdata1 = locationconsume.Result;

                if (readdata1.IsSuccessStatusCode)
                {
                    using (MailMessage mm = new MailMessage("childplus11@gmail.com", "childplus11@gmail.com"))
                    {
                        mm.Subject = "Password Changed";
                        mm.Body = "Hi " + empname + " , Your Password has been successfully changed !....New Password '" + Employer.e_password + "'";
                        //if (model.Attachment.Length > 0)
                        //{
                        //    string fileName = Path.GetFileName(model.Attachment.FileName);
                        //    mm.Attachments.Add(new Attachment(model.Attachment.OpenReadStream(), fileName));
                        //}
                        mm.IsBodyHtml = false;
                        using (SmtpClient smtp = new SmtpClient())
                        {
                            smtp.Host = "smtp.gmail.com";
                            smtp.EnableSsl = true;
                            NetworkCredential NetworkCred = new NetworkCredential("childplus11@gmail.com", "mgtxdfwmpbqzrtil");
                            smtp.UseDefaultCredentials = true;
                            smtp.Credentials = NetworkCred;
                            smtp.Port = 587;
                            smtp.Send(mm);
                            ViewBag.Message = "Email sent.";
                        }
                    }

                    _notyf.Information("Password changed..!", 5);
                    //return RedirectToAction("jobregisterlist", "Jobregister");

                }
                else
                {
                    _notyf.Error("Error..! Try Again..", 5);

                }
                return RedirectToAction("Org_EmployerList", "Employer");




            }

        }

        public async Task<ActionResult> Emp_status(string activeStat, int passedId, Employer employer)
        {
            //Employer.id = Convert.ToInt32(HttpContext.Session.GetString("employer_loginid"));

            if (employer.activeStat == "Active")
            {
                employer.status = false;
                employer.isactive = "InActive";

            }
            else
            {
                employer.status = true;
                employer.isactive = "Active";

                //  country_ab.identifier = false;
            }
            employer.id = employer.passedId;
            HttpClient hc = new HttpClient();

            hc.BaseAddress = new Uri(apiBaseUrl + "/Employer/");
            var locationconsume = hc.PutAsJsonAsync<Employer>("Update_status", employer);
            locationconsume.Wait();
            var readdata = locationconsume.Result;

            if (readdata.IsSuccessStatusCode)
            {

                _notyf.Information("Updated Successfully", 3);
                return RedirectToAction("Admin_EmployerList", "Employer");

            }
            else
            {
                _notyf.Error("Error..! Try Again..", 5);
                return RedirectToAction("Admin_EmployerList", "Employer");
            }
            //return View(Employer);


        }
        public async Task<ActionResult> Org_Emp_status(string activeStat, int passedId, Employer employer)
        {
            //Employer.id = Convert.ToInt32(HttpContext.Session.GetString("employer_loginid"));

            if (employer.activeStat == "Active")
            {
                employer.status = false;
                employer.isactive = "InActive";

            }
            else
            {
                employer.status = true;
                employer.isactive = "Active";

                //  country_ab.identifier = false;
            }
            employer.id = employer.passedId;
            HttpClient hc = new HttpClient();

            hc.BaseAddress = new Uri(apiBaseUrl + "/Employer/");
            var locationconsume = hc.PutAsJsonAsync<Employer>("Update_status", employer);
            locationconsume.Wait();
            var readdata = locationconsume.Result;

            if (readdata.IsSuccessStatusCode)
            {

                _notyf.Information("Updated Successfully", 3);
                return RedirectToAction("Org_EmployerList", "Employer");

            }
            else
            {
                _notyf.Error("Error..! Try Again..", 5);
                return RedirectToAction("Org_EmployerList", "Employer");
            }
            //return View(Employer);


        }
        public async Task<ActionResult> Org_Emp_role(string activeStat, int passedId, Employer employer)
        {
            //Employer.id = Convert.ToInt32(HttpContext.Session.GetString("employer_loginid"));
            int role_id = 2;
            if (employer.activeStat == "3")
            {
                role_id = 2;

            }
            else
            {
                role_id = 3;
                //  country_ab.identifier = false;
            }
            employer.id = employer.passedId;
            employer.role_id = role_id;
            HttpClient hc = new HttpClient();

            hc.BaseAddress = new Uri(apiBaseUrl + "/Employer/");
            var locationconsume = hc.PutAsJsonAsync<Employer>("Update_role", employer);
            locationconsume.Wait();
            var readdata = locationconsume.Result;

            if (readdata.IsSuccessStatusCode)
            {

                _notyf.Information("Updated Successfully", 3);
                return RedirectToAction("Org_EmployerList", "Employer");

            }
            else
            {
                _notyf.Error("Error..! Try Again..", 5);
                return RedirectToAction("Org_EmployerList", "Employer");
            }
            //return View(Employer);


        }

        

        public async Task<ActionResult> EditProfile()
        {
            List<SelectListItem> ObjPhoneType = new List<SelectListItem>()
            {
                //new SelectListItem { Text = "Select Gender", Value = "0" },
                new SelectListItem { Text = "Cell", Value = "Cell" },
                new SelectListItem { Text = "Home", Value = "Home" },
                new SelectListItem { Text = "Work", Value = "Work" },
                new SelectListItem { Text = "Other", Value = "Other" },

            };
            ViewBag.PhoneType = ObjPhoneType;
            HttpClient client = new HttpClient();
            List<Org_physical> org_name = new List<Org_physical>();
            Employer seek = null;
            string apiUrl1 = apiBaseUrl + "/Org/getAll1";
            string apiUrl2 = apiBaseUrl + "/Employer/getAllemployer_by_id/";

            HttpResponseMessage response1 = client.GetAsync(apiUrl1).Result;
            if (response1.IsSuccessStatusCode)
            {
                org_name = JsonConvert.DeserializeObject<List<Org_physical>>(response1.Content.ReadAsStringAsync().Result);
            }
            ViewBag.org_name = org_name;

            int id = Convert.ToInt32(HttpContext.Session.GetString("employer_loginid"));
            HttpResponseMessage readdata = await client.GetAsync(apiUrl2 + id);

            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<Employer>();
                displayresults.Wait();
                seek = displayresults.Result;

            }
            ViewBag.profilepic = seek.empimage_path;
            return View(seek);



        }

        public async Task<ActionResult> Update(Employer Employer, List<IFormFile> s_pic1)
        {
            Employer.id = Convert.ToInt32(HttpContext.Session.GetString("employer_loginid"));

            string wwwPath = this.Environment.WebRootPath;
            string contentPath = this.Environment.ContentRootPath;
            string path = Path.Combine(this.Environment.WebRootPath, "Emp_img_uploads");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            List<string> uploadedFiles = new List<string>();
            
            foreach (IFormFile postedFile in s_pic1)
            {
                string fileName = Path.GetFileName(postedFile.FileName);
                using (FileStream stream = new FileStream(Path.Combine(path, fileName), FileMode.Create))
                {
                    postedFile.CopyTo(stream);
                    uploadedFiles.Add(fileName);
                    // ViewBag.Message += string.Format("<b>{0}</b> uploaded.<br />", fileName);
                }
                Employer.emp_image = path;
                Employer.empimage_path = fileName;
            }

            HttpClient hc = new HttpClient();

            hc.BaseAddress = new Uri(apiBaseUrl + "/Employer/");
            var locationconsume = hc.PutAsJsonAsync<Employer>("Update", Employer);
            locationconsume.Wait();
            var readdata = locationconsume.Result;
            if (readdata.IsSuccessStatusCode)
            {

                _notyf.Information("Updated Successfully", 5);
                return RedirectToAction("jobregisterlist", "Jobregister");
                
            }
            else
            {
                _notyf.Error("Error..! Try Again..", 5);
                return RedirectToAction("EditProfile", "Employer");
            }
            //return View(Employer);


        }

        public IActionResult Admin_EmployerList()
        {
            int org_id = 0;
            if (HttpContext.Session.GetString("org_loginid").ToString() != null){
                org_id = Convert.ToInt32(HttpContext.Session.GetString("org_loginid").ToString());
            }
            List<Employer> customers = new List<Employer>();
            string apiUrl = apiBaseUrl + "/Employer/getAll/";

            HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync(apiUrl + org_id).Result;
            if (response.IsSuccessStatusCode)
            {
                customers = JsonConvert.DeserializeObject<List<Employer>>(response.Content.ReadAsStringAsync().Result);
            }
            //List<Job_cat> job_cat = new List<Job_cat>();
            //string apiUrl2 = apiBaseUrl + "/Job_cat/getAll";
            //HttpResponseMessage response2 = client.GetAsync(apiUrl2).Result;
            //if (response2.IsSuccessStatusCode)
            //{
            //    job_cat = JsonConvert.DeserializeObject<List<Job_cat>>(response2.Content.ReadAsStringAsync().Result);
            //}

            //ViewBag.job_cat = job_cat;
            return View(customers);
        }
        public IActionResult Org_JobList(string search, int employeer_id, int job_cat_id,jobFilterValues category)
        {
            int org_id = 0;
            bool is_login = true;
            if (HttpContext.Session.GetString("org_loginid").ToString() != null)
            {
                org_id = Convert.ToInt32(HttpContext.Session.GetString("org_loginid").ToString());
            }
            List<job_reg_tbl> customers = new List<job_reg_tbl>();
            string apiUrl = apiBaseUrl + "/Employer/getall_job_for_search_org_id/";
            HttpClient client = new HttpClient();
            if (category.filterOn != "true")
            {
                string categorytext = job_cat_id.ToString();
                string exptext = employeer_id.ToString();
                string searchval = search;

                if (string.IsNullOrEmpty(categorytext))
                {
                    categorytext = "0";
                }
                if (string.IsNullOrEmpty(exptext))
                {
                    exptext = "0";
                }
                if (string.IsNullOrEmpty(searchval))
                {
                    searchval = "all";
                }


                HttpResponseMessage response = client.GetAsync(apiUrl + org_id + "/" + categorytext + "/" + exptext + "/" + searchval).Result;
                //  HttpResponseMessage response = client.GetAsync(apiUrl + org_id).Result;
                if (response.IsSuccessStatusCode)
                {
                    customers = JsonConvert.DeserializeObject<List<job_reg_tbl>>(response.Content.ReadAsStringAsync().Result);
                }
                ViewBag.countjobs = customers.Count();
                ViewBag.searchText = search;
            }

            else
            {
                List<job_title> jobTitleList = new List<job_title>();
                List<String> jobFilterValues = new List<String>();

                List<String> jobTitleValues = new List<String>();
                List<String> jobTypeValues = new List<String>();
                List<String> jobCategoryValues = new List<String>();
                List<String> filterValues = new List<String>();
                JobFilter jbFilter = new JobFilter();

                if (category.Administrator == "on")
                {
                    //  jobTitleList.Add(new job_title { Text = "Administrator/Director", Value = "Administrator/Director" });
                    jobTitleValues.Add("Administrator/Director");
                    filterValues.Add("Administrator");
                }
                if (category.AssistantTeacher == "on")
                {
                    //jobTitleList.Add(new job_title { Text = "Assistant Teacher", Value = "Assistant Teacher" });
                    jobTitleValues.Add("Assistant Teacher");
                    filterValues.Add("AssistantTeacher");
                }
                if (category.Floater == "on")
                {
                    //jobTitleList.Add(new job_title { Text = "Floater", Value = "Floater" });
                    jobTitleValues.Add("Floater");
                    filterValues.Add("Floater");
                }
                if (category.LeadTeacher == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Lead Teacher", Value = "Lead Teacher" });
                    jobTitleValues.Add("Lead Teacher");
                    filterValues.Add("LeadTeacher");
                }
                if (category.Substitute == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Substitute", Value = "Substitute" });
                    jobTitleValues.Add("Substitute");
                    filterValues.Add("Substitute");
                }
                if (category.DayCampAdministrator == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Day Camp Administrator", Value = "Day Camp Administrator" });
                    jobTitleValues.Add("Day Camp Administrator");
                    filterValues.Add("DayCampAdministrator");
                }
                if (category.DayCampJuniorCounselor == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Day Camp Junior Counselor", Value = "Day Camp Junior Counselor" });
                    jobTitleValues.Add("Day Camp Junior Counselor");
                    filterValues.Add("DayCampJuniorCounselor");
                }
                if (category.DayCampLeadCounselor == "on")
                {
                    //  jobTitleList.Add(new job_title { Text = "Day Camp Lead Counselor", Value = "Day Camp Lead Counselor" });
                    jobTitleValues.Add("Day Camp Lead Counselor");
                    filterValues.Add("DayCampLeadCounselor");
                }
                if (category.DayCampOwner == "on")
                {
                    //  jobTitleList.Add(new job_title { Text = "Day Camp Owner", Value = "Day Camp Owner" });
                    jobTitleValues.Add("Day Camp Owner");
                    filterValues.Add("DayCampOwner");
                }
                if (category.FullTime == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Full Time", Value = "Full Time" });
                    jobTypeValues.Add("Full Time");
                    filterValues.Add("FullTime");
                }
                if (category.PartTime == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Part Time", Value = "Part Time" });
                    jobTypeValues.Add("Part Time");
                    filterValues.Add("PartTime");
                }
                if (category.Hybrid == "on")
                {
                    //  jobTitleList.Add(new job_title { Text = "Hybrid", Value = "Hybrid" });
                    jobTypeValues.Add("Hybrid");
                    filterValues.Add("Hybrid");
                }
                if (category.Remote == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Remote", Value = "Remote" });
                    jobTypeValues.Add("Remote");
                    filterValues.Add("Remote");
                }
                if (category.Others == "on")
                {
                    //jobTitleList.Add(new job_title { Text = "Others", Value = "Others" });
                    jobTypeValues.Add("Others");
                    filterValues.Add("Others");
                }

                if (category.InOffice == "on")
                {
                    //  jobTitleList.Add(new job_title { Text = "In Office", Value = "In Office" });
                    jobTypeValues.Add("In Office");
                    filterValues.Add("In Office");
                }
                if (category.Administration == "on")
                {
                    //  jobTitleList.Add(new job_title { Text = "Administration", Value = "Administration" });
                    jobCategoryValues.Add("Administration");
                    filterValues.Add("Administration");
                }
                if (category.Management == "on")
                {
                    //  jobTitleList.Add(new job_title { Text = "Management", Value = "Management" });
                    jobCategoryValues.Add("Management");
                    filterValues.Add("Management");
                }
                if (category.EarlyEducation == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Education", Value = "Education" });
                    jobCategoryValues.Add("Early Education");
                    filterValues.Add("EarlyEducation");
                }



                //string apiUrl1 = apiBaseUrl + "/County/getAllActiveCounty";
                string apiUrlFilter = apiBaseUrl + "/Jobregister/get_job_search_filter_by_org/";
                jbFilter.jobCategoryValues = jobCategoryValues;
                jbFilter.org_id = org_id;
                jbFilter.jobTitleValues = jobTitleValues;
                jbFilter.jobTypeValues = jobTypeValues;
                jbFilter.is_login = is_login;
                //HttpClient client = new HttpClient();

                //  HttpResponseMessage response = client.GetAsync(apiUrl + seek_id+"/" + jobTitleValues + "/" + jobTypeValues+"/"+ jobCategoryValues).Result;
                client.BaseAddress = new Uri(apiBaseUrl + "/Jobregister/");

                var locationconsume = client.PostAsJsonAsync<JobFilter>("get_job_search_filter_by_org", jbFilter);
                locationconsume.Wait();
                var readdata = locationconsume.Result;

                if (readdata.IsSuccessStatusCode)
                {
                    customers = JsonConvert.DeserializeObject<List<job_reg_tbl>>(readdata.Content.ReadAsStringAsync().Result);
                }

                ViewBag.viewjobs = customers;
                ViewBag.countjobs = customers.Count;
                ViewBag.Adm = category.Administrator;
                ViewBag.cate = filterValues;

            }

            List<Job_cat> job_cat = new List<Job_cat>();
            string apiUrl2 = apiBaseUrl + "/Job_cat/getAll";
            HttpResponseMessage response2 = client.GetAsync(apiUrl2).Result;
            if (response2.IsSuccessStatusCode)
            {
                job_cat = JsonConvert.DeserializeObject<List<Job_cat>>(response2.Content.ReadAsStringAsync().Result);
            }
            ViewBag.job_cat = job_cat;

            List<Employer> emp = new List<Employer>();
            string apiUrl3 = apiBaseUrl + "/Employer/getAll/";
            HttpResponseMessage response3 = client.GetAsync(apiUrl3 + org_id).Result;
            if (response3.IsSuccessStatusCode)
            {
                emp = JsonConvert.DeserializeObject<List<Employer>>(response3.Content.ReadAsStringAsync().Result);
            }
            ViewBag.emp = emp;
          
            string apiUrl4 = apiBaseUrl + "/Jobregister/getJobcategoryWithJobCount_by_Org/";
            List<Job_cat_count> category1 = new List<Job_cat_count>();
            HttpResponseMessage response4 = client.GetAsync(apiUrl4 + org_id+ "/" + is_login).Result;

            if (response4.IsSuccessStatusCode)
            {
                category1 = JsonConvert.DeserializeObject<List<Job_cat_count>>(response4.Content.ReadAsStringAsync().Result);
            }
            ViewBag.category = category1;
            ViewBag.Job_catCount = category1.Count();

            string apiUrl5 = apiBaseUrl + "/Jobregister/getJobTitleWithJobCount_by_Org/";
            List<job_title> jobTitle = new List<job_title>();
            HttpResponseMessage response5 = client.GetAsync(apiUrl5 + org_id+ "/" + is_login).Result;

            if (response5.IsSuccessStatusCode)
            {
                jobTitle = JsonConvert.DeserializeObject<List<job_title>>(response5.Content.ReadAsStringAsync().Result);
            }
            ViewBag.jobTitle = jobTitle;

            string apiUrl6 = apiBaseUrl + "/Jobregister/getJobTypeWithJobCount_by_Org/";
            List<job_title> jobType = new List<job_title>();
            HttpResponseMessage response6 = client.GetAsync(apiUrl6 + org_id+ "/"+ is_login).Result;

            if (response6.IsSuccessStatusCode)
            {
                jobType = JsonConvert.DeserializeObject<List<job_title>>(response6.Content.ReadAsStringAsync().Result);
            }
            ViewBag.jobType = jobType;

            return View(customers);
        }
        public IActionResult Org_JobList_Home(int org_id, string org_name, string search, int county_id, int job_cat_id, jobFilterValues category)
        {
            bool is_login = false;
            //int org_id = 0;
            //if (HttpContext.Session.GetString("org_loginid").ToString() != null)
            //{
            //    org_id = Convert.ToInt32(HttpContext.Session.GetString("org_loginid").ToString());
            //}
            if (org_id != 0)
            {
                HttpContext.Session.SetString("orgId", org_id.ToString());
            }
            else
            {
                org_id= Convert.ToInt32(HttpContext.Session.GetString("orgId").ToString());
            }

            List<job_reg_tbl> customers = new List<job_reg_tbl>();
            List<Job_cat> job_cat = new List<Job_cat>();
            //  string apiUrl = apiBaseUrl + "/Employer/getall_job_by_Orgid/";
            string apiUrl = apiBaseUrl + "/Jobregister/get_job_search_by_org/";

            HttpClient client = new HttpClient();
            if (category.filterOn != "true")
            {
                string categorytext = job_cat_id.ToString();
                string exptext = county_id.ToString();
                string searchval = search;
               
                if (string.IsNullOrEmpty(categorytext))
                {
                    categorytext = "0";
                }
                if (string.IsNullOrEmpty(exptext))
                {
                    exptext = "0";
                }
                if (string.IsNullOrEmpty(searchval))
                {
                    searchval = "all";
                }

                HttpResponseMessage response = client.GetAsync(apiUrl + org_id + "/" + categorytext + "/" + exptext + "/" + searchval).Result;
                //  HttpResponseMessage response = client.GetAsync(apiUrl + org_id).Result;
                if (response.IsSuccessStatusCode)
                {
                    customers = JsonConvert.DeserializeObject<List<job_reg_tbl>>(response.Content.ReadAsStringAsync().Result);
                }
                ViewBag.countjobs = customers.Count();
                ViewBag.orgName = org_name;
                ViewBag.searchText = search;
            }

            else
            {
                List<job_title> jobTitleList = new List<job_title>();
                List<String> jobFilterValues = new List<String>();

                List<String> jobTitleValues = new List<String>();
                List<String> jobTypeValues = new List<String>();
                List<String> jobCategoryValues = new List<String>();
                List<String> filterValues = new List<String>();
                JobFilter jbFilter = new JobFilter();

                if (category.Administrator == "on")
                {
                    //  jobTitleList.Add(new job_title { Text = "Administrator/Director", Value = "Administrator/Director" });
                    jobTitleValues.Add("Administrator/Director");
                    filterValues.Add("Administrator");
                }
                if (category.AssistantTeacher == "on")
                {
                    //jobTitleList.Add(new job_title { Text = "Assistant Teacher", Value = "Assistant Teacher" });
                    jobTitleValues.Add("Assistant Teacher");
                    filterValues.Add("AssistantTeacher");
                }
                if (category.Floater == "on")
                {
                    //jobTitleList.Add(new job_title { Text = "Floater", Value = "Floater" });
                    jobTitleValues.Add("Floater");
                    filterValues.Add("Floater");
                }
                if (category.LeadTeacher == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Lead Teacher", Value = "Lead Teacher" });
                    jobTitleValues.Add("Lead Teacher");
                    filterValues.Add("LeadTeacher");
                }
                if (category.Substitute == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Substitute", Value = "Substitute" });
                    jobTitleValues.Add("Substitute");
                    filterValues.Add("Substitute");
                }
                if (category.DayCampAdministrator == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Day Camp Administrator", Value = "Day Camp Administrator" });
                    jobTitleValues.Add("Day Camp Administrator");
                    filterValues.Add("DayCampAdministrator");
                }
                if (category.DayCampJuniorCounselor == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Day Camp Junior Counselor", Value = "Day Camp Junior Counselor" });
                    jobTitleValues.Add("Day Camp Junior Counselor");
                    filterValues.Add("DayCampJuniorCounselor");
                }
                if (category.DayCampLeadCounselor == "on")
                {
                    //  jobTitleList.Add(new job_title { Text = "Day Camp Lead Counselor", Value = "Day Camp Lead Counselor" });
                    jobTitleValues.Add("Day Camp Lead Counselor");
                    filterValues.Add("DayCampLeadCounselor");
                }
                if (category.DayCampOwner == "on")
                {
                    //  jobTitleList.Add(new job_title { Text = "Day Camp Owner", Value = "Day Camp Owner" });
                    jobTitleValues.Add("Day Camp Owner");
                    filterValues.Add("DayCampOwner");
                }
                if (category.FullTime == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Full Time", Value = "Full Time" });
                    jobTypeValues.Add("Full Time");
                    filterValues.Add("FullTime");
                }
                if (category.PartTime == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Part Time", Value = "Part Time" });
                    jobTypeValues.Add("Part Time");
                    filterValues.Add("PartTime");
                }
                if (category.Hybrid == "on")
                {
                    //  jobTitleList.Add(new job_title { Text = "Hybrid", Value = "Hybrid" });
                    jobTypeValues.Add("Hybrid");
                    filterValues.Add("Hybrid");
                }
                if (category.Remote == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Remote", Value = "Remote" });
                    jobTypeValues.Add("Remote");
                    filterValues.Add("Remote");
                }
                if (category.Others == "on")
                {
                    //jobTitleList.Add(new job_title { Text = "Others", Value = "Others" });
                    jobTypeValues.Add("Others");
                    filterValues.Add("Others");
                }

                if (category.InOffice == "on")
                {
                    //  jobTitleList.Add(new job_title { Text = "In Office", Value = "In Office" });
                    jobTypeValues.Add("In Office");
                    filterValues.Add("In Office");
                }
                if (category.Administration == "on")
                {
                    //  jobTitleList.Add(new job_title { Text = "Administration", Value = "Administration" });
                    jobCategoryValues.Add("Administration");
                    filterValues.Add("Administration");
                }
                if (category.Management == "on")
                {
                    //  jobTitleList.Add(new job_title { Text = "Management", Value = "Management" });
                    jobCategoryValues.Add("Management");
                    filterValues.Add("Management");
                }
                if (category.EarlyEducation == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Education", Value = "Education" });
                    jobCategoryValues.Add("Early Education");
                    filterValues.Add("EarlyEducation");
                }



                //string apiUrl1 = apiBaseUrl + "/County/getAllActiveCounty";
                string apiUrlFilter = apiBaseUrl + "/Jobregister/get_job_search_filter_by_org/";
                jbFilter.jobCategoryValues = jobCategoryValues;
                jbFilter.org_id = org_id;
                jbFilter.jobTitleValues = jobTitleValues;
                jbFilter.jobTypeValues = jobTypeValues;
                jbFilter.is_login = is_login;
                //HttpClient client = new HttpClient();

                //  HttpResponseMessage response = client.GetAsync(apiUrl + seek_id+"/" + jobTitleValues + "/" + jobTypeValues+"/"+ jobCategoryValues).Result;
                client.BaseAddress = new Uri(apiBaseUrl + "/Jobregister/");

                var locationconsume = client.PostAsJsonAsync<JobFilter>("get_job_search_filter_by_org", jbFilter);
                locationconsume.Wait();
                var readdata = locationconsume.Result;

                if (readdata.IsSuccessStatusCode)
                {
                    customers = JsonConvert.DeserializeObject<List<job_reg_tbl>>(readdata.Content.ReadAsStringAsync().Result);
                }

                ViewBag.viewjobs = customers;
                ViewBag.countjobs = customers.Count;
                //ViewBag.JobCount = customers.Count;
                ViewBag.Adm = category.Administrator;
                ViewBag.cate = filterValues;

            }

            string apiUrl2 = apiBaseUrl + "/Job_cat/getAll";
            HttpResponseMessage response2 = client.GetAsync(apiUrl2).Result;
            if (response2.IsSuccessStatusCode)
            {
                job_cat = JsonConvert.DeserializeObject<List<Job_cat>>(response2.Content.ReadAsStringAsync().Result);
            }
            ViewBag.job_cat = job_cat;

            List<County_tbl> county = new List<County_tbl>();

            string apiUrl1 = apiBaseUrl + "/County/getAllActiveCounty"; 
            HttpResponseMessage response1 = client.GetAsync(apiUrl1).Result;
            if (response1.IsSuccessStatusCode)
            {
                county = JsonConvert.DeserializeObject<List<County_tbl>>(response1.Content.ReadAsStringAsync().Result);
            }
            ViewBag.county = county;
            string apiUrl3 = apiBaseUrl + "/Jobregister/getJobcategoryWithJobCount_by_Org/";
            List<Job_cat_count> category1 = new List<Job_cat_count>();
            HttpResponseMessage response3 = client.GetAsync(apiUrl3 + org_id+ "/" + is_login).Result;

            if (response3.IsSuccessStatusCode)
            {
                category1 = JsonConvert.DeserializeObject<List<Job_cat_count>>(response3.Content.ReadAsStringAsync().Result);
            }
            ViewBag.category = category1;
            ViewBag.Job_catCount = category1.Count();

            string apiUrl4 = apiBaseUrl + "/Jobregister/getJobTitleWithJobCount_by_Org/";
            List<job_title> jobTitle = new List<job_title>();
            HttpResponseMessage response4 = client.GetAsync(apiUrl4 + org_id+ "/" + is_login).Result;

            if (response4.IsSuccessStatusCode)
            {
                jobTitle = JsonConvert.DeserializeObject<List<job_title>>(response4.Content.ReadAsStringAsync().Result);
            }
            ViewBag.jobTitle = jobTitle;

            string apiUrl5 = apiBaseUrl + "/Jobregister/getJobTypeWithJobCount_by_Org/";
            
            List<job_title> jobType = new List<job_title>();
            HttpResponseMessage response5 = client.GetAsync(apiUrl5 + org_id+"/"+ is_login).Result;

            if (response5.IsSuccessStatusCode)
            {
                jobType = JsonConvert.DeserializeObject<List<job_title>>(response5.Content.ReadAsStringAsync().Result);
            }
            ViewBag.jobType = jobType;

            return View(customers);
        }
        public IActionResult Org_JobList_AfterLogin(int org_id, string org_name, string search, int county_id, int job_cat_id, jobFilterValues category)
        {
            bool is_login = true;
            //int org_id = 0;
            //if (HttpContext.Session.GetString("org_loginid").ToString() != null)
            //{
            //    org_id = Convert.ToInt32(HttpContext.Session.GetString("org_loginid").ToString());
            //}
            if (org_id != 0)
            {
                HttpContext.Session.SetString("orgId", org_id.ToString());
            }
            else
            {
                org_id = Convert.ToInt32(HttpContext.Session.GetString("orgId").ToString());
            }
          
            int seeker_id = Convert.ToInt32(HttpContext.Session.GetString("jobseeker_loginid").ToString());
            
            List<job_reg_tbl> customers = new List<job_reg_tbl>();
            List<Job_cat> job_cat = new List<Job_cat>();

            string apiUrl1 = apiBaseUrl + "/County/getAllActiveCounty";
            //  string apiUrl = apiBaseUrl + "/Employer/getall_job_by_Orgid/";
            string apiUrl = apiBaseUrl + "/Jobregister/get_job_search_by_org_and_seeker/";

            HttpClient client = new HttpClient();
            if (category.filterOn != "true")
            {
                string categorytext = job_cat_id.ToString();
                string exptext = county_id.ToString();
                string searchval = search;

                if (string.IsNullOrEmpty(categorytext))
                {
                    categorytext = "all";
                }
                if (string.IsNullOrEmpty(exptext))
                {
                    exptext = "all";
                }
                if (string.IsNullOrEmpty(searchval))
                {
                    searchval = "all";
                }

                HttpResponseMessage response = client.GetAsync(apiUrl + org_id + "/" + seeker_id + "/" + categorytext + "/" + exptext + "/" + searchval).Result;
                //  HttpResponseMessage response = client.GetAsync(apiUrl + org_id).Result;
                if (response.IsSuccessStatusCode)
                {
                    customers = JsonConvert.DeserializeObject<List<job_reg_tbl>>(response.Content.ReadAsStringAsync().Result);
                }
                ViewBag.countjobs = customers.Count();
                ViewBag.orgName = org_name;
                ViewBag.searchText = search;
            }

            else
            {
                List<job_title> jobTitleList = new List<job_title>();
                List<String> jobFilterValues = new List<String>();

                List<String> jobTitleValues = new List<String>();
                List<String> jobTypeValues = new List<String>();
                List<String> jobCategoryValues = new List<String>();
                List<String> filterValues = new List<String>();
                JobFilter jbFilter = new JobFilter();

                if (category.Administrator == "on")
                {
                    //  jobTitleList.Add(new job_title { Text = "Administrator/Director", Value = "Administrator/Director" });
                    jobTitleValues.Add("Administrator/Director");
                    filterValues.Add("Administrator");
                }
                if (category.AssistantTeacher == "on")
                {
                    //jobTitleList.Add(new job_title { Text = "Assistant Teacher", Value = "Assistant Teacher" });
                    jobTitleValues.Add("Assistant Teacher");
                    filterValues.Add("AssistantTeacher");
                }
                if (category.Floater == "on")
                {
                    //jobTitleList.Add(new job_title { Text = "Floater", Value = "Floater" });
                    jobTitleValues.Add("Floater");
                    filterValues.Add("Floater");
                }
                if (category.LeadTeacher == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Lead Teacher", Value = "Lead Teacher" });
                    jobTitleValues.Add("Lead Teacher");
                    filterValues.Add("LeadTeacher");
                }
                if (category.Substitute == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Substitute", Value = "Substitute" });
                    jobTitleValues.Add("Substitute");
                    filterValues.Add("Substitute");
                }
                if (category.DayCampAdministrator == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Day Camp Administrator", Value = "Day Camp Administrator" });
                    jobTitleValues.Add("Day Camp Administrator");
                    filterValues.Add("DayCampAdministrator");
                }
                if (category.DayCampJuniorCounselor == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Day Camp Junior Counselor", Value = "Day Camp Junior Counselor" });
                    jobTitleValues.Add("Day Camp Junior Counselor");
                    filterValues.Add("DayCampJuniorCounselor");
                }
                if (category.DayCampLeadCounselor == "on")
                {
                    //  jobTitleList.Add(new job_title { Text = "Day Camp Lead Counselor", Value = "Day Camp Lead Counselor" });
                    jobTitleValues.Add("Day Camp Lead Counselor");
                    filterValues.Add("DayCampLeadCounselor");
                }
                if (category.DayCampOwner == "on")
                {
                    //  jobTitleList.Add(new job_title { Text = "Day Camp Owner", Value = "Day Camp Owner" });
                    jobTitleValues.Add("Day Camp Owner");
                    filterValues.Add("DayCampOwner");
                }
                if (category.FullTime == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Full Time", Value = "Full Time" });
                    jobTypeValues.Add("Full Time");
                    filterValues.Add("FullTime");
                }
                if (category.PartTime == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Part Time", Value = "Part Time" });
                    jobTypeValues.Add("Part Time");
                    filterValues.Add("PartTime");
                }
                if (category.Hybrid == "on")
                {
                    //  jobTitleList.Add(new job_title { Text = "Hybrid", Value = "Hybrid" });
                    jobTypeValues.Add("Hybrid");
                    filterValues.Add("Hybrid");
                }
                if (category.Remote == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Remote", Value = "Remote" });
                    jobTypeValues.Add("Remote");
                    filterValues.Add("Remote");
                }
                if (category.Others == "on")
                {
                    //jobTitleList.Add(new job_title { Text = "Others", Value = "Others" });
                    jobTypeValues.Add("Others");
                    filterValues.Add("Others");
                }

                if (category.InOffice == "on")
                {
                    //  jobTitleList.Add(new job_title { Text = "In Office", Value = "In Office" });
                    jobTypeValues.Add("In Office");
                    filterValues.Add("In Office");
                }
                if (category.Administration == "on")
                {
                    //  jobTitleList.Add(new job_title { Text = "Administration", Value = "Administration" });
                    jobCategoryValues.Add("Administration");
                    filterValues.Add("Administration");
                }
                if (category.Management == "on")
                {
                    //  jobTitleList.Add(new job_title { Text = "Management", Value = "Management" });
                    jobCategoryValues.Add("Management");
                    filterValues.Add("Management");
                }
                if (category.EarlyEducation == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Education", Value = "Education" });
                    jobCategoryValues.Add("Early Education");
                    filterValues.Add("EarlyEducation");
                }



                //string apiUrl1 = apiBaseUrl + "/County/getAllActiveCounty";
                string apiUrlFilter = apiBaseUrl + "/Jobregister/get_job_search_filter_by_org/";
                jbFilter.jobCategoryValues = jobCategoryValues;
                jbFilter.org_id = org_id;
                jbFilter.jobTitleValues = jobTitleValues;
                jbFilter.jobTypeValues = jobTypeValues;
                jbFilter.is_login = is_login;
                //HttpClient client = new HttpClient();

                //  HttpResponseMessage response = client.GetAsync(apiUrl + seek_id+"/" + jobTitleValues + "/" + jobTypeValues+"/"+ jobCategoryValues).Result;
                client.BaseAddress = new Uri(apiBaseUrl + "/Jobregister/");

                var locationconsume = client.PostAsJsonAsync<JobFilter>("get_job_search_filter_by_org", jbFilter);
                locationconsume.Wait();
                var readdata = locationconsume.Result;

                if (readdata.IsSuccessStatusCode)
                {
                    customers = JsonConvert.DeserializeObject<List<job_reg_tbl>>(readdata.Content.ReadAsStringAsync().Result);
                }

                ViewBag.viewjobs = customers;
                ViewBag.countjobs = customers.Count;
                ViewBag.Adm = category.Administrator;
                
                ViewBag.cate = filterValues;

            }

            string apiUrl2 = apiBaseUrl + "/Job_cat/getAll";
            HttpResponseMessage response2 = client.GetAsync(apiUrl2).Result;
            if (response2.IsSuccessStatusCode)
            {
                job_cat = JsonConvert.DeserializeObject<List<Job_cat>>(response2.Content.ReadAsStringAsync().Result);
            }
            ViewBag.job_cat = job_cat;

            List<County_tbl> county = new List<County_tbl>();


            HttpResponseMessage response1 = client.GetAsync(apiUrl1).Result;
            if (response1.IsSuccessStatusCode)
            {
                county = JsonConvert.DeserializeObject<List<County_tbl>>(response1.Content.ReadAsStringAsync().Result);
            }
            ViewBag.county = county;

            string apiUrl3 = apiBaseUrl + "/Jobregister/getJobcategoryWithJobCount_by_Org/";
            List<Job_cat_count> category1 = new List<Job_cat_count>();
            HttpResponseMessage response3 = client.GetAsync(apiUrl3 + org_id + "/" + is_login).Result;

            if (response3.IsSuccessStatusCode)
            {
                category1 = JsonConvert.DeserializeObject<List<Job_cat_count>>(response3.Content.ReadAsStringAsync().Result);
            }
            ViewBag.category = category1;
            ViewBag.Job_catCount = category1.Count();

            string apiUrl4 = apiBaseUrl + "/Jobregister/getJobTitleWithJobCount_by_Org/";
            List<job_title> jobTitle = new List<job_title>();
            HttpResponseMessage response4 = client.GetAsync(apiUrl4 + org_id+"/"+ is_login).Result;

            if (response4.IsSuccessStatusCode)
            {
                jobTitle = JsonConvert.DeserializeObject<List<job_title>>(response4.Content.ReadAsStringAsync().Result);
            }
            ViewBag.jobTitle = jobTitle;

            string apiUrl5 = apiBaseUrl + "/Jobregister/getJobTypeWithJobCount_by_Org/";

            List<job_title> jobType = new List<job_title>();
            HttpResponseMessage response5 = client.GetAsync(apiUrl5 + org_id + "/" + is_login).Result;

            if (response5.IsSuccessStatusCode)
            {
                jobType = JsonConvert.DeserializeObject<List<job_title>>(response5.Content.ReadAsStringAsync().Result);
            }
            ViewBag.jobType = jobType;

            return View(customers);
        }

        public IActionResult Org_EmployerList()
        {
            int org_id = Convert.ToInt32(HttpContext.Session.GetString("org_loginid").ToString());
            List<Employer> customers = new List<Employer>();
            string apiUrl = apiBaseUrl + "/Employer/getAll/";

            HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync(apiUrl + org_id).Result;
            if (response.IsSuccessStatusCode)
            {
                customers = JsonConvert.DeserializeObject<List<Employer>>(response.Content.ReadAsStringAsync().Result);
            }

            return View(customers);
        }
        public async Task<ActionResult> Admin_EmployerDetails(int id)
        {
            HttpClient client = new HttpClient();

            Employer jobs = null;

            string apiUrl2 = apiBaseUrl + "/Employer/getAllemployer_by_id/";

            string apiUrl1 = apiBaseUrl + "/Jobregister/getall_job_by_employid/";

            List<Job_register_ab> job = new List<Job_register_ab>();

            HttpResponseMessage response1 = client.GetAsync(apiUrl1 + id).Result;
            if (response1.IsSuccessStatusCode)
            {
                job = JsonConvert.DeserializeObject<List<Job_register_ab>>(response1.Content.ReadAsStringAsync().Result);

            }
            ViewBag.job = job;
            ViewBag.job_count = job.Count();

            HttpResponseMessage readdata = await client.GetAsync(apiUrl2 + id);

            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<Employer>();
                displayresults.Wait();
                jobs = displayresults.Result;
                ViewBag.id = jobs.id;
                ViewBag.e_name = jobs.e_name;
                ViewBag.last_name = jobs.last_name;
                ViewBag.org_name = jobs.org_name;
                //ViewBag.job_skills = jobs.job_skills;
                //ViewBag.job_desc = jobs.job_desc;
                //ViewBag.responsibilities = jobs.responsibilities;
                //ViewBag.benefits = jobs.benefits;
                //ViewBag.schedule = jobs.shedule;
                //ViewBag.licence_certification = jobs.licence_certification;
                ViewBag.profilepic = jobs.empimage_path;
                ViewBag.start_date = jobs.start_date.ToString("MM/dd/yyyy");
                ViewBag.end_date = jobs.end_date.ToString("MM/dd/yyyy");
                ViewBag.registration_date = jobs.registration_date.ToString("MM/dd/yyyy");
                ViewBag.org_name = jobs.org_name;
                ViewBag.orgdescription = jobs.orgdescription;
                ViewBag.org_address = jobs.org_address;
                ViewBag.contact1 = jobs.contact1;
                ViewBag.email_id = jobs.email_id;
            }

            return View(jobs);



        }

        public async Task<ActionResult> Delete_admin_emp(int id)
        {

            string apiUrl = apiBaseUrl + "/Employer/Delete/";

            HttpClient client = new HttpClient();

            HttpResponseMessage readdata = await client.GetAsync(apiUrl + id);
            if (readdata.IsSuccessStatusCode)
            {


                //TempData["AlertMessage"] = "Deleted Successfully..!";
                _notyf.Error("Deleted Successfully", 3);

            }
            else
            {
                _notyf.Error("Error..! Try Again..", 5);

            }

            return RedirectToAction("Admin_EmployerList", "Employer");

        }
        public async Task<ActionResult> PostedJobsList(int id)
        {

            //int seekid=id;
            List<job_reg_tbl> customers = new List<job_reg_tbl>();
            // string apiUrl = apiBaseUrl + "/Employer/getall_job_by_id/";
            string apiUrl = apiBaseUrl + "/Jobregister/getall_job_by_employid/";
            
            HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync(apiUrl + id).Result;

            if (response.IsSuccessStatusCode)
            {
                customers = JsonConvert.DeserializeObject<List<job_reg_tbl>>(response.Content.ReadAsStringAsync().Result);
            }
            ViewBag.customers = customers;

            return View(customers);
            //return View(jobs);
        }
        public async Task<ActionResult> EmployPostedJobs_Org(int id, string jobtypetext, string search, int county_id, int job_cat_id, jobFilterValues category)
        {


            int emp_id=id;
            bool is_login = true;

            if (id != 0)
            {
                HttpContext.Session.SetString("empId", id.ToString());
            }
            else
            {
                emp_id = Convert.ToInt32(HttpContext.Session.GetString("empId").ToString());
            }

            int org_id = 0;
            //Convert.ToInt32(HttpContext.Session.GetString("org_loginid").ToString());
            List<job_reg_tbl> customers = new List<job_reg_tbl>();

            HttpClient client = new HttpClient();
            if (category.filterOn != "true")
            {
                string apiUrl = apiBaseUrl + "/Employer/getall_job_for_search_by_employee/";

                string categorytext = job_cat_id.ToString();
                string exptext = county_id.ToString();
                string searchval = search;

                if (string.IsNullOrEmpty(jobtypetext))
                {
                    jobtypetext = "all";
                }
                if (string.IsNullOrEmpty(categorytext))
                {
                    categorytext = "all";
                }
                if (string.IsNullOrEmpty(exptext))
                {
                    exptext = "all";
                }
                if (string.IsNullOrEmpty(searchval))
                {
                    searchval = "all";
                }

                
                HttpResponseMessage response = client.GetAsync(apiUrl + id + "/" + categorytext + "/" + exptext + "/" + searchval).Result;

                if (response.IsSuccessStatusCode)
                {
                    customers = JsonConvert.DeserializeObject<List<job_reg_tbl>>(response.Content.ReadAsStringAsync().Result);
                }
                ViewBag.countjobs = customers.Count();
                ViewBag.searchText = search;

            }
            else
            {
                List<job_title> jobTitleList = new List<job_title>();
                List<String> jobFilterValues = new List<String>();

                List<String> jobTitleValues = new List<String>();
                List<String> jobTypeValues = new List<String>();
                List<String> jobCategoryValues = new List<String>();
                List<String> filterValues = new List<String>();
                JobFilter jbFilter = new JobFilter();

                if (category.Administrator == "on")
                {
                    //  jobTitleList.Add(new job_title { Text = "Administrator/Director", Value = "Administrator/Director" });
                    jobTitleValues.Add("Administrator/Director");
                    filterValues.Add("Administrator");
                }
                if (category.AssistantTeacher == "on")
                {
                    //jobTitleList.Add(new job_title { Text = "Assistant Teacher", Value = "Assistant Teacher" });
                    jobTitleValues.Add("Assistant Teacher");
                    filterValues.Add("AssistantTeacher");
                }
                if (category.Floater == "on")
                {
                    //jobTitleList.Add(new job_title { Text = "Floater", Value = "Floater" });
                    jobTitleValues.Add("Floater");
                    filterValues.Add("Floater");
                }
                if (category.LeadTeacher == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Lead Teacher", Value = "Lead Teacher" });
                    jobTitleValues.Add("Lead Teacher");
                    filterValues.Add("LeadTeacher");
                }
                if (category.Substitute == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Substitute", Value = "Substitute" });
                    jobTitleValues.Add("Substitute");
                    filterValues.Add("Substitute");
                }
                if (category.DayCampAdministrator == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Day Camp Administrator", Value = "Day Camp Administrator" });
                    jobTitleValues.Add("Day Camp Administrator");
                    filterValues.Add("DayCampAdministrator");
                }
                if (category.DayCampJuniorCounselor == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Day Camp Junior Counselor", Value = "Day Camp Junior Counselor" });
                    jobTitleValues.Add("Day Camp Junior Counselor");
                    filterValues.Add("DayCampJuniorCounselor");
                }
                if (category.DayCampLeadCounselor == "on")
                {
                    //  jobTitleList.Add(new job_title { Text = "Day Camp Lead Counselor", Value = "Day Camp Lead Counselor" });
                    jobTitleValues.Add("Day Camp Lead Counselor");
                    filterValues.Add("DayCampLeadCounselor");
                }
                if (category.DayCampOwner == "on")
                {
                    //  jobTitleList.Add(new job_title { Text = "Day Camp Owner", Value = "Day Camp Owner" });
                    jobTitleValues.Add("Day Camp Owner");
                    filterValues.Add("DayCampOwner");
                }
                if (category.FullTime == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Full Time", Value = "Full Time" });
                    jobTypeValues.Add("Full Time");
                    filterValues.Add("FullTime");
                }
                if (category.PartTime == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Part Time", Value = "Part Time" });
                    jobTypeValues.Add("Part Time");
                    filterValues.Add("PartTime");
                }
                if (category.Hybrid == "on")
                {
                    //  jobTitleList.Add(new job_title { Text = "Hybrid", Value = "Hybrid" });
                    jobTypeValues.Add("Hybrid");
                    filterValues.Add("Hybrid");
                }
                if (category.Remote == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Remote", Value = "Remote" });
                    jobTypeValues.Add("Remote");
                    filterValues.Add("Remote");
                }
                if (category.Others == "on")
                {
                    //jobTitleList.Add(new job_title { Text = "Others", Value = "Others" });
                    jobTypeValues.Add("Others");
                    filterValues.Add("Others");
                }

                if (category.InOffice == "on")
                {
                    //  jobTitleList.Add(new job_title { Text = "In Office", Value = "In Office" });
                    jobTypeValues.Add("In Office");
                    filterValues.Add("In Office");
                }
                if (category.Administration == "on")
                {
                    //  jobTitleList.Add(new job_title { Text = "Administration", Value = "Administration" });
                    jobCategoryValues.Add("Administration");
                    filterValues.Add("Administration");
                }
                if (category.Management == "on")
                {
                    //  jobTitleList.Add(new job_title { Text = "Management", Value = "Management" });
                    jobCategoryValues.Add("Management");
                    filterValues.Add("Management");
                }
                if (category.EarlyEducation == "on")
                {
                    // jobTitleList.Add(new job_title { Text = "Education", Value = "Education" });
                    jobCategoryValues.Add("Early Education");
                    filterValues.Add("EarlyEducation");
                }



                //string apiUrl1 = apiBaseUrl + "/County/getAllActiveCounty";
                string apiUrlFilter = apiBaseUrl + "/Jobregister/get_job_search_filter_by_employee/";
                jbFilter.jobCategoryValues = jobCategoryValues;
                jbFilter.emp_id = emp_id;
                jbFilter.jobTitleValues = jobTitleValues;
                jbFilter.jobTypeValues = jobTypeValues;
                jbFilter.is_login = is_login;
                //HttpClient client = new HttpClient();

                //  HttpResponseMessage response = client.GetAsync(apiUrl + seek_id+"/" + jobTitleValues + "/" + jobTypeValues+"/"+ jobCategoryValues).Result;
                client.BaseAddress = new Uri(apiBaseUrl + "/Jobregister/");

                var locationconsume = client.PostAsJsonAsync<JobFilter>("get_job_search_filter_by_employee", jbFilter);
                locationconsume.Wait();
                var readdata = locationconsume.Result;

                if (readdata.IsSuccessStatusCode)
                {
                    customers = JsonConvert.DeserializeObject<List<job_reg_tbl>>(readdata.Content.ReadAsStringAsync().Result);
                }

                ViewBag.viewjobs = customers;
                ViewBag.countjobs = customers.Count;
                ViewBag.Adm = category.Administrator;
                ViewBag.cate = filterValues;

            }


            List<Job_cat> job_cat = new List<Job_cat>();
            string apiUrl2 = apiBaseUrl + "/Job_cat/getAll";
            HttpResponseMessage response2 = client.GetAsync(apiUrl2).Result;
            if (response2.IsSuccessStatusCode)
            {
                job_cat = JsonConvert.DeserializeObject<List<Job_cat>>(response2.Content.ReadAsStringAsync().Result);
            }
            ViewBag.job_cat = job_cat;

            List<County_tbl> county = new List<County_tbl>();
            string apiUrl1 = apiBaseUrl + "/County/getAllActiveCounty";

            HttpResponseMessage response1 = client.GetAsync(apiUrl1).Result;
            if (response1.IsSuccessStatusCode)
            {
                county = JsonConvert.DeserializeObject<List<County_tbl>>(response1.Content.ReadAsStringAsync().Result);
            }
            ViewBag.county = county;


            string apiUrl4 = apiBaseUrl + "/Jobregister/getJobcategoryWithJobCount_by_Employee/";
            List<Job_cat_count> category1 = new List<Job_cat_count>();
            HttpResponseMessage response4 = client.GetAsync(apiUrl4 + emp_id).Result;

            if (response4.IsSuccessStatusCode)
            {
                category1 = JsonConvert.DeserializeObject<List<Job_cat_count>>(response4.Content.ReadAsStringAsync().Result);
            }
            ViewBag.category = category1;
            ViewBag.Job_catCount = category1.Count();

            string apiUrl5 = apiBaseUrl + "/Jobregister/getJobTitleWithJobCount_by_Employee/";
            List<job_title> jobTitle = new List<job_title>();
            HttpResponseMessage response5 = client.GetAsync(apiUrl5 + emp_id).Result;

            if (response5.IsSuccessStatusCode)
            {
                jobTitle = JsonConvert.DeserializeObject<List<job_title>>(response5.Content.ReadAsStringAsync().Result);
            }
            ViewBag.jobTitle = jobTitle;
            
            string apiUrl6 = apiBaseUrl + "/Jobregister/getJobTypeWithJobCount_by_Employee/";
            List<job_title> jobType = new List<job_title>();
            HttpResponseMessage response6 = client.GetAsync(apiUrl6 + emp_id).Result;

            if (response6.IsSuccessStatusCode)
            {
                jobType = JsonConvert.DeserializeObject<List<job_title>>(response6.Content.ReadAsStringAsync().Result);
            }
            ViewBag.jobType = jobType;

            return View(customers);
            //return View(jobs);
        }

        public async Task<ActionResult> Admin_emp_EditProfile(int id)
        {
            HttpClient client = new HttpClient();
            List<Org_physical> org_name = new List<Org_physical>();
            List<State_tbl> state = new List<State_tbl>();
            List<Country_tbl> country = new List<Country_tbl>();
            List<District_tbl> district = new List<District_tbl>();

            Employer seek = null;
            string apiUrl = apiBaseUrl + "/Org/getAll";
            string apiUrl1 = apiBaseUrl + "/State/getAll";
            string apiUrl2 = apiBaseUrl + "/Country/getAll";
            //string apiUrl3 = "http://localhost:62332/District/getAll";
            string apiUrl4 = apiBaseUrl + "/Employer/getAllemployer_by_id/";

            HttpResponseMessage response = client.GetAsync(apiUrl).Result;
            if (response.IsSuccessStatusCode)
            {
                org_name = JsonConvert.DeserializeObject<List<Org_physical>>(response.Content.ReadAsStringAsync().Result);
            }
            ViewBag.org_name = org_name;

            HttpResponseMessage response1 = client.GetAsync(apiUrl1).Result;
            if (response1.IsSuccessStatusCode)
            {
                state = JsonConvert.DeserializeObject<List<State_tbl>>(response1.Content.ReadAsStringAsync().Result);
            }
            ViewBag.state = state;

            HttpResponseMessage response2 = client.GetAsync(apiUrl2).Result;
            if (response2.IsSuccessStatusCode)
            {
                country = JsonConvert.DeserializeObject<List<Country_tbl>>(response2.Content.ReadAsStringAsync().Result);
            }
            ViewBag.country = country;

            //HttpResponseMessage response3 = client.GetAsync(apiUrl3).Result;
            //if (response3.IsSuccessStatusCode)
            //{
            //    district = JsonConvert.DeserializeObject<List<District_tbl>>(response3.Content.ReadAsStringAsync().Result);
            //}
            //ViewBag.district = district;




            //int id = Convert.ToInt32(HttpContext.Session.GetString("employer_loginid"));
            HttpResponseMessage readdata = await client.GetAsync(apiUrl4 + id);

            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<Employer>();
                displayresults.Wait();
                seek = displayresults.Result;

            }

            return View(seek);



        }

        public async Task<ActionResult> Admin_emp_Update(Employer Employer)
        {
            //Employer.id = Convert.ToInt32(HttpContext.Session.GetString("employer_loginid"));

            HttpClient hc = new HttpClient();
            if (Employer.isactive == "Active")
            {
                Employer.status = true;

            }
            else
            {
                Employer.status = false;

                //  country_ab.identifier = false;
            }

            hc.BaseAddress = new Uri(apiBaseUrl + "/Employer/");
            var locationconsume = hc.PutAsJsonAsync<Employer>("Update", Employer);
            locationconsume.Wait();
            var readdata = locationconsume.Result;

            if (readdata.IsSuccessStatusCode)
            {
                _notyf.Information("Updated Successfully", 5);
                return RedirectToAction("Admin_emp_EmployerList", "Employer");

            }
            else
            {
                _notyf.Error("Error..! Try Again..", 5);
                return RedirectToAction("Admin_emp_EmployerList", "Employer");
            }
            //return View(Employer);


        }

    }
}
