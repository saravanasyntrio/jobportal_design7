﻿using AspNetCoreHero.ToastNotification.Abstractions;
using Jobportal_design.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Jobportal_design.Controllers
{
    public class CurrencyController : Controller
    {
        private IWebHostEnvironment Environment;
        private readonly INotyfService _notyf;
        private IConfiguration _Configure;
        string apiBaseUrl;
        public CurrencyController(IWebHostEnvironment _environment, INotyfService notyf, IConfiguration configuration)
        {
            Environment = _environment;
            _notyf = notyf;
            _Configure = configuration;
            apiBaseUrl = _Configure.GetValue<string>("WebAPIBaseUrl");
        }
       
        public IActionResult Currencylist()
        {
            List<Currency_tbl> customers = new List<Currency_tbl>();
            string apiUrl = apiBaseUrl + "/Currency/getAll";

            HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync(apiUrl).Result;
            if (response.IsSuccessStatusCode)
            {
                customers = JsonConvert.DeserializeObject<List<Currency_tbl>>(response.Content.ReadAsStringAsync().Result);
            }

            return View(customers);
        }

        public IActionResult AddCurrency()
        {
           
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Create(Currency_tbl currency_ab, List<IFormFile> s_currency)
        {
            string wwwPath = this.Environment.WebRootPath;
            string contentPath = this.Environment.ContentRootPath;
            string path = Path.Combine(this.Environment.WebRootPath, "General_uploads");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            List<string> uploadedFiles = new List<string>();
            foreach (IFormFile postedFile in s_currency)
            {
                string fileName = Path.GetFileName(postedFile.FileName);
                using (FileStream stream = new FileStream(Path.Combine(path, fileName), FileMode.Create))
                {
                    postedFile.CopyTo(stream);
                    uploadedFiles.Add(fileName);
                    
                }
                currency_ab.s_currency = fileName;
                currency_ab.currency_path = path;
            }
            if (currency_ab.isactive=="Active")
            {
                currency_ab.is_active = true;
            }
            else
            {
                currency_ab.is_active = false;
            }
            HttpClient hc = new HttpClient();
            hc.BaseAddress = new Uri(apiBaseUrl + "/Currency/Create");
            var locationconsume = hc.PostAsJsonAsync<Currency_tbl>("Create", currency_ab);
            locationconsume.Wait();
            var readdata = locationconsume.Result;

            if (readdata.IsSuccessStatusCode)
            {
                _notyf.Success("Success", 5);
                return RedirectToAction("Currencylist", "Currency");

            }
            else
            {
                _notyf.Error("Error..!", 5);
                return RedirectToAction("Currencylist", "Currency");
            }
            //return View(currency_ab);


        }

      
        public async Task<ActionResult> EditCurrency(int id)
        {
            Currency_tbl currency = null;
            string apiUrl = apiBaseUrl + "/Currency/get_by_id/";
            HttpClient client = new HttpClient();
           
            HttpResponseMessage readdata = await client.GetAsync(apiUrl + id);
        
            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<Currency_tbl>();
                displayresults.Wait();
                currency = displayresults.Result;
              
            }
          
            return View(currency);

        }
        public async Task<ActionResult> DeleteCurrency(int id)
        {
            
            string apiUrl = apiBaseUrl + "/Currency/Delete/";

            HttpClient client = new HttpClient();

            HttpResponseMessage readdata = await client.GetAsync(apiUrl + id);
            //  var result = responseTask.Result;
            if (readdata.IsSuccessStatusCode)
            {
                _notyf.Error("Deleted Successfully",5);

            }
            else
            {
                _notyf.Error("Try Again..!", 5);
            }

            return RedirectToAction("Currencylist", "Currency");

        }
        public async Task<ActionResult> Update(Currency_tbl currency_ab, List<IFormFile> s_currency1)
        {
            string wwwPath = this.Environment.WebRootPath;
            string contentPath = this.Environment.ContentRootPath;
            string path = Path.Combine(this.Environment.WebRootPath, "Jobseeker_uploads");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            List<string> uploadedFiles = new List<string>();
            foreach (IFormFile postedFile in s_currency1)
            {
                string fileName = Path.GetFileName(postedFile.FileName);
                using (FileStream stream = new FileStream(Path.Combine(path, fileName), FileMode.Create))
                {
                    postedFile.CopyTo(stream);
                    uploadedFiles.Add(fileName);
                    // ViewBag.Message += string.Format("<b>{0}</b> uploaded.<br />", fileName);
                }
                currency_ab.s_currency = fileName;
                currency_ab.currency_path = path;
            }
            if (currency_ab.isactive == "Active")
            {
                currency_ab.is_active = true;
            }
            else
            {
                currency_ab.is_active = false;
            }

            HttpClient hc = new HttpClient();

            hc.BaseAddress = new Uri(apiBaseUrl + "/Currency/");
            var locationconsume = hc.PutAsJsonAsync<Currency_tbl>("Update", currency_ab);
            locationconsume.Wait();
            var readdata = locationconsume.Result;

            if (readdata.IsSuccessStatusCode)
            {
                _notyf.Information("Updated Successfully",5);
                return RedirectToAction("Currencylist", "Currency");

            }
            else
            {
                _notyf.Error("Error..!", 5);
                return RedirectToAction("Currencylist", "Currency");
            }
            return View(currency_ab);


        }

    }
}
