#pragma checksum "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Org\OrganizationListPartialView.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "8c47405f1ed2a966dc07d2463e11d252f4727e16"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Org_OrganizationListPartialView), @"mvc.1.0.view", @"/Views/Org/OrganizationListPartialView.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\_ViewImports.cshtml"
using Jobportal_design;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\_ViewImports.cshtml"
using Jobportal_design.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"8c47405f1ed2a966dc07d2463e11d252f4727e16", @"/Views/Org/OrganizationListPartialView.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"ac5d920cda243f2c4c384251bb14f2cc56de9d41", @"/Views/_ViewImports.cshtml")]
    #nullable restore
    public class Views_Org_OrganizationListPartialView : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    #nullable disable
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("width", new global::Microsoft.AspNetCore.Html.HtmlString("100"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("height", new global::Microsoft.AspNetCore.Html.HtmlString("100"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/Org_img_uploads/org_profile.png"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("alt", new global::Microsoft.AspNetCore.Html.HtmlString("loading--"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("alt", new global::Microsoft.AspNetCore.Html.HtmlString("jobBox"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/imgs/template/icons/star.svg"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral(@"

<section class=""section-box mt-30"">
    <div class=""container"">
        <div class=""row flex-row-reverse"">
            <div class=""col-lg-9 col-md-12 col-sm-12 col-12 float-right"">


                <div class=""content-page"">
                    <div class=""box-filters-job"">
                        <div class=""row"">
                            <div class=""col-xl-6 col-lg-5"" id=""showCount"">Showing <strong>");
#nullable restore
#line 12 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Org\OrganizationListPartialView.cshtml"
                                                                                     Write(ViewBag.orgCount);

#line default
#line hidden
#nullable disable
            WriteLiteral(@"</strong> Organizations </div>
                            <div class=""col-xl-6 col-lg-5""><span class=""text-small text-showing""><strong> *Note:</strong> Click the heading and view the details.</span></div>

                        </div>
                    </div>

                    <div class=""row"">
                        <div id=""myTable"" class=""row"">
");
#nullable restore
#line 20 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Org\OrganizationListPartialView.cshtml"
                             foreach (var item in Model)
                            {

#line default
#line hidden
#nullable disable
            WriteLiteral(@"                                <div class=""col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12"">
                                    <div class=""card-grid-1 hover-up wow animate__animated animate__fadeIn"">
                                        <div class=""image-box"">
");
#nullable restore
#line 25 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Org\OrganizationListPartialView.cshtml"
                                             if (item.cmp_image == null)
                                            {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                                <a href=\"company-details.php\">");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "8c47405f1ed2a966dc07d2463e11d252f4727e167596", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("</a>\r\n");
#nullable restore
#line 28 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Org\OrganizationListPartialView.cshtml"
                                            }
                                            else
                                            {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                                <a href=\"company-details.php\">");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "8c47405f1ed2a966dc07d2463e11d252f4727e169333", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            BeginAddHtmlAttributeValues(__tagHelperExecutionContext, "src", 2, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            AddHtmlAttributeValue("", 1708, "~/Org_img_uploads/", 1708, 18, true);
#nullable restore
#line 31 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Org\OrganizationListPartialView.cshtml"
AddHtmlAttributeValue("", 1726, item.cmp_image, 1726, 17, false);

#line default
#line hidden
#nullable disable
            EndAddHtmlAttributeValues(__tagHelperExecutionContext);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("</a>\r\n");
#nullable restore
#line 32 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Org\OrganizationListPartialView.cshtml"
                                            }

#line default
#line hidden
#nullable disable
            WriteLiteral("                                        </div>\r\n                                        <div class=\"info-text mt-10\">\r\n                                            <h5 class=\"font-bold\"><a");
            BeginWriteAttribute("href", " href=\"", 2001, "\"", 2075, 1);
#nullable restore
#line 35 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Org\OrganizationListPartialView.cshtml"
WriteAttributeValue("", 2008, Url.Action("OrganizationProfile","Org", new { id = @item.org_id }), 2008, 67, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">");
#nullable restore
#line 35 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Org\OrganizationListPartialView.cshtml"
                                                                                                                                           Write(item.org_name);

#line default
#line hidden
#nullable disable
            WriteLiteral("</a></h5>\r\n                                            <div class=\"mt-5\">");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "8c47405f1ed2a966dc07d2463e11d252f4727e1612512", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "8c47405f1ed2a966dc07d2463e11d252f4727e1613592", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "8c47405f1ed2a966dc07d2463e11d252f4727e1614672", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "8c47405f1ed2a966dc07d2463e11d252f4727e1615752", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "8c47405f1ed2a966dc07d2463e11d252f4727e1616832", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("<span class=\"font-xs color-text-mutted ml-10\"><span>(</span><span>66</span><span>)</span></span></div><span class=\"card-location\">");
#nullable restore
#line 36 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Org\OrganizationListPartialView.cshtml"
                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Write(item.county_name);

#line default
#line hidden
#nullable disable
            WriteLiteral("</span>\r\n                                            <div class=\"mt-30\"><a class=\"btn btn-grey-big\"");
            BeginWriteAttribute("href", " href=\"", 2685, "\"", 2796, 1);
#nullable restore
#line 37 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Org\OrganizationListPartialView.cshtml"
WriteAttributeValue("", 2692, Url.Action("Org_JobList_AfterLogin","Employer", new { org_id = @item.org_id, org_name=@item.org_name }), 2692, 104, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral("><span></span><span>");
#nullable restore
#line 37 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Org\OrganizationListPartialView.cshtml"
                                                                                                                                                                                                                         Write(item.job_count);

#line default
#line hidden
#nullable disable
            WriteLiteral(" Jobs Open</span></a></div>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n");
#nullable restore
#line 41 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Org\OrganizationListPartialView.cshtml"
                            }

#line default
#line hidden
#nullable disable
            WriteLiteral(@"                        </div>
                        <div class=""col-md-3 text-center paginations"" style=""line-height: 45px;"">Showing <strong><span id=""currentP"">1</span></strong> of <strong><span id=""totalP""></span></strong>  pages </div>
                        <div class=""col-md-6 text-center paginations"">

                            <ul class=""pager"" id=""myPager"">
                            </ul>


                        </div>
                        <div class=""col-md-3 text-center paginations"">


                        </div>
                    </div>

                </div>


            </div>

            <div class=""col-lg-3 col-md-12 col-sm-12 col-12"">
                <div class=""sidebar-shadow none-shadow mb-30"">
                    <div class=""sidebar-filters sidebar-border"">
                        <div class=""filter-block head-border mt-10 mb-20"">
                            <h5>Menu </h5>
                        </div>
                        <div class=""filte");
            WriteLiteral("r-block mb-30\">\r\n                            <div class=\"select-style select-style-icon\">\r\n                                <a class=\"form-control\"");
            BeginWriteAttribute("href", " href=\"", 4194, "\"", 4236, 1);
#nullable restore
#line 70 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Org\OrganizationListPartialView.cshtml"
WriteAttributeValue("", 4201, Url.Action("Profile", "Jobseeker"), 4201, 35, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(@" style=""background: #f1f1f1; color: black;"">My Profile <i id=""ic1"" class=""fi fi-rr-angle-small-down""></i></a>
                            </div>

                        </div>
                        <div class=""filter-block mb-30"">
                            <div class=""form-group select-style select-style-icon"">
                                <a class=""form-control""");
            BeginWriteAttribute("href", " href=\"", 4616, "\"", 4662, 1);
#nullable restore
#line 76 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Org\OrganizationListPartialView.cshtml"
WriteAttributeValue("", 4623, Url.Action("EditProfile", "Jobseeker"), 4623, 39, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" style=\"background: #f1f1f1; color: black;\">Edit Profile</a>\r\n                            </div>\r\n                        </div>\r\n");
            WriteLiteral("\r\n\r\n                        <div class=\"filter-block mb-30\">\r\n                            <div class=\"form-group select-style select-style-icon\">\r\n                                <a class=\"form-control\"");
            BeginWriteAttribute("href", " href=\"", 5456, "\"", 5507, 1);
#nullable restore
#line 88 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Org\OrganizationListPartialView.cshtml"
WriteAttributeValue("", 5463, Url.Action("Appliedjobdetail", "Jobseeker"), 5463, 44, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(@" style=""background: #f1f1f1; color: black;"">Applied Jobs</a>
                            </div>
                        </div>
                        <div class=""filter-block mb-30"">
                            <div class=""form-group select-style select-style-icon"">
                                <a class=""form-control""");
            BeginWriteAttribute("href", " href=\"", 5836, "\"", 5946, 1);
#nullable restore
#line 93 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Org\OrganizationListPartialView.cshtml"
WriteAttributeValue("", 5843, Url.Action("Appliedjobdetail", "Jobseeker", new { ActiveTab = "tab-saved-jobs", isRedirect = "true" }), 5843, 103, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(@" style=""background: #f1f1f1; color: black;"">Saved Jobs</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
        $.fn.pageMe = function (opts) {
            var $this = this,
                defaults = {
                    perPage: 15,
                    showPrevNext: false,
                    hidePageNumbers: false
                },
                settings = $.extend(defaults, opts);
            console.log(settings)
            var listElement = $this;
            var perPage = settings.perPage;
            var children = listElement.children();
            var pager = $('.pager');

            if (typeof settings.childSelector != ""undefined"") {
                children = listElement.find(settings.childSelector);
            }

            if (typeof settings.pagerSelector != ""undefined"") {
                pager = $(settings.pagerSelecto");
            WriteLiteral(@"r);
            }

            var numItems = children.size();
            var numPages = Math.ceil(numItems / perPage);

            pager.data(""curr"", 0);

            if (settings.showPrevNext) {
                $('<li><a href=""#"" class=""pager-prev""></a></li>').appendTo(pager);
            }

            var curr = 0;
            // Added class and id in li start
            while (numPages > curr && (settings.hidePageNumbers == false)) {
                $('<li id=""pg' + (curr + 1) + '"" class=""pg""><a href=""#"" class=""page_link"">' + (curr + 1) + '</a></li>').appendTo(pager);
                curr++;
            }
            // Added class and id in li end

            if (settings.showPrevNext) {
                $('<li><a href=""#"" class=""pager-next""></a></li>').appendTo(pager);
            }

            pager.find('.page_link:first').addClass('active');
            pager.find('.pager-prev').hide();
            if (numPages <= 1) {
                pager.find('.pager-next').hide();");
            WriteLiteral(@"
            }
           // pager.children().eq(1).addClass(""active"");

            children.hide();
            children.slice(0, perPage).show();
            if (numPages > 3) {
                $('.pg').hide();
                $('#pg1,#pg2,#pg3').show();
                $(""#pg3"").after($(""<li class='ell'>"").html(""<span>...</span>""));
            }

            pager.find('li .page_link').click(function () {
                var clickedPage = $(this).html().valueOf() - 1;
                goTo(clickedPage, perPage);
                return false;
            });
            pager.find('li .pager-prev').click(function () {
                previous();
                return false;
            });
            pager.find('li .pager-next').click(function () {
                next();
                return false;
            });

            function previous() {
                var goToPage = parseInt(pager.data(""curr"")) - 1;
                $('.pg').children().eq(goToPage + 1).removeClas");
            WriteLiteral(@"s(""active"");
                goTo(goToPage);
            }

            function next() {
                goToPage = parseInt(pager.data(""curr"")) + 1;
                goTo(goToPage);
            }

            function goTo(page) {
                var startAt = page * perPage,
                    endOn = startAt + perPage;

                // Added few lines from here start

                $('.pg').hide();
                $("".ell"").remove();
                var prevpg = $(""#pg"" + page).show();
                var currpg = $(""#pg"" + (page + 1)).show();
                var nextpg = $(""#pg"" + (page + 2)).show();
                if (prevpg.length == 0) nextpg = $(""#pg"" + (page + 3)).show();
                if (prevpg.length == 1 && nextpg.length == 0) {
                    prevpg = $(""#pg"" + (page - 1)).show();
                }
                $(""#pg1"").show()
                if (curr > 3) {
                    if (page > 1) prevpg.before($(""<li class='ell'>"").html(""<span>...</span>""))");
            WriteLiteral(@";
                    if (page < curr - 2) nextpg.after($(""<li class='ell'>"").html(""<span>...</span>""));
                }

                if (page <= numPages - 3) {
                    $(""#pg"" + numPages.toString()).show();
                }
                currpg.addClass(""active"").siblings().removeClass(""active"");
                // Added few lines till here end


                children.css('display', 'none').slice(startAt, endOn).show();

                if (page >= 1) {
                    pager.find('.pager-prev').show();
                } else {
                    pager.find('.pager-prev').hide();
                }

                if (page < (numPages - 1)) {
                    pager.find('.pager-next').show();
                } else {
                    pager.find('.pager-next').hide();
                }

                pager.data(""curr"", page);
                if ($('.pg').children().eq(page - 1).hasClass(""active"")) {
                    $('.pg').children().eq(page");
            WriteLiteral(@" - 1).removeClass(""active"");
                }
                if ($('.pg').children().eq(page + 1).hasClass(""active"")) {
                    $('.pg').children().eq(page + 1).removeClass(""active"");
                }
                $('.pg').children().eq(page).addClass(""active"");
                var fp = (page * 15)+1;
                var tp = (page+1) * 15;
                $(""#startP"").text(fp.toString());
                if(tp<");
#nullable restore
#line 241 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Org\OrganizationListPartialView.cshtml"
                 Write(ViewBag.orgCount);

#line default
#line hidden
#nullable disable
            WriteLiteral(")\r\n                $(\"#endP\").text(tp.toString());\r\n                else\r\n                $(\"#endP\").text(");
#nullable restore
#line 244 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Org\OrganizationListPartialView.cshtml"
                           Write(ViewBag.orgCount);

#line default
#line hidden
#nullable disable
            WriteLiteral(@");
                $(""#currentP"").text((page+1).toString());
                $(""#totalP"").text(numPages.toString());
                window.scrollTo(0, 250);

            }
        };

        $(document).ready(function () {
            $(""#currentP"").text(""1"");
                $(""#totalP"").text(Math.ceil(");
#nullable restore
#line 254 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Org\OrganizationListPartialView.cshtml"
                                       Write(ViewBag.orgCount);

#line default
#line hidden
#nullable disable
            WriteLiteral("/15).toString());\r\n            $(\'#myTable\').pageMe({\r\n                pagerSelector: \'#myPager\',\r\n                showPrevNext: true,\r\n                hidePageNumbers: false,\r\n                perPage: 15\r\n            });\r\n\r\n        });\r\n</script>");
        }
        #pragma warning restore 1998
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; } = default!;
        #nullable disable
    }
}
#pragma warning restore 1591
