#pragma checksum "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Org_mem\Org_memlist.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "90e08405481f22fa5d8fd333111a207d7ef8068f"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Org_mem_Org_memlist), @"mvc.1.0.view", @"/Views/Org_mem/Org_memlist.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\_ViewImports.cshtml"
using Jobportal_design;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\_ViewImports.cshtml"
using Jobportal_design.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"90e08405481f22fa5d8fd333111a207d7ef8068f", @"/Views/Org_mem/Org_memlist.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"ac5d920cda243f2c4c384251bb14f2cc56de9d41", @"/Views/_ViewImports.cshtml")]
    #nullable restore
    public class Views_Org_mem_Org_memlist : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<Jobportal_design.Models.Org_mem_ab>>
    #nullable disable
    {
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 2 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Org_mem\Org_memlist.cshtml"
  
    ViewData["Title"] = "Org_memlist";
    Layout = "~/Views/Shared/_Layout.cshtml";


#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("head", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "90e08405481f22fa5d8fd333111a207d7ef8068f3684", async() => {
                WriteLiteral(@"
    <link rel=""Stylesheet"" href=""https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css"" />
    <script type=""text/javascript"" src=""https://code.jquery.com/jquery-1.12.3.js""></script>
    <script type=""text/javascript"" src=""https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js""></script>
    <link rel=""stylesheet"" href=""https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"">
    <script>
        $(document).ready(function () {
            $('#MyTable').DataTable({
                initComplete: function () {
                    this.api().columns().every(function () {
                        var column = this;
                        var select = $('<select><option value=""""></option></select>')
                            .appendTo($(column.footer()).empty())
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
         ");
                WriteLiteral(@"                       );
                                //to select and search from grid
                                column
                                    .search(val ? '^' + val + '$' : '', true, false)
                                    .draw();
                            });

                        column.data().unique().sort().each(function (d, j) {
                            select.append('<option value=""' + d + '"">' + d + '</option>')
                        });
                    });
                }
            });
        });
    </script>
");
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("body", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "90e08405481f22fa5d8fd333111a207d7ef8068f6318", async() => {
                WriteLiteral("\r\n    <div class=\"header-btn d-none f-right d-lg-block\">\r\n\r\n\r\n\r\n        <input type=\"button\" class=\"genric-btn primary circle arrow\" value=\"Add Organization members details\"");
                BeginWriteAttribute("onclick", " onclick=\"", 1961, "\"", 2023, 3);
                WriteAttributeValue("", 1971, "location.href=\'", 1971, 15, true);
#nullable restore
#line 46 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Org_mem\Org_memlist.cshtml"
WriteAttributeValue("", 1986, Url.Action("AddOrg_mem", "Org_mem"), 1986, 36, false);

#line default
#line hidden
#nullable disable
                WriteAttributeValue("", 2022, "\'", 2022, 1, true);
                EndWriteAttribute();
                WriteLiteral(@" />

    </div>


    <br />
    <br />
    <br />
    <table id=""MyTable"" class=""display"" cellspacing=""0"" width=""100%"">
        <thead>
            <tr>
                <th>organization Name</th>
                <th>Stakeholder name</th>

                <th>Cont No:</th>
                <th>Website:</th>
                <th>Status</th>
                <th></th>

            </tr>
        </thead>

        <tbody>
");
#nullable restore
#line 69 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Org_mem\Org_memlist.cshtml"
             foreach (var item in Model)
            {
                bool str = item.status;
                string str1 = str.ToString();


#line default
#line hidden
#nullable disable
                WriteLiteral("                <tr>\r\n                    <td>\r\n                        ");
#nullable restore
#line 76 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Org_mem\Org_memlist.cshtml"
                   Write(Html.DisplayFor(modelItem => item.org_name));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n                    </td>\r\n\r\n                    <td>\r\n                        ");
#nullable restore
#line 80 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Org_mem\Org_memlist.cshtml"
                   Write(Html.DisplayFor(modelItem => item.stake_holder));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n                    </td>\r\n\r\n\r\n                    <td>\r\n                        ");
#nullable restore
#line 85 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Org_mem\Org_memlist.cshtml"
                   Write(Html.DisplayFor(modelItem => item.con_number1));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
#nullable restore
#line 88 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Org_mem\Org_memlist.cshtml"
                   Write(Html.DisplayFor(modelItem => item.website_personal));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
#nullable restore
#line 91 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Org_mem\Org_memlist.cshtml"
                   Write(Html.DisplayFor(modelItem => str1));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n                    </td>\r\n\r\n\r\n                    <td>\r\n                        ");
#nullable restore
#line 96 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Org_mem\Org_memlist.cshtml"
                   Write(Html.ActionLink("EDIT", "EditOrg_mem", "Org_mem", new { id = @item.org_m_id }, new { @class = "genric-btn info circle small" }));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n                        ");
#nullable restore
#line 97 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Org_mem\Org_memlist.cshtml"
                   Write(Html.ActionLink("DELETE", "DeleteOrg_mem", "Org_mem", new { id = @item.org_m_id }, new { @class = "genric-btn danger circle small" }));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n");
                WriteLiteral("                    </td>\r\n\r\n\r\n                </tr>\r\n");
#nullable restore
#line 103 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Org_mem\Org_memlist.cshtml"
            }

#line default
#line hidden
#nullable disable
                WriteLiteral("        </tbody>\r\n    </table>\r\n");
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n");
        }
        #pragma warning restore 1998
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<Jobportal_design.Models.Org_mem_ab>> Html { get; private set; } = default!;
        #nullable disable
    }
}
#pragma warning restore 1591
