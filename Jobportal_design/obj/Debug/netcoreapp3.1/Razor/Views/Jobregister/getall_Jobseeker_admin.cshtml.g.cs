#pragma checksum "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Jobregister\getall_Jobseeker_admin.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "2302c6673722be6b41e0c893f11d37d583691935"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Jobregister_getall_Jobseeker_admin), @"mvc.1.0.view", @"/Views/Jobregister/getall_Jobseeker_admin.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\_ViewImports.cshtml"
using Jobportal_design;

#line default
#line hidden
#nullable disable
#nullable restore
#line 1 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Jobregister\getall_Jobseeker_admin.cshtml"
using Jobportal_design.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"2302c6673722be6b41e0c893f11d37d583691935", @"/Views/Jobregister/getall_Jobseeker_admin.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"ac5d920cda243f2c4c384251bb14f2cc56de9d41", @"/Views/_ViewImports.cshtml")]
    #nullable restore
    public class Views_Jobregister_getall_Jobseeker_admin : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    #nullable disable
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("href", new global::Microsoft.AspNetCore.Html.HtmlString("~/css/new-style.css"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("rel", new global::Microsoft.AspNetCore.Html.HtmlString("stylesheet"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("width", new global::Microsoft.AspNetCore.Html.HtmlString("100"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("height", new global::Microsoft.AspNetCore.Html.HtmlString("100"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/imgs/page/candidates/candidate_profile1.png"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("alt", new global::Microsoft.AspNetCore.Html.HtmlString("loading.."), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_6 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/imgs/template/icons/star.svg"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_7 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("alt", new global::Microsoft.AspNetCore.Html.HtmlString("jobBox"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 3 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Jobregister\getall_Jobseeker_admin.cshtml"
  
    ViewData["Title"] = "View";
    Layout = "~/Views/Shared/_Layout_admin.cshtml";


#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("link", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "2302c6673722be6b41e0c893f11d37d5836919356608", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("body", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "2302c6673722be6b41e0c893f11d37d5836919357722", async() => {
                WriteLiteral(@"
    <a href=""#"" class=""ml-5"" style=""color: #85c440;"" id=""btnBack""><i class=""fi fi-rr-arrow-left"" style=""color: #85c440; font-size: 13px;""></i><span style=""color: black;"">&nbsp;Back</span></a>

    <section class=""section-box-2 sidebar-border mb-0 mt-5"">
        <div class=""container"">
            <div class=""banner-hero banner-company bg-white"">
                <div class=""block-banner text-center"">
                    <h3 class=""wow animate__animated animate__fadeInUp"" style=""color: #80287d;""> Job Applicants (");
#nullable restore
#line 17 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Jobregister\getall_Jobseeker_admin.cshtml"
                                                                                                            Write(ViewBag.countjobs);

#line default
#line hidden
#nullable disable
                WriteLiteral(@")</h3>
                    <div class=""font-sm color-text-paragraph-2 mt-10 wow animate__animated animate__fadeInUp text-dark"" data-wow-delay="".1s"">Lorem ipsum dolor sit amet consectetur adipisicing elit. Vero repellendus magni, <br class=""d-none d-xl-block"">atque delectus molestias quis?</div>
");
                WriteLiteral(@"                </div>
            </div>
        </div>
    </section>
    <section class=""section-box mt-30 sidebar-border"">
        <div class=""container"">
            <div class=""content-page"">
                <div class=""box-filters-job"">
                    <div class=""row"">
                        <div class=""col-xl-8 col-lg-6"">
                            <span> Showing <strong>");
#nullable restore
#line 59 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Jobregister\getall_Jobseeker_admin.cshtml"
                                              Write(ViewBag.countjobs);

#line default
#line hidden
#nullable disable
                WriteLiteral(@" </strong> Job Applicants</span>
                        </div>
                        <div class=""col-xl-4 col-lg-6""><span class=""text-small text-showing""><strong> *Note:</strong> Click the heading and view the details.</span></div>

                    </div>
                </div>
                <div class=""col-md-12 row"">
                    <div id=""myTable"" class=""row"">
");
#nullable restore
#line 67 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Jobregister\getall_Jobseeker_admin.cshtml"
                         if (ViewBag.Jobseeker != null)
                        {
                            

#line default
#line hidden
#nullable disable
#nullable restore
#line 69 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Jobregister\getall_Jobseeker_admin.cshtml"
                             foreach (var item in ViewBag.Jobseeker)
                            {

#line default
#line hidden
#nullable disable
                WriteLiteral(@"                                <div class=""col-xl-4 col-lg-4 col-md-6"">
                                    <div class=""card-grid-2 hover-up"">
                                        <div class=""card-grid-2-image-left jobseeker-getall-div01"">
                                            <div class=""card-grid-2-image-rd online"">
                                                <a href=""candidate-details.php"">
");
#nullable restore
#line 76 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Jobregister\getall_Jobseeker_admin.cshtml"
                                                     if (item.s_pic == null)
                                                    {

#line default
#line hidden
#nullable disable
                WriteLiteral("                                                        <figure> ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "2302c6673722be6b41e0c893f11d37d58369193511872", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("</figure>\r\n");
#nullable restore
#line 79 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Jobregister\getall_Jobseeker_admin.cshtml"
                                                    }
                                                    else
                                                    {

#line default
#line hidden
#nullable disable
                WriteLiteral("                                                        <figure> ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "2302c6673722be6b41e0c893f11d37d58369193513699", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
                BeginAddHtmlAttributeValues(__tagHelperExecutionContext, "src", 2, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                AddHtmlAttributeValue("", 4762, "~/Jobseeker_uploads/", 4762, 20, true);
#nullable restore
#line 82 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Jobregister\getall_Jobseeker_admin.cshtml"
AddHtmlAttributeValue("", 4782, item.s_pic, 4782, 13, false);

#line default
#line hidden
#nullable disable
                EndAddHtmlAttributeValues(__tagHelperExecutionContext);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("</figure>\r\n");
#nullable restore
#line 83 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Jobregister\getall_Jobseeker_admin.cshtml"
                                                                                                                                                       
                                                    }

#line default
#line hidden
#nullable disable
                WriteLiteral("                                                </a>\r\n                                            </div>\r\n                                            <div class=\"card-profile pt-10 jobseeker-getall01\">\r\n                                                <a");
                BeginWriteAttribute("href", " href=\"", 5285, "\"", 5360, 1);
#nullable restore
#line 88 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Jobregister\getall_Jobseeker_admin.cshtml"
WriteAttributeValue("", 5292, Url.Action("Admin_seek_Profile", "Jobseeker",new { id = @item.id }), 5292, 68, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(">\r\n                                                    <h5>");
#nullable restore
#line 89 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Jobregister\getall_Jobseeker_admin.cshtml"
                                                   Write(item.fname);

#line default
#line hidden
#nullable disable
                WriteLiteral("&nbsp;");
#nullable restore
#line 89 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Jobregister\getall_Jobseeker_admin.cshtml"
                                                                    Write(item.lname);

#line default
#line hidden
#nullable disable
                WriteLiteral("</h5>\r\n                                                </a><span class=\"font-xs color-text-mutted\">");
#nullable restore
#line 90 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Jobregister\getall_Jobseeker_admin.cshtml"
                                                                                       Write(item.email_id);

#line default
#line hidden
#nullable disable
                WriteLiteral("</span>\r\n                                                <div class=\"rate-reviews-small pt-5\"><span>");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "2302c6673722be6b41e0c893f11d37d58369193517993", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_6);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_7);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("</span><span>");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "2302c6673722be6b41e0c893f11d37d58369193519177", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_6);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_7);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("</span><span>");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "2302c6673722be6b41e0c893f11d37d58369193520361", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_6);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_7);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("</span><span>");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "2302c6673722be6b41e0c893f11d37d58369193521545", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_6);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_7);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("</span><span>");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "2302c6673722be6b41e0c893f11d37d58369193522729", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_6);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_7);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral(@"</span><span class=""ml-10 color-text-mutted font-xs"">(65)</span></div>
                                            </div>
                                        </div>
                                        <div class=""card-block-info"">
                                            
                                            <div class=""card-2-bottom card-2-bottom-candidate"">
                                                <div class=""text-start"">
                                                    <p class=""font-xs color-text-paragraph-2"">Gender : &nbsp;&nbsp;<strong>");
#nullable restore
#line 98 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Jobregister\getall_Jobseeker_admin.cshtml"
                                                                                                                      Write(item.gender);

#line default
#line hidden
#nullable disable
                WriteLiteral("</strong></p>\r\n                                                    <p class=\"font-xs color-text-paragraph-2\">Education : &nbsp;&nbsp;<strong>");
#nullable restore
#line 99 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Jobregister\getall_Jobseeker_admin.cshtml"
                                                                                                                         Write(item.highest_degree);

#line default
#line hidden
#nullable disable
                WriteLiteral("</strong></p>\r\n");
                WriteLiteral(@"                                                </div>
                                            </div>
                                            <div class=""employers-info align-items-center justify-content-center mt-15"">
                                                <div class=""row"">
                                                    <div class=""col-md-12""><span class=""d-flex align-items-center""><i class=""fi-rr-marker mr-5 ml-0""></i> Location : &nbsp;<span class=""font-sm color-text-mutted"">");
#nullable restore
#line 105 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Jobregister\getall_Jobseeker_admin.cshtml"
                                                                                                                                                                                                              Write(item.seekcity);

#line default
#line hidden
#nullable disable
                WriteLiteral(@"</span></span></div>
                                                    <div class=""col-md-12 mt-3""><span class=""d-flex justify-content-start align-items-center""><i class=""fi-rr-calendar mr-5 ml-0""></i> Applied Date : &nbsp;<span class=""font-sm color-brand-1"">");
#nullable restore
#line 106 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Jobregister\getall_Jobseeker_admin.cshtml"
                                                                                                                                                                                                                                           Write(item.date_applied.ToString("MM/dd/yyyy"));

#line default
#line hidden
#nullable disable
                WriteLiteral(@"</span></span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
");
#nullable restore
#line 112 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Jobregister\getall_Jobseeker_admin.cshtml"
                            }

#line default
#line hidden
#nullable disable
#nullable restore
#line 112 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Jobregister\getall_Jobseeker_admin.cshtml"
                             
                        }

#line default
#line hidden
#nullable disable
                WriteLiteral("                    </div>\r\n");
#nullable restore
#line 115 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Jobregister\getall_Jobseeker_admin.cshtml"
                     if (@ViewBag.countjobs == 0)
                    {

#line default
#line hidden
#nullable disable
                WriteLiteral("                        <div class=\"col-md-3 text-center paginations\" style=\"line-height: 45px;\">  <strong>No Job Applied Applicants</strong>  </div>\r\n");
#nullable restore
#line 118 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Jobregister\getall_Jobseeker_admin.cshtml"
                    }
                    else
                    {

#line default
#line hidden
#nullable disable
                WriteLiteral("                        <div class=\"col-md-3 text-center paginations\" style=\"line-height: 45px;\">Showing <strong><span id=\"currentP\">1</span></strong> of <strong><span id=\"totalP\"></span></strong>  pages </div>\r\n");
#nullable restore
#line 122 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Jobregister\getall_Jobseeker_admin.cshtml"
                    }

#line default
#line hidden
#nullable disable
                WriteLiteral(@"                    <div class=""col-md-6 text-center paginations"">

                        <ul class=""pager"" id=""myPager"">
                        </ul>


                    </div>
                    <div class=""col-md-3 text-center paginations"">


                    </div>
                </div>
            </div>

        </div>
    </section>
    <br />


");
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n");
            DefineSection("scripts", async() => {
                WriteLiteral(@"
    <script type=""text/javascript"">
        $(""#btnBack"").click(function () {
            window.history.back();
        });
    </script>

    <script>
        $.fn.pageMe = function (opts) {
            var $this = this,
                defaults = {
                    perPage: 15,
                    showPrevNext: false,
                    hidePageNumbers: false
                },
                settings = $.extend(defaults, opts);
            console.log(settings)
            var listElement = $this;
            var perPage = settings.perPage;
            var children = listElement.children();
            var pager = $('.pager');

            if (typeof settings.childSelector != ""undefined"") {
                children = listElement.find(settings.childSelector);
            }

            if (typeof settings.pagerSelector != ""undefined"") {
                pager = $(settings.pagerSelector);
            }

            var numItems = children.size();
            var numPages ");
                WriteLiteral(@"= Math.ceil(numItems / perPage);

            pager.data(""curr"", 0);

            if (settings.showPrevNext) {
                $('<li><a href=""#"" class=""pager-prev""></a></li>').appendTo(pager);
            }

            var curr = 0;
            // Added class and id in li start
            while (numPages > curr && (settings.hidePageNumbers == false)) {
                $('<li id=""pg' + (curr + 1) + '"" class=""pg""><a href=""#"" class=""page_link"">' + (curr + 1) + '</a></li>').appendTo(pager);
                curr++;
            }
            // Added class and id in li end

            if (settings.showPrevNext) {
                $('<li><a href=""#"" class=""pager-next""></a></li>').appendTo(pager);
            }

            pager.find('.page_link:first').addClass('active');
            pager.find('.pager-prev').hide();
            if (numPages <= 1) {
                pager.find('.pager-next').hide();
            }
           // pager.children().eq(1).addClass(""active"");

            chil");
                WriteLiteral(@"dren.hide();
            children.slice(0, perPage).show();
            if (numPages > 3) {
                $('.pg').hide();
                $('#pg1,#pg2,#pg3').show();
                $(""#pg3"").after($(""<li class='ell'>"").html(""<span>...</span>""));
            }

            pager.find('li .page_link').click(function () {
                var clickedPage = $(this).html().valueOf() - 1;
                goTo(clickedPage, perPage);
                return false;
            });
            pager.find('li .pager-prev').click(function () {
                previous();
                return false;
            });
            pager.find('li .pager-next').click(function () {
                next();
                return false;
            });

            function previous() {
                var goToPage = parseInt(pager.data(""curr"")) - 1;
                $('.pg').children().eq(goToPage + 1).removeClass(""active"");
                goTo(goToPage);
            }

            function next() ");
                WriteLiteral(@"{
                goToPage = parseInt(pager.data(""curr"")) + 1;
                goTo(goToPage);
            }

            function goTo(page) {
                var startAt = page * perPage,
                    endOn = startAt + perPage;

                // Added few lines from here start

                $('.pg').hide();
                $("".ell"").remove();
                var prevpg = $(""#pg"" + page).show();
                var currpg = $(""#pg"" + (page + 1)).show();
                var nextpg = $(""#pg"" + (page + 2)).show();
                if (prevpg.length == 0) nextpg = $(""#pg"" + (page + 3)).show();
                if (prevpg.length == 1 && nextpg.length == 0) {
                    prevpg = $(""#pg"" + (page - 1)).show();
                }
                $(""#pg1"").show()
                if (curr > 3) {
                    if (page > 1) prevpg.before($(""<li class='ell'>"").html(""<span>...</span>""));
                    if (page < curr - 2) nextpg.after($(""<li class='ell'>"").html(""<span>.");
                WriteLiteral(@"..</span>""));
                }

                if (page <= numPages - 3) {
                    $(""#pg"" + numPages.toString()).show();
                }
                currpg.addClass(""active"").siblings().removeClass(""active"");
                // Added few lines till here end


                children.css('display', 'none').slice(startAt, endOn).show();

                if (page >= 1) {
                    pager.find('.pager-prev').show();
                } else {
                    pager.find('.pager-prev').hide();
                }

                if (page < (numPages - 1)) {
                    pager.find('.pager-next').show();
                } else {
                    pager.find('.pager-next').hide();
                }

                pager.data(""curr"", page);
                if ($('.pg').children().eq(page - 1).hasClass(""active"")) {
                    $('.pg').children().eq(page - 1).removeClass(""active"");
                }
                if ($('.pg').children().eq(");
                WriteLiteral(@"page + 1).hasClass(""active"")) {
                    $('.pg').children().eq(page + 1).removeClass(""active"");
                }
                $('.pg').children().eq(page).addClass(""active"");
                var fp = (page * 15)+1;
                var tp = (page+1) * 15;
                $(""#startP"").text(fp.toString());
                if (tp < ");
#nullable restore
#line 287 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Jobregister\getall_Jobseeker_admin.cshtml"
                    Write(ViewBag.countjobs);

#line default
#line hidden
#nullable disable
                WriteLiteral(")\r\n                    $(\"#endP\").text(tp.toString());\r\n                else\r\n                    $(\"#endP\").text(");
#nullable restore
#line 290 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Jobregister\getall_Jobseeker_admin.cshtml"
                               Write(ViewBag.countjobs);

#line default
#line hidden
#nullable disable
                WriteLiteral(@");
                $(""#currentP"").text((page+1).toString());
                $(""#totalP"").text(numPages.toString());
                window.scrollTo(0, 250);

            }
        };

        $(document).ready(function () {
            $(""#currentP"").text(""1"");
                $(""#totalP"").text(Math.ceil(");
#nullable restore
#line 300 "D:\Saravana\New Projects\Jobs\job_git\jobportal-design\Jobportal_design\Views\Jobregister\getall_Jobseeker_admin.cshtml"
                                       Write(ViewBag.countjobs);

#line default
#line hidden
#nullable disable
                WriteLiteral("/15).toString());\r\n            $(\'#myTable\').pageMe({\r\n                pagerSelector: \'#myPager\',\r\n                showPrevNext: true,\r\n                hidePageNumbers: false,\r\n                perPage: 15\r\n            });\r\n\r\n        });\r\n    </script>\r\n");
            }
            );
            WriteLiteral(@"<style>
    .card-grid-2 .card-grid-2-image-rd {
        min-width: 60px !important;
        max-width: 75px !important;
        height: 60px;
    }
    .card-grid-2 .card-grid-2-image-rd figure img {            
        height: 60px!important;
        width: 60px!important;
    }
    .banner-hero .block-banner {
        background-color: white!important;
    }
</style>");
        }
        #pragma warning restore 1998
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; } = default!;
        #nullable disable
    }
}
#pragma warning restore 1591
