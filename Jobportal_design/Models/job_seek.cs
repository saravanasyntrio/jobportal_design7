﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Jobportal_design.Models
{
    public class job_seek
    {
        public int id { get; set; }
        public int org_cat_id { get; set; }
        public int seek_id { get; set; }
        public string s_pic { get; set; }
        public string category_name { get; set; }
        public string job_title { get; set; }
        public string job_type { get; set; }
        public string salary_package { get; set; }
        public string job_skills { get; set; }
        public string experience { get; set; }
        public DateTime posting_date { get; set; }
        public DateTime start_date { get; set; }
        public DateTime end_date { get; set; }
        public string job_desc { get; set; }
        public int state_fk { get; set; }
        public int state_id { get; set; }
        public string state_name { get; set; }
        public int desired_state_fk { get; set; }

        public int employeer_id { get; set; }
        public string e_name { get; set; }
        public int org_id { get; set; }
        public string org_name { get; set; }


        [Required]
        public string fname { get; set; }

        [Required]
        public string lname { get; set; }

        [Required]
        public string email_id { get; set; }

        [Required]
        public string e_password { get; set; }

        [Required]
        public int seeker_type { get; set; }

        [Required]
        public DateTime registration_date { get; set; }

        [Required]
        public string racial { get; set; }
        [Required]
        public string gender { get; set; }

        public string seekaddress { get; set; }
    
        public string seekcity { get; set; }

        [Required]
        [ForeignKey("country")]
        public int country_id { get; set; }

        [Column(TypeName = "varchar(100)")]
        [Required]


        public string country_name { get; set; }

        [Column(TypeName = "varchar(100)")]

        public string zipcode { get; set; }
        [Column(TypeName = "varchar(100)")]
        [Required]
        public string phone { get; set; }

        [Column(TypeName = "varchar(25)")]
        [Required]
        public string contact1 { get; set; }

        [Column(TypeName = "varchar(25)")]

        public string contact2 { get; set; }



        [Column(TypeName = "varchar(50)")]
        public string disability { get; set; }
        [Column(TypeName = "varchar(100)")]
        public string dis_reason { get; set; }


        [Column(TypeName = "varchar(50)")]
        [Required]
        public string DOB { get; set; }
        [Required]
        public Boolean status { get; set; }

        [Column(TypeName = "varchar(500)")]
        public string schoolattended { get; set; }
        [Column(TypeName = "varchar(500)")]
        public string lastgrade { get; set; }
        [Column(TypeName = "varchar(500)")]
        public string collegeattended { get; set; }
        [Column(TypeName = "varchar(500)")]
        public string degreereceived { get; set; }
        [Column(TypeName = "varchar(500)")]
        public string certificatereceived { get; set; }
        [Column(TypeName = "varchar(500)")]
        public string academic { get; set; }
        [Column(TypeName = "varchar(500)")]
        public string s_resume { get; set; }
        [Column(TypeName = "varchar(100)")]
        public string total_exp { get; set; }
        public string current_job { get; set; }

        public string previous_job { get; set; }

        public string list_skill { get; set; }

        public State_tbl state { get; set; }


        public Country_tbl country { get; set; }



        public string primary_lng { get; set; }

        public string secondary_lng { get; set; }

        public string about_me { get; set; }


        public int job_reg_id { get; set; }

        public DateTime date_applied { get; set; }
        public string secondary_phone_type { get; set; }
        public string primary_phone_type { get; set; }

        public string highest_degree { get; set; }

    }
}
