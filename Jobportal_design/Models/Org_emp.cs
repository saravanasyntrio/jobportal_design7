﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Jobportal_design.Models
{
    public class Org_emp
    {
        public int org_id { get; set; }
        public int passedId { get; set; }
        public string activeStat { get; set; }
        public string OrgAct { get; set; }
        public string OrgInAct { get; set; }

        public string org_name { get; set; }
        public string short_name { get; set; }
        public int parent_id { get; set; }
        public int org_type_id { get; set; }
        public string category_name { get; set; }
        public int country_id { get; set; }
        public string country_name { get; set; }
        public int state_id { get; set; }
        public string state_name { get; set; }
        public int currency_id { get; set; }
        public string currency_name { get; set; }
        public int dist_id { get; set; }
        public string dist_name { get; set; }
        public string code { get; set; }
        public string tax_number { get; set; }
        public string vat_number { get; set; }
        public DateTime tl_expiry_date { get; set; }


        public string c_group_name { get; set; }
        public string comme_name { get; set; }
        public Byte cmp_img { get; set; }
        public Byte cmp_logo { get; set; }
        public string land_mark { get; set; }
        public string lat_loc { get; set; }
        public string lng_loc { get; set; }
        public string door_no { get; set; }
        public string street_name { get; set; }
        public string loc_name { get; set; }
        public string dist { get; set; }
        public string post_no { get; set; }
        public DateTime est_year { get; set; }
        public int no_branch { get; set; }
        public Boolean status { get; set; }
        public string isactive { get; set; }
        public Boolean is_active { get; set; }
        public string ProviderID { get; set; }
        public string LicenseID { get; set; }
        public string Providertype { get; set; }
        public string district_name { get; set; }

        [NotMapped]
        [DisplayName("Upload Symbol")]
        public IFormFile ImageFile { get; set; }



        public string cmp_image { get; set; }
        public string cmp_image1 { get; set; }

        public string cmpimage_path { get; set; }
        public string cmplogo { get; set; }
        public string cmplogo1 { get; set; }

        public string cmplogo_path { get; set; }

        public DateTime DBA { get; set; }
        public string org_address { get; set; }

        public string county_name { get; set; }
        public string location_name { get; set; }
        public string zip { get; set; }

        public string Phone1 { get; set; }

        public string Phone2 { get; set; }
        public string Email1 { get; set; }

        public string Email2 { get; set; }
        public string website { get; set; }

        public string orgdescription { get; set; }

        public int id { get; set; }


        public string email_id { get; set; }


        public string e_password { get; set; }
        public string retype_password { get; set; }
        public DateTime registration_date { get; set; }

        public string contact1 { get; set; }
        public string contact2 { get; set; }
        public DateTime start_date { get; set; }
        public DateTime end_date { get; set; }



       


        public string e_name { get; set; }

        public string last_name { get; set; }
       

        public string Name { get; set; }

        public string secondary_phone_type { get; set; }
        public string primary_phone_type { get; set; }
        public string primary_extension { get; set; }
        public string secondary_extension { get; set; }
        public string new_password { get; set; }

        public string user_name { get; set; }
    }
}
