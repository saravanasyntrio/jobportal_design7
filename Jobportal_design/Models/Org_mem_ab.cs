﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jobportal_design.Models
{
    public class Org_mem_ab
    {
        public int org_m_id { get; set; }
        public int org_id { get; set; }
        public string org_name { get; set; }
        public int org_c_id { get; set; }
        public string stake_holder { get; set; }
        public string short_name { get; set; }
        public string con_number1 { get; set; }
        public string con_number2 { get; set; }
        public string con_email1 { get; set; }
        public string con_email2 { get; set; }
        public string whatsapp_no1 { get; set; }
        public string whatsapp_no2 { get; set; }
        public string twitter_acc { get; set; }
        public string linked_in_acc { get; set; }
        public string website_personal { get; set; }
        public string Role { get; set; }
        public Boolean status { get; set; }

        public string isactive { get; set; }
        public Boolean is_active { get; set; }
    }
}
