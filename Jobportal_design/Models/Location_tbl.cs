﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jobportal_design.Models
{
    public class Location_tbl
    {
       
        public int id { get; set; }

        public string location_name { get; set; }


       
        public string code { get; set; }


      
        public int province_area_id { get; set; }

       

        public string identifier { get; set; }

        public Boolean is_active { get; set; }

        public District_tbl district { get; set; }

        public string isactive { get; set; }

        public string dist_name { get; set; }
        public string loc_path { get; set; }
        public string loc_pic { get; set; }

    }
}
