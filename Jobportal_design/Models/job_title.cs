﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jobportal_design.Models
{
    public class job_title
    {
        public string Text { get; set; }
        public string Value { get; set; }
        public int Count { get; set; }
        public int seek_id {get; set;}
    }
    public class job_type
    {

        public string Text { get; set; }
        public string Value { get; set; }
        public int Count { get; set; }

    }
}
