﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jobportal_design.Models
{
    public class adminrole_tbl
    {
        public int id { get; set; }
        public string roletype { get; set; }
        public string isactive { get; set; }
    }
}
