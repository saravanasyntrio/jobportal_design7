﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jobportal_design.Models
{
    public class Employer
    {

        public int id { get; set; }

        public int passedId { get; set; }

        public string email_id { get; set; }

        public string activeStat { get; set; }

        public string e_password { get; set; }
        public string retype_password { get; set; }
        public DateTime registration_date { get; set; }

        public string contact1 { get; set; }
        public string contact2 { get; set; }
        public DateTime start_date { get; set; }
        public DateTime end_date { get; set; }
        public int jobCount { get; set; }


        public Boolean status { get; set; }
        public string OrgAct { get; set; }
        public string OrgInAct { get; set; }

        public int org_id { get; set; }

        public string org_name { get; set; }


        public string e_name { get; set; }

        public string last_name { get; set; }
        public string isactive { get; set; }
        public Boolean is_active { get; set; }

        public string Name { get; set; }

        public string secondary_phone_type { get; set; }
        public string primary_phone_type { get; set; }
        public string primary_extension { get; set; }
        public string secondary_extension { get; set; }
        public string new_password { get; set; }

        public string old_password { get; set; }

        public int role_id { get; set; }

        public string org_address { get; set; }
        public string orgdescription { get; set; }

        public string org_county { get; set; }
        public string emp_image { get; set; }

        public string empimage_path { get; set; }
        public string user_name { get; set; }
    }
}
