﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jobportal_design.Models
{
    public class Org_cont
    {
        public int org_c_id { get; set; }
        public int org_id { get; set; }
        public string con_number1 { get; set; }
        public string con_number2 { get; set; }
        public string con_email1 { get; set; }
        public string con_email2 { get; set; }
        public string whatsapp_no1 { get; set; }

        public string whatsapp_no2 { get; set; }
        public string twitter_acc { get; set; }
        public string linked_in_acc { get; set; }
        public string website_address { get; set; }
        public int total_employee { get; set; }
        public int total_board_mem { get; set; }
        public Boolean org_closed { get; set; }
        public DateTime org_closed_date { get; set; }
        public string org_closed_reason { get; set; }
        public Boolean org_inactive { get; set; }
        public DateTime org_inactive_date { get; set; }
        public string org_inactive_reason { get; set; }
        public string Description { get; set; }

        public string isactive { get; set; }
        public string isactive1 { get; set; }
        public Boolean is_active { get; set; }

        public string org_name { get; set; }
    }
}
