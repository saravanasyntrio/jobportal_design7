﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jobportal_design.Models
{
    public class District_tbl
    {
      
        public int id { get; set; }

        public string dist_name { get; set; }


       
        public string code { get; set; }


       
        public int state_id { get; set; }


      
        public string identifier { get; set; }

      
        public Boolean is_active { get; set; }

        public State_tbl state { get; set; }

        public string isactive { get; set; }

        public string state_name { get; set; }
        public int Value { get; set; }

        public string Text { get; set; }
       
    }
}
