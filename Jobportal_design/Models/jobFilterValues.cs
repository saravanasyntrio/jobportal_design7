﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jobportal_design.Models
{
    public class jobFilterValues
    {
        
        public string Administrator { get; set; }

        public string AssistantTeacher { get; set; }
        public string Floater { get; set; }
        public string LeadTeacher { get; set; }

        public string Substitute { get; set; }
        public string DayCampAdministrator { get; set; }
        public string DayCampJuniorCounselor { get; set; }

        public string DayCampLeadCounselor { get; set; }
        public string DayCampOwner { get; set; }
        public string Administration { get; set; }
        public string Management { get; set; }
        public string EarlyEducation { get; set; }

        public string FullTime { get; set; }
        public string PartTime { get; set; }
        public string InOffice { get; set; }
        public string Hybrid { get; set; }
        public string Remote { get; set; }
        public string Others { get; set; }
        public string filterOn { get; set; }


    }
}
