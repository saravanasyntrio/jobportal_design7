﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jobportal_design.Models
{
    public class County_tbl
    {

        public int id { get; set; }

        public string county_name { get; set; }

        public string code { get; set; }

        public string cnty_path { get; set; }
        public string cnty_pic { get; set; }

        public Boolean is_active { get; set; }

        public string isactive { get; set; }
        public int company_count { get; set; }
    }
}
