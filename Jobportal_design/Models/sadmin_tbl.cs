﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jobportal_design.Models
{
    public class sadmin_tbl
    {
        public int id { get; set; }

       
        public string name { get; set; }

      
        public string email_id { get; set; }

      
        public string a_password { get; set; }

        public int passedId { get; set; }


        public string activeStat { get; set; }
        public string employee_type { get; set; }

        public string contact1 { get; set; }

      
        public string contact2 { get; set; }

    
        public DateTime start_date { get; set; }

      
        public DateTime end_date { get; set; }

       
        public Boolean status { get; set; }

      
        public string a_token { get; set; }


       
        public DateTime last_login { get; set; }
        public string isactive { get; set; }
        public int roleid { get; set; }

        public string roletype { get; set; }
        public string org_password { get; set; }
        public string new_password { get; set; }
        
        public string retype_password { get; set; }

        public string old_password { get; set; }
    }
}
