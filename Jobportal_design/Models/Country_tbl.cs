﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jobportal_design.Models
{
    public class Country_tbl
    {
      
        public int id { get; set; }
       
        public string country_name { get; set; }
      
        public string code { get; set; }
      
        public int currency_id { get; set; }

        public string currency_name { get; set; }
        public string nationality { get; set; }

        public string identifier { get; set; }

        public Boolean is_active { get; set; }

        public string isactive { get; set; }

        public Currency_tbl currency { get; set; }

        public List<Currency_tbl> currencies  {get; set; }

        public int currencies_selected { get; set; }
    }
}
