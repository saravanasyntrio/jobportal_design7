﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jobportal_design.Models
{
    public class user_tbl_ab
    {
       
        public int id { get; set; }

        public int user_id { get; set; }

        public string user_type { get; set; }

        
        public string user_name { get; set; }

        
        public string password { get; set; }

        public Boolean is_active { get; set; }

        public Boolean is_login { get; set; }

     
        public string token { get; set; }

        public DateTime last_login { get; set; }


       

        public string code { get; set; }
        public int role_id { get; set; }
    }
}
