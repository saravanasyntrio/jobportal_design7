﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jobportal_design.Models
{
    public class Org_cat
    {
        public int id { get; set; }
        public string category_name { get; set; }
        public Boolean is_active { get; set; }
        public string isactive { get; set; }
        public int job_count { get; set; }

    }

}
