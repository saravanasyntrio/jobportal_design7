﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jobportal_design.Models
{
    public class Job_register_ab
    {
        public int id { get; set; }

        public int job_reg_id { get; set; }
        public string job_title { get; set; }

        public int seekCount { get; set; }
        public string job_skills { get; set; }

        public string job_desc { get; set; }


        public DateTime posting_date { get; set; }

        public DateTime start_date { get; set; }


        public DateTime end_date { get; set; }


        public string job_type { get; set; }


        public string salary_package { get; set; }

        public string experience { get; set; }

        public int state_fk { get; set; }

        public string state_name { get; set; }

        public int district_id { get; set; }

        public string dist_name { get; set; }

        public int? employeer_id { get; set; }

        public string e_name { get; set; }

        public int job_cat_id { get; set; }
        public string job_category_name { get; set; }
        public int org_id { get; set; }

        public string org_name { get; set; }
        public string org_desc { get; set; }
        public string org_county_name { get; set; }
        public string org_address { get; set; }
        public string org_state { get; set; }
        public string org_mail { get; set; }
        public string org_phone { get; set; }
        public string Value { get; set; }
        public string cmp_image { get; set; }
        public int county_id { get; set; }
        public string county_name { get; set; }
        public string oth_county_name { get; set; }
        public string address { get; set; }
        public string benefits { get; set; }
        public string licence_certification { get; set; }
        public string responsibilities { get; set; }
        public string salary_pay { get; set; }
        public string shedule { get; set; }
        public string isactive { get; set; }

        public Boolean is_approved { get; set; }
        public string salary_to { get; set; }
    }
}
