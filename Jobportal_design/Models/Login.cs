﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jobportal_design.Models
{
    public class Login
    {
        public int id { get; set; }
        public string email_id { get; set; }

        public string e_password { get; set; }
        public string role { get; set; }

        public string e_name { get; set; }
        public int role_id { get; set; }

        public int org_id { get; set; }
        public string image_name { get; set; }

        public string image_path { get; set; }
        public string adminRole { get; set; }
    }
}
