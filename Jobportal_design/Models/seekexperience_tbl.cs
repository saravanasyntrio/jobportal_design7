﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Jobportal_design.Models
{
    public class seekexperience_tbl
    {
		public int id { get; set; }

		public string employername { get; set; }
		public string employmenttype { get; set; }
		public string designation { get; set; }
		public DateTime from_date { get; set; }


		
		public DateTime to_date { get; set; }
		public string to_date1 { get; set; }
		public string ctc { get; set; }
		public string skills { get; set; } 
		public int seek_id { get; set; }
		
		public string employername2 { get; set; }
		public string employmenttype2 { get; set; }
		public string designation2 { get; set; }
		public DateTime from_date2 { get; set; }
		public DateTime to_date2 { get; set; }
		public string skills2 { get; set; }
		
		public string employername3 { get; set; }
		public string employmenttype3 { get; set; }
		public string designation3 { get; set; }
		public DateTime from_date3 { get; set; }
		public DateTime to_date3 { get; set; }
		public string skills3 { get; set; }
		
		public string employername4 { get; set; }
		public string employmenttype4 { get; set; }
		public string designation4 { get; set; }
		public DateTime from_date4 { get; set; }
		public DateTime to_date4 { get; set; }
		public string skills4 { get; set; }

		public string employername5 { get; set; }
		public string employmenttype5 { get; set; }
		public string designation5 { get; set; }
		public DateTime from_date5 { get; set; }
		public DateTime to_date5 { get; set; }
		public string skills5 { get; set; }
	}
}
