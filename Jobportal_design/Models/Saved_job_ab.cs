﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jobportal_design.Models
{
    public class Saved_job_ab
    {
        public int id { get; set; }


        public int job_reg_id { get; set; }


        public int seek_id { get; set; }

        public bool is_saved { get; set; }
    }
}
