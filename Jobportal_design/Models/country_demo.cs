﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jobportal_design.Models
{
    public class country_demo
    {
        //public int Id { get; set; }
        //public string CountryName { get; set; }
        //public string Code { get; set; }
        //public string currency_name { get; set; }
        //public string Currency { get; set; }

        ////public int Currencyid { get; set; }
        //public string Nationality { get; set; }
        //public string Countrystatus { get; set; }

        public int Id { get; set; }
        public string CountryName { get; set; }
        public string Code { get; set; }
        public string Currency { get; set; }
        public string Nationality { get; set; }
        public string Countrystatus { get; set; }

        public List<Currency_tbl> currname { get; set; }
    }
}
