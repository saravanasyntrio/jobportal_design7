﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Jobportal_design.Models
{

    public class Currency_tbl
    {
        [Key]
        public int id { get; set; }
        [Column(TypeName = "varchar(50)")]
        [Required]
        public string currency_name { get; set; }
        [Column(TypeName = "varchar(20)")]
        [Required]
        public string code { get; set; }

        public bool symbol { get; set; }

        [Required]
        public Boolean is_default { get; set; }

        [Required]
        public Boolean is_active { get; set; }

        public string isactive { get; set; }

        //public int selected { get; set; }
        [NotMapped]
        [DisplayName("Upload Symbol")]
        public IFormFile ImageFile { get; set; }

        public string s_currency { get; set; }
        public string s_currency1 { get; set; }

        public string currency_path { get; set; }



    }
}
