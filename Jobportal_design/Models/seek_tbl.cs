﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jobportal_design.Models
{
	public class seek_tbl
	{

		public int id { get; set; }

		public int passedId { get; set; }


		public string activeStat { get; set; }
		public string fname { get; set; }


		public string lname { get; set; }


		public string email_id { get; set; }


		public string e_password { get; set; }
		public string e_passwordViewbag { get; set; }

		public string retype_password { get; set; }
		public string old_password { get; set; }
		public int seeker_type { get; set; }

		public DateTime registration_date { get; set; }

        public int appliedCount { get; set; }

        public string racial { get; set; }


		public string gender { get; set; }


		public string seekaddress { get; set; }


		public int? dist_id { get; set; }

		public string dist_name { get; set; }


		public int? state_id { get; set; }

		public string state_name { get; set; }


		public int? county_id { get; set; }

		public string county_name { get; set; }

		
		public string othCounty { get; set; }



		public string zipcode { get; set; }


		public string phone { get; set; }


		public string contact1 { get; set; }



		public string contact2 { get; set; }
		public DateTime start_date { get; set; }

		public DateTime end_date { get; set; }

		public string disability { get; set; }


		public string dis_reason { get; set; }
		public DateTime DOB { get; set; }

		public string status { get; set; }



		public string s_resume { get; set; }




		public string list_skill { get; set; }


		public string primary_lng { get; set; }


		public string secondary_lng { get; set; }


		public string about_me { get; set; }

		public string s_pic { get; set; }

		public string sresume_path { get; set; }

		public string spic_path { get; set; }
		public string secondary_phone_type { get; set; }
		public string primary_phone_type { get; set; }
		public string primary_extension { get; set; }
		public string secondary_extension { get; set; }
		public string year_of_birth { get; set; }
		public string highest_degree { get; set; }
		public string highest_workHistory { get; set; }
		public Boolean is_active { get; set; }

		
		
	}
}
