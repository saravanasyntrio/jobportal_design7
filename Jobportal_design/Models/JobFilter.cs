﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jobportal_design.Models
{
    public class JobFilter
    {
         
        public int seek_id { get; set; }
        public int org_id { get; set; }
        public int emp_id { get; set; }
        public List<String> jobTitleValues { get; set; }
        public List<String> jobTypeValues { get; set; }
        public List<String> jobCategoryValues { get; set; }
        public bool is_login { get; set; }
    }
}
