﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jobportal_design.Models
{
    public class app_job_tbl
    {
		public int id { get; set; }
		public int seek_id { get; set; }

		public int? employeer_id { get; set; }
		public int job_reg_id { get; set; }
		public string job_title { get; set; }


		public string job_skills { get; set; }
		public string job_desc { get; set; }
		public DateTime posting_date { get; set; }

		public int org_id { get; set; }
		public string org_name { get; set; }
		public string cmp_image { get; set; }
		public string job_type { get; set; }
		public string salary_pay { get; set; }
		public string salary_package { get; set; }
		public string salary_to { get; set; }
		public string experience { get; set; }
		public string state { get; set; }
		public string district { get; set; }
		public int org_cat_id { get; set; }
		public string category_name { get; set; }
		public DateTime date_applied { get; set; }
		public DateTime end_date { get; set; }

	}
}
