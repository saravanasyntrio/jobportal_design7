﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jobportal_design.Models
{
    public class Job_cat
    {
        public int id { get; set; }
        public string job_category_name { get; set; }
        public Boolean is_active { get; set; }
        public string isactive { get; set; }
        public int job_count { get; set; }

    }
    public class Job_cat_count
    {
        public int id { get; set; }
        public string job_category_name { get; set; }
        public int job_count { get; set; }
    }
}
