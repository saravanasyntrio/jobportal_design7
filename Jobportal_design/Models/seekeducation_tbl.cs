﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jobportal_design.Models
{
    public class seekeducation_tbl
    {
        public int id { get; set; }

		public string qualification { get; set; }
		public string school_college { get; set; }
		public string yearpassing { get; set; }

		public string percentage { get; set; }
		public string stream { get; set; }
		public string cgpa { get; set; }
		public string academic { get; set; }
		public int seek_id { get; set; }

		public string schoolattended { get; set; }
		public string lastgrade { get; set; }
		public string collegeattended { get; set; }
		public string degreereceived { get; set; }
		public string certificatereceived { get; set; }
		public string schoolattended2 { get; set; }
		public string lastgrade2 { get; set; }
		public string collegeattended2 { get; set; }
		public string degreereceived2 { get; set; }
		public string certificatereceived2 { get; set; }
		public string academic2 { get; set; }
		public string schoolattended3 { get; set; }
		public string lastgrade3 { get; set; }
		public string collegeattended3 { get; set; }
		public string degreereceived3 { get; set; }
		public string certificatereceived3 { get; set; }
		public string academic3 { get; set; }

		public string schoolattended4 { get; set; }
		public string lastgrade4 { get; set; }
		public string collegeattended4 { get; set; }
		public string degreereceived4 { get; set; }
		public string certificatereceived4 { get; set; }
		public string academic4 { get; set; }

		public string schoolattended5 { get; set; }
		public string lastgrade5 { get; set; }
		public string collegeattended5 { get; set; }
		public string degreereceived5 { get; set; }
		public string certificatereceived5 { get; set; }
		public string academic5 { get; set; }


	}
}
