﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jobportal_design.Models
{
    public class job_reg_tbl
    {
    
        public int id { get; set; }

        
        public string job_title { get; set; }

        public string search { get; set; }

        public string job_skills { get; set; }

      
        public string job_desc { get; set; }

        
        public DateTime posting_date { get; set; }

      
        public DateTime start_date { get; set; }

        
        public DateTime end_date { get; set; }

        public int state_fk { get; set; }

      
        public int desired_state_fk { get; set; }
    
        public int? employeer_id { get; set; }
        public string state_name { get; set; }
        public int district_id { get; set; }
        public string district_name { get; set; }
        public string dist_name { get; set; }
        
        public int county_id { get; set; }

        public string county_name { get; set; }

        public int org_id { get; set; }
        public string org_name { get; set; }
        public string org_desc { get; set; }
        public string org_county_name { get; set; }
        public string org_address { get; set; }
        public string org_state { get; set; }
        public string org_mail { get; set; }
        public string org_phone { get; set; }
        public string cmp_image { get; set; }
      
        public string cmpimage_path { get; set; }
        public string e_name { get; set; }

        public string job_type { get; set; }
        public string salary_package { get; set; }
        public string experience { get; set; }
        public int org_cat_id { get; set; }

        public string category_name { get; set; }
        public int job_cat_id { get; set; }
        public string job_category_name { get; set; }

        public List<SelectListItem> Select { set; get; }

        public List<SelectListItem> jobreg { get; set; }

        public string salary_to { get; set; }

        public string salary_pay { get; set; }
        public string shedule { get; set; }
        public string isactive { get; set; }

        public Boolean is_approved { get; set; }
        public bool is_saved { get; set; }
    }
}
